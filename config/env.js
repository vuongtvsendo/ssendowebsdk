const fs = require('fs');
const path = require('path');
const paths = require('./paths');

// Make sure that including paths.js after env.js will read .env variables.
delete require.cache[require.resolve('./paths')];

const NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
  throw new Error(
    'The NODE_ENV environment variable is required but was not specified.'
  );
}

// https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
var dotenvFiles = [
  `${paths.dotenv}.${NODE_ENV}.local`,
  `${paths.dotenv}.${NODE_ENV}`,
  // Don't include `.env.local` for `test` environment
  // since normally you expect tests to produce the same
  // results for everyone
  NODE_ENV !== 'test' && `${paths.dotenv}.local`,
  paths.dotenv,
].filter(Boolean);

// Load environment variables from .env* files. Suppress warnings using silent
// if this file is missing. dotenv will never modify any environment variables
// that have already been set.
// https://github.com/motdotla/dotenv
dotenvFiles.forEach(dotenvFile => {
  if (fs.existsSync(dotenvFile)) {
    require('dotenv').config({
      path: dotenvFile,
    });
  }
});

// We support resolving modules according to `NODE_PATH`.
// This lets you use absolute paths in imports inside large monorepos:
// https://github.com/facebookincubator/create-react-app/issues/253.
// It works similar to `NODE_PATH` in Node itself:
// https://nodejs.org/api/modules.html#modules_loading_from_the_global_folders
// Note that unlike in Node, only *relative* paths from `NODE_PATH` are honored.
// Otherwise, we risk importing Node.js core modules into an app instead of Webpack shims.
// https://github.com/facebookincubator/create-react-app/issues/1023#issuecomment-265344421
// We also resolve them to make sure all tools using them work consistently.
const appDirectory = fs.realpathSync(process.cwd());
process.env.NODE_PATH = (process.env.NODE_PATH || '')
  .split(path.delimiter)
  .filter(folder => folder && !path.isAbsolute(folder))
  .map(folder => path.resolve(appDirectory, folder))
  .join(path.delimiter);

// Grab NODE_ENV and APP_* or ENABLE_* environment variables and prepare them to be
// injected into the application via DefinePlugin in Webpack configuration.
const KEY = /^(APP_|ENABLE_)/;

function getClientEnvironment(publicUrl) {
  const NODE_ENV = process.env.NODE_ENV || 'development';
  const raw = Object.keys(process.env).filter(key => KEY.test(key)).reduce((
    env,
    key
  ) => {
    if (/^ENABLE_/.test(key)) {
      env[key] =
        NODE_ENV === 'development' ||
        process.env[key] === '1' ||
        process.env[key] === 'true';
    } else {
      env[key] = process.env[key];
    }
    return env;
  }, {
    NODE_ENV,
    PUBLIC_URL: publicUrl,
    SERVICE_WORKER: 'service-worker',
    APP_PUBLIC_KEY:
      process.env.APP_PUBLIC_KEY ||
        'BPBtkQ6Am4Np-rYeq1_VNnr3bJhk1w3-MyUUGiL_K2lQ3qlQueGQasaAxw9pAgseaJbYR89oT3yCRxoLBuVpnrc',
  });
  // Stringify all values so we can feed into Webpack DefinePlugin
  var stringified = {};
  for (var key in raw) {
    stringified['process.env.' + key] = JSON.stringify(raw[key]);
  }

  return { raw, stringified };
}

module.exports = getClientEnvironment;
