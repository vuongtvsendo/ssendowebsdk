const path = require('path');
const fs = require('fs');
const url = require('url');

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const resolveVar = relativePath =>
  path.resolve(appDirectory, 'var', relativePath);

function ensureSlash(path, needsSlash) {
  const hasSlash = path.endsWith('/');
  if (hasSlash && !needsSlash) {
    return path.substr(path, path.length - 1);
  } else if (!hasSlash && needsSlash) {
    return `${path}/`;
  } else {
    return path;
  }
}

const envPublicUrl = process.env.PUBLIC_URL || '';

const getPublicUrl = () => {
  return ensureSlash(envPublicUrl, false);
};

function getServedPath() {
  const publicUrl = getPublicUrl();
  const servedUrl = publicUrl ? url.parse(publicUrl).pathname : '/';
  return ensureSlash(servedUrl, true);
}

// config after eject: we're in ./config/
module.exports = {
  resolveVar,
  dotenv: resolveApp('.env'),
  appBuild: resolveApp('build/frontend'),
  appBackend: resolveApp('build/backend'),
  appBuildTmp: resolveApp('build/tmp'),
  appPublic: resolveApp('public'),
  appStyles: resolveApp('src/styles'),
  appHtml: resolveApp('public/index.html'),
  appHtmlDev: resolveApp('public/dev.html'),
  appManifestTemplate: resolveApp('public/manifest.tpl.json'),
  appIndexJs: resolveApp('src/index.js'),
  serverIndexJs: resolveApp('server/index.js'),
  appPackageJson: resolveApp('package.json'),
  appSrc: resolveApp('src'),
  appVar: resolveApp('var'),
  serverSrc: resolveApp('server'),
  yarnLockFile: resolveApp('yarn.lock'),
  testsSetup: resolveApp('src/setupTests.js'),
  appNodeModules: resolveApp('node_modules'),
  publicUrl: getPublicUrl(),
  servedPath: getServedPath(),
  proxyConfigPath: resolveApp('config/proxy.js'),
  sendoWebSdkPath: fs.realpathSync(resolveApp('node_modules/sendo-web-sdk')),
};
