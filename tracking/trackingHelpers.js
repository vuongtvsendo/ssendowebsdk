import _get from 'lodash/get';
import _filter from 'lodash/filter';
import qs from 'query-string';
import { PAGE_ID_SENDO, PAGE_ID_SHOP } from './const_tracking';
import {
  SENDO_SESSIONID,
  LOGIN_TYPE_COOKIE_NAME,
  SENDO_PLATFORM,
  LOGIN_ID_COOKIE_NAME,
} from 'sendo-web-sdk/helpers/const';
import { get as getCookie } from 'sendo-web-sdk/helpers/cookie';
import * as auth from 'sendo-web-sdk/helpers/auth';
import { encodeUrl } from 'sendo-web-sdk/helpers/url';

export const formartUserInfo = evt => {
  const identity = _get(evt, 'identity', {});
  return {
    email: _get(identity, 'email', null),
    login_type: getCookie(LOGIN_TYPE_COOKIE_NAME) || null,
    userid: auth.getFptId(identity) || getCookie(LOGIN_ID_COOKIE_NAME),
    user_id: _get(evt, 'cookies.tracking_id', null),
    login_id: auth.getFptId(identity) || getCookie(LOGIN_ID_COOKIE_NAME),
    login_email: _get(identity, 'email', null),
  };
};
export const trackingGeneral = evt => {
  return {
    sessionid: getCookie(SENDO_SESSIONID),
    sendo_platform: SENDO_PLATFORM,
    ip_client: '',
    session_key: _get(evt, 'session.session_key', null),
    href: encodeUrl(),
    referrer_internal: getIsReferrerInternalSendo(_get(evt, 'referrer')),
    client_time: _get(evt, 'client_time', null),
    domain: _get(window, 'location.host', null),
    u_agent: _get(window, 'navigator.userAgent', null),
  };
};

export const getListingAlgo = () => {
  return getCookie('listing_algo') || 'default';
};

export const gaTrackPageView = () => {
  if (window._gaq) {
    window._gaq.push(['_trackPageview']);
  }
  if (window.ga) {
    window.ga('send', 'pageview');
  }
};

export const pushDataEventTrackingGATagManager = (
  category,
  action,
  opt_label,
  opt_value,
  opt_noninteraction
) => {
  if (window._gaq) {
    window._gaq.push([
      '_trackEvent',
      category,
      action,
      opt_label,
      opt_value,
      opt_noninteraction,
    ]);
  }

  let { dataLayer } = window;

  if (dataLayer)
    dataLayer.push({
      eventCategory: category,
      eventAction: action,
      eventLabel: opt_label,
      eventValue: opt_value,
      eventNoninteraction: opt_noninteraction,
      event: 'gaEvent',
    });
};

/**
 * Hàm này chưa dùng, đề phòng trong tương lai cần bulid href theo các param ajax api
 * @param requestInfo
 * @returns {string}
 */

export const buildHref = (requestInfo = {}) => {
  const { origin, pathname } = window.location;
  const search = qs.parse(_get(window, 'location.search', '?').split('?')[1]);
  requestInfo = {
    ...requestInfo,
    ...search,
  };
  return `${origin}${pathname}?` + qs.stringify(requestInfo);
};

export const getIdProductIfProductIsAds = product_id => {
  if (product_id.toString().indexOf('keyword_ads') > -1) {
    const _id = product_id.split('keyword_ads_')[1];
    if (_id) return _id;
  }
  return product_id;
};

export const getShopId = data => {
  return _get(data, 'admin_id', null);
};
export const getCategoryInfoTracking = (category_info = [], type = null) => {
  let categoryParam = {
    cate1: '',
    cate2: '',
    cate3: '',
    cate1_id: '',
    cate2_id: '',
    cate3_id: '',
    cate1_name: '',
    cate2_name: '',
    cate3_name: '',
  };
  if (type === 'extra') {
    const extra = {
      belong_cate_lvl1_id: '',
      belong_cate_lvl2_id: '',
      belong_cate_lvl3_id: '',
      belong_cate_lvl1_name: '',
      belong_cate_lvl2_name: '',
      belong_cate_lvl3_name: '',
    };
    categoryParam = {
      ...categoryParam,
      ...extra,
    };
  }
  if (category_info !== null) {
    for (let i = 0; i < category_info.length; i++) {
      categoryParam['cate' + (i + 1)] = _get(category_info[i], 'url_key', null);
      categoryParam['cate' + (i + 1) + '_id'] = _get(
        category_info[i],
        'id',
        null
      );
      categoryParam['cate' + (i + 1) + '_name'] = _get(
        category_info[i],
        'title',
        _get(category_info[i], 'name', null)
      );
      if (type === 'extra') {
        categoryParam['belong_cate_lvl' + (i + 1) + '_id'] = _get(
          category_info[i],
          'id',
          null
        );
        categoryParam['belong_cate_lvl' + (i + 1) + '_name'] = _get(
          category_info[i],
          'title',
          _get(category_info[i], 'name', null)
        );
      }
    }
  }
  return categoryParam;
};

export const getStringCategory = category_info => {
  let stringResult = '';
  if (category_info !== null) {
    let cateArray = category_info.map(cate => {
      return cate.title || cate.name;
    });
    if (cateArray.length) stringResult = cateArray.join(' / ');
  }
  return stringResult;
};

export const getIsReferrerInternalSendo = (referrer = '') => {
  let isFromSendo = 0;
  // eslint-disable-next-line no-useless-escape
  if (referrer.match(/https:\/\/(www\.)?(m\.)?[^\/]*\.sendo\.vn/) !== null)
    isFromSendo = 1;
  return isFromSendo;
};

export const getPageId = (href = '') => {
  let pageId = PAGE_ID_SENDO;
  if (href.match(/sendo\.vn\/shop\/.+\/san-pham\//) !== null)
    pageId = PAGE_ID_SHOP;
  return pageId;
};

export const getBelongTab = (
  search = '',
  context = '',
  belong_tab_from_server = null
) => {
  let belong_tab = belong_tab_from_server;
  if (!belong_tab) {
    let searchObj = qs.parse(search);
    let sortTypeDefault = 'rank';
    if (context === 'listing_product') sortTypeDefault = 'vasup_desc';
    belong_tab = _get(searchObj, 'sortType', sortTypeDefault);
  }
  return belong_tab;
};

export const getListName = context => {
  let listName = null;
  const listPage = {
    listing_product: 'Category Results',
    search_products: 'Search Results',
  };
  if (listPage[context]) listName = listPage[context];
  return listName;
};

export const getEventName = context => {
  let eventName = null;
  const listEvent = {
    listing_product: 'trackProductClick',
    search_products: 'trackSearchProductClick',
  };
  if (listEvent[context]) eventName = listEvent[context];
  return eventName;
};

export const getEventImpressName = context => {
  let eventImpressName = null;
  const listEvent = {
    listing_product: 'trackProductImpression',
    search_products: 'trackSearchProductImpression',
  };
  if (listEvent[context]) eventImpressName = listEvent[context];
  return eventImpressName;
};

export const getLastCategory = category_info => {
  let category = { id: null, title: null };
  if (category_info !== null && category_info.length) {
    category['id'] = category_info[category_info.length - 1].id;
    category['title'] = category_info[category_info.length - 1].title;
  }
  return category;
};

export const getVariantInfoFromAttribute = (attribute, dataCheckOut) => {
  let data_push = {
    data_ecommerce: '',
    data_normal: {},
  };
  let data_ecommerce_arr = [];
  if (attribute.length) {
    for (let key in dataCheckOut) {
      if (dataCheckOut.hasOwnProperty(key)) {
        const attrLabel = _filter(attribute, a => {
          return a.product_option === key;
        })[0];
        if (attrLabel) {
          const attrValue = _filter(attrLabel.value, b => {
            return b.product_option_id === dataCheckOut[key][0];
          })[0];
          if (attrValue) {
            data_ecommerce_arr.push(
              attrLabel.search_key + ':' + (attrValue.name || attrValue.value)
            );
            data_push.data_normal[attrLabel.search_key] = {
              attribute_value: attrValue.name || attrValue.value,
              attribute_id: attrValue.option_id,
            };
          }
        }
      }
    }
    if (data_ecommerce_arr.length)
      data_push.data_ecommerce = data_ecommerce_arr.join('|');
  }
  return data_push;
};

export const getVariantInfoFromCartAttribute = dataCartAttribute => {
  let data_push = {
    data_ecommerce: '',
    data_normal: {},
  };
  let data_ecommerce_arr = [];
  dataCartAttribute.map(attr => {
    data_ecommerce_arr.push(attr.search_key + ':' + (attr.name || attr.value));
    data_push.data_normal[attr.search_key] = {
      attribute_value: attr.name || attr.value,
      attribute_id: attr.option_id,
    };
    return false;
  });
  if (data_ecommerce_arr.length)
    data_push.data_ecommerce = data_ecommerce_arr.join('|');
  return data_push;
};
