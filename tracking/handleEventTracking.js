import _get from 'lodash/get';

import { LISTING_PRODUCT_PER_PAGE } from 'sendo-web-sdk/helpers/const';
import * as trackingHelpers from './trackingHelpers';
import { encodeUrl } from 'sendo-web-sdk/helpers/url';

export const handleProductClick = (evt, data) => {
  let data_push = {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    event: trackingHelpers.getEventName(_get(evt, 'session.context', '')),
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(data, 'productMetadata.belong_tab', null)
    ),
    experiment_id: _get(data, 'productMetadata.experiment_id', 0),
    num_result_per_page: LISTING_PRODUCT_PER_PAGE,
    special_res: _get(data, 'productMetadata.special_res', 0),
    ecommerce: {
      click: {
        actionField: {
          action: 'click',
          list: trackingHelpers.getListName(_get(evt, 'session.context', '')),
        },
        products: [],
      },
    },
  };
  let product = {
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(data, 'productMetadata.belong_tab', null)
    ),
    brand: null,
    category: trackingHelpers.getLastCategory(_get(data, 'category_info', null))
      .id,
    id: trackingHelpers.getIdProductIfProductIsAds(_get(data, 'id', null)),
    name: _get(data, 'name', null),
    is_sponsored: _get(data, 'is_keyword_ads', null),
    list: trackingHelpers.getListName(_get(evt, 'session.context', '')),
    position: _get(data, 'index', null),
    price: _get(data, 'final_price', null),
    product_type: _get(data, 'product_type', 0),
    referrer_internal: trackingHelpers.getIsReferrerInternalSendo(
      _get(evt, 'referrer')
    ),
    shop_id: trackingHelpers.getShopId(data),
    shop_name: _get(data, 'shop_info.shop_name', null),
    variant: '', //variant
  };
  let category = trackingHelpers.getCategoryInfoTracking(
    _get(data, 'category_info', [])
  );
  product = {
    ...product,
    ...category,
  };
  data_push.ecommerce.click.products.push(product);
  return data_push;
};

export const handleProductAccess = (evt, data) => {
  let data_push = {
    ...trackingHelpers.formartUserInfo(evt),
    ...trackingHelpers.trackingGeneral(evt),
    event: 'PAGE_VIEW_DETAIL',
    ecommerce: {
      detail: {
        actionField: [{ action: 'detail' }],
        page_id: trackingHelpers.getPageId(window.location.href),
        products: [],
      },
    },
    pageurl: encodeUrl(),
    cate_dimension: _get(data, 'category_info[0].url_key', null),
    google_tag_params: {
      ecomm_prodid: _get(data, 'id', null),
      ecomm_pagetype: 'product',
      ecomm_totalvalue: _get(data, 'final_price', null),
      ecomm_category: trackingHelpers.getStringCategory(
        _get(data, 'category_info', [])
      ),
    },
  };
  const category = trackingHelpers.getCategoryInfoTracking(
    _get(data, 'category_info', [])
  );
  let product = {
    name: _get(data, 'name', null),
    id: _get(data, 'id', null),
    product_ext_id: _get(data, 'id', null),
    price: _get(data, 'final_price', null),
    old_price: _get(data, 'price', null),
    brand: '',
    category: trackingHelpers.getStringCategory(
      _get(data, 'category_info', [])
    ),
    shop_id: trackingHelpers.getShopId(data),
    shop_name: _get(data, 'shop_info.shop_name', null),
    shop_ext_id: trackingHelpers.getShopId(data),
    shop_num_lotuses: _get(data, 'shop_info.lotus', null),
    shop_lotus_type: _get(data, 'shop_info.lotus_class', null),
  };
  data_push.ecommerce.detail.products.push(product);
  data_push = {
    ...data_push,
    ...category,
  };
  return data_push;
};

export const handleCommonImpression = (evt, data) => {
  return {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    total_result: '',
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(data, 'productMetadata.belong_tab', null)
    ),
    special_res: _get(data, 'productMetadata.special_res', 0),
    num_result_per_page: LISTING_PRODUCT_PER_PAGE,
    event: trackingHelpers.getEventImpressName(
      _get(evt, 'session.context', '')
    ),
    number_of_items: '',
    experiment_id: _get(data, 'productMetadata.experiment_id', 0),
    listing_algo: _get(data, 'productMetadata.listing_algo', 'default'),
    ecommerce: {
      impressions: [],
    },
  };
};

export const handleProductImpression = (evt, data) => {
  let product = {
    position: _get(data, 'index', null),
    id: trackingHelpers.getIdProductIfProductIsAds(_get(data, 'id', null)),
    name: _get(data, 'name', null),
    shop_id: trackingHelpers.getShopId(data),
    shop_name: _get(data, 'shop_info.shop_name', null),
    is_sponsored: _get(data, 'is_keyword_ads', null),
    price: _get(data, 'final_price', null),
    variant: '', // variant
    category: null, // category j đó
    brand: '',
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(data, 'productMetadata.belong_tab', null)
    ),
    referrer_internal: '',
    product_type: _get(data, 'product_type', 0),
    list: trackingHelpers.getListName(_get(evt, 'session.context', '')),
  };
  const category = trackingHelpers.getCategoryInfoTracking(
    _get(data, 'category_info', [])
  );
  product = {
    ...product,
    ...category,
  };
  return product;
};

export const handleCategoryAccess = (evt, data) => {
  let data_push = {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    google_tag_params: {
      ecomm_pagetype: 'category',
      ecomm_prodid: trackingHelpers.getLastCategory(
        _get(data, 'category_info', null)
      ).id,
      ecomm_category: trackingHelpers.getStringCategory(
        _get(data, 'category_info', [])
      ),
    },
    pageurl: encodeUrl(),
    event: 'PAGE_VIEW_CATEGORY_LEVEL3',
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(evt, 'session.requestInfo.belong_tab', null)
    ),
    listing_algo: _get(evt, 'session.requestInfo.listing_algo', 'default'),
    listing_page: _get(evt, 'session.listing_page', null),
    experiment_id: _get(evt, 'session.requestInfo.experiment_id', 0),
  };
  const category = trackingHelpers.getCategoryInfoTracking(
    _get(data, 'category_info', [])
  );
  data_push = {
    ...data_push,
    ...category,
  };
  return data_push;
};

export const handleSearch = (evt, data) => {
  return {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    total_result: null,
    num_sponsored_results: _get(evt, 'session.num_sponsored_result', 4),
    belong_tab: trackingHelpers.getBelongTab(
      _get(window, 'location.search', ''),
      _get(evt, 'session.context', ''),
      _get(evt, 'session.requestInfo.belong_tab', null)
    ),
    num_result_per_page: LISTING_PRODUCT_PER_PAGE,
    keyword: _get(data, 'keyword_tokenize', null),
    event: 'PAGE_VIEW_SEARCH',
    search_algo: _get(evt, 'session.requestInfo.search_algo', null),
  };
};

export const handleAddToCart = (evt, data) => {
  let product = {
    id: _get(data, 'id', null),
    name: _get(data, 'name', null),
    price: _get(data, 'price', null),
    final_price: _get(data, 'final_price', null),
    quantity: '1',
    belong_shop_id: trackingHelpers.getShopId(data),
    belong_shop_name: _get(data, 'shop_info.shop_name', null),
    brand: '',
    belong_shop_reputation: {
      num_lotuses: _get(data, 'shop_info.lotus', null),
      lotus_type: _get(data, 'shop_info.lotus_class', null),
    },
    variant: trackingHelpers.getVariantInfoFromAttribute(
      _get(data, 'attribute', []),
      _get(data, 'data_checkout.options', {})
    ).data_normal,
    variant_ecommerce: trackingHelpers.getVariantInfoFromAttribute(
      _get(data, 'attribute', []),
      _get(data, 'data_checkout.options', {})
    ).data_ecommerce,
    shipping_fee: 0,
  };
  const category = trackingHelpers.getCategoryInfoTracking(
    _get(data, 'cateMetadata.category_info', []),
    'extra'
  );
  product = {
    ...product,
    ...category,
  };
  return {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    products: [product],
    thumbnail: '',
    total_item: 0,
    shop_id: trackingHelpers.getShopId(data),
    shop_url: _get(data, 'shop_info.shop_url', null),
    name: _get(data, 'shop_info.shop_name', null),
    ecommerce: {
      currencyCode: 'VND',
      add: {
        products: [
          {
            name: _get(data, 'name', null),
            id: _get(data, 'id', null),
            price: _get(data, 'price', null),
            brand: '',
            category: trackingHelpers.getLastCategory(
              _get(data, 'cateMetadata.category_info', null)
            ).title,
            variant: trackingHelpers.getVariantInfoFromAttribute(
              _get(data, 'attribute', []),
              _get(data, 'data_checkout.options', {})
            ).data_ecommerce,
            quantity: '1',
          },
        ],
      },
    },
    event: 'addToCart',
  };
};

export const handleRemoveFromCart = (evt, data) => {
  const product_list = data.map(dt => {
    return {
      id: _get(dt, 'id', null),
      name: _get(dt, 'name', null),
      price: null,
      final_price: _get(dt, 'final_price', null),
      variant: trackingHelpers.getVariantInfoFromCartAttribute(
        _get(dt, 'cart_attribute', {})
      ).data_normal,
      variant_ecommerce: trackingHelpers.getVariantInfoFromCartAttribute(
        _get(dt, 'cart_attribute', {})
      ).data_ecommerce,
      quantity: _get(dt, 'qty'),
      brand: '',
      category: '',
    };
  });
  return {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    products: product_list,
    ecommerce: {
      currencyCode: 'VND',
      remove: {
        products: product_list,
      },
    },
    event: 'removeFromCart',
  };
};

export const handleNotFoundPageAccess = (evt, data) => {
  return {
    page: {
      type: 404,
      referer: evt.referrer,
    },
    event: 'ERROR_PAGE',
  };
};

export const handlePageViewGeneral = (evt, data) => {
  return {
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    page: {
      type: _get(evt, 'session.context'),
      referer: evt.referrer,
    },
    event: 'PAGE_VIEW',
  };
};

export const handleHomePageTracking = (evt, data) => {
  return {
    google_tag_params: {
      ecomm_pagetype: 'home',
      ecomm_prodid: 1,
    },
    ...trackingHelpers.trackingGeneral(evt),
    ...trackingHelpers.formartUserInfo(evt),
    pageurl: window.location.href,
    event: 'PAGE_VIEW_HOME',
  };
};
