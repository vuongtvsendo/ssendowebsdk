import _get from 'lodash/get';
import _debounce from 'lodash/debounce';
import store from 'store';
import _isEmpty from 'lodash/isEmpty';
import * as auth from 'sendo-web-sdk/helpers/auth';
import emitter, * as eventType from 'sendo-web-sdk/helpers/emitter';
import * as handleEventTracking from './handleEventTracking';
import * as trackingHelpers from './trackingHelpers';
import { SENDO_PLATFORM } from 'sendo-web-sdk/helpers/const';

export const homepageTracking = () => {
  let google_tag_params = {};
  google_tag_params['ecomm_pagetype'] = 'home';
  google_tag_params['ecomm_prodid'] = 1;

  let data_push = {};
  data_push['pageurl'] = window.location.href;
  data_push['sendo_platform'] = SENDO_PLATFORM;
  data_push['google_tag_params'] = google_tag_params;
  data_push['event'] = 'PAGE_VIEW_HOME';
  const state = store.getState();
  const identity = _get(state, 'user.identity', {});
  if (identity && identity.id) {
    data_push['email'] = identity.email;
    data_push['userid'] = auth.getTrackingUserId(identity);
    data_push['login_type'] = identity.login_type;
  }
  let { dataLayer } = window;
  dataLayer && dataLayer.push(data_push);
  if (window.ga) {
    window.ga('send', 'pageview');
  }
  let _gaq = window._gaq || [];
  _gaq.push(['_trackPageview']);
};

export const categoryTracking = metaData => {
  if (_isEmpty(metaData.category_id)) {
    return;
  }
  let google_tag_params = {};
  let ecomm_category = '';
  let data_push = {};
  let len_category = _get(
    metaData,
    'category_info.list_category_name.length',
    0
  );
  ecomm_category = metaData.category_info.list_category_name.join('/');
  for (let i = 0; i < len_category; i++) {
    data_push['cate' + (i + 1)] = metaData.category_info.list_category_path[i];
    data_push['cate' + (i + 1) + '_id'] =
      metaData.category_info.list_category_id[i];
    data_push['cate' + (i + 1) + '_name'] =
      metaData.category_info.list_category_name[i];
  }
  google_tag_params['ecomm_pagetype'] = 'category';
  google_tag_params['ecomm_prodid'] = metaData.category_id;
  google_tag_params['ecomm_category'] = ecomm_category;

  const state = store.getState();
  const identity = _get(state, 'user.identity', {});
  if (identity && identity.id) {
    data_push['email'] = identity.email;
    data_push['userid'] = auth.getTrackingUserId(identity);
    data_push['login_type'] = identity.login_type;
  }

  data_push['google_tag_params'] = google_tag_params;
  data_push['event'] = 'PAGE_VIEW_CATEGORY_LEVEL3';
  data_push['href'] = window.location.href;
  data_push['pageurl'] = window.location.href;
  data_push['cate_dimension'] = metaData.category_info.list_category_path[0];
  let { dataLayer } = window;
  dataLayer.push(data_push);
  if (window.ga) {
    window.ga('send', 'pageview');
  }
  let _gaq = window._gaq || [];
  _gaq.push(['_trackPageview']);
};

export const productTracking = productData => {
  let google_tag_params = {};
  let ecomm_category = [];
  let data_push = {};
  let len_category = _get(productData, 'category_info.length', 0);
  for (let i = 0; i < len_category; i++) {
    data_push['cate' + (i + 1)] = productData.category_info[i].path;
    data_push['cate' + (i + 1) + '_id'] = productData.category_info[i].id;
    data_push['cate' + (i + 1) + '_name'] = productData.category_info[i].name;
    ecomm_category.push(productData.category_info[i].name);
  }
  google_tag_params['ecomm_pagetype'] = 'product';
  google_tag_params['ecomm_prodid'] = productData.id;
  google_tag_params['ecomm_category'] = ecomm_category.join('/');
  google_tag_params['ecomm_totalvalue'] = productData.final_price;

  const state = store.getState();
  const identity = _get(state, 'user.identity', {});
  if (identity && identity.id) {
    data_push['email'] = identity.email;
    data_push['userid'] = auth.getTrackingUserId(identity);
    data_push['login_type'] = identity.login_type;
  }

  data_push['google_tag_params'] = google_tag_params;
  data_push['event'] = 'PAGE_VIEW_DETAIL';
  data_push['href'] = window.location.href;
  data_push['pageurl'] = window.location.href;
  data_push['cate_dimension'] = _get(productData, 'category_info[0].path', '');
  let { dataLayer } = window;
  dataLayer.push(data_push);
  if (window.ga) {
    window.ga('send', 'pageview');
  }
  let _gaq = window._gaq || [];
  _gaq.push(['_trackPageview']);
};

export const productClick = (product, cateMetaData) => {
  /* debugger;
  if (_isEmpty(cateMetaData)) {
    return;
  } */
  let data_push = {};
  data_push['event'] = 'PRODUCT_CLICK';
  data_push['sendo_platform'] = SENDO_PLATFORM;
  data_push['id'] = product.id;
  data_push['referrer_internal'] = 1;
  let track_product = {};
  track_product['referrer_internal'] = 1;
  track_product['list'] = 'Category Results';
  track_product['name'] = product.name;
  track_product['price'] = product.final_price;
  track_product['product_type'] = 1;
  track_product['shop_id'] = product.admin_id;
  track_product['shop_name'] = _get(product, 'shop_info.shop_name', '');
  track_product['variant'] = '';

  data_push['product'] = track_product;

  let { dataLayer } = window;
  return dataLayer.push(data_push);
};

emitter.addListener(eventType.PRODUCT_CLICK, function(evt, data) {
  if (
    _get(evt, 'session.context') === 'listing_product' ||
    _get(evt, 'session.context') === 'search_products'
  ) {
    // trackingHelpers.pushDataEventTrackingGATagManager(
    //   'Ecommerce',
    //   'Product Click'
    // );
    const data_push = handleEventTracking.handleProductClick(evt, data);
    let { dataLayer } = window;
    return dataLayer.push(data_push);
  }
});

emitter.addListener(eventType.PRODUCT_ACCESS, function(evt, data) {
  const data_push = handleEventTracking.handleProductAccess(evt, data);
  let { dataLayer } = window;
  return dataLayer.push(data_push);
});

(function() {
  var allDataListingPush = null;
  var allDataSearchPush = null;
  // const gaScroll = () => {
  //   trackingHelpers.pushDataEventTrackingGATagManager(
  //     'Ecommerce',
  //     'User Scroll'
  //   );
  // };
  var pushListingToTracking = _debounce(() => {
    let { dataLayer } = window;
    dataLayer.push(allDataListingPush);
    allDataListingPush = null;
    //gaScroll();
  }, 500);
  var pushSearchToTracking = _debounce(() => {
    let { dataLayer } = window;
    dataLayer.push(allDataSearchPush);
    allDataSearchPush = null;
    //gaScroll();
  }, 500);

  emitter.addListener(eventType.PRODUCT_IN_VIEWPORT, function(evt, data) {
    if (_get(evt, 'session.context') === 'listing_product') {
      if (allDataListingPush === null)
        allDataListingPush = handleEventTracking.handleCommonImpression(
          evt,
          data
        );
      const product = handleEventTracking.handleProductImpression(evt, data);
      let impressionsArr = allDataListingPush.ecommerce.impressions;
      impressionsArr.push(product);
      if (impressionsArr.length > 0) {
        pushListingToTracking(allDataListingPush);
      }
    }
    if (_get(evt, 'session.context') === 'search_products') {
      if (allDataSearchPush === null)
        allDataSearchPush = handleEventTracking.handleCommonImpression(
          evt,
          data
        );
      const product = handleEventTracking.handleProductImpression(evt, data);
      let impressionsArr = allDataSearchPush.ecommerce.impressions;
      impressionsArr.push(product);
      if (impressionsArr.length > 0) {
        pushSearchToTracking(allDataSearchPush);
      }
    }
  });
})();

emitter.addListener(eventType.CATEGORY_ACCESS, function(evt, data) {
  const data_push = handleEventTracking.handleCategoryAccess(evt, data);
  let { dataLayer } = window;
  if (dataLayer) {
    dataLayer.push(data_push);
    if (_get(evt, 'session.requestInfo.isOutOfItem', false)) {
      const param = [
        'category',
        'listing_algo',
        _get(evt, 'session.requestInfo.page'),
      ];
      trackingHelpers.pushDataEventTrackingGATagManager(...param);
    }
  }
  return false;
});

emitter.addListener(eventType.SEARCH_ACCESS, function(evt, data) {
  const data_push = handleEventTracking.handleSearch(evt, data);
  let { dataLayer } = window;
  if (dataLayer) {
    dataLayer.push(data_push);
    if (_get(evt, 'session.requestInfo.isOutOfItem', false)) {
      const param = [
        'search',
        'search_algo',
        _get(evt, 'session.requestInfo.page'),
      ];
      trackingHelpers.pushDataEventTrackingGATagManager(...param);
    }
  }
  return false;
});
emitter.addListener(eventType.CLICK_ADDTOCART, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager('Ecommerce', 'Add To Cart');
  const data_push = handleEventTracking.handleAddToCart(evt, data);
  let { dataLayer } = window;
  if (dataLayer) return dataLayer.push(data_push);
});
emitter.addListener(eventType.CLICK_ADDTOCART_SUCCESS, function(evt, data) {
  const state = store.getState();
  const total_cart_count = parseInt(
    _get(state, 'cart.metadata.total_count', 0),
    10
  );
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Add to cart',
    'Product detail page',
    total_cart_count
  );
});
emitter.addListener(eventType.CLICK_BUYNOW_SUCCESS, function(evt, data) {
  const state = store.getState();
  const total_cart_count = parseInt(
    _get(state, 'cart.metadata.total_count', 0),
    10
  );
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Buy now',
    'Product detail page',
    total_cart_count
  );
});
emitter.addListener(eventType.CLICK_ADDTOCART_FAILED, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Errors - Product detail page',
    'Error - Add to cart',
    data.error,
    -1
  );
});
emitter.addListener(eventType.CLICK_BUYNOW_FAILED, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Errors - Product detail page',
    'Error - Buy now',
    data.error,
    -1
  );
});

emitter.addListener(eventType.CART_DELETE, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Ecommerce',
    'Remove From Cart'
  );
  const data_push = handleEventTracking.handleRemoveFromCart(evt, data);
  let { dataLayer } = window;
  if (dataLayer) return dataLayer.push(data_push);
});
emitter.addListener(eventType.PAGE_ACCESS, function(evt, data) {
  trackingHelpers.gaTrackPageView();
});
emitter.addListener(eventType.HISTORY_REPLACE, function(evt, data) {
  trackingHelpers.gaTrackPageView();
});
emitter.addListener(eventType.CLICK_BUYNOW, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager('Ecommerce', 'Checkout');
  const data_push = handleEventTracking.handleAddToCart(evt, data);
  let { dataLayer } = window;
  if (dataLayer) return dataLayer.push(data_push);
});
emitter.addListener(eventType.NOT_FOUND_PAGE_ACCESS, function(evt, data) {
  const data_push = handleEventTracking.handleNotFoundPageAccess(evt, data);
  let { dataLayer } = window;
  if (dataLayer) return dataLayer.push(data_push);
});
emitter.addListener(eventType.CLICK_WISHLIST, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Favorite'
  );
});
emitter.addListener(eventType.CLICK_REMOVE_WISHLIST, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Unfavorite'
  );
});
emitter.addListener(eventType.CLICK_LOGIN_GOOGLE, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Social Login'
  );
});
emitter.addListener(eventType.CLICK_LOGIN_FACEBOOK, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Buttons',
    'Click - Social Login'
  );
});

emitter.addListener(eventType.CLICK_CARRIER, function(evt, data) {
  trackingHelpers.pushDataEventTrackingGATagManager(
    'Radio buttons',
    'Click - Carrier option',
    'Carrier popup'
  );
});

emitter.addListener(eventType.PAGE_VIEW, function(evt, data) {
  const data_push = handleEventTracking.handlePageViewGeneral(evt, data);
  let { dataLayer } = window;
  return dataLayer.push(data_push);
});

emitter.addListener(eventType.HOME_PAGE_ACCESS, function(evt, data) {
  const data_push = handleEventTracking.handleHomePageTracking(evt, data);
  let { dataLayer } = window;
  return dataLayer.push(data_push);
});
