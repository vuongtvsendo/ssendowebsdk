import sinon from 'sinon';
import {
  handleProductClick,
  handleProductAccess,
  handleProductImpression,
  handleCategoryAccess,
  handleSearch,
  handleCommonImpression,
  handleAddToCart,
  handleRemoveFromCart,
} from '../handleEventTracking';
import * as trackingHelpers from '../trackingHelpers';
import _get from 'lodash/get';
import {
  LISTING_PRODUCT_PER_PAGE,
  SENDO_PLATFORM,
} from 'sendo-web-sdk/helpers/const';
import * as cookie from 'sendo-web-sdk/helpers/cookie';
import * as urlHelpers from 'sendo-web-sdk/helpers/url';
const data_received = {
  id: 7845259,
  name: 'sản phẩm để test',
  admin_id: 234252,
  cat_path: 'san-pham/san-pham-de-test-7845259',
  price: 100000,
  image:
    'https://media3.scdn.vn/img2/2017/11_22/rcFbXF_simg_b5529c_250x250_maxb.png',
  special_price: 0,
  final_price: 100000,
  promotion_percent: 0,
  order_count: 0,
  brand_id: 0,
  is_keyword_ads: 0,
  sku_user: 'sản phẩm để test',
  quantity: 9986,
  index: 1,
  rating_info: {
    total_rated: 0,
  },
  category_info: [
    {
      id: 94,
      title: 'Thời trang nam',
      path: 'thoi-trang-nam',
      url_key: 'thoi-trang-nam',
    },
    {
      id: 673,
      title: 'Áo khoác nam',
      path: 'thoi-trang-nam/ao-khoac-nam',
      url_key: 'ao-khoac-nam',
    },
    {
      id: 681,
      title: 'Áo khoác chống nắng cho nam',
      path: 'thoi-trang-nam/ao-khoac-nam/ao-khoac-chong-nang-cho-nam',
      url_key: 'ao-khoac-chong-nang-cho-nam',
    },
  ],
  uid: 'get_basic_product_7845259',
  short_description: '',
  metadata: {
    page_title: 'sản phẩm để test - sản phẩm để test',
    heading_search: 'Mã: sản phẩm để test',
    og_title: 'sản phẩm để test 7845259',
    og_description: 'Giá : 100,000VNĐ - Mua tại shop Siêu Cẩu trên Sendo.vn',
    og_image: 'https://media3.scdn.vn/img2/2017/11_22/rcFbXF.png',
    index: true,
    follow: true,
    keywords: 'test sản phẩm  hàng không bán',
  },
  weight: 100,
  category_id: '1/2/94/673/681',
  total_comment: 0,
  url_key: 'san-pham-de-test-7845259',
  sku: '680_7845259',
  has_options: true,
  product_relateds: '',
  shop_free_shipping: 0,
  status: 1,
  status_new: 2,
  stock_status: 1,
  media: [
    {
      image: 'https://media3.scdn.vn/img2/2017/11_22/rcFbXF.png',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/11_22/rcFbXF_simg_de2fe0_500x500_maxb.png',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/11_22/rcFbXF_simg_02d57e_50x50_maxb.png',
      type: 'image',
    },
  ],
  brand_info: [],
  counter_view: 0,
  required_options: 'mau_sac,kich_thuoc_1',
  attribute: [
    {
      attribute_id: 284,
      name: 'Màu sắc',
      product_option: '7845259_284',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 628,
          color_id: 16,
          product_option_id: '7845259_628',
          name: 'Đỏ',
          color_hex_rgb: 'ff0000',
          image: '',
          background: '#ff0000',
        },
      ],
      search_key: 'mau_sac',
    },
    {
      attribute_id: 298,
      name: 'Kích thước',
      product_option: '7845259_298',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 818,
          value: 'XL',
          product_option_id: '7845259_818',
        },
      ],
      search_key: 'kich_thuoc_1',
    },
  ],
  description:
    '<div style="font-family: SFUIText, arial;"><div class="attrs-block">\n                                     <h4 style="font-size: 13px; line-height: 40px; font-weight: 400; margin: 0; border-bottom: 1px solid #a2a2a2; margin-bottom: 10px;">THUỘC TÍNH SẢN PHẨM</h4>\n                                     <ul style="font-size: 13px; list-style: none; padding: 0; margin: 0 0 10px; line-height: 1.4;"><li style="padding: 5px 12px;background-color: #ebebeb";> <strong>Chất liệu:</strong> <span>Chất liệu khác,</span> </li><li style="padding: 5px 12px;";> <strong>Xuất xứ:</strong> <span>Madagascar,</span> </li></ul>\n                                    </div><div class="details-block">\n                              <h4 style="font-size: 13px; line-height: 40px; font-weight: 400; margin: 0; border-bottom: 1px solid #a2a2a2; margin-bottom: 10px;">CHI TIẾT SẢN PHẨM</h4>\n                              <div id="editor-content"><img src="http://media3.scdn.vn/img2/2017/8_4/LhOcbf_simg_d0daf0_800x1200_max.jpg" style="margin: 0 auto;display:block;"><br><br><p><br></p></div>\n                            </div>\n                        </div>',
  shop_info: {
    shop_id: 234252,
    shop_name: 'Siêu Cẩu',
    shop_logo: 'https://mpilot.sendo.vn/images/ecom/shop_blank-logo-2.jpg',
    good_review_percent: 0,
    score: 0,
    is_certified: 0,
    lotus: 0,
    lotus_class: 'red',
    warehourse_region_id: 1,
    warehourse_region_name: 'Hồ Chí Minh',
    shop_external_id: 234252,
    phone_number: '0914719728',
    delivery_info: {
      free_delivery: [],
      shop_discount_delivery: [],
    },
    self_transport: [],
    shop_url: 'shop/sieu-cau',
  },
  return_policy: [
    {
      title: '48 giờ đổi trả',
      color: '#e82f3a',
      icon: 'FortyEight',
      tooltip_title: 'Chính sách đảm bảo sự hài lòng',
      tooltip_content:
        '<div class="policy-container" style="line-height: 1.4;"> <ul style="list-style: disc inside none; padding-left: 0; margin: 0;"> <li style="margin-bottom: 10px;">Đổi trả trong vòng 48h</li> <li style="margin-bottom: 10px;">Áp dụng hàng bị lỗi kỹ thuật, bể vỡ, không đúng như mô tả sản phẩm trên website, hoặc không đúng như đơn đặt hàng</li> <li style="margin-bottom: 10px;">Đổi trả khi không ưng ý kiểu dáng, màu sắc...</li> <li style="margin-bottom: 10px;">Đảm bảo hoàn tiền <a style="text-decoration: underline; color: black;" href="https://www.sendo.vn/bao-ve-nguoi-mua#bao-ve-quyen-loi-cua-ban"><strong>Xem thêm chi tiết tại đây.</strong></a></li> </ul> </div>',
    },
  ],
  price_discount: {
    total_min_price: '100,000',
    list_discount: [],
  },
  size_guide: '',
  belong_tab: 'vasup_desc',
  productMetadata: {
    listing_algo: 123,
    belong_tab: 'vasup_desc',
    experiment_id: 123,
    special_res: 123,
  },
};

const data_received_category = {
  page_title:
    'Điện thoại di động 2017: giá rẻ, chính hãng, trả góp 0% | Sendo.vn',
  description:
    'Mua điện thoại di động 2017 giá rẻ tại Sendo.vn✅ Điện thoại chính hãng✅ Trả góp 0% ✅Thanh toán khi nhận hàng✅ Đặt mua ngay✅',
  heading_search: 'Điện thoại mới',
  redirect_link: '',
  category_id: 1664,
  category_name: 'Điện thoại mới',
  category_level: 2,
  category_info: [
    {
      id: 94,
      path: 'thoi-trang-nam',
      title: 'Thời trang nam',
      url_key: 'thoi-trang-nam',
    },
    {
      id: 673,
      path: 'thoi-trang-nam/ao-khoac-nam',
      title: 'Áo khoác nam',
      url_key: 'ao-khoac-nam',
    },
  ],
  keywords:
    'điện thoại di động, dien thoai di dong, thiết bị di động, thiet bi di dong',
  index: false,
  follow: true,
  belong_tab: 'vasup_desc',
};

const data_search = {
  keyword_recommend: 'váy',
  keyword_tokenize: 'váy||',
  time_run: {
    service_listing: {
      get: -1511688794691,
      format: 1511688794691,
    },
    es: {
      get: 45,
      format: 106,
    },
  },
  type_view: 'grid_view',
  category_info: {},
  redirect_link: '',
  title: 'váy đẹp, chính hãng chất lượng, giá rẻ hấp dẫn ',
  description:
    'váy chính hãng, chất lượng cao, giá rẻ hơn tại Sendo.vn ✅ Giảm giá lên đến 70% ✅ Hàng ngàn mẫu mới ✅ Mua bán đảm bảo.',
  index: true,
  follow: true,
  keywords: 'váy,váy quần váy,váy xoè ,váy ôm',
  image:
    'https://media3.scdn.vn/img2/2017/8_22/SXEpaE_simg_b5529c_250x250_maxb.png',
  page: 2,
  belong_tab: 'vasup_desc',
};

const event_received = {
  reactEvent: {},
  identity: {
    access_token:
      't9HbI1bwSeCVu52DMLfuCixw2qDnQDhLYem0LacRc/lJgT7xGhici5qetfkDCzDXo19Oxd4j9Bpzj7S2IinAmlb074mgXht/1KMX4sQ2HVooMAS2l+q1eKQF3JdOlGTIr2ndESbGHb5Vf6mDBROXqpGv2R1xc3Qx9RCWP++Mlo4=',
    user_id: '2017367597',
    birthday: '1970-01-01 00:00:00',
    status: 0,
    email: 'thuatlt@sendo.vn',
    created_time: 1509418103,
    gender: 1,
    last_name: 'Thuat',
    first_name: 'Le',
    fpt_id: '2017367597',
    avatar_url: 'https://media3.scdn.vn/images/apps/icon_user_default.png',
    avatar: '',
    full_name: 'Le Thuat',
    phone: '01228918306',
    id: '59f7e477793b4079d31d541e',
    user_type: 0,
    verify_my_phone: 0,
    chat_token: '23a4d79243e04cc68838e9a46c6f18142c289eb9',
    loyalty_point_total: 0,
    order_total: 0,
    favorite_product_total: 2,
    favorite_shop_total: 0,
    comment_total: 0,
    city_name: 'Hồ Chí Minh',
    city_id: 1,
    customer_id: 2017367597,
    is_active: 1,
    password_hash: '',
    created_at: 1509418103,
    updated_at: 1509418103,
    dob: -28800,
    merge_openid: 0,
    telephone: '01228918306',
    default_shipping: 7033128,
    confirm_cod: 0,
    remember_token: '',
    series_id_entifier: '',
    login_type: 'fosp',
  },
  cookies: {
    tracking_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
  },
  client_time: 123456,
  referrer: 'https://m.sendo.vn/san-pham/dam-cong-so-tay-lo-hera-7838242',
  session: {
    session_key: 123,
    context: 'listing_product',
    listing_page: 1,
    requestInfo: {
      belong_tab: 'vasup_desc',
      search_algo: 123,
      listing_algo: 123,
      experiment_id: 123,
    },
  },
};

const data_cart = {
  id: 7367520,
  uid: 'listing_1_7367520',
  name: 'Đầm suông thời trang nữ , kiểu dáng độc đáo cá tính 114',
  admin_id: 30854,
  price: 207142,
  cat_path:
    'san-pham/da-m-suong-tho-i-trang-nu-kie-u-da-ng-do-c-da-o-ca-ti-nh-114-7367520',
  total_comment: 0,
  order_count: 5,
  image:
    'https://media3.scdn.vn/img2/2017/10_16/LPamUt_simg_b5529c_250x250_maxb.jpg',
  brand_id: 0,
  counter_view: 25,
  special_price: 114000,
  final_price: 114000,
  is_promotion: 1,
  promotion_percent: 45,
  shop_info: {
    shop_id: 30854,
    shop_name: 'hotly',
    shop_logo:
      'https://media3.scdn.vn/img1/2015/8_11/c8b0f5_simg_b11550_222x60_max_simg_34545b_120x60_maxb.jpg',
    good_review_percent: 85.44424,
    score: 0,
    is_certified: 0,
    lotus: 0,
    lotus_class: 'red',
    warehourse_region_id: 1,
    warehourse_region_name: 'Hồ Chí Minh',
    shop_external_id: 30854,
    phone_number: '0901340786',
    delivery_info: {
      free_delivery: [],
      shop_discount_delivery: [],
    },
    self_transport: [],
    shop_url: 'shop/hotly_vn',
  },
  options: {
    is_shipping_fee_support: 0,
    is_installment: 0,
    is_loyalty: 1,
  },
  category_info: [],
  rating_info: {
    total_rated: 0,
  },
  is_keyword_ads: 0,
  sku_user: '92114',
  sku: '2457_7367520',
  cateMetadata: {
    page_title: 'Đầm, váy chất lượng, đa dạng, giá tốt nhất! | Sendo.vn',
    description:
      'Đầm, váy chất lượng, đa dạng, giá cực tốt, siêu khuyến mãi, vận chuyển toàn quốc miễn phí. Mua ngay!',
    heading_search: 'Đầm, váy',
    redirect_link: '',
    category_id: 26,
    category_name: 'Đầm, váy',
    category_level: 1,
    category_info: [
      {
        id: 8,
        title: 'Thời trang nữ',
        path: 'thoi-trang-nu',
        url_key: 'thoi-trang-nu',
      },
      {
        id: 26,
        title: 'Đầm, váy',
        path: 'thoi-trang-nu/dam',
        url_key: 'dam',
        images: [
          'https://media3.scdn.vn/images/ecom/category/2486.jpg',
          'https://media3.scdn.vn/images/ecom/category/699.jpg',
          'https://media3.scdn.vn/images/ecom/category/700.jpg',
          'https://media3.scdn.vn/images/ecom/category/698.jpg',
          'https://media3.scdn.vn/images/ecom/category/31.jpg',
          'https://media3.scdn.vn/images/ecom/category/30.jpg',
          'https://media3.scdn.vn/images/ecom/category/27.jpg',
          'https://media3.scdn.vn/images/ecom/category/2489.jpg',
          'https://media3.scdn.vn/images/ecom/category/1946.jpg',
          'https://media3.scdn.vn/images/ecom/category/2485.jpg',
          'https://media3.scdn.vn/images/ecom/category/701.jpg',
          'https://media3.scdn.vn/images/ecom/category/2484.jpg',
          'https://media3.scdn.vn/images/ecom/category/2487.jpg',
          'https://media3.scdn.vn/images/ecom/category/2488.jpg',
          'https://media3.scdn.vn/images/ecom/category/1860.jpg',
          'https://media3.scdn.vn/images/ecom/category/2483.jpg',
          'https://media3.scdn.vn/images/ecom/category/33.jpg',
        ],
      },
    ],
    keywords: 'Đầm, váy,  gia re,  sieu khuyen mai',
    index: false,
    follow: true,
  },
  weight: 400,
  category_id: '1/2/8/26/2489',
  url_key:
    'da-m-suong-tho-i-trang-nu-kie-u-da-ng-do-c-da-o-ca-ti-nh-114-7367520',
  quantity: 0,
  has_options: true,
  product_relateds: '',
  shop_free_shipping: 0,
  status: 1,
  status_new: 2,
  stock_status: 1,
  media: [
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/LPamUt.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/LPamUt_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/LPamUt_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/naI6fg.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/naI6fg_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/naI6fg_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/Sk1AFy.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/Sk1AFy_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/Sk1AFy_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/01DgTg.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/01DgTg_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/01DgTg_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/GShkVY.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/GShkVY_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/GShkVY_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/kxooZh.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/kxooZh_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/kxooZh_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/EsKW8D.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/EsKW8D_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/EsKW8D_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/sP1Wom.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/sP1Wom_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/sP1Wom_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/39hIGW.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/39hIGW_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/39hIGW_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
    {
      image: 'https://media3.scdn.vn/img2/2017/10_16/xoAkXv.jpg',
      image_500x500:
        'https://media3.scdn.vn/img2/2017/10_16/xoAkXv_simg_de2fe0_500x500_maxb.jpg',
      image_50x50:
        'https://media3.scdn.vn/img2/2017/10_16/xoAkXv_simg_02d57e_50x50_maxb.jpg',
      type: 'image',
    },
  ],
  brand_info: [],
  required_options: 'kich_thuoc_1,mau_sac',
  attribute: [
    {
      attribute_id: 298,
      name: 'Kích thước',
      product_option: '7367520_298',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 815,
          value: 'S',
          product_option_id: '7367520_815',
        },
        {
          option_id: 816,
          value: 'M',
          product_option_id: '7367520_816',
        },
        {
          option_id: 817,
          value: 'L',
          product_option_id: '7367520_817',
        },
        {
          option_id: 818,
          value: 'XL',
          product_option_id: '7367520_818',
        },
        {
          option_id: 18975,
          value: '2XL',
          product_option_id: '7367520_18975',
        },
      ],
      search_key: 'kich_thuoc_1',
    },
    {
      attribute_id: 284,
      name: 'Màu sắc',
      product_option: '7367520_284',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 604,
          color_id: 9,
          product_option_id: '7367520_604',
          name: 'Trắng',
          color_hex_rgb: 'ffffff',
          image: '',
          background: '#ffffff',
        },
        {
          option_id: 605,
          color_id: 13,
          product_option_id: '7367520_605',
          name: 'Đen',
          color_hex_rgb: '000000',
          image: '',
          background: '#000000',
        },
      ],
      search_key: 'mau_sac',
    },
  ],
  description: '',
  short_description: '',
  return_policy: [
    {
      title: '48 giờ đổi trả',
      color: '#e82f3a',
      icon: 'FortyEight',
      tooltip_title: 'Chính sách đảm bảo sự hài lòng',
      tooltip_content:
        '<div class="policy-container" style="line-height: 1.4;"> <ul style="list-style: disc inside none; padding-left: 0; margin: 0;"> <li style="margin-bottom: 10px;">Đổi trả trong vòng 48h</li> <li style="margin-bottom: 10px;">Áp dụng hàng bị lỗi kỹ thuật, bể vỡ, không đúng như mô tả sản phẩm trên website, hoặc không đúng như đơn đặt hàng</li> <li style="margin-bottom: 10px;">Đổi trả khi không ưng ý kiểu dáng, màu sắc...</li> <li style="margin-bottom: 10px;">Đảm bảo hoàn tiền <a style="text-decoration: underline; color: black;" href="https://www.sendo.vn/bao-ve-nguoi-mua#bao-ve-quyen-loi-cua-ban"><strong>Xem thêm chi tiết tại đây.</strong></a></li> </ul> </div>',
    },
  ],
  price_discount: {
    total_min_price: '109,440',
    list_discount: [
      {
        title: 'Giảm thêm khi mua trên App Sendo',
        discount: '2,280',
        tooltip_title: 'Để mua sản phẩm này trên Sendo App',
        tooltip_content:
          '<div class="policy-container" style="line-height: 1.4;"> <ol style="list-style: disc inside none; padding-left: 0; margin: 0;"> <li style="margin-bottom: 10px;">Vui lòng đăng nhập Sendo.vn</li> <li style="margin-bottom: 10px;">Bấm <svg style="display: inline-block; vertical-align: middle; width: 24px; height: 24px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"> <g id="Layer_2" data-name="Layer 2"> <g id="Layer_1-2" data-name="Layer 1"> <rect width="48" height="48" fill="none" /> <path fill="red" d="M41.94,13a10.68,10.68,0,0,0-3.18-3.7,8.43,8.43,0,0,0-5.14-1.79,9.93,9.93,0,0,0-4.12,1A17.38,17.38,0,0,0,25,11.59c-.39.36-.72.7-1,1a21.24,21.24,0,0,0-2.23-2.07A16,16,0,0,0,18.5,8.46a9.93,9.93,0,0,0-4.12-1A8.44,8.44,0,0,0,9.24,9.28h0A10.68,10.68,0,0,0,6.06,13,9.92,9.92,0,0,0,5,17.52C5,21.14,6.72,25.07,9.85,29A51.29,51.29,0,0,0,23.48,40.36l.52.31.52-.31A51.3,51.3,0,0,0,38.15,29C41.28,25.07,43,21.14,43,17.52A9.92,9.92,0,0,0,41.94,13ZM36.59,27.73A48.82,48.82,0,0,1,24,38.33a48.82,48.82,0,0,1-12.59-10.6C8.45,24,7,20.49,7,17.52a7.92,7.92,0,0,1,.84-3.64,8.68,8.68,0,0,1,2.6-3h0a6.42,6.42,0,0,1,3.94-1.39,8,8,0,0,1,3.29.78,15.43,15.43,0,0,1,4,2.77c.5.47.9.89,1.17,1.19l.31.35.07.09,0,0,.79,1,.79-1h0A18.53,18.53,0,0,1,27.63,12a13.67,13.67,0,0,1,2.8-1.72,7.88,7.88,0,0,1,3.19-.74,6.42,6.42,0,0,1,3.94,1.39,8.68,8.68,0,0,1,2.6,3A7.92,7.92,0,0,1,41,17.52C41,20.49,39.55,24,36.59,27.73Z" /> </g> </g> </svg> để thêm sản phẩm cần mua vào danh sách yêu thích.</li> <li style="margin-bottom: 10px;">Mở Sendo App và đăng nhập tài khoản Sendo của bạn.</li> <li style="margin-bottom: 10px;">Truy cập “Tài khoản”/”Sản phẩm yêu thích” để mua hàng.</li> </ol> <div style="display: flex; justify-content: space-around; align-items: center; padding-top: 20px;"> <a href="https://itunes.apple.com/vn/app/sendo.vn-mua-sam-online-am-bao/id940313804?mt=8&referrer=utm_source%3Dgoogle%2C%20SendoWap%26utm_medium%3Dlistingbanner%26utm_term%3Dinstallment%26utm_campaign%3DiOSinstall"> <img src="http://media3.scdn.vn/img2/2017/8_4/Pbp6SB.png" alt="App Store" style="width: 92px; height: 32px; "> </a> <a href="https://play.google.com/store/apps/details?id=com.sendo&referrer=utm_source%3DSendoWap%26utm_medium%3Dlistingbanner%26utm_term%3Dinstallment%26utm_campaign%3DAndroidinstall"> <img src="http://media3.scdn.vn/img2/2017/8_4/ZBMCNJ.png" alt="Play Store" style="width: 92px; height: 32px; "></a> </div> </div>',
      },
      {
        title: 'Hoàn tiền sau khi mua hàng và đánh giá',
        discount: 2280,
        tooltip_title: 'Hoàn tiền điểm Sen',
        tooltip_content:
          '<div class="policy-container" style="line-height: 1.4;"> <p>Số tiền <strong>2,280 Đ</strong> sẽ được hoàn vào ví điểm Sen sau khi đơn hàng hoàn tất và đánh giá.</p> </div>',
      },
    ],
  },
  size_guide: '',
  metadata: {
    page_title:
      'Đầm suông thời trang nữ , kiểu dáng độc đáo cá tính 114 - 92114',
    heading_search: 'Mã: 92114',
    og_title:
      'Đầm suông thời trang nữ , kiểu dáng độc đáo cá tính 114 7367520 - giảm 45%',
    og_description: 'Giá KM: 114,000VNĐ - Mua tại shop hotly trên Sendo.vn',
    og_image: 'https://media3.scdn.vn/img2/2017/10_16/xoAkXv.jpg',
    index: true,
    follow: true,
    keywords: 'thời trang nữ',
  },
  isFavorite: false,
  ratings: [],
  comments: [],
  relateds: [],
  recommends: [
    {
      id: 7813132,
      name: 'Đầm Body Thể Thao',
      admin_id: 52050,
      cat_path:
        'san-pham/dam-body-the-thao-7813132?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 467000,
      image:
        'https://media3.scdn.vn/img2/2017/11_20/QF71el_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 467000,
      promotion_percent: 0,
      order_count: 4,
      brand_id: 0,
      sku_user: 'GDN-98820',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm maxi rẻ,đầm maxi ren,đầm maxi',
      uid: 'recommend_product_7813132',
      short_description:
        'Đầm Body Thể ThaoHotline: 0984.76.06.43....Zalo: 0128.343.7079 (Sỉ),&nbsp;0984.76.06.43 (Lẻ)&nbsp;Chi tiết sản phẩm+ Kiểu dáng thời trang, hiện đại.+ ',
    },
    {
      id: 7218046,
      name: 'Đầm váy thun, thời trang đầm váy thun, váy đầm',
      admin_id: 229970,
      cat_path:
        'san-pham/dam-vay-thun-thoi-trang-dam-vay-thun-vay-dam-7218046?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 478000,
      image:
        'https://media3.scdn.vn/img2/2017/10_4/DaW8rT_simg_b5529c_250x250_maxb.png',
      special_price: 239000,
      final_price: 239000,
      promotion_percent: 50,
      order_count: 7,
      brand_id: 0,
      sku_user: 'DV0032',
      quantity: 93,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm váy thun,thời trang đầm váy thun,váy đầm',
      uid: 'recommend_product_7218046',
      short_description:
        'Đầm váy thun, thời trang đầm váy thun, váy đầmĐầm váy thun công sở trang trọng lịch sự phù hợp với đi làm đi chơi.Chất liệu: Thun cao cấpSize: LShop h',
    },
    {
      id: 5816346,
      name: 'Đầm 2 dây đai thun chữ body cotton',
      admin_id: 4525,
      cat_path:
        'san-pham/dam-2-day-dai-thun-chu-body-cotton-5816346?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 210000,
      image:
        'https://media3.scdn.vn/img2/2017/5_27/49v5pM_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 210000,
      promotion_percent: 0,
      order_count: 1,
      brand_id: 0,
      sku_user: 'qc877',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm thun body 2 dây',
      uid: 'recommend_product_5816346',
      short_description:
        'Đầm 2 dây đai thun chữ body cotton Dài:88 Ngang ngực:90 Eo:70hàng nhập quảng châu shop đăng ảnh thật rồi nha các bạn.đặt hàng tư vấn gọi &nbsp;zalo vi',
    },
    {
      id: 6324483,
      name: 'chân váy bút chì',
      admin_id: 225696,
      cat_path:
        'san-pham/chan-vay-but-chi-6324483?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 209000,
      image:
        'https://media3.scdn.vn/img2/2017/7_13/0RdKMr_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 209000,
      promotion_percent: 0,
      order_count: 1,
      brand_id: 0,
      sku_user: 'd02',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'CHÂN VAY,Chân váy,chân vấy,Chân váy bút chì,bút chì',
      uid: 'recommend_product_6324483',
      short_description: '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n',
    },
    {
      id: 6170783,
      name: 'Đầm thun  da cá Fashion Girls  DTC99',
      admin_id: 62660,
      cat_path:
        'san-pham/dam-thun-da-ca-fashion-girls-dtc99-6170783?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 155000,
      image:
        'https://media3.scdn.vn/img2/2017/6_29/ekqTtC_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 155000,
      promotion_percent: 0,
      order_count: 3,
      brand_id: 0,
      sku_user: ' DTC99 ',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords:
        'bộ đồ mặc nhà,đồ bộ quần dài,quần áo nữ,quần áo thể thao,bộ quần áo nữ',
      uid: 'recommend_product_6170783',
      short_description:
        '2SEXYCam Kết-Hàng Giống Hình 95%-&nbsp;&nbsp;&nbsp;Không Bán Sản Phẩm Kém Chất Lượng-&nbsp;&nbsp;&nbsp;Giao Nhận Hàng Rồi Mới Thanh Toán Toàn Quốc-&nb',
    },
    {
      id: 7489158,
      name: 'Đầm thun kẻ sọc tay loe',
      admin_id: 254123,
      cat_path:
        'san-pham/dam-thun-ke-soc-tay-loe-7489158?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 89000,
      image:
        'https://media3.scdn.vn/img2/2017/10_25/kFZYNL_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 89000,
      promotion_percent: 0,
      order_count: 1,
      brand_id: 0,
      sku_user: '0126',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm thu,Đầm thun,Đầm thun kẻ sọc,Kẻ sọc,tay lỡ',
      uid: 'recommend_product_7489158',
      short_description:
        'Đầm thun kẻ sọc tay loe đầm xinh đầm đẹp đầm yêu...Freesize dưới 56kg tủy chiều cao',
    },
    {
      id: 7673811,
      name: 'Hàng nhập đầm chữ A sọc viền hở vai cổ tròn',
      admin_id: 154780,
      cat_path:
        'san-pham/hang-nhap-dam-chu-a-soc-vien-ho-vai-co-tron-7673811?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 320000,
      image:
        'https://media3.scdn.vn/img2/2017/11_9/27RngG_simg_b5529c_250x250_maxb.jpg',
      special_price: 160000,
      final_price: 160000,
      promotion_percent: 50,
      order_count: 1,
      brand_id: 0,
      sku_user: '08876',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm',
      uid: 'recommend_product_7673811',
      short_description:
        'Hàng nhập ,Đầm xòe chữ A khoét vai phối sọc thời trang&nbsp;&nbsp;Kiểu dáng hiện đại, cô tròn tay ngắn khoét vai phối viền sọc trẻ trung, dáng đầm xòe',
    },
    {
      id: 7480720,
      name: 'Đầm thun ôm quyến rũ phong cách Hàn Quốc',
      admin_id: 72923,
      cat_path:
        'san-pham/dam-thun-om-quyen-ru-phong-cach-han-quoc-7480720?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 460000,
      image:
        'https://media3.scdn.vn/img2/2017/10_24/l5OeCV_simg_b5529c_250x250_maxb.jpg',
      special_price: 230000,
      final_price: 230000,
      promotion_percent: 50,
      order_count: 1,
      brand_id: 0,
      sku_user: 'C 04 0',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm thun,đầm thun suông',
      uid: 'recommend_product_7480720',
      short_description:
        'Đầm ôm&nbsp;quyến rũ phong cách Thời trang Hàn Quốc, màu sắc quý phái, tôn dáng và da,&nbsp;tạo&nbsp;nên sự&nbsp;thoải&nbsp;mái khi mặc, thích hợp đến',
    },
    {
      id: 6965068,
      name: 'Hàng nhập - Đầm thun suông thêu hoa tay lỡ',
      admin_id: 110661,
      cat_path:
        'san-pham/hang-nhap-dam-thun-suong-theu-hoa-tay-lo-6965068?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 481000,
      image:
        'https://media3.scdn.vn/img2/2017/9_12/xYP17w_simg_b5529c_250x250_maxb.jpg',
      special_price: 395000,
      final_price: 395000,
      promotion_percent: 18,
      order_count: 2,
      brand_id: 0,
      sku_user: 'GT1152',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords:
        'đầm babydoll thêu hoa tay lỡ,đầm suông thêu tay,đầm suông cổ trụ hoa đào tay lỡ,đầm suông ren tay lỡ,đầm suông thêu hoa hồng',
      uid: 'recommend_product_6965068',
      short_description:
        'Hàng nhập - Đầm thun suông thêu hoa tay lỡ -&nbsp;GT1152Thông tin sản phẩm :+ Xuất xứ : Quảng Châu+ Chất liệu : thun lụa phối voan+ Màu sắc : &nbsp;Đe',
    },
    {
      id: 7632297,
      name: 'BT922 - ĐẦM THUN IN CHỮ CÓ TAY DÀI PHỐI NÓN CỰC XINH',
      admin_id: 74006,
      cat_path:
        'san-pham/bt922-dam-thun-in-chu-co-tay-dai-phoi-non-cuc-xinh-7632297?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 230000,
      image:
        'https://media3.scdn.vn/img2/2017/11_6/bw7qqh_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 230000,
      promotion_percent: 0,
      order_count: 1,
      brand_id: 0,
      sku_user: 'BT922',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords:
        'đầm thun in chữ,đầm thun in chữ hàn,áo thun hoodie có nón chữ faded,quần thun nam có in chữ',
      uid: 'recommend_product_7632297',
      short_description:
        'CHẤT LIỆU;&nbsp; THUN DA CÁ&nbsp;&nbsp;CAO CẤP MỀM MỊN&nbsp;THOÁNG MÁT RẤT ĐẸP&nbsp;&nbsp; SIZE : 45-57KG TÙY CHIỀU CAOFORM CHUẨN, CHẤT LƯỢNG CAO CẤP,',
    },
    {
      id: 7793980,
      name: 'Hàng Nhập -  Đầm thun họa tiết cao cấp',
      admin_id: 153728,
      cat_path:
        'san-pham/hang-nhap-dam-thun-hoa-tiet-cao-cap-7793980?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 465000,
      image:
        'https://media3.scdn.vn/img2/2017/11_18/QP6xEc_simg_b5529c_250x250_maxb.jpg',
      special_price: 385000,
      final_price: 385000,
      promotion_percent: 18,
      order_count: 1,
      brand_id: 0,
      sku_user: 'HN99783',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm thu,Đầm thun,Đầm thun hoa,họa tiết,cao cap',
      uid: 'recommend_product_7793980',
      short_description:
        'Cham San Fashion - Bán Uy Tín; Khách - Mua Chất Lượng!&nbsp;Tên sản phẩm: Áo khoác khăn len cao cấp&nbsp;Kích thước :&nbsp;M, L, XL, 2XL&nbsp;+ Size M',
    },
    {
      id: 6170880,
      name: 'Đầm thun mặc nhà size XXXL  MC99',
      admin_id: 62660,
      cat_path:
        'san-pham/dam-thun-mac-nha-size-xxxl-mc99-6170880?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 155000,
      image:
        'https://media3.scdn.vn/img2/2017/6_29/ZnceZE_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 155000,
      promotion_percent: 0,
      order_count: 1,
      brand_id: 0,
      sku_user: ' MC99',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'bộ đồ mặc nhà,đồ bộ quần dài,bộ quần áo nữ,đầm thun,đầm nữ',
      uid: 'recommend_product_6170880',
      short_description:
        '2SEXYCam Kết-Hàng Giống Hình 99%-&nbsp;&nbsp;&nbsp;Không Bán Sản Phẩm Kém Chất Lượng-&nbsp;&nbsp;&nbsp;Giao Nhận Hàng Rồi Mới Thanh Toán Toàn Quốc-&nb',
    },
    {
      id: 7716760,
      name: 'ĐẦM THUN SUÔNG PHỐI SỌC',
      admin_id: 231088,
      cat_path:
        'san-pham/dam-thun-suong-phoi-soc-7716760?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 260000,
      image:
        'https://media3.scdn.vn/img2/2017/11_12/ZEfskX_simg_b5529c_250x250_maxb.jpg',
      special_price: 199000,
      final_price: 199000,
      promotion_percent: 24,
      order_count: 1,
      brand_id: 0,
      sku_user: 'MDT0196',
      quantity: 0,
      rating_info: {
        total_rated: 1,
        percent_star_rating: 100,
        percent_number: 100,
      },
      category_info: [],
      keywords:
        'đầm suông sọc phối,đầm thun sọc phối,đầm suông kẻ sọc ngang phối màu,đầm suông phối chân sọc dk9311,đầm thun sọc suông dài',
      uid: 'recommend_product_7716760',
      short_description:
        'ĐẦM THUN SUÔNG PHỐI SỌC-MDT0196Chất liệu: thun da cáMàu: đỏ, đen Kiểu dáng: đầm cổ tròn khoét sao, tay ngắn phối lưới, 2 túi, đắp logo, viền sọc trắng',
    },
    {
      id: 6840405,
      name: 'Đầm thun phối ren',
      admin_id: 153728,
      cat_path:
        'san-pham/dam-thun-phoi-ren-6840405?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 325000,
      image:
        'https://media3.scdn.vn/img2/2017/8_30/RqCMQ9_simg_b5529c_250x250_maxb.jpg',
      special_price: 230000,
      final_price: 230000,
      promotion_percent: 30,
      order_count: 13,
      brand_id: 0,
      sku_user: 'HT78900',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords:
        'đầm ren xòe cao cấp,đầm suông ren cao cấp,đầm ren body cao cấp,đầm phối ren cao cấp,áo đầm ren cao cấp',
      uid: 'recommend_product_6840405',
      short_description:
        'Cham San Fashion - Bán Uy Tín; Khách - Mua Chất Lượng!&nbsp;Tên sản phẩm: Đầm thun phôi ren cao cấp&nbsp;Kích thước :&nbsp;M, L, XL, 2XL&nbsp;+ Size M',
    },
    {
      id: 6365901,
      name: 'Đầm nữ',
      admin_id: 45194,
      cat_path:
        'san-pham/dam-nu-6365901?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 65000,
      image:
        'https://media3.scdn.vn/img2/2017/7_17/8zBKZi_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 65000,
      promotion_percent: 0,
      order_count: 3,
      brand_id: 0,
      sku_user: '949',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords:
        'đầm nữ,đầm ôm đầm nữ,đầm ôm nữ, đầm body nữ,đầm ôm nữ tính,đầm cho thiếu nữ',
      uid: 'recommend_product_6365901',
      short_description:
        'Thun da cá dày đẹp &nbsp;- free size : dưới 55kg là mặc vừa và đẹp - sản phẩm giống hình 100% nhé khách - có ảnh thật của đầm bên dưới nhé bạn.sản phẩ',
    },
    {
      id: 5985899,
      name: 'Đầm thun rộng',
      admin_id: 109891,
      cat_path:
        'san-pham/dam-thun-rong-5985899?source_block_id = rec_may_like&source_page_id = product_detail',
      price: 110000,
      image:
        'https://media3.scdn.vn/img2/2017/6_12/sMDcy8_simg_b5529c_250x250_maxb.jpg',
      special_price: 0,
      final_price: 110000,
      promotion_percent: 0,
      order_count: 5,
      brand_id: 0,
      sku_user: 'AK2186',
      quantity: 0,
      rating_info: {
        total_rated: 0,
      },
      category_info: [],
      keywords: 'đầm thu,Đầm thun',
      uid: 'recommend_product_5985899',
      short_description:
        'MiA Chan ShopThời trang giá sỉ▶️☎️Liên hệ: 0984373861( Zalo/Viber/ Ims) hoặc add zalo 0898451592 để xem mẫu và mua sỉ👌🏻👍🏻 ĐẦM THUN FORM RỘNG&nbsp;được',
    },
  ],
  selectedAttr: {
    kich_thuoc_1: {
      groupOption: {
        attribute_id: 298,
        name: 'Kích thước',
        product_option: '7367520_298',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 815,
            value: 'S',
            product_option_id: '7367520_815',
          },
          {
            option_id: 816,
            value: 'M',
            product_option_id: '7367520_816',
          },
          {
            option_id: 817,
            value: 'L',
            product_option_id: '7367520_817',
          },
          {
            option_id: 818,
            value: 'XL',
            product_option_id: '7367520_818',
          },
          {
            option_id: 18975,
            value: '2XL',
            product_option_id: '7367520_18975',
          },
        ],
        search_key: 'kich_thuoc_1',
      },
      selected: {
        option_id: 815,
        value: 'S',
        product_option_id: '7367520_815',
      },
    },
    mau_sac: {
      groupOption: {
        attribute_id: 284,
        name: 'Màu sắc',
        product_option: '7367520_284',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 604,
            color_id: 9,
            product_option_id: '7367520_604',
            name: 'Trắng',
            color_hex_rgb: 'ffffff',
            image: '',
            background: '#ffffff',
          },
          {
            option_id: 605,
            color_id: 13,
            product_option_id: '7367520_605',
            name: 'Đen',
            color_hex_rgb: '000000',
            image: '',
            background: '#000000',
          },
        ],
        search_key: 'mau_sac',
      },
      selected: {
        option_id: 604,
        color_id: 9,
        product_option_id: '7367520_604',
        name: 'Trắng',
        color_hex_rgb: 'ffffff',
        image: '',
        background: '#ffffff',
      },
    },
  },
  data_checkout: {
    shop_id: 30854,
    product_id: 7367520,
    qty: 1,
    options: {
      '7367520_298': ['7367520_815'],
      '7367520_284': ['7367520_604'],
    },
    buynow: 0,
    source_block_id: 'listing_products',
    source_page_id: 'cate2_vasup_desc',
    source_info: '',
  },
};
const data_delete_cart = [
  {
    id: '49fc2d17d11bfd22a58f53f83307f4df',
    name: 'Đầm Suông Tay Lỡ Thời Trang CAO CẤP ANN365',
    stock_status: 1,
    image:
      'https://media3.scdn.vn/img2/2017/8_11/l40nC2_simg_02d57e_50x50_maxb.jpg',
    shop_id: 61652,
    final_price: 540000,
    qty: 1,
    quantity: 0,
    hash: '49fc2d17d11bfd22a58f53f83307f4df',
    attribute: [
      {
        attribute_id: 298,
        name: 'Kích thước',
        product_option: '6649929_298',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 815,
            value: 'S',
            product_option_id: '6649929_815',
          },
          {
            option_id: 816,
            value: 'M',
            product_option_id: '6649929_816',
          },
          {
            option_id: 817,
            value: 'L',
            product_option_id: '6649929_817',
          },
          {
            option_id: 818,
            value: 'XL',
            product_option_id: '6649929_818',
          },
          {
            option_id: 18975,
            value: '2XL',
            product_option_id: '6649929_18975',
          },
        ],
        search_key: 'kich_thuoc_1',
      },
      {
        attribute_id: 284,
        name: 'Màu sắc',
        product_option: '6649929_284',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 1476,
            color_id: 19,
            product_option_id: '6649929_1476',
            name: 'Khác',
            color_hex_rgb: '',
            image:
              'https://media3.scdn.vn/img/2014/3_6/khac_2ikqk5jehmtts_simg_2df889_740x613_max.gif',
            background:
              'url(https://media3.scdn.vn/img/2014/3_6/khac_2ikqk5jehmtts_simg_2df889_740x613_max.gif)',
          },
        ],
        search_key: 'mau_sac',
      },
    ],
    cart_attribute: [
      {
        option_id: 815,
        search_key: 'kich_thuoc_1',
        value: 'S',
        product_option_id: '6649929_815',
        checkout_name: 'options[6649929_298][]',
        checkout_value: '6649929_815',
      },
      {
        option_id: 1476,
        search_key: 'mau_sac',
        value: 'Khác',
        product_option_id: '6649929_1476',
        color_hex_rgb: '',
        image:
          'https://media3.scdn.vn/img/2014/3_6/khac_2ikqk5jehmtts_simg_2df889_740x613_max.gif',
        background:
          'url(https://media3.scdn.vn/img/2014/3_6/khac_2ikqk5jehmtts_simg_2df889_740x613_max.gif)',
        checkout_name: 'options[6649929_284][]',
        checkout_value: '6649929_1476',
      },
    ],
  },
];
describe('test function getVariantInfoFromAttribute', () => {
  it('should return correct value', () => {
    const attribute = [
      {
        attribute_id: 298,
        name: 'Kích thước',
        product_option: '7367520_298',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 815,
            value: 'S',
            product_option_id: '7367520_815',
          },
          {
            option_id: 816,
            value: 'M',
            product_option_id: '7367520_816',
          },
          {
            option_id: 817,
            value: 'L',
            product_option_id: '7367520_817',
          },
          {
            option_id: 818,
            value: 'XL',
            product_option_id: '7367520_818',
          },
          {
            option_id: 18975,
            value: '2XL',
            product_option_id: '7367520_18975',
          },
        ],
        search_key: 'kich_thuoc_1',
      },
      {
        attribute_id: 284,
        name: 'Màu sắc',
        product_option: '7367520_284',
        show_required: 1,
        type: 'Option',
        value: [
          {
            option_id: 604,
            color_id: 9,
            product_option_id: '7367520_604',
            name: 'Trắng',
            color_hex_rgb: 'ffffff',
            image: '',
            background: '#ffffff',
          },
          {
            option_id: 605,
            color_id: 13,
            product_option_id: '7367520_605',
            name: 'Đen',
            color_hex_rgb: '000000',
            image: '',
            background: '#000000',
          },
        ],
        search_key: 'mau_sac',
      },
    ];
    const dataCheckOut = {
      '7367520_298': ['7367520_815'],
      '7367520_284': ['7367520_604'],
    };
    const data_expect = {
      data_ecommerce: 'kich_thuoc_1:S|mau_sac:Trắng',
      data_normal: {
        mau_sac: {
          attribute_value: 'Trắng',
          attribute_id: 604,
        },
        kich_thuoc_1: {
          attribute_value: 'S',
          attribute_id: 815,
        },
      },
    };
    expect(
      trackingHelpers.getVariantInfoFromAttribute(attribute, dataCheckOut)
    ).toEqual(data_expect);
  });
});
describe('test function getVariantInfoFromCartAttribute', () => {
  it('should return correct value', () => {
    const attribute = [
      {
        option_id: 814,
        search_key: 'kich_thuoc_1',
        value: 'Free size',
        product_option_id: '4313992_814',
        checkout_name: 'options[4313992_298][]',
        checkout_value: '4313992_814',
      },
      {
        option_id: 605,
        search_key: 'mau_sac',
        value: 'Đen',
        product_option_id: '4313992_605',
        color_hex_rgb: '000000',
        image: '',
        background: '#000000',
        checkout_name: 'options[4313992_284][]',
        checkout_value: '4313992_605',
      },
    ];
    const data_expect = {
      data_ecommerce: 'kich_thuoc_1:Free size|mau_sac:Đen',
      data_normal: {
        mau_sac: {
          attribute_value: 'Đen',
          attribute_id: 605,
        },
        kich_thuoc_1: {
          attribute_value: 'Free size',
          attribute_id: 814,
        },
      },
    };
    expect(trackingHelpers.getVariantInfoFromCartAttribute(attribute)).toEqual(
      data_expect
    );
  });
});
describe('test function getCategoryInfoTracking', () => {
  it('should return correct value', () => {
    const category_info = [
      {
        id: 94,
        title: 'Thời trang nam',
        path: 'thoi-trang-nam',
        url_key: 'thoi-trang-nam',
      },
      {
        id: 673,
        title: 'Áo khoác nam',
        path: 'thoi-trang-nam/ao-khoac-nam',
        url_key: 'ao-khoac-nam',
      },
      {
        id: 681,
        title: 'Áo khoác chống nắng cho nam',
        path: 'thoi-trang-nam/ao-khoac-nam/ao-khoac-chong-nang-cho-nam',
        url_key: 'ao-khoac-chong-nang-cho-nam',
      },
    ];
    const res = {
      cate1: 'thoi-trang-nam',
      cate2: 'ao-khoac-nam',
      cate3: 'ao-khoac-chong-nang-cho-nam',
      cate1_id: 94,
      cate2_id: 673,
      cate3_id: 681,
      cate1_name: 'Thời trang nam',
      cate2_name: 'Áo khoác nam',
      cate3_name: 'Áo khoác chống nắng cho nam',
    };
    expect(trackingHelpers.getCategoryInfoTracking(category_info)).toEqual(res);
  });
});
describe('test function getIdProductIfProductIsAds', () => {
  it('should return correct value', () => {
    const product_id = 'keyword_ads_7729510';
    //const product_id = 7729510;
    expect(trackingHelpers.getIdProductIfProductIsAds(product_id)).toEqual(
      '7729510'
    );
  });
});
describe('test function getPageId', () => {
  it('should return correct value', () => {
    const href =
      'https://www.sendo.vn/shop/bao-ngoc-store/san-pham/lg-v10-ram-4g-bo-nho-64g-han-quoc-5719369/';
    expect(trackingHelpers.getPageId(href)).toEqual('detail_shop');
  });
});
describe('test function buildHref', () => {
  it('should return correct value', () => {
    const requestInfo = {
      category_id: 123,
      sortType: 'vasup_desc',
      page: 2,
    };
    const res = 'http://localhost/?category_id=123&page=2&sortType=vasup_desc';
    expect(trackingHelpers.buildHref(requestInfo)).toEqual(res);
  });
});
describe('test function getIsReferrerInternalSendo', () => {
  it('should return correct value', () => {
    const href =
      'https://m.sendo.vn/san-pham/lg-v10-ram-4g-bo-nho-64g-han-quoc-5719369/';
    expect(trackingHelpers.getIsReferrerInternalSendo(href)).toEqual(1);
  });
});
describe('test function getBelongTab', () => {
  it('should return correct value', () => {
    const search = '?sortType=vasup_asc';
    expect(
      trackingHelpers.getBelongTab(search, 'listing_product', null)
    ).toEqual('vasup_asc');
  });
});
describe('test function getListName', () => {
  it('should return correct value', () => {
    const context = 'listing_product';
    expect(trackingHelpers.getListName(context)).toEqual('Category Results');
  });
});
describe('test function getEventName', () => {
  it('should return correct value', () => {
    const context = 'listing_product';
    expect(trackingHelpers.getEventName(context)).toEqual('trackProductClick');
  });
});
describe('test function getEventImpressName', () => {
  it('should return correct value', () => {
    const context = 'listing_product';
    expect(trackingHelpers.getEventImpressName(context)).toEqual(
      'trackProductImpression'
    );
  });
});
describe('test function getLastCategory', () => {
  it('should return correct value', () => {
    const category_info = [
      {
        id: 94,
        path: 'thoi-trang-nam',
        title: 'Thời trang nam',
        url_key: 'thoi-trang-nam',
      },
      {
        id: 673,
        path: 'thoi-trang-nam/ao-khoac-nam',
        title: 'Áo khoác nam',
        url_key: 'ao-khoac-nam',
      },
    ];
    expect(trackingHelpers.getLastCategory(category_info)).toEqual({
      id: 673,
      title: 'Áo khoác nam',
    });
  });
});
describe('test count handleProductAccess return value', () => {
  it('should return correct value', () => {
    expect(
      Object.keys(handleProductAccess(event_received, data_received)).length
    ).toEqual(29);
  });
});
describe('test count handleProductClick return value', () => {
  it('should return correct value', () => {
    expect(
      Object.keys(handleProductClick(event_received, data_received)).length
    ).toEqual(21);
  });
});
describe('test count handleProductImpression return value', () => {
  it('should return correct value', () => {
    expect(
      Object.keys(handleProductImpression(event_received, data_received)).length
    ).toEqual(23);
  });
});
describe('test count handleCategoryAccess return value', () => {
  it('should return correct value', () => {
    expect(
      Object.keys(handleCategoryAccess(event_received, data_received_category))
        .length
    ).toEqual(31);
  });
});
describe('test count handleSearch return value', () => {
  it('should return correct value', () => {
    expect(
      Object.keys(handleSearch(event_received, data_received_category)).length
    ).toEqual(22);
  });
});
describe('tracking handle', () => {
  afterEach(function() {
    cookie.get.restore();
    urlHelpers.encodeUrl.restore();
  });
  beforeEach(function() {
    sinon.stub(cookie, 'get').returns(123);
    sinon.stub(urlHelpers, 'encodeUrl').returns('http://localhost/');
  });
  describe('test function handleProductAccess', () => {
    it('should return correct value', () => {
      let expect_value = {
        event: 'PAGE_VIEW_DETAIL',
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        cate1: 'thoi-trang-nam',
        cate2: 'ao-khoac-nam',
        cate3: 'ao-khoac-chong-nang-cho-nam',
        cate1_id: 94,
        cate2_id: 673,
        cate3_id: 681,
        cate1_name: 'Thời trang nam',
        cate2_name: 'Áo khoác nam',
        cate3_name: 'Áo khoác chống nắng cho nam',
        pageurl: _get(window, 'location.href'),
        cate_dimension: 'thoi-trang-nam',
        ecommerce: {
          detail: {
            actionField: [{ action: 'detail' }],
            products: [
              {
                name: 'sản phẩm để test',
                id: 7845259,
                product_ext_id: 7845259,
                price: 100000,
                old_price: 100000,
                brand: '',
                category:
                  'Thời trang nam / Áo khoác nam / Áo khoác chống nắng cho nam',
                shop_id: 234252,
                shop_name: 'Siêu Cẩu',
                shop_ext_id: 234252,
                shop_num_lotuses: 0,
                shop_lotus_type: 'red',
              },
            ],
            page_id: 'detail_sendo',
          },
        },
        google_tag_params: {
          ecomm_prodid: 7845259,
          ecomm_pagetype: 'product',
          ecomm_totalvalue: 100000,
          ecomm_category:
            'Thời trang nam / Áo khoác nam / Áo khoác chống nắng cho nam',
        },
      };
      expect(handleProductAccess(event_received, data_received)).toEqual(
        expect_value
      );
    });
  });

  describe('test function handleProductClick', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        event: 'trackProductClick',
        belong_tab: 'vasup_desc',
        experiment_id: 123,
        num_result_per_page: LISTING_PRODUCT_PER_PAGE,
        special_res: 123,
        ecommerce: {
          click: {
            actionField: {
              action: 'click',
              list: 'Category Results',
            },
            products: [
              {
                referrer_internal: 1,
                belong_tab: 'vasup_desc',
                brand: null,
                category: 681,
                id: 7845259,
                name: 'sản phẩm để test',
                is_sponsored: 0,
                list: 'Category Results',
                position: 1,
                price: 100000,
                product_type: 0,
                shop_id: 234252,
                shop_name: 'Siêu Cẩu',
                variant: '', //variant
                cate1_id: 94,
                cate2_id: 673,
                cate3_id: 681,
                cate1_name: 'Thời trang nam',
                cate2_name: 'Áo khoác nam',
                cate3_name: 'Áo khoác chống nắng cho nam',
                cate1: 'thoi-trang-nam',
                cate2: 'ao-khoac-nam',
                cate3: 'ao-khoac-chong-nang-cho-nam',
              },
            ],
          },
        },
      };
      expect(handleProductClick(event_received, data_received)).toEqual(
        expect_value
      );
    });
  });

  describe('test function handleCategoryAccess', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        cate1: 'thoi-trang-nam',
        cate2: 'ao-khoac-nam',
        cate3: '',
        cate1_id: 94,
        cate2_id: 673,
        cate3_id: '',
        cate1_name: 'Thời trang nam',
        cate2_name: 'Áo khoác nam',
        cate3_name: '',
        google_tag_params: {
          ecomm_pagetype: 'category',
          ecomm_prodid: 673,
          ecomm_category: 'Thời trang nam / Áo khoác nam',
        },
        pageurl: _get(window, 'location.href'),
        event: 'PAGE_VIEW_CATEGORY_LEVEL3',
        belong_tab: 'vasup_desc',
        listing_algo: 123,
        listing_page: 1,
        experiment_id: 123,
      };
      expect(
        handleCategoryAccess(event_received, data_received_category)
      ).toEqual(expect_value);
    });
  });

  describe('test function handleProductImpression', () => {
    it('should return correct value', () => {
      let expect_value = {
        cate1: 'thoi-trang-nam',
        cate2: 'ao-khoac-nam',
        cate3: 'ao-khoac-chong-nang-cho-nam',
        cate1_id: 94,
        cate2_id: 673,
        cate3_id: 681,
        cate1_name: 'Thời trang nam',
        cate2_name: 'Áo khoác nam',
        cate3_name: 'Áo khoác chống nắng cho nam',
        position: 1,
        id: 7845259,
        name: 'sản phẩm để test',
        shop_id: 234252,
        shop_name: 'Siêu Cẩu',
        is_sponsored: 0,
        price: 100000,
        variant: '', // variant
        category: null, // category j đó
        brand: '',
        belong_tab: 'vasup_desc',
        referrer_internal: '',
        product_type: 0,
        list: 'Category Results',
      };
      expect(handleProductImpression(event_received, data_received)).toEqual(
        expect_value
      );
    });
  });

  describe('test function handleCommonImpression', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        total_result: '',
        belong_tab: 'vasup_desc',
        special_res: 123,
        num_result_per_page: LISTING_PRODUCT_PER_PAGE,
        event: 'trackProductImpression',
        number_of_items: '',
        ecommerce: {
          impressions: [],
        },
        experiment_id: 123,
        listing_algo: 123,
      };
      expect(handleCommonImpression(event_received, data_received)).toEqual(
        expect_value
      );
    });
  });

  describe('test function handleSearch', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        total_result: null,
        num_sponsored_results: 4,
        belong_tab: 'vasup_desc',
        num_result_per_page: LISTING_PRODUCT_PER_PAGE,
        keyword: 'váy||',
        event: 'PAGE_VIEW_SEARCH',
        search_algo: 123,
      };
      expect(handleSearch(event_received, data_search)).toEqual(expect_value);
    });
  });
  describe('test function handleAddToCart', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        event: 'addToCart',
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        products: [],
        thumbnail: '',
        total_item: 0,
        shop_id: 30854,
        shop_url: 'shop/hotly_vn',
        name: 'hotly',
        ecommerce: {
          currencyCode: 'VND',
          add: {
            products: [
              {
                name: 'Đầm suông thời trang nữ , kiểu dáng độc đáo cá tính 114',
                id: 7367520,
                price: 207142,
                brand: '',
                category: 'Đầm, váy',
                variant: 'kich_thuoc_1:S|mau_sac:Trắng',
                quantity: '1',
              },
            ],
          },
        },
      };
      const product = {
        cate1: 'thoi-trang-nu',
        cate2: 'dam',
        cate3: '',
        cate1_id: 8,
        cate2_id: 26,
        cate3_id: '',
        cate1_name: 'Thời trang nữ',
        cate2_name: 'Đầm, váy',
        cate3_name: '',
        belong_cate_lvl1_id: 8,
        belong_cate_lvl2_id: 26,
        belong_cate_lvl3_id: '',
        belong_cate_lvl1_name: 'Thời trang nữ',
        belong_cate_lvl2_name: 'Đầm, váy',
        belong_cate_lvl3_name: '',
        id: 7367520,
        name: 'Đầm suông thời trang nữ , kiểu dáng độc đáo cá tính 114',
        price: 207142,
        final_price: 114000,
        quantity: '1',
        belong_shop_id: 30854,
        belong_shop_name: 'hotly',
        brand: '',
        belong_shop_reputation: {
          num_lotuses: 0,
          lotus_type: 'red',
        },
        variant: {
          mau_sac: {
            attribute_value: 'Trắng',
            attribute_id: 604,
          },
          kich_thuoc_1: {
            attribute_value: 'S',
            attribute_id: 815,
          },
        },
        variant_ecommerce: 'kich_thuoc_1:S|mau_sac:Trắng',
        shipping_fee: 0,
      };
      expect_value.products.push(product);
      expect(handleAddToCart(event_received, data_cart)).toEqual(expect_value);
    });
  });
  describe('test function handleRemoveFromCart', () => {
    it('should return correct value', () => {
      let expect_value = {
        email: 'thuatlt@sendo.vn',
        login_type: 123,
        userid: '2017367597',
        sessionid: 123,
        sendo_platform: SENDO_PLATFORM,
        ip_client: '',
        session_key: _get(event_received, 'session.session_key', null),
        href: 'http://localhost/',
        referrer_internal: 1,
        domain: _get(window, 'location.host', null),
        u_agent: _get(window, 'navigator.userAgent', null),
        event: 'removeFromCart',
        user_id: '30d858f1-1e44-4cc1-ad74-77b171d21a19',
        client_time: _get(event_received, 'client_time', null),
        login_id: _get(event_received, 'identity.user_id', null),
        login_email: 'thuatlt@sendo.vn',
        products: [
          {
            id: '49fc2d17d11bfd22a58f53f83307f4df',
            name: 'Đầm Suông Tay Lỡ Thời Trang CAO CẤP ANN365',
            price: null,
            final_price: 540000,
            variant: {
              kich_thuoc_1: { attribute_value: 'S', attribute_id: 815 },
              mau_sac: { attribute_value: 'Khác', attribute_id: 1476 },
            },
            variant_ecommerce: 'kich_thuoc_1:S|mau_sac:Khác',
            quantity: 1,
            brand: '',
            category: '',
          },
        ],
        ecommerce: {
          currencyCode: 'VND',
          remove: {
            products: [
              {
                id: '49fc2d17d11bfd22a58f53f83307f4df',
                name: 'Đầm Suông Tay Lỡ Thời Trang CAO CẤP ANN365',
                price: null,
                final_price: 540000,
                variant: {
                  kich_thuoc_1: { attribute_value: 'S', attribute_id: 815 },
                  mau_sac: { attribute_value: 'Khác', attribute_id: 1476 },
                },
                variant_ecommerce: 'kich_thuoc_1:S|mau_sac:Khác',
                quantity: 1,
                brand: '',
                category: '',
              },
            ],
          },
        },
      };
      expect(handleRemoveFromCart(event_received, data_delete_cart)).toEqual(
        expect_value
      );
    });
  });
});
