var Profiler = function(options = {}) {
  this.options = {
    debug: false,
    ...options,
  };
  this.init();
};

Profiler.prototype.init = function() {
  this.data = {};
};

Profiler.prototype.start = function(key) {
  if (!this.data.hasOwnProperty(key)) {
    this.data[key] = {};
  }
  this.data[key].start = new Date();
};

Profiler.prototype.end = function(key) {
  this.data[key].end = new Date();
};

Profiler.prototype.getProfilerData = function() {
  var data = {};
  for (var key in this.data) {
    var p = this.data[key];
    data[
      key
    ] = `start: ${p.start.toISOString()}, end: ${p.end.toISOString()}, diff: ${p.end -
      p.start}`;
  }
  return data;
};

module.exports = Profiler;
