import { fetchJSON } from 'sendo-web-sdk/helpers/url';

/**
 * Get basic infomation about product,
 */
export const loadBasicHomepage = () => {
  var url = 'wap_v2/home/basic';
  return fetchJSON(url);
};
