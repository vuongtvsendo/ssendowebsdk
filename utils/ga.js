import qs from 'query-string';
import { getBaseDomain } from 'sendo-web-sdk/helpers/url';
import { importScriptOnce } from 'sendo-web-sdk/helpers/base';
import { GA_ID, GA_ID_IFRAME } from 'sendo-web-sdk/helpers/const';
import { gaTrackPageView } from '../tracking/trackingHelpers';

export const loadAndInit = () => {
  var search = qs.parse(window.location.search);
  var w = window;
  w._gaq = w._gaq || [];
  /**
   * Loadtest
   */
  if (search.iFrame === '1') {
    w._gaq.push(['_setAccount', GA_ID_IFRAME]);
  } else {
    w._gaq.push(['_setAccount', GA_ID]);
  }
  const baseDomain = getBaseDomain();
  if (baseDomain !== 'localhost') {
    w._gaq.push(['_setDomainName', baseDomain]);
  }
  gaTrackPageView();
  const url = '//stats.g.doubleclick.net/dc.js';
  return importScriptOnce(url, 'google-ga');
};
