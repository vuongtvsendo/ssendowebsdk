import { importScriptOnce } from 'sendo-web-sdk/helpers/base';
import qs from 'query-string';

export const loadAndInit = () => {
  /**
  |-----------------------------------------------------------
  | @deprecated
  | 1. Co gang tao ra bien global danh cho GTM
  |----------------------------------------------------------- 
  */
  window.VERSION_JS_CSS = 0;
  const scripts = document.getElementsByTagName('script');
  for (var i = 0; i < scripts.length; i++) {
    const s = scripts[i];
    const matcher = s.src.match(/main\.([\d\w]+)\.js$/);
    if (matcher) {
      window.VERSION_JS_CSS = matcher[0];
      break;
    }
  }
  const l = 'dataLayer';
  const id = process.env.APP_GOOGLE_TAG_MANAGER_ID || 'GTM-WFTDK6';
  const src =
    '//www.googletagmanager.com/gtm.js?' +
    qs.stringify({
      id,
      l,
    });
  return importScriptOnce(src, 'google-gtm');
};
