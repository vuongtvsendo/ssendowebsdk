import { fetchJSON } from 'sendo-web-sdk/helpers/url';
import qs from 'query-string';
import _merge from 'lodash/merge';
import { LISTING_PRODUCT_PER_PAGE } from 'sendo-web-sdk/helpers/const';

export const getSearchSuggestions = params => {
  let url = 'wap_v2/search/suggest?' + qs.stringify(params);
  return fetchJSON(url);
};

export const getSearchTrending = () => {
  let url = 'wap_v2/keyword/top';
  return fetchJSON(url, {});
};

export const getProducts = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'rank',
      // num_keyword_ads: NUMBER_KEYWORD_ADD,
      // keyword_ads: 1,
    },
    params || {}
  );
  for (let p in search) {
    if (search[p] === '' || search[p] === null || search[p] === undefined) {
      delete search[p];
    }
  }
  const url = 'wap_v2/search/product?' + qs.stringify(search);
  return fetchJSON(url, {});
};

export const getSearchInfo = params => {
  let url = 'wap_v2/search/search-info?' + qs.stringify(params);
  return fetchJSON(url);
};
