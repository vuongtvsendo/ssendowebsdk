import _merge from 'lodash/merge';
import qs from 'query-string';
import { fetchJSON, fetchWithToken } from 'sendo-web-sdk/helpers/url';
import {
  LISTING_PRODUCT_PER_PAGE,
  SORT_TYPE_VASUP_DESC,
} from 'sendo-web-sdk/helpers/const';

export const getShopDetail = shop_alias => {
  const url = `mob/shop/${shop_alias}/detail`;
  return fetchJSON(url, {});
};

export const getShopProducts = (shop_alias, params) => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: SORT_TYPE_VASUP_DESC,
    },
    params || {}
  );
  const url = `mob/${shop_alias}/product?` + qs.stringify(search);
  return fetchJSON(url, {});
};

export const getShopCategories = shop_alias => {
  const url = `mob/${shop_alias}/category`;
  return fetchJSON(url, {});
};

export const getShopComments = (shopId, params) => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: SORT_TYPE_VASUP_DESC,
    },
    params || {}
  );
  const url = `wap_v2/shop/comment/${shopId}?` + qs.stringify(search);
  return fetchJSON(url, {});
};

export const getShopRatings = (shop_alias, params) => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: SORT_TYPE_VASUP_DESC,
    },
    params || {}
  );
  const url = `mob/shop/${shop_alias}/rating?` + qs.stringify(search);
  return fetchJSON(url, {});
};

export const postShopComment = params => {
  const url = 'wap_v2/comment/addnew';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

export const deleteShopComment = params => {
  const url = 'wap_v2/comment/deletenew';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

export const checkLikeShop = shopId => {
  const url = `mob/user/shop/${shopId}/checkLike`;
  return fetchWithToken(url, { method: 'POST' });
};

export const likeShop = shopId => {
  const url = `mob/user/shop/${shopId}/like`;
  return fetchWithToken(url, { method: 'POST' });
};
export const unlikeShop = shopId => {
  const url = `mob/user/shop/${shopId}/unlike`;
  return fetchWithToken(url, { method: 'POST' });
};
