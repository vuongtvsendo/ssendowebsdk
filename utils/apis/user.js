import _merge from 'lodash/merge';
import _isEmpty from 'lodash/isEmpty';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { SENDO_PLATFORM } from 'sendo-web-sdk/helpers/const';
import qs from 'query-string';
import {
  fetchJSON,
  fetchWithToken,
  buildToFormData,
} from 'sendo-web-sdk/helpers/url';

export const login = params => {
  //1. Neu system support /a/general thi tro ve wap cu thi minh lam the nay
  // let url = 'general/login/login/';
  //2. Neu ma System khong support thi lam ben duoi
  let url = normalizeUrl('general/login/login/', true);
  /**
   |--------------------------------------------------
   | API nen support param o dang post JSON.
   |--------------------------------------------------
   */
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const fbLogin = (params, options = {}) => {
  let form = buildToFormData(params);
  var url = 'wap_v2/user/login/login-facebook';
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const googleLogin = (params, options = {}) => {
  let form = buildToFormData(params);
  var url = 'wap_v2/user/login/login-google';
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const logout = () => {
  // const url = 'wap_v2/user/logout';
  const url = normalizeUrl('dang-xuat', true, false);
  return fetchJSON(url, {
    method: 'GET',
    redirect: 'manual',
  });
};
export const getUserInfo = () => {
  let search = qs.parse(window.location.search);
  search.sendo_platform = SENDO_PLATFORM;
  let url = normalizeUrl('general/login/getSession/', true);
  if (!_isEmpty(search)) {
    url += '?' + qs.stringify(search);
  }
  const options = {
    headers: {
      'X-Alt-Referer': window.document.referrer || '',
    },
  };
  return fetchWithToken(url, options);
};
export const register = params => {
  let url = 'mob/user/sign-up';
  /**
   |--------------------------------------------------
   | API nen support param o dang post JSON.
   |--------------------------------------------------
   */
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const checkValidOtp = params => {
  let url = 'mob/user/register/valid-otp';
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const checkValidAndPostPhoneEmail = params => {
  let url = 'mob/user/register/valid-unique';
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const resendOtp = params => {
  let url = 'mob/user/register/resend-otp';
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};
export const recoverPassword = params => {
  let url = 'quen-mat-khau';
  /**
   |--------------------------------------------------
   | API nen support param o dang post JSON.
   |--------------------------------------------------
   */
  let form = buildToFormData(params);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};

export const getUserInfoFull = () => {
  let url = 'wap_v2/user/info/full';
  return fetchWithToken(url);
};

export const getUserOrderStatus = token => {
  let url = 'wap_v2/home/order';
  return fetchWithToken(url);
};

export const getUserRecommendCategories = tracking_id => {
  let url = 'wap_v2/home/recommend-category?tracking_id=' + tracking_id;
  return fetchWithToken(url);
};

export const getUserRecommendProducts = (tracking_id, options) => {
  let search = _merge(
    {
      p: 1,
      s: 50,
    },
    options || {}
  );
  var url = `wap_v2/home/recommend-product?tracking_id=${tracking_id}&${qs.stringify(
    search
  )}`;
  return fetchJSON(url);
};

export const sendSubscription = subscription => {
  let url = 'wap_v2/user/gcm-browser-customer';
  return fetchJSON(url, {
    method: 'POST',
    body: buildToFormData({ regid_browser: subscription }),
  });
};

export const fbLoginOld = (params = {}, options = {}) => {
  params = {
    ...params,
    _t: Date.now(),
    auth_url: true,
  };
  let form = buildToFormData(params);
  let url = normalizeUrl('general/login/facebook-pwa/', true);
  return fetchJSON(url, {
    method: 'POST',
    body: form,
  });
};

export const ggLoginOld = (params = {}, options = {}) => {
  let url = normalizeUrl(
    'general/login/google/?' + qs.stringify(params, { encode: false }),
    true
  );
  return fetchJSON(url, {
    method: 'GET',
  });
};
