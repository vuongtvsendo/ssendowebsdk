import _merge from 'lodash/merge';
import qs from 'query-string';
import { fetchJSON } from 'sendo-web-sdk/helpers/url';
import { LISTING_PRODUCT_PER_PAGE } from 'sendo-web-sdk/helpers/const';

export const getBestSellingBasicInfo = params => {
  const url = `wap_v2/hotsell/basic/` + params.sort_path;
  return fetchJSON(url, {});
};

export const getBestSellingProducts = params => {
  if (params.category_id === 0) delete params['category_id'];
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'norder_30_desc',
    },
    params || {}
  );
  const url = `wap_v2/hotsell/product?` + qs.stringify(search);
  return fetchJSON(url, {});
};
