import _merge from 'lodash/merge';
import qs from 'query-string';
import { fetchJSON } from 'sendo-web-sdk/helpers/url';
import { LISTING_PRODUCT_PER_PAGE } from 'sendo-web-sdk/helpers/const';
import _get from 'lodash/get';

export const getEventDetail = params => {
  const event_name = _get(params, 'event_name.cate1', '');
  const url = `wap_v2/event/get-event-by-name/` + event_name;
  return fetchJSON(url, {});
};

export const getEventDetailFullBasic = params => {
  const url = `wap_v2/event/basic-detail/?` + qs.stringify(params);
  return fetchJSON(url, {});
};

export const getEvent = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
    },
    params || {}
  );
  const url = `wap_v2/su-kien/?` + qs.stringify(search);
  return fetchJSON(url, {});
};

export const getEventDetailFullProducts = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'vasup_desc',
    },
    params || {}
  );
  const url = `wap_v2/event/cabinet-products/?` + qs.stringify(search);
  return fetchJSON(url, {});
};
