import _merge from 'lodash/merge';
import qs from 'query-string';
import { fetchJSON } from 'sendo-web-sdk/helpers/url';
import { LISTING_PRODUCT_PER_PAGE } from 'sendo-web-sdk/helpers/const';

export const getTrendingProducts = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'vasup_desc',
    },
    params || {}
  );
  const url = `wap_v2/trend/` + params.url_key + '?' + qs.stringify(search);
  return fetchJSON(url, {});
};

export const getTrendingHome = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'vasup_desc',
    },
    params || {}
  );
  const url = `wap_v2/trend/home?` + qs.stringify(search);
  return fetchJSON(url, {});
};
