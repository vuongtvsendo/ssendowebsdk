import { fetchJSON } from 'sendo-web-sdk/helpers/url';

export const getCmsContentStaticPage = contentKey => {
  const url = `wap_v2/page/p/?key=` + contentKey;
  return fetchJSON(url, {});
};
