import { fetchWithToken } from 'sendo-web-sdk/helpers/url';

/**
 * Get data cart,
 */

export const loadTotalCart = () => {
  let url = 'wap_v2/cart/total';
  return fetchWithToken(url);
};

export const loadDataCart = () => {
  let url = 'wap_v2/cart/list';
  return fetchWithToken(url);
};

export const addToCart = params => {
  let url = 'wap_v2/cart/add';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

export const updateCart = params => {
  let url = 'wap_v2/cart/update';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

export const deleteCart = params => {
  let url = 'wap_v2/cart/delete';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};
