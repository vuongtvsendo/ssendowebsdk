export const asyncConnect = fetch => match => store => {
  if (__CLIENT__ && window.firstLoad) {
    delete window.firstLoad;
    return Promise.resolve({message: 'Could not fetch data here, because this data has already load from server side.'});
  }
  return fetch(match)(store);
};

export default {
  asyncConnect,
};
