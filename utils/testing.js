import React from 'react';
import { Provider } from 'react-redux';
import { MemoryRouter as Router } from 'react-router-dom';
import { mount as _mount, shallow as _shallow } from 'enzyme';
import sinon from 'sinon';
import mock from './testing/mockActions';
import * as helpers from './testing/helpers';

/**
 |---------------------------------------------------------
 | 1. Can suy nghi ve viec truyen store
 |---------------------------------------------------------
 */
import store from '../store';

export const ctx = {
  insertCss: () => {},
};

export const mockActions = mock;
export const emptyPromise = helpers.emptyPromise;

export const waitForAllPromises = (timeout = 0, res = {}) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(res);
    }, timeout);
  });
};

export const mount = (Element, options = {}) => {
  options = {
    context: {
      ...ctx,
    },
    ...options,
  };
  const wrapper = _mount(
    <Provider store={store}>
      <Router>{Element}</Router>
    </Provider>,
    options
  );

  return wrapper;
};

export const mountWithPromise = (Element, options = {}, timeout = 0) => {
  return waitForAllPromises(timeout, mount(Element, options));
};

export const shallow = (Element, options = {}) => {
  options = {
    context: {
      ...ctx,
    },
    ...options,
  };
  /**
   |-----------------------------------------------------------
   | 1. Cho nay la shallow vs voi mount
   | 2. mount => html
   | 3. shallow => don thuan la code template react
   | => Khong the push Provider -> Router -> Element
   | 4. Nen khong the nao thuc hien dc danh cho nhung test case
   | ma no co nhu cau find cai element detail
   | Do vay khong co cung cap Provider -> Rounter -> Element.
   |-----------------------------------------------------------
   */
  return _shallow(Element, options);
};

export const stubFetch = (res, status = 200) => {
  const stub_res = {
    json: function() {
      return {
        then: () => {
          return res;
        },
      };
    },
    status: status,
  };
  sinon.stub(global, 'fetch').resolves(stub_res);
};
