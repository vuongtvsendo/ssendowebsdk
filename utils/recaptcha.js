import { importScriptOnce } from 'sendo-web-sdk/helpers/base';

export const loadAndInit = () => {
  const url = '//www.google.com/recaptcha/api.js?hl=vi';
  return importScriptOnce(url, 'recatcha-jssdk', { defer: true });
};
