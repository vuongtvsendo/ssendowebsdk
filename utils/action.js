import identity from 'lodash/identity';
import _get from 'lodash/get';
import { isInArray } from 'sendo-web-sdk/helpers/common';
import { STATUS_REDIRECT } from 'sendo-web-sdk/helpers/const';
export const makePromiseAction = (
  type,
  asyncAction,
  dataSelector = identity,
  errSelector = identity
) => dispatch => {
  dispatch({
    type,
  });

  return asyncAction
    .then(data => {
      return dispatch({
        type: `${type}_SUCCESS`,
        payload: dataSelector(data),
      });
    })
    .catch(err => {
      /**
       |------------------------------------------
       | 1. Ngu canh de vo thang nay phan lon la do 
       | dispatch cua hamn `then` phia tren co sai syntax
       | hoac co tinh throw new Error => Practice khong nen dung.
       |------------------------------------------
       */
      var redirecting = isInArray(STATUS_REDIRECT, _get(err, 'status.code'));
      return dispatch({
        type: `${type}_FAILED`,
        payload: errSelector(err),
        redirecting: redirecting,
      });
    });
};
