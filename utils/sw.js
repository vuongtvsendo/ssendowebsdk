/*global self, clients*/
/* eslint no-restricted-globals: ["off", "self"] */

// Do cai nay khong dc build cung voi Webpack nen phai hardcode /a/ o day
var ENDPOINT_URL = '/a/wap_v2/user/gcm-get-message';
self.addEventListener('push', function(event) {
  if (!(self.Notification && self.Notification.permission === 'granted')) {
    console.error('Failed to display notification - not supported');
    return;
  }

  var callbackSubscription = function(subscription) {
    var url =
      ENDPOINT_URL +
      '?endpoint=' +
      subscription.endpoint.split('/').slice(-1)[0];
    fetch(url, {
      method: 'post',
      body: JSON.stringify(subscription),
    })
      .then(function(response) {
        return response.json();
      })
      .then(function(res) {
        if (!res || res.error) {
          return;
        }
        if (!res.data) {
          return;
        }
        var title = res.data.title || 'Sendo.vn';
        var data = {
          data: res.data.url,
          body: res.data.body,
          icon: res.data.icon,
          requireInteraction: true,
        };
        if (res.data.image) {
          data.image = res.data.image;
        }
        self.registration.showNotification(title, data);
      })
      .catch(function(err) {
        console.log(err);
      });
  };
  var getSubscription = self.registration.pushManager
    .getSubscription()
    .then(callbackSubscription);
  event.waitUntil(getSubscription);
});

self.addEventListener('notificationclick', function(event) {
  console.log('On notification click: ', event.notification);
  var url = '/';
  if (event.notification.data) {
    url = event.notification.data;
  }

  event.waitUntil(
    clients
      .matchAll({
        type: 'window',
      })
      .then(function(windowClients) {
        for (var i = 0; i < windowClients.length; i++) {
          var client = windowClients[i];
          if (client.url === url && 'focus' in client) {
            return client.focus();
          }
        }
        if (clients.openWindow) {
          return clients.openWindow(url);
        }
      })
  );
});
