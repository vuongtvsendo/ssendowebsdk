import _merge from 'lodash/merge';
import _get from 'lodash/get';
import qs from 'query-string';
import _values from 'lodash/values';
import _compact from 'lodash/compact';
import {
  fetchJSON,
  fetchWithToken,
  buildAPIUrl,
} from 'sendo-web-sdk/helpers/url';
import {
  LISTING_PRODUCT_PER_PAGE,
  CITY_CODE_HCM,
} from 'sendo-web-sdk/helpers/const';

export const getProductInfoViaIds = params => {
  let search = params.join();
  var url = 'wap_v2/product/list?product_id=' + search;
  return fetchJSON(url);
};

/**
 * Get all categories lv1 + lv2,
 */
export const getAllCategories = () => {
  const url = 'wap_v2/category/sitemap';
  return fetchJSON(url, {});
};

export const getCatLevel3rd = params => {
  let url = '';
  if (params.is_shop) {
    let catePath = {
      cate1: _get(params, 'params.cate1'),
      cate2: _get(params, 'params.cate2'),
      cate3: _get(params, 'params.cate3'),
    };
    catePath = _compact(_values(catePath));
    catePath = catePath.join('/');
    url = `mob/shop/${_get(params, 'params.shopAlias')}/category/${catePath}`;
  } else {
    url = buildAPIUrl(`m/wap_v2${params.category_path}`);
  }
  var option = {};
  if (__SERVER__) {
    option = {
      headers: {
        cookie: _get(window.document, 'cookie', ''),
      },
    };
  }
  return fetchJSON(url, option);
};

export const getProducts = params => {
  let search = _merge(
    {
      p: 1,
      s: LISTING_PRODUCT_PER_PAGE,
      sortType: 'vasup_desc',
    },
    params || {}
  );
  /**
   * Nen xu ly o tang giao dien hay API
   * Nen xu ly o day, boi vi trong truong hop code version truoc bi loi
   * Thi du co update version moi ma nguoi dung van su dung version cu
   */
  for (let p in search) {
    if (
      search[p] === '' ||
      search[p] === null ||
      search[p] === undefined ||
      search[p] === -1
    ) {
      delete search[p];
    }
  }
  const url = buildAPIUrl('m/wap_v2/category/product?' + qs.stringify(search));
  return fetchJSON(url, {});
};

/**
 * Check product isset wishlist,
 */
export const isProductFavorite = product_id => {
  const url = 'wap_v2/product/checkLike/' + product_id;
  return fetchWithToken(url);
};

/**
 * Get basic infomation about product,
 */
export const getBasicProductDetail = id => {
  var url = 'wap_v2/basic' + id;
  return fetchJSON(url);
};

/**
 * Get full infomation about product
 */
export const getProductDetail = id => {
  var url = 'wap_v2/full' + id;
  return fetchJSON(url);
};

/**
 * Get list Comment of Product
 */
export const getProductComment = (product, options) => {
  let search = _merge(
    {
      p: 1,
      s: 10,
    },
    options || {}
  );
  var url = `wap_v2/san-pham/comment/${_get(product, 'id')}?${qs.stringify(
    search
  )}`;
  return fetchJSON(url);
};

/**
 * Post comment
 */
export const postProductComment = params => {
  const url = `wap_v2/comment/add`;
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

/**
 * Delete comment
 */
export const deleteProductComment = params => {
  const url = 'wap_v2/comment/delete';
  return fetchWithToken(url, {
    method: 'POST',
    body: JSON.stringify(params),
  });
};

/**
 * Get list rating of Product
 */
export const getProductRating = (product, options) => {
  let search = _merge(
    {
      p: 1,
      s: 10,
      is_rating_image: 0,
    },
    options || {}
  );
  var url = `wap_v2/san-pham/rating/${_get(product, 'id')}?${qs.stringify(
    search
  )}`;
  return fetchJSON(url);
};

/**
 * Get list products related with ProductID
 */

export const getProductRelated = product => {
  const url = 'wap_v2/san-pham/related/' + _get(product, 'id');
  return fetchJSON(url);
};

/**
 * Get list products related with ProductID
 */

export const getProductRecommend = product => {
  const url = 'wap_v2/san-pham/recommend/' + _get(product, 'id');
  return fetchJSON(url);
};

/**
 * Get shipping info
 */
export const getShippingInfo = (product, options) => {
  let search = _merge(
    {
      product_id: _get(product, 'id'),
      merchant_id: _get(product, 'admin_id'),
      from: _get(product, 'shop_info.warehourse_region_id'),
      to: CITY_CODE_HCM,
      quantity: 1,
      weight: _get(product, 'weight', 0),
      price: _get(product, 'final_price'),
      category_id: _get(product, 'category_id'),
      type: 'mob_detail',
      product_width: _get(product, 'witdh_product', 0),
      product_length: _get(product, 'length_product', 0),
      product_height: _get(product, 'height_product', 0),
    },
    options || {}
  );
  var url = 'wap_v2/shipping-fee-new/?' + qs.stringify(search);
  return fetchJSON(url);
};

/**
 * Get all province/city
 */

export const getAllCity = () => {
  const url = 'wap_v2/city/all/';
  return fetchJSON(url);
};

/**
 * Add product to wishlist
 */
export const addToWishlist = product_id => {
  const url = 'wap_v2/product/like/' + product_id;
  return fetchWithToken(url);
};
/**
 * Remove product to wishlist
 */
export const removeToWishlist = product_id => {
  const url = 'wap_v2/product/unlike/' + product_id;
  return fetchWithToken(url);
};

/**
 * Get City by lat/long
 */

export const getCityByLatlng = latlng => {
  const url = 'wap_v2/user/info/location/' + latlng;
  return fetchJSON(url);
};

/**
 * @deprecated
 */
// export const getFilterByCategory = params => {
//   const search = {
//     ...params,
//     cate_level: 2,
//   };
//   var url = 'v1/catalog/product/filter?' + qs.stringify(search);
//   return fetchJSON(url);
// };

export const getFilterByCategory = params => {
  const search = {
    sortType: 'vasup_desc',
    ...params,
  };
  var url = 'wap_v2/category/filter?' + qs.stringify(search);
  return fetchJSON(url);
};

export const getFilterSearch = params => {
  const search = {
    sortType: 'vasup_desc',
    ...params,
  };
  var url = 'wap_v2/search/filter?' + qs.stringify(search);
  return fetchJSON(url);
};

export const getFilterPromotion = params => {
  const search = {
    sortType: 'vasup_desc',
    ...params,
  };
  var url = 'wap_v2/promotion/filter?' + qs.stringify(search);
  return fetchJSON(url);
};

export const getFilterBestSelling = params => {
  const search = {
    sortType: 'norder_30_desc',
    ...params,
  };
  var url = 'wap_v2/hotsell/filter?' + qs.stringify(search);
  return fetchJSON(url);
};

export const getSponsoredProducts = params => {
  let search = _merge(
    {
      p: 1,
      s: 20,
      sortType: 'vasup_desc',
    },
    params || {}
  );
  // var url = 'wap_v2/category/product?' + qs.stringify(search);
  const url = buildAPIUrl('m/wap_v2/category/product?' + qs.stringify(search));
  return fetchJSON(url);
};
