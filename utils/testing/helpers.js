export const emptyPromise = () => {
  return new Promise(resolve => {
    resolve();
  });
};

export const buildMock = actions => {
  var mockActions = {};
  for (var fn in actions) {
    mockActions[fn] = emptyPromise;
  }
  return mockActions;
};
