import { buildMock } from './helpers';

import * as productActions from 'sendo-web-sdk/actions/product';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as cartActions from 'sendo-web-sdk/actions/cart';
import * as categoryActions from 'sendo-web-sdk/actions/category';

const mock = {
  productActions: buildMock(productActions),
  userActions: buildMock(userActions),
  toastActions: buildMock(toastActions),
  cartActions: buildMock(cartActions),
  categoryActions: buildMock(categoryActions),
};

export default mock;
