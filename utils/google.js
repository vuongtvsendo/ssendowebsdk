import { importScriptOnce } from 'sendo-web-sdk/helpers/base';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { openPopupWindow } from 'sendo-web-sdk/helpers/common';
import { GOOGLE_APP_ID } from 'sendo-web-sdk/helpers/const';
import qs from 'query-string';

export const loadAndInit = () => {
  window.googleAsyncInit = () => {
    const gapi = window.gapi;
    gapi.load('auth2', () => {
      /*
      window.gAuthInstance = gapi.auth2.init({
        client_id: GOOGLE_APP_ID,
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      */
    });
  };
  return importScriptOnce(
    '//apis.google.com/js/platform.js?onload=googleAsyncInit',
    'google-plus-jssdk'
  );
};

export const googleLogin = (cb, opts = {}) => {
  let redirect_uri = normalizeUrl('general/login/google/', true);
  var scope = [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email',
  ];
  var params = {
    response_type: 'code',
    redirect_uri: redirect_uri,
    client_id: GOOGLE_APP_ID,
    scope: scope.join(' '),
    access_type: 'offline',
    approval_prompt: 'auto',
  };
  var url = 'https://accounts.google.com/o/oauth2/auth?' + qs.stringify(params);
  var popup = openPopupWindow(url, 'loginwindow');
  var timer = setInterval(function() {
    if (popup.closed) {
      clearInterval(timer);
      cb({});
    }
  }, 100);
};
