import { importScriptOnce } from 'sendo-web-sdk/helpers/base';
import { SENTRY_CLIENT_KEY } from 'sendo-web-sdk/helpers/const';

export const loadAndInit = () => {
  const url = '//cdn.ravenjs.com/3.21.0/raven.min.js';
  return importScriptOnce(url, 'ravenjs', {
    crossorigin: 'anonymous',
  }).then(() => {
    window.Raven.config(SENTRY_CLIENT_KEY).install();
    return Promise.resolve({});
  });
};
