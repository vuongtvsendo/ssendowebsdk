import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { importScriptOnce } from 'sendo-web-sdk/helpers/base';
import { FB_APP_ID, FB_APP_VERSION, FB_APP_SCOPES } from 'sendo-web-sdk/helpers/const';

export const loadAndInit = () => {
  window.fbAsyncInit = () => {
    window.FB.init({
      appId: FB_APP_ID,
      xfbml: false,
      version: FB_APP_VERSION,
      cookie: true,
    });
  };
  const url = '//connect.facebook.net/en_US/all.js';
  return importScriptOnce(url, 'facebook-jssdk');
};

export const fbLogin = (cb, opts = {}) => {
  if (!opts.scope) {
    opts.scope = FB_APP_SCOPES;
  }
  if (!window.FB) {
    return;
  }
  window.FB.login(cb, opts);
};

export const fbShare = (url, options = {}) => {
  if (!window.FB) {
    return false;
  }
  window.FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: normalizeUrl(url, true),
  });
};
