import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Classic } from './index';

describe('<Classic /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      title: 'Modal title',
    };
    const wrapper = shallow(<Classic {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(Classic);
    expect(wrapper.find('h4').text()).toEqual(props.title);
  });
});
