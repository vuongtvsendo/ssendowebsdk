import React, { Component } from 'react';
import { Modal, ModalBody } from 'reactstrap';

import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './classic.css';

export class Classic extends Component {
  state = {
    isSubmit: false,
  };
  close(e) {
    this.props.toggle();
  }
  submit(e) {
    e.preventDefault();
    this.setState({
      isSubmit: true,
    });
    return false;
  }
  render() {
    return (
      <Modal
        modalClassName={this.props.className}
        {...this.props}
        className={styles.modalClassic}>
        <ModalBody>
          <div className={styles.title}>
            <h4>{this.props.title}</h4>
            <span className={styles.quit} onClick={e => this.close(e)}>
              <CloseCircle className={`${styles.iconClose} icon`} />
            </span>
          </div>
          <div>
            {React.Children.map(this.props.children, node => {
              const props = {
                ...node.props,
                isSubmit: this.state.isSubmit,
                close: () => this.close(),
              };
              return React.cloneElement(node, props);
            })}
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

export default withStyles(styles)(Classic);
