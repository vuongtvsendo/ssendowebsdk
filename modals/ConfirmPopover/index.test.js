import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ConfirmPopover } from './index';

describe('<ConfirmPopover /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      title: 'Modal title',
    };
    const wrapper = shallow(<ConfirmPopover {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(ConfirmPopover);
    expect(wrapper.find('header').text()).toEqual(props.title);
  });
});
