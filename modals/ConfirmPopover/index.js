import React, { PureComponent } from 'react';
import styles from './confirmPopover.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { Modal } from 'reactstrap';

export class ConfirmPopover extends PureComponent {
  checkOpen() {
    return this.props.open;
  }
  handleCancel() {
    if (this.props.onCancel && this.props.onCancel instanceof Function) {
      this.props.onCancel();
    }
  }
  handleDelete() {
    if (this.props.onOk && this.props.onOk instanceof Function) {
      this.props.onOk();
    }
  }
  render() {
    this.checkOpen();
    return (
      <Modal
        className={styles.popover}
        modalClassName={styles.modalPopoverRoot}
        contentClassName={styles.modalPopoverContent}
        isOpen={this.checkOpen()}>
        <header className={styles.popoverHeader}>
          {this.props.title}
        </header>
        <div className={styles.content}>
          {this.props.children}
        </div>
        <div className={styles.control}>
          <button
            className={styles.btnCancel}
            onClick={() => this.handleCancel()}>
            Hủy
          </button>
          <button className={styles.btnOk} onClick={() => this.handleDelete()}>
            Xóa
          </button>
        </div>
      </Modal>
    );
  }
}
export default withStyles(styles)(ConfirmPopover);
