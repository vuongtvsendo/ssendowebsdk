import React, { Component } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Ripple from 'sendo-web-sdk/components/Ripple';
import IconClose from 'sendo-web-sdk/components/Icons/IconClose';

import styles from './attr.css';

export class Attributes extends Component {
  close(e) {
    this.props.toggle();
  }
  render() {
    return (
      <Modal
        className={styles.modalBottom}
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}>
        <ModalBody>
          <div className={styles.block + ` ` + styles.headingTitle}>
            <h4>{this.props.headingTitle}</h4>
            <Ripple
              tagName="button"
              className="btn-icon"
              onClick={e => this.close(e)}>
              <IconClose className="icon" />
            </Ripple>
          </div>
          {React.Children.map(this.props.children, node => {
            const props = {
              ...node.props,
            };
            return React.cloneElement(node, props);
          })}
        </ModalBody>
      </Modal>
    );
  }
}

export default withStyles(styles)(Attributes);
