import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Attributes } from './index';

describe('<Attributes /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<Attributes />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(Attributes);
    expect(wrapper.find('.btn-icon').exists()).toEqual(true);
  });
});
