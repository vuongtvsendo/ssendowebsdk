import { configureStore } from './redux';

let state = {};
if (__CLIENT__ && window.__INITIAL_STATE__) {
  state = window.__INITIAL_STATE__;
  delete window.__INITIAL_STATE__;
}
const store = configureStore(state);

export default store;
