import { buildExternalSendoUrl } from 'sendo-web-sdk/helpers/url';

import IconHome from 'sendo-web-sdk/components/Icons/IconHome';
import AllCate from 'sendo-web-sdk/components/Icons/AllCate';
import Trend from 'sendo-web-sdk/components/Icons/Trend';
import Promotion from 'sendo-web-sdk/components/Icons/Promotion';
import Hotsale from 'sendo-web-sdk/components/Icons/Hotsale';
import IconDiamond from 'sendo-web-sdk/components/Icons/IconDiamond';
import IconCalendar from 'sendo-web-sdk/components/Icons/IconCalendar';
import Avatar from 'sendo-web-sdk/components/Icons/Avatar';
import IconTicketCheck from 'sendo-web-sdk/components/Icons/IconTicketCheck';
import Shop from 'sendo-web-sdk/components/Icons/Shop';
import Info from 'sendo-web-sdk/components/Icons/Info';
import IconDownload from 'sendo-web-sdk/components/Icons/IconDownload';
import SenpayIcon from 'sendo-web-sdk/components/Icons/SenpayIcon';

export const shortCutLink = [
  {
    title: 'Trang chủ',
    icon: IconHome,
    link: '',
  },
  {
    title: 'Tất cả danh mục',
    icon: AllCate,
    link: '/sitemap/',
  },
  {
    title: 'Xu hướng',
    icon: Trend,
    link: '/xu-huong/',
  },
  {
    title: 'Khuyến mãi',
    icon: Promotion,
    link: '/khuyen-mai/',
  },
  {
    title: 'Bán chạy',
    icon: Hotsale,
    link: '/ban-chay/',
  },
  {
    title: 'Thương hiệu',
    icon: IconDiamond,
    link: buildExternalSendoUrl('/thuong-hieu/'),
  },
  {
    title: 'Sự kiện',
    icon: IconCalendar,
    link: '/event/',
  },
  {
    title: 'Tiện ích Sen Đỏ',
    icon: SenpayIcon,
    link: '/tien-ich/',
    label: 'Mới',
  },
];

export const defaultAllCategoryNav = {
  name: 'Tất cả',
  url_path: '',
};

export const extraLink = [
  {
    title: 'Trang cá nhân',
    icon: Avatar,
    link: buildExternalSendoUrl('/thong-tin-tai-khoan'),
    requireLogin: true,
  },
  {
    title: 'Quản lý đơn hàng',
    icon: IconTicketCheck,
    link: buildExternalSendoUrl('/sales/order/history'),
    requireLogin: true,
  },
  {
    title: 'Mở shop',
    icon: Shop,
    link: 'https://ban.sendo.vn/',
  },
  {
    title: 'Hướng dẫn',
    icon: Info,
    link: 'https://help.sendo.vn/hc/vi',
  },
  {
    title: 'Tải app',
    icon: IconDownload,
    link: buildExternalSendoUrl('/apps'),
  },
];
