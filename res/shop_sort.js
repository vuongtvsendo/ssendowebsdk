export const shopSortData = [
  {
    name: 'Đề cử',
    search: {
      sortType: 'vasup_desc',
    },
  },
  {
    name: 'Bán chạy',
    search: {
      sortType: 'norder_30_desc',
    },
  },
  {
    name: 'Đánh giá tốt',
    search: {
      sortType: 'rating_percent_desc',
    },
  },
];
