import {
  PASSWORD_MINLENGTH,
  PASSWORD_MAXLENGTH,
  FULLNAME_MINLENGTH,
  FULLNAME_MAXLENGTH,
} from 'sendo-web-sdk/helpers/const';

export const LANDSCAPE_HINT_LABEL = 'Vui lòng xoay dọc màn hình';
export const LANDSCAPE_HINT_DESC =
  'Hiện Sendo.vn chưa hỗ trợ hiển thị trên màn hình ngang. Bạn vui lòng xoay dọc màn hình để có trải nghiệm tốt nhất.';
export const ERROR_MESSAGE = 'Có lỗi xảy ra, vui lòng thử lại';
export const ERROR_GA_CHECKOUT_MESSAGE = 'User quên chọn thuộc tính sản phẩm';
export const FORGOTPASSWORD_MESSAGE =
  'Vui lòng cung cấp thông tin dưới đây để khôi phục mật khẩu của bạn';
export const LABEL_LOGIN = 'Đăng nhập';
export const TEXT_PANEL_LOGIN_BANNER = 'Để có trải nghiệm mua sắm tốt nhất';
export const LABEL_RECENTLY_VIEWED = 'Sản phẩm vừa xem';
export const LABEL_PRODUCT_CATE_RECOMMEND = 'Dành riêng cho bạn';
export const LABEL_RECOMMEND_CATEGORY = 'Đề cử cho bạn';
export const LABEL_PRODUCT_BENEFIT = 'Quyền lợi khách hàng';
export const LABEL_PRODUCT_COMMENT = 'Hỏi đáp';
export const LABEL_PRODUCT_DESCRIPTION = 'Chi tiết sản phẩm';
export const LABEL_RELATED_TAGS = 'Từ khoá liên quan';
export const TEXT_PRODUCT_EMPTY = 'Sản phẩm tìm kiếm hiện tại không có';
export const TEXT_KEYWORDS =
  'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe';

export const MESSAGE_INPROGRESS = 'Đang xử lý...';
export const MESSAGE_ADDTOCART_SUCCESS = 'Thêm vào giỏ hàng thành công';

export const NOTI_EMAIL_EMPTY = 'Vui lòng nhập địa chỉ email.';
export const NOTI_OTP_EMPTY = 'Vui lòng nhập mã xác nhận.';
export const NOTI_EMAIL_PHONE_EMPTY =
  'Vui lòng nhập địa chỉ email hoặc số điện thoại';
export const NOTI_EMAIL_WRONG_FORMAT = 'Địa chỉ email không hợp lệ.';
export const NOTI_EMAIL_PHONE_WRONG_FORMAT =
  'Địa chỉ email hoặc số điện thoại không hợp lệ.';
export const NOTI004 = 'Sai mật khẩu hoặc tài khoản đăng nhập.';
export const NOTI_PASSWORD_EMPTY = 'Vui lòng nhập mật khẩu đăng nhập.';
export const NOTI006 = 'Mật khẩu không đúng.';
export const NOTI_EMAIL_EMPTY_OR_WRONG_FORMAT =
  'Vui lòng nhập địa chỉ email hợp lệ. Ví dụ abc@domain.com.';
export const NOTI_PASSWORD_EMPTY_OR_WRONG_FORMAT =
  'Độ dài mật khẩu từ ' +
  PASSWORD_MINLENGTH +
  ' đến ' +
  PASSWORD_MAXLENGTH +
  ' ký tự, vui lòng nhập lại.';
export const NOTI_PASSWORD_NOT_MATCH =
  'Mật khẩu không khớp nhau, vui lòng nhập lại.';
export const NOTI_LOGIN_ERROR = 'Sai mật khẩu hoặc tài khoản đăng nhập.';
export const NOTI_LOGIN_SOCIAL_ERROR =
  'Đăng nhập không thành công, vui lòng thử lại sau.';
export const NOTI_LOGIN_SUCCESS = ' đã đăng nhập thành công';
export const NOTI_LOGIN_FB_ERROR = 'Đăng nhập Facebook không thành công.';
export const NOTI_FULLNAME_EMPTY = 'Vui lòng nhập đầy đủ Họ và Tên';
export const NOTI_FULLNAME_WRONG_MAXLENGTH =
  'Bạn chỉ được phép nhập tối đa ' + FULLNAME_MAXLENGTH + ' ký tự';
export const NOTI_FULLNAME_WRONG_MINLENGTH =
  'Bạn chỉ được phép nhập tối thiểu ' + FULLNAME_MINLENGTH + ' ký tự';
export const NOTI_REGISTER_SUCCESS = 'Bạn đã đăng ký thành công';
export const NOTI_REGISTER_ERROR = 'Đăng ký không thành công, vui lòng thử lại';

export const TEXT_SHOP_NORATING = 'Chưa có đánh giá';
export const TEXT_TITLE_EVENT_PAGE = 'Trang chủ sự kiện';
export const TEXT_GUIDE_REGISTER_EMAIL = 'Đăng ký bằng email';
export const TEXT_GUIDE_REGISTER_PHONE = 'Đăng ký bằng số điện thoại';
export const NOTI_CAPTCHA = 'Vui lòng chọn "Tôi không phải là người máy"';
export const NOTI_RESEND_OTP_SUCCESS =
  'Yêu cầu gửi lại OTP thành công, vui lòng kiểm tra điện thoại của bạn.';

export const TITLE_MODAL_LOGIN_BUYNOW = 'Đăng nhập để đặt hàng';
