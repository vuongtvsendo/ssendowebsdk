import MobileIcon from 'sendo-web-sdk/components/Icons/Mobile';
import BagIcon from 'sendo-web-sdk/components/Icons/BagIcon';
import BookIcon from 'sendo-web-sdk/components/Icons/BookIcon';
import CameraIcon from 'sendo-web-sdk/components/Icons/CameraIcon';
import CarIcon from 'sendo-web-sdk/components/Icons/CarIcon';
import ElectricIcon from 'sendo-web-sdk/components/Icons/ElectricIcon';
import FashionStuffIcon from 'sendo-web-sdk/components/Icons/FashionStuffIcon';
import FruitIcon from 'sendo-web-sdk/components/Icons/FruitIcon';
import HomeStuffIcon from 'sendo-web-sdk/components/Icons/HomeStuffIcon';
import HouseDecoIcon from 'sendo-web-sdk/components/Icons/HouseDecoIcon';
import HouseIcon from 'sendo-web-sdk/components/Icons/HouseIcon';
import LaptopIcon from 'sendo-web-sdk/components/Icons/LaptopIcon';
import LifeIcon from 'sendo-web-sdk/components/Icons/LifeIcon';
import MeatIcon from 'sendo-web-sdk/components/Icons/MeatIcon';
import MedicIcon from 'sendo-web-sdk/components/Icons/MedicIcon';
import MenFashionIcon from 'sendo-web-sdk/components/Icons/MenFashionIcon';
import MomBabyIcon from 'sendo-web-sdk/components/Icons/MomBabyIcon';
import PerfumeIcon from 'sendo-web-sdk/components/Icons/PerfumeIcon';
import ServiceIcon from 'sendo-web-sdk/components/Icons/ServiceIcon';
import ShoeIcon from 'sendo-web-sdk/components/Icons/ShoeIcon';
import SoundDeviceIcon from 'sendo-web-sdk/components/Icons/SoundDeviceIcon';
import SportIcon from 'sendo-web-sdk/components/Icons/SportIcon';
import TechStuffIcon from 'sendo-web-sdk/components/Icons/TechStuffIcon';
import TiviIcon from 'sendo-web-sdk/components/Icons/TiviIcon';
import ToyIcon from 'sendo-web-sdk/components/Icons/ToyIcon';
import WatchIcon from 'sendo-web-sdk/components/Icons/WatchIcon';
import WomenFashionIcon from 'sendo-web-sdk/components/Icons/WomenFashionIcon';

export const IconCategories = [
  {
    url_key: 'cong-nghe',
    icon: MobileIcon,
  },
  {
    url_key: 'thoi-trang-nu',
    icon: WomenFashionIcon,
  },
  {
    url_key: 'thoi-trang-nam',
    icon: MenFashionIcon,
  },
  {
    url_key: 'giay-dep',
    icon: ShoeIcon,
  },
  {
    url_key: 'tui-xach',
    icon: BagIcon,
  },
  {
    url_key: 'me-va-be',
    icon: MomBabyIcon,
  },
  {
    url_key: 'may-tinh',
    icon: LaptopIcon,
  },
  {
    url_key: 'phu-kien-cong-nghe',
    icon: TechStuffIcon,
  },
  {
    url_key: 'tivi-thiet-bi-giai-tri',
    icon: TiviIcon,
  },
  {
    url_key: 'may-anh-may-quay-phim',
    icon: CameraIcon,
  },
  {
    url_key: 'dien-may',
    icon: ElectricIcon,
  },
  {
    url_key: 'do-dien-gia-dung',
    icon: HomeStuffIcon,
  },
  {
    url_key: 'thiet-bi-y-te',
    icon: MedicIcon,
  },
  {
    url_key: 'dong-ho-phu-kien',
    icon: WatchIcon,
  },
  {
    url_key: 'thiet-bi-am-thanh',
    icon: SoundDeviceIcon,
  },
  {
    url_key: 'phu-kien-thoi-trang',
    icon: FashionStuffIcon,
  },
  {
    url_key: 'my-pham',
    icon: PerfumeIcon,
  },
  {
    url_key: 'do-dung-trong-nha',
    icon: HouseIcon,
  },
  {
    url_key: 'sach-van-phong-pham',
    icon: BookIcon,
  },
  {
    url_key: 'do-choi',
    icon: ToyIcon,
  },
  {
    url_key: 'khong-gian-song',
    icon: LifeIcon,
  },
  {
    url_key: 'o-to-xe-may',
    icon: CarIcon,
  },
  {
    url_key: 'the-thao-giai-tri',
    icon: SportIcon,
  },
  {
    url_key: 'thuc-pham',
    icon: FruitIcon,
  },
  {
    url_key: 'thuc-pham-tuoi-song',
    icon: MeatIcon,
  },
  {
    url_key: 'tan-trang-nha-cua',
    icon: HouseDecoIcon,
  },
  {
    url_key: 'voucher-dich-vu',
    icon: ServiceIcon,
  },
];
