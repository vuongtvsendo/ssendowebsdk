export const homePageMeta = {
  title: 'SENDO.VN - Mua hàng online đảm bảo, giá rẻ từ các Shop uy tín',
  description:
    'Mua bán thời trang nam nữ, mẹ và bé, phụ kiện công nghệ, gia dụng...khuyến mãi hấp dẫn, vận chuyển toàn quốc. Sàn TMĐT chính thức của tập đoàn FPT.',
  image: 'https://static.scdn.vn/images/ecom/logo/logo-sendo_09-16.jpg',
  keywords:
    'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe',
};
export const trendingPageMeta = {
  title: 'Xu hướng: Thời trang, c&ocirc;ng nghệ, thiết bị gia dụng HOT 2017',
  description:
    'Mua sản phẩm hot lĩnh vực c&ocirc;ng nghệ ✅ Sản phẩm thời trang ✅ Thiết bị gia dụng. Tại sendo.vn  ✅ Mua b&aacute;n đảm bảo  ✅Khuyến m&atilde;i khủng  ✅ Giao h&agrave;ng tận nơi',
  image:
    'https://estatic.scdn.vn/images/ecom/events/event20161003_tapchisendo/486x255-ver17.jpg',
  keywords:
    'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe',
};
export const eventPageMeta = {
  title: 'Trang chủ sự kiện',
  description: 'Tổng hợp các trang sự kiện',
  image:
    'https://estatic.scdn.vn/images/ecom/events/event20150728_sukien/avatarFB_470x265.jpg',
  keywords:
    'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe',
};
export const eventDetailPageMeta = {
  keywords:
    'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe',
};
export const promotionPageMeta = {
  image: 'https://static.scdn.vn/images/ecom/logo/logo-sendo_09-16.jpg',
  keywords: 'khuyen mai sendo, khuyến mãi sendo',
};
export const bestsellingPageMeta = {
  image: 'https://static.scdn.vn/images/ecom/logo/logo-sendo_09-16.jpg',
  keywords: 'ban chay sendo, bán chạy sendo',
};
export const sitemapPageMeta = {
  description:
    'Mua bán thời trang nam nữ, mẹ và bé, phụ kiện công nghệ, gia dụng...khuyến mãi hấp dẫn, vận chuyển toàn quốc. Sàn TMĐT chính thức của tập đoàn FPT.',
  image: 'https://static.scdn.vn/images/ecom/logo/logo-sendo_09-16.jpg',
  keywords:
    'mua bán thời trang, shop thời trang, mua sắm trực tuyến, mua sắm online, phu kien cong nghe',
};
