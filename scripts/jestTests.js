// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
// require('../config/env');

const jest = require('jest');
const argv = process.argv.slice(2);

/**
 |--------------------------------------------------------------------------------
 | https://stackoverflow.com/questions/43458748/jest-unexpected-token-import
 |--------------------------------------------------------------------------------
 */
argv.push('--no-cache');

// Watch unless on CI or in coverage mode
if (argv.indexOf('--ci') !== -1 && argv.indexOf('--coverage') !== -1) {
  argv.push('--watch');
}

jest.run(argv);
