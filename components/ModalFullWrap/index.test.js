import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ModalFullWrap } from './index';

describe('<ModalFullWrap /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      title: 'title1',
    };
    const wrapper = shallow(<ModalFullWrap {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ModalFullWrap);
    expect(wrapper.find('h3').text()).toEqual(props.title);
  });
});
