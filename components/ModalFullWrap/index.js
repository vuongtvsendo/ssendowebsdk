import React, { Children } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './modalFullWrap.css';
import Modal from 'reactstrap/lib/Modal';
import ArrowLeft from '../Icons/ArrowLeft';
import ModalParent from 'sendo-web-sdk/components/Modal';

export class ModalFullWrap extends ModalParent {
  static contextTypes = {
    router: PropTypes.object,
  };
  onClickBtnBack() {
    if (_get(this, 'context.router.history.location.hash')) {
      this.context.router.history.replace(
        _get(this, 'props.router.history.location.pathname')
      );
    }
    if (_get(this, 'props.onClickBtnBack') instanceof Function) {
      this.props.onClickBtnBack();
    }
  }
  onEnterHandler() {
    if (_get(this, 'props.onEnter') instanceof Function) {
      this.props.onEnter();
    }
  }
  render() {
    const { title } = this.props;
    return (
      <Modal
        modalClassName="modal-full modal-rtl"
        isOpen={this.props.isOpen}
        onEnter={() => this.onEnterHandler()}>
        <div className="modal-content-inner">
          <header className="modal-header" ref={ref => (this.header = ref)}>
            <button className="btn-close" onClick={() => this.onClickBtnBack()}>
              <ArrowLeft className="icon" />
            </button>
            <h3 className={styles.modalHeaderTitle}>{title}</h3>
          </header>
          {Children.map(this.props.children, (element, index) => {
            if (index === -0) {
              return React.cloneElement(element, { ref: 'scroller' });
            } else {
              return React.cloneElement(element);
            }
          })}
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(ModalFullWrap);
