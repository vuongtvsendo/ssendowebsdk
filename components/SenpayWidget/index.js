import React, { PureComponent } from 'react';
import { buildExternalSendoUrl } from 'sendo-web-sdk/helpers/url';
import DataMobileIcon from 'sendo-web-sdk/components/Icons/DataMobileIcon';
import GameCartIcon from 'sendo-web-sdk/components/Icons/GameCartIcon';
import MobileCardIcon from 'sendo-web-sdk/components/Icons/MobileCardIcon';
import MobileIconMoney from 'sendo-web-sdk/components/Icons/MobileIconMoney';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './style.css';

const NAPTIEN = '/tien-ich/nap-tien/';
const THECAO = '/tien-ich/the-cao/';
const THEGAME = '/tien-ich/the-game/';
const THEDATA = '/tien-ich/the-du-lieu/';

export class SenpayWidget extends PureComponent {
  render() {
    return (
      <section className="senpay-widget">
        <a href={buildExternalSendoUrl(NAPTIEN)} className="row main-feature">
          <div className="col-6 flex-vertical-middle">
            <MobileIconMoney className="icon" />
            <h3 className="title">Nạp tiền điện thoại</h3>
          </div>
          <div className="col-6 flex-vertical-middle">
            <span className="input-fake">Nhập số điện thoại</span>
          </div>
        </a>
        <div className="other-features row">
          <a href={buildExternalSendoUrl(THECAO)} className="col item">
            <MobileCardIcon className="icon" />
            <p>Thẻ cào</p>
          </a>
          <a href={buildExternalSendoUrl(THEGAME)} className="col item">
            <GameCartIcon className="icon" />
            <p>Thẻ game</p>
          </a>
          <a href={buildExternalSendoUrl(THEDATA)} className="col item">
            <DataMobileIcon className="icon" />
            <p>Thẻ 3G/4G</p>
          </a>
        </div>
      </section>
    );
  }
}

export default (withStyles(styles)(SenpayWidget));
