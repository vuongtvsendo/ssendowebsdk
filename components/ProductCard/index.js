import React, { PureComponent } from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productCard.css';

import _get from 'lodash/get';

import IsBrandIcon from 'sendo-web-sdk/components/Icons/IsBrandIcon';
import IconFreeShip from 'sendo-web-sdk/components/Icons/IconFreeShip';
import ReputationIcon from 'sendo-web-sdk/components/Icons/ReputationIcon';
import IconCamera from 'sendo-web-sdk/components/Icons/IconCamera';
import StarRating from 'sendo-web-sdk/components/StarRating';
import Mobile from 'sendo-web-sdk/components/Icons/Mobile';
import HeartFull from 'sendo-web-sdk/components/Icons/HeartFull';
import OrderIcon from 'sendo-web-sdk/components/Icons/OrderIcon';

import * as helperFunction from 'sendo-web-sdk/helpers/common';
import defaultImg from 'sendo-web-sdk/images/default.png';

/**
 * @doto
 * Theo Phu neu khong co xu ly dat biet viet tach ra thanh cai
 * ProductCardWrap la khong can thiet
 */
import ProductCardWrap from '../ProductCardWrap';
export class Product extends PureComponent {
  render() {
    const { product, cateMetadata, dataIndex, isTrackInviewport } = this.props;
    return (
      <ProductCardWrap
        dataIndex={dataIndex}
        product={product}
        isTrackInviewport={isTrackInviewport}
        cateMetadata={cateMetadata}>
        <div className="product-card">
          <div className="thumbnail">
            <figure className="image-square">
              <img
                className={`lazyload`}
                src={defaultImg}
                data-src={product.image || product.img_url}
                alt={product.name}
              />
            </figure>
            <div className="group-discount">
              {product.promotion_percent > 0 &&
                <span className="discount-label">
                  -{product.promotion_percent} %
                </span>}
              {product.discount_app > 0 &&
                <span className="discount-label discount-label-app">
                  <Mobile className="icon-small" />
                  -{product.discount_app} %
                </span>}
            </div>
            {product.is_keyword_ads > 0 &&
              <span className="shop-ads">Được tài trợ</span>}
            {product.brand_id > 0 && <IsBrandIcon className="is-brand" />}
            {product.has_video &&
              <span className="has-video">
                <IconCamera className="icon" />
              </span>}
          </div>
          <div className="caption">
            <h3 className="heading-medium">
              <span className="truncate-medium-2">
                {product.name}
              </span>
            </h3>
            <div className="product-price">
              <strong className="current">
                {helperFunction.formatPrice(product.final_price)}
              </strong>
              {product.promotion_percent > 0 &&
                <span className="old">
                  {helperFunction.formatPrice(product.price)}
                </span>}
            </div>
            <div className="info">
              <div className="rating">
                {_get(product, 'rating_info.total_rated') > 0 &&
                  <StarRating
                    value={_get(product, 'rating_info.percent_star_rating')}
                  />}
                {!_get(product, 'rating_info.total_rated') > 0 &&
                  <em className="text-small">Chưa có đánh giá</em>}
              </div>
              <div className="statistic">
                {product.order_count > 0 &&
                  <span className="order">
                    <OrderIcon className="icon icon-small" />
                    <span className="text-small">
                      {helperFunction.friendlyNumber(product.order_count, 1)}
                    </span>
                  </span>}
                {product.like_count > 0 &&
                  <span className="like">
                    <HeartFull className="icon icon-small" />
                    <span className="text-small">
                      {helperFunction.friendlyNumber(product.like_count, 1)}
                    </span>
                  </span>}
              </div>
            </div>
            <div className="shop-info">
              <span className="name">
                {_get(product, 'shop_info.is_certified') > 0 &&
                  <ReputationIcon className="reputation" />}
                {!_get(product, 'shop_info.is_certified') > 0 &&
                  <span className="shop-label">shop</span>}
                <span className="text-small text-overflow">
                  {_get(product, 'shop_info.shop_name')}
                </span>
              </span>
              <span className="promotion">
                {_get(product, 'options.is_shipping_fee_support') > 0 &&
                  <IconFreeShip className="icon icon-free-ship" />}
              </span>
            </div>
          </div>
        </div>

      </ProductCardWrap>
    );
  }
}

export default withStyles(styles)(Product);
