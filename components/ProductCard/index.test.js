import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Product } from './index';

describe('<Product /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {},
    };
    const wrapper = shallow(<Product {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(Product);
  });
});
