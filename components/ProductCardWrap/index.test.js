import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductCardWrap } from './index';
import emitter, { PRODUCT_CLICK } from 'sendo-web-sdk/helpers/emitter';
import sinon from 'sinon';

describe('<ProductCardWrap /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        cat_path: '',
      },
    };
    const wrapper = shallow(<ProductCardWrap {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductCardWrap);
    expect(wrapper.find('.product-card-wrap').exists()).toEqual(true);
  });

  it('It should emit PRODUCT_CLICK', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        cat_path: '',
      },
      dataIndex: 1,
      productActions: {
        setProductDetail: () => {},
      },
      cateMetadata: {
        category_info: [],
      },
    };
    const wrapper = shallow(<ProductCardWrap {...props} />, {
      context,
    });
    var spy = sinon.spy(emitter, 'emit');
    // wrapper.find('.product-card-wrap > div').simulate('click');
    wrapper.instance().handleClick({});
    expect(
      spy.calledWithMatch(
        sinon.match(PRODUCT_CLICK),
        sinon.match({}),
        sinon.match({
          ...props.product,
          index: props.dataIndex + 1,
          category_info: [],
        })
      )
    ).toEqual(true);
  });
});
