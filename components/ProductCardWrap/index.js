import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from 'sendo-web-sdk/helpers/webview/Link';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productCardWrap.css';
import _get from 'lodash/get';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';

import { mergeQueryUrl } from 'sendo-web-sdk/helpers/url';
import VisibilitySensor from 'react-visibility-sensor';
import emitter, { PRODUCT_CLICK, PRODUCT_IN_VIEWPORT } from 'sendo-web-sdk/helpers/emitter';
import { encodeUrl, buildShopProductLink } from 'sendo-web-sdk/helpers/url';
import { buildSourceInfoString } from 'sendo-web-sdk/helpers/formatData';
export class ProductCardWrap extends PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = { active: props.isTrackInviewport || false };
  }
  handleClick(e) {
    const { product, dataIndex, cateMetadata } = this.props;
    const currenttUrl = encodeUrl();
    this.props.productActions.setProductDetail(
      { ...product, source_url: currenttUrl },
      cateMetadata
    );
    emitter.emit(PRODUCT_CLICK, e, {
      ...product,
      index: dataIndex + 1,
      category_info: _get(cateMetadata, 'category_info', null),
    });
  }
  onChange(isVisible) {
    if (isVisible) {
      this.setState({ active: false });
      /**
       * 1. Muc tieu la de Promise chay truoc
       * 2. => Render se dc ra truoc
       */
      setTimeout(() => {
        const { product, dataIndex, cateMetadata } = this.props;
        emitter.emit(
          PRODUCT_IN_VIEWPORT,
          {},
          {
            ...product,
            index: dataIndex + 1,
            category_info: _get(cateMetadata, 'category_info', null),
          }
        );
      });
    }
  }
  trackingParam() {
    const { product, dataIndex } = this.props;
    return {
      ..._get(product, 'productMetadata.source_info', {}),
      source_info: buildSourceInfoString({
        ...product,
        index: dataIndex + 1,
      }),
    };
  }
  buildProducUrl(url) {
    const shopAlias = _get(this.context, 'router.route.match.params.shopAlias');
    if (shopAlias) {
      url = buildShopProductLink(url, shopAlias);
    }
    return mergeQueryUrl(url, this.trackingParam());
  }
  render() {
    const { isHorizontalSensor } = this.props;
    let paramSensor = {
      partialVisibility: true,
    };
    if (!isHorizontalSensor)
      paramSensor = {
        minTopValue: 46,
        partialVisibility: 'left',
      };
    return (
      <Link
        to={this.buildProducUrl(this.props.product.cat_path)}
        className="product-card-wrap">
        <VisibilitySensor
          onChange={isVisible => this.onChange(isVisible)}
          active={this.state.active}
          {...paramSensor}>
          <div
            onClick={e => this.handleClick(e)}
            className={this.props.className}>
            {this.props.children}
          </div>
        </VisibilitySensor>
      </Link>
    );
  }
}

export default connect(null, dispatch => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
  };
})(withStyles(styles)(ProductCardWrap));
