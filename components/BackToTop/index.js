import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './backToTop.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import IconBackToTop from 'sendo-web-sdk/components/Icons/IconBackToTop';
import { smoothScroll } from 'sendo-web-sdk/helpers/common';

export class BackToTop extends Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
    this.state = { active: false };
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll() {
    if (window && window.scrollY > window.innerHeight * 1.2) {
      this.setState({ active: true });
    } else {
      this.setState({ active: false });
    }
  }
  scrollToTop() {
    smoothScroll.scrollTo('root');
  }
  render() {
    const backToTopClass = classnames({
      [styles.backToTop]: true,
      'btn-icon-small': true,
      [styles.active]: this.state.active,
    });
    return (
      <button className={backToTopClass} onClick={() => this.scrollToTop()}>
        <IconBackToTop className="icon" />
      </button>
    );
  }
}

export default withStyles(styles)(BackToTop);
