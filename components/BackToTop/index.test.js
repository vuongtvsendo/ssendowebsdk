import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { BackToTop } from './index';
import IconBackToTop from 'sendo-web-sdk/components/Icons/IconBackToTop';

describe('<BackToTop /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<BackToTop />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(BackToTop);
    expect(wrapper.find(IconBackToTop).exists()).toEqual(true);
  });
});
