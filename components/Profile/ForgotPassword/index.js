import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, SubmissionError, reduxForm } from 'redux-form';
import styles from './styles.css';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as userActions from 'sendo-web-sdk/actions/user';
import { REGEX_EMAIL } from 'sendo-web-sdk/helpers/const';
import { handleInputForIphone } from 'sendo-web-sdk/helpers/layout';
import * as css from 'sendo-web-sdk/helpers/css';
import { FORGOTPASSWORD_MESSAGE } from '../../../res/text';

const validate = values => {
  let errors = {};
  if (!values.email) {
    errors.email = 'Email không được để trống';
  } else if (!REGEX_EMAIL.test(values.email)) {
    errors.email = 'Email không hợp lệ';
  }
  return errors;
};

const warn = values => {
  const warnings = {};
  return warnings;
};

const renderField = ({
  input,
  type,
  placeholder,
  inputRef,
  meta: { touched, error, warning },
}) => {
  return (
    <div>
      <div className="form-group">
        <input
          {...input}
          className="form-control input-standard"
          placeholder={placeholder}
          type={type}
          ref={inputRef}
        />
        {touched &&
          error &&
          <p className="form-error">
            {error}
          </p>}
        {touched &&
          warning &&
          <p>
            {warning}
          </p>}
      </div>
    </div>
  );
};

export class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.email) this.email.focus();
    });
  }
  submit(values) {
    const params = {
      email: values.email,
    };
    css.fixCssFormAfter();
    return this.props.userActions.register(params).then(res => {
      if (res.err_code) {
        throw new SubmissionError({
          _error: res.message,
        });
      }
      this.props.done(null, res);
    });
  }
  render() {
    const { error, handleSubmit, pristine, submitting } = this.props;
    return (
      <form className={styles.form} onSubmit={handleSubmit(this.submit)}>
        <p>
          {FORGOTPASSWORD_MESSAGE}
        </p>
        {error &&
          <strong className="formError">
            {error}
          </strong>}
        <Field
          tabIndex="1"
          name="email"
          placeholder="Email"
          component={renderField}
          type="text"
          inputRef={input => (this.email = input)}
          onFocus={() => handleInputForIphone('focus')}
          onBlur={() => handleInputForIphone('blur')}
        />
        <div className={styles.action}>
          <button
            className={classnames({
              [styles.disabled]: pristine || submitting,
            })}>
            Lấy lại mật khẩu
          </button>
        </div>
      </form>
    );
  }
}

const ForgotPasswordForm = reduxForm({
  form: 'register',
  validate,
  warn,
  // touchOnBlur: false,
  // touchOnChange: true,
})(withStyles(styles)(ForgotPassword));

export default connect(
  state => ({
    initialValues: {
      subscribe: true,
    },
  }),
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
    };
  }
)(ForgotPasswordForm);
