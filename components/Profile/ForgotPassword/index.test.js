import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ForgotPassword } from './index';
import _identity from 'lodash/identity';
import { FORGOTPASSWORD_MESSAGE } from '../../../res/text';

describe('<ForgotPassword /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      handleSubmit: _identity,
    };
    const wrapper = shallow(<ForgotPassword {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ForgotPassword);
    expect(wrapper.find('p').text()).toEqual(FORGOTPASSWORD_MESSAGE);
  });
});
