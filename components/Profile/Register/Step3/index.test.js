import React from 'react';
import { shallow } from 'sendo-web-sdk/utils/testing';
import { RegisterStepThree } from './index';
import _identity from 'lodash/identity';
import * as text from 'sendo-web-sdk/res/text';

describe('<RegisterStepThree /> component', () => {
  it('It should render without error', () => {
    const context = {};
    const wrapper = shallow(<RegisterStepThree handleSubmit={_identity} />, {
      context,
    });
    expect(wrapper.find('.btn-round').text()).toEqual('Đăng ký');
  });
  it('It should render text guide TEXT_GUIDE_REGISTER_EMAIL', () => {
    const context = {};
    const props = {
      registerParamSubmit: {
        phone: '123',
      },
    };
    const wrapper = shallow(
      <RegisterStepThree {...props} handleSubmit={_identity} />,
      {
        context,
      }
    );
    expect(wrapper.find('.block-form').at(1).find('div').at(3).text()).toEqual(
      text.TEXT_GUIDE_REGISTER_EMAIL
    );
  });
  it('It should render text guide TEXT_GUIDE_REGISTER_PHONE', () => {
    const context = {};
    const props = {
      registerParamSubmit: {
        email: '123',
      },
    };
    const wrapper = shallow(
      <RegisterStepThree {...props} handleSubmit={_identity} />,
      {
        context,
      }
    );
    expect(wrapper.find('.block-form').at(1).find('div').at(3).text()).toEqual(
      text.TEXT_GUIDE_REGISTER_PHONE
    );
  });
});
