import React, { Component } from 'react';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import { bindActionCreators } from 'redux';
import { Field, SubmissionError, reduxForm } from 'redux-form';
import styles from '../register.css';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { handleInputForIphone } from 'sendo-web-sdk/helpers/layout';
import * as css from 'sendo-web-sdk/helpers/css';
import * as text from 'sendo-web-sdk/res/text';
import {
  PASSWORD_MINLENGTH,
  PASSWORD_MAXLENGTH,
  FULLNAME_MINLENGTH,
  FULLNAME_MAXLENGTH,
} from 'sendo-web-sdk/helpers/const';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as loadingActions from 'sendo-web-sdk/actions/loading';
import CommonInput from '../../commonInput';
import EyeIcon from 'sendo-web-sdk/components/Icons/EyeIcon';
import { FORM_LOGIN } from '../../const';
import { APP_GOOGLE_CAPTCHA_PUBLIC_KEY } from 'sendo-web-sdk/helpers/const';
import Recaptcha from 'react-recaptcha';
import { isLoggedIn } from 'sendo-web-sdk/helpers/auth';

const validate_three = values => {
  let errors = {};
  if (!values.password) {
    errors.password = text.NOTI_PASSWORD_EMPTY;
  } else if (
    values.password.length < PASSWORD_MINLENGTH ||
    values.password.length > PASSWORD_MAXLENGTH
  ) {
    errors.password = text.NOTI_PASSWORD_EMPTY_OR_WRONG_FORMAT;
  } else if (values.re_password !== values.password) {
    errors.re_password = text.NOTI_PASSWORD_NOT_MATCH;
  }
  if (!values.fullname) {
    errors.fullname = text.NOTI_FULLNAME_EMPTY;
  } else if (values.fullname.length > FULLNAME_MAXLENGTH) {
    errors.fullname = text.NOTI_FULLNAME_WRONG_MAXLENGTH;
  } else if (values.fullname.length < FULLNAME_MINLENGTH) {
    errors.fullname = text.NOTI_FULLNAME_WRONG_MINLENGTH;
  }
  return errors;
};

const warn = values => {
  const warnings = {};
  return warnings;
};

export class RegisterStepThree extends Component {
  responseGoogleCaptcha = null;
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      isShowPassword: false,
      errorInputText: '',
      errorInputPassword: '',
    };
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.fullname) this.fullname.focus();
    });
  }
  submit(values) {
    if (!this.responseGoogleCaptcha)
      return new Promise(() => {
        throw new SubmissionError({
          _error: text.NOTI_CAPTCHA,
        });
      });
    this.setState({ isShowPassword: false });
    const params = {
      password: values.password,
      firstname: values.fullname,
      'g-recaptcha-response': this.responseGoogleCaptcha,
      ..._get(this.props, 'registerParamSubmit', {}),
    };
    css.fixCssFormAfter();
    // console.log(params);
    //return;
    this.props.loadingActions.openPageLoading();
    return this.props.userActions.register(params).then(res => {
      let dataRegister = _get(res, 'payload.result.data', {});
      if (dataRegister.err_code) {
        throw new SubmissionError({
          _error: dataRegister.message,
        });
        this.props.loadingActions.closePageLoading();
      }
      this.props.userActions
        .login({ username: params.username, password: params.password })
        .then(res => {
          this.props.loadingActions.closePageLoading();
          let dataLogin = _get(res, 'payload.user_info', {});
          if (isLoggedIn(dataLogin)) {
            this.props.done(null, res);
            this.props.toastActions.openToast({
              content: text.NOTI_REGISTER_SUCCESS,
            });
          } else {
            this.props.toastActions.openToast({
              content: text.NOTI_REGISTER_ERROR,
            });
          }
        });
    });
  }
  onChangePasswordVisibility() {
    this.setState({ isShowPassword: !this.state.isShowPassword });
  }
  getInputError(error, inputName) {
    if (inputName === 'fullname') this.setState({ errorInputText: error });
    if (inputName === 'password') this.setState({ errorInputPassword: error });
  }
  verifyCallbackCaptcha(res) {
    this.responseGoogleCaptcha = res;
  }
  expiredCallbackCaptcha() {
    this.responseGoogleCaptcha = null;
  }
  renderTextGuide() {
    let textGuide = text.TEXT_GUIDE_REGISTER_PHONE;
    const phone = _get(this.props, 'registerParamSubmit.phone', false);
    if (phone) textGuide = text.TEXT_GUIDE_REGISTER_EMAIL;
    return textGuide;
  }
  render() {
    const { error, handleSubmit, submitting } = this.props;
    return (
      <form className={styles.form} onSubmit={handleSubmit(this.submit)}>
        <div id="register-step-3">
          <div className="block-form">
            <div className="form-group">
              <label className="form-login-label">
                Họ và tên
              </label>
              <Field
                name="fullname"
                component={CommonInput}
                type="text"
                tabIndex="1"
                inputRef={input => (this.fullname = input)}
                onFocus={() => handleInputForIphone('focus')}
                onBlur={() => handleInputForIphone('blur')}
                props={{
                  getInputError: (error, inputName) =>
                    this.getInputError(error, inputName),
                }}
              />
            </div>

            <div className="form-group">
              <label className="form-login-label">Mật khẩu</label>
              <div className="form-group-custom">
                <Field
                  name="password"
                  component={CommonInput}
                  type={this.state.isShowPassword ? 'text' : 'password'}
                  tabIndex="2"
                  inputRef={input => (this.passwordCtr = input)}
                  onFocus={() => handleInputForIphone('focus')}
                  onBlur={() => handleInputForIphone('blur')}
                  props={{
                    getInputError: (error, inputName) =>
                      this.getInputError(error, inputName),
                  }}
                />
                <EyeIcon
                  onClick={() => this.onChangePasswordVisibility()}
                  className="icon icon-show-password"
                />
              </div>
            </div>
            <div className={`form-group ${styles.captcha}`}>
              <Recaptcha
                sitekey={APP_GOOGLE_CAPTCHA_PUBLIC_KEY}
                render="explicit"
                verifyCallback={res => this.verifyCallbackCaptcha(res)}
                expiredCallback={() => this.expiredCallbackCaptcha()}
                hl="vi"
              />
            </div>
          </div>
          <div className="block-form">
            <div className="form-error">
              {error && <p>{error}</p>}
              {this.state.errorInputText && <p>{this.state.errorInputText}</p>}
              {this.state.errorInputPassword &&
                <p>{this.state.errorInputPassword}</p>}
            </div>
            <button
              disabled={submitting}
              className={classnames({
                'btn-primary btn-block btn-round orange': true,
                [styles.disabled]: submitting,
              })}>
              Đăng ký
            </button>
            <div className={styles.wrap}>
              <div
                onClick={() => this.props.changeStep('step_1', {})}
                className={styles.register}>
                {this.renderTextGuide()}
              </div>
              <div
                onClick={() => this.props.switchForm(FORM_LOGIN)}
                className={styles.login}>
                Đăng nhập
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const RegisterFormThree = reduxForm({
  /**
   * touchOnBlur : marks fields as touched when the blur action is fired. Defaults to true.
   * touchOnChange : marks fields as touched when the change action is fired. Defaults to false.
   */
  form: 'register_three',
  validate: validate_three,
  warn,
  touchOnBlur: false,
  // touchOnChange: true,
})(withStyles(styles)(RegisterStepThree));

export default connect(
  state => ({
    initialValues: {
      subscribe: true,
    },
  }),
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
      loadingActions: bindActionCreators(loadingActions, dispatch),
    };
  }
)(RegisterFormThree);
