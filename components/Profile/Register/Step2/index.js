import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import { Field, SubmissionError, reduxForm } from 'redux-form';
import styles from '../register.css';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { handleInputForIphone } from 'sendo-web-sdk/helpers/layout';
import * as css from 'sendo-web-sdk/helpers/css';
import * as text from 'sendo-web-sdk/res/text';
import ResendIcon from 'sendo-web-sdk/components/Icons/ResendIcon';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as loadingActions from 'sendo-web-sdk/actions/loading';
import CommonInput from '../../commonInput';
import { FORM_LOGIN } from '../../const';
import { COUNT_DOWN_SECONDE_REGISTER } from 'sendo-web-sdk/helpers/const';

const validate_two = values => {
  let errors = {};
  if (!values.otp) {
    errors.otp = text.NOTI_OTP_EMPTY;
  }
  return errors;
};

const warn = values => {
  const warnings = {};
  return warnings;
};

export class RegisterStepTwo extends Component {
  countdownTimer = null;
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      errorInputText: '',
      isDisabledBtnResendOtp: true,
      isShowBtnResendOtp: true,
    };
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.otp) this.otp.focus();
      this.startCountDown();
    });
  }
  componentWillUnmount() {
    if (this.countdownTimer !== null) {
      clearInterval(this.countdownTimer);
    }
  }
  submit(values) {
    const otp = values.otp;
    const phone = _get(this.props, 'registerParam.phone', false);
    if (!phone) return;
    const params = {
      otp_code: otp,
      phone,
    };
    this.props.loadingActions.openPageLoading();
    return this.props.userActions.checkValidOtp(params).then(res => {
      this.props.loadingActions.closePageLoading();
      if (res.err_code) {
        throw new SubmissionError({
          _error: res.message,
        });
      }
      this.props.changeStep('step_3', { token_otp: otp });
    });
  }
  getInputError(error) {
    this.setState({ errorInputText: error });
  }
  startCountDown() {
    let timeLeft = COUNT_DOWN_SECONDE_REGISTER;
    this.countdownTimer = setInterval(() => {
      timeLeft--;
      if (this.countdown) this.countdown.textContent = timeLeft;
      if (timeLeft <= 0) {
        this.setState({ isDisabledBtnResendOtp: false });
        clearInterval(this.countdownTimer);
      }
    }, 1000);
  }
  onReSendOtp() {
    this.setState({ isDisabledBtnResendOtp: true });
    this.startCountDown();
    const phone = _get(this.props, 'registerParam.phone', false);
    if (!phone) return;
    this.props.userActions.resendOtp({ phone }).then(res => {
      if (res.err_code) {
        this.setState({ errorInputText: res.message });
        return;
      }
      this.props.toastActions.openToast({
        content: text.NOTI_RESEND_OTP_SUCCESS,
      });
      this.setState({ errorInputText: '' });
    });
  }
  render() {
    const { error, handleSubmit, submitting } = this.props;
    return (
      <form className={styles.form} onSubmit={handleSubmit(this.submit)}>
        <div id="register-step-2">
          <div className="block-form">
            <p className={styles.otpText}>
              <span>Mã xác thực đã được gửi đến số điện thoại :</span>
              <br />
              <strong>{_get(this.props, 'registerParam.phone', '')}</strong>
              <br />
              <span>Vui lòng nhập mã xác thực vào ô bên dưới</span>
            </p>
            <div className="form-group">
              <label className="form-login-label">Nhập mã xác nhận</label>
              <Field
                name="otp"
                component={CommonInput}
                type="text"
                inputRef={input => (this.otp = input)}
                onFocus={() => handleInputForIphone('focus')}
                onBlur={() => handleInputForIphone('blur')}
                props={{
                  getInputError: (error, inputName) =>
                    this.getInputError(error, inputName),
                }}
              />
            </div>
            {this.state.isShowBtnResendOtp && (
              <div className={styles.resend}>
                {!this.state.isDisabledBtnResendOtp && (
                  <button
                    type="button"
                    onClick={() => this.onReSendOtp()}
                    className="resend-button">
                    <ResendIcon />
                  </button>
                )}
                {this.state.isDisabledBtnResendOtp && (
                  <label
                    ref={countdown => (this.countdown = countdown)}
                    className="resend-button disable">
                    {COUNT_DOWN_SECONDE_REGISTER}
                  </label>
                )}
                <p>Gửi lại mã xác nhận</p>
              </div>
            )}
          </div>
          <div className="block-form">
            <div className="form-error">
              {error && <p>{error}</p>}
              {this.state.errorInputText && <p>{this.state.errorInputText}</p>}
            </div>
            <button
              disabled={submitting}
              className={classnames({
                'btn-primary btn-block btn-round orange': true,
                [styles.disabled]: submitting,
              })}>
              Tiếp tục
            </button>
            <div className={styles.wrap}>
              <div
                onClick={() => this.props.changeStep('step_1', {})}
                className={styles.register}>
                {text.TEXT_GUIDE_REGISTER_EMAIL}
              </div>
              <div
                onClick={() => this.props.switchForm(FORM_LOGIN)}
                className={styles.login}>
                Đăng nhập
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const RegisterFormTwo = reduxForm({
  /**
   * touchOnBlur : marks fields as touched when the blur action is fired. Defaults to true.
   * touchOnChange : marks fields as touched when the change action is fired. Defaults to false.
   */
  form: 'register_two',
  validate: validate_two,
  warn,
  touchOnBlur: false,
  // touchOnChange: true,
})(withStyles(styles)(RegisterStepTwo));

export default connect(
  state => ({
    initialValues: {
      subscribe: true,
    },
  }),
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
      loadingActions: bindActionCreators(loadingActions, dispatch),
    };
  }
)(RegisterFormTwo);
