import React from 'react';
import { shallow } from 'enzyme';
import { RegisterStepTwo } from './index';
import _identity from 'lodash/identity';

describe('<RegisterStepTwo /> component', () => {
  it('It should render without error', () => {
    const context = {};
    const wrapper = shallow(<RegisterStepTwo handleSubmit={_identity} />, {
      context,
    });
    expect(wrapper.find('.btn-round').text()).toEqual('Tiếp tục');
  });
});
