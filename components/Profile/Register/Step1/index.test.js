import React from 'react';
import { shallow } from 'enzyme';
import { RegisterStepOne } from './index';
import _identity from 'lodash/identity';

describe('<RegisterStepOne /> component', () => {
  it('It should render without error', () => {
    const context = {};
    const wrapper = shallow(<RegisterStepOne handleSubmit={_identity} />, {
      context,
    });
    expect(wrapper.find('button').text()).toEqual('Tiếp tục');
  });
});
