import React, { Component } from 'react';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import { bindActionCreators } from 'redux';
import { Field, SubmissionError, reduxForm } from 'redux-form';
import styles from '../register.css';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { handleInputForIphone } from 'sendo-web-sdk/helpers/layout';
import * as text from 'sendo-web-sdk/res/text';
import { REGEX_EMAIL, REGEX_PHONE } from 'sendo-web-sdk/helpers/const';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as loadingActions from 'sendo-web-sdk/actions/loading';
import CommonInput from '../../commonInput';
import { FORM_LOGIN } from '../../const';

const validate_one = values => {
  const _values = { ...values };
  let errors = {};
  if (!_values.username) {
    errors.username = text.NOTI_EMAIL_PHONE_EMPTY;
  } else {
    _values.username = _values.username.trim().replace(/^(\+|)84/g, '0');
    const valid_email = REGEX_EMAIL.test(_values.username);
    const valid_phone = REGEX_PHONE.test(_values.username);
    if (!valid_email && !valid_phone) {
      errors.username = text.NOTI_EMAIL_PHONE_WRONG_FORMAT;
    }
  }
  return errors;
};

const warn = values => {
  const warnings = {};
  return warnings;
};

export class RegisterStepOne extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      errorInputText: '',
    };
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.username) this.username.focus();
    });
  }
  submit(values) {
    let username = values.username;
    this.props.loadingActions.openPageLoading();
    return this.props.userActions
      .checkValidAndPostPhoneEmail({ username })
      .then(res => {
        this.props.loadingActions.closePageLoading();
        if (res.err_code) {
          throw new SubmissionError({
            _error: res.message,
          });
        }
        const paramDefault = { username: username };
        if (_get(res, 'data.register_type') === 'email')
          this.props.changeStep('step_3', {
            email: username,
            ...paramDefault,
          });
        if (_get(res, 'data.register_type') === 'phone') {
          if (_get(res, 'data.receive_otp', true)) {
            this.props.changeStep('step_2', {
              phone: username,
              ...paramDefault,
            });
          } else {
            this.props.changeStep('step_3', {
              phone: username,
              ...paramDefault,
            });
          }
        }
      });
  }
  getInputError(error) {
    this.setState({ errorInputText: error });
  }
  render() {
    const { error, handleSubmit, submitting } = this.props;
    return (
      <form className={styles.form} onSubmit={handleSubmit(this.submit)}>
        <div id="register-step-1">
          <div className="block-form">
            <div className="form-group">
              <label className="form-login-label">
                Nhập Email hoặc Số điện thoại
              </label>
              <Field
                name="username"
                component={CommonInput}
                type="text"
                inputRef={input => (this.username = input)}
                onFocus={() => handleInputForIphone('focus')}
                onBlur={() => handleInputForIphone('blur')}
                props={{
                  getInputError: (error, inputName) =>
                    this.getInputError(error, inputName),
                }}
              />
            </div>
          </div>
          <div className="block-form">
            <div className="form-error">
              {error && <p>{error}</p>}
              {this.state.errorInputText && <p>{this.state.errorInputText}</p>}
            </div>
            <button
              disabled={submitting}
              className={classnames({
                'btn-primary btn-block btn-round orange': true,
                [styles.disabled]: submitting,
              })}>
              Tiếp tục
            </button>
            <div className={styles.wrap}>
              <div
                onClick={() => this.props.switchForm(FORM_LOGIN)}
                className={styles.full}>
                Đăng nhập
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const RegisterFormOne = reduxForm({
  /**
   * touchOnBlur : marks fields as touched when the blur action is fired. Defaults to true.
   * touchOnChange : marks fields as touched when the change action is fired. Defaults to false.
   */
  form: 'register_one',
  validate: validate_one,
  warn,
  touchOnBlur: false,
  // touchOnChange: true,
})(withStyles(styles)(RegisterStepOne));

export default connect(
  state => ({
    initialValues: {
      subscribe: true,
    },
  }),
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
      loadingActions: bindActionCreators(loadingActions, dispatch),
    };
  }
)(RegisterFormOne);
