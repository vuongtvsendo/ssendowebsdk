import React from 'react';
import { shallow } from 'sendo-web-sdk/utils/testing';
import { Register } from './index';
import RegisterStepOne from './Step1';
import RegisterStepTwo from './Step2';
import RegisterStepThree from './Step3';

describe('<Register /> component', () => {
  it('It should render without error', () => {
    const context = {};
    const wrapper = shallow(<Register />, {
      context,
    });
    expect(wrapper.find(RegisterStepOne).exists()).toEqual(true);
  });
  it('It should render change step without error', () => {
    const wrapper = shallow(<Register />);
    expect(wrapper.find(RegisterStepOne).exists()).toEqual(true);
    wrapper.setState({ step: 'step_2' });
    expect(wrapper.find(RegisterStepTwo).exists()).toEqual(true);
    wrapper.setState({ step: 'step_3' });
    expect(wrapper.find(RegisterStepThree).exists()).toEqual(true);
  });
});
