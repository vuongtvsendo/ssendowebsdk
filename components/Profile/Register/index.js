import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './register.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import RegisterStepOne from './Step1/index';
import RegisterStepTwo from './Step2/index';
import RegisterStepThree from './Step3/index';

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 'step_1',
      registerParam: {},
    };
  }
  changeStep(step, registerParam) {
    this.setState({
      registerParam: step === 'step_1'
        ? {}
        : { ...this.state.registerParam, ...registerParam },
      step: step,
    });
  }
  render() {
    return (
      <div>
        {this.state.step === 'step_1' &&
          <RegisterStepOne
            switchForm={form => this.props.switchForm(form)}
            changeStep={(step, registerParam) =>
              this.changeStep(step, registerParam)}
          />}
        {this.state.step === 'step_2' &&
          <RegisterStepTwo
            switchForm={form => this.props.switchForm(form)}
            changeStep={(step, registerParam) =>
              this.changeStep(step, registerParam)}
            registerParam={this.state.registerParam}
          />}
        {this.state.step === 'step_3' &&
          <RegisterStepThree
            switchForm={form => this.props.switchForm(form)}
            changeStep={(step, registerParam) =>
              this.changeStep(step, registerParam)}
            registerParamSubmit={this.state.registerParam}
            done={(a, res) => this.props.done(a, res)}
          />}
      </div>
    );
  }
}

export default connect(
  state => ({
    initialValues: {
      subscribe: true,
    },
  }),
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(Register));
