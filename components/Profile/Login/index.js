import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import {
  PASSWORD_MINLENGTH,
  PASSWORD_MAXLENGTH,
  REGEX_EMAIL,
  REGEX_PHONE,
} from 'sendo-web-sdk/helpers/const';
import { handleInputForIphone } from 'sendo-web-sdk/helpers/layout';
import * as css from 'sendo-web-sdk/helpers/css';
import * as auth from 'sendo-web-sdk/helpers/auth';
import * as text from 'sendo-web-sdk/res/text';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { fbLogin } from 'sendo-web-sdk/utils/facebook';
import { googleLogin } from 'sendo-web-sdk/utils/google';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { FORM_REGISTER } from '../const';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import styles from './login.css';
import GoogleIcon from 'sendo-web-sdk/components/Icons/IconGooglePlus';
import FbIcon from 'sendo-web-sdk/components/Icons/IconFb';
import { forgotPasswordLink } from 'sendo-web-sdk/res/externalLink';
import emitter, {
  CLICK_LOGIN_GOOGLE,
  CLICK_LOGIN_FACEBOOK,
} from 'sendo-web-sdk/helpers/emitter';
import EyeIcon from 'sendo-web-sdk/components/Icons/EyeIcon';
import CommonInput from '../commonInput';
const validate = values => {
  const _values = { ...values };
  let errors = {};
  if (!_values.email) {
    errors.email = text.NOTI_EMAIL_PHONE_EMPTY;
  } else {
    _values.email = _values.email.replace(/^(\+|)84/g, '0');
    if (!REGEX_EMAIL.test(_values.email) && !REGEX_PHONE.test(_values.email))
      errors.email = text.NOTI_EMAIL_PHONE_WRONG_FORMAT;
  }
  if (!_values.password) {
    errors.password = text.NOTI_PASSWORD_EMPTY;
  } else if (
    _values.password.length < PASSWORD_MINLENGTH ||
    _values.password.length > PASSWORD_MAXLENGTH
  ) {
    errors.password = text.NOTI_PASSWORD_EMPTY_OR_WRONG_FORMAT;
  }
  return errors;
};

const warn = values => {
  const warnings = {};
  return warnings;
};

export class Login extends Component {
  fbResponse = {};
  state = {
    isShowLoginSendoAccount: false,
    errors: {},
    isShowPassword: false,
    errorInputText: '',
    errorInputPassword: '',
  };
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      if (this.email) this.email.focus();
    });
    /**
     |-------------------------------------------------------
     | @note
     | 1. Important: Cho nay can dc goi truoc de khi user
     | da authorize APP thi khong bi loi khi goi Login.
     | 2. Tranh goi getLoginStatus => not_authorized => login
     | se bi block popup
     |-------------------------------------------------------
     */
    if (!window.FB) {
      return;
    }
    window.FB.getLoginStatus(response => {
      this.fbResponse = response;
    });
  }
  handleSubmit(values) {
    this.setState({ isShowPassword: false });
    let username = values.email;
    if (!username.trim()) {
      this.usernameCtr.focus();
      return false;
    }
    let password = values.password;
    if (!password) {
      this.passwordCtr.focus();
      return false;
    }
    this.props.toastActions.openToast({
      content: text.MESSAGE_INPROGRESS,
    });
    css.fixCssFormAfter();
    return this.props.userActions
      .login({
        username,
        password,
      })
      .then(() => {
        return this.props.userActions
          .getUserInfo()
          .then(res => this.onLogged(res, 'sendoId'));
      });
  }
  onLogged(res, loginType) {
    const identity = _get(res, 'payload.user_info', {});
    if (!auth.isLoggedIn(identity)) {
      if (_get(window, 'Raven.captureMessage') instanceof Function) {
        window.Raven.captureMessage(`${loginType} login error`, {
          level: 'error',
          extra: res,
        });
      }
      if (loginType === 'sendoId') {
        throw new SubmissionError({
          _error: text.NOTI_LOGIN_ERROR,
        });
      }
      this.setState({
        errors: {
          message: text.NOTI_LOGIN_SOCIAL_ERROR,
        },
      });
      return;
    }
    this.props.toastActions.openToast({
      content: `${auth.getFullname(identity)} ${text.NOTI_LOGIN_SUCCESS}`,
    });
    this.props.done(res);
  }
  handleWithoutLogin(e) {
    e.preventDefault();
    this.props.done();
    return;
  }
  loginFB(e) {
    emitter.emit(CLICK_LOGIN_FACEBOOK, e, {});
    this.props.toastActions.openToast({
      content: text.MESSAGE_INPROGRESS,
    });
    if (this.fbResponse.status === 'connected') {
      this.props.userActions.fbLoginOld().then(() => {
        this.props.userActions
          .getUserInfo()
          .then(res => this.onLogged(res, 'facebook'));
      });
      return;
    }
    fbLogin(res => {
      if (res.status !== 'connected') {
        this.props.toastActions.openToast({
          content: text.NOTI_LOGIN_FB_ERROR,
        });
        return;
      }
      this.props.userActions.fbLoginOld().then(() => {
        this.props.userActions
          .getUserInfo()
          .then(res => this.onLogged(res, 'facebook'));
      });
    });
  }
  loginGoogle(e) {
    emitter.emit(CLICK_LOGIN_GOOGLE, e, {});
    this.props.toastActions.openToast({
      content: text.MESSAGE_INPROGRESS,
    });
    googleLogin(res => {
      this.props.userActions
        .getUserInfo()
        .then(res => this.onLogged(res, 'google'));
    });
  }
  handleLoginSendoAccount() {
    if (!this.state.isShowLoginSendoAccount) {
      setTimeout(() => {
        if (this.email) this.email.focus();
      });
    }
    this.setState({
      isShowLoginSendoAccount: !this.state.isShowLoginSendoAccount,
    });
  }
  onChangePasswordVisibility() {
    this.setState({ isShowPassword: !this.state.isShowPassword });
  }
  getInputError(error, inputName) {
    if (inputName === 'email') this.setState({ errorInputText: error });
    if (inputName === 'password') this.setState({ errorInputPassword: error });
  }
  render() {
    const { errors } = this.state;
    const { error, handleSubmit, submitting } = this.props;
    return (
      <form onSubmit={handleSubmit(this.handleSubmit)}>
        <div className={!this.state.isShowLoginSendoAccount ? '' : 'hidden'}>
          <div className="block-form">
            <div className="form-group">
              <button
                type="button"
                onClick={e => this.loginFB()}
                className="btn btn-primary btn-block btn-social-login btn-social-login-fb">
                <span className="icon-btn-social-login">
                  <FbIcon className="svg-btn-login-social icon" />
                </span>
                <span>Facebook</span>
              </button>
            </div>
            <div className="form-group">
              <button
                type="button"
                onClick={e => this.loginGoogle()}
                className="btn btn-primary btn-block btn-social-login btn-social-login-gg">
                <span className="icon-btn-social-login">
                  <GoogleIcon className="svg-btn-login-social icon" />
                </span>
                <span>Google +</span>
              </button>
            </div>
          </div>

          <div className={styles.guide}>
            <div className="form-error">
              {errors.message && <p>{errors.message}</p>}
            </div>
            <div className={styles.wrap}>
              <div
                onClick={e => this.handleLoginSendoAccount()}
                className={styles.forgot}>
                Đã có SendoID
              </div>
              <div
                onClick={() => this.props.switchForm(FORM_REGISTER)}
                className={styles.register}>
                Đăng ký
              </div>
            </div>
          </div>
        </div>

        <div className={this.state.isShowLoginSendoAccount ? '' : 'hidden'}>
          <div className="block-form">
            <div className="form-group">
              <label className="form-login-label">
                Nhập Email hoặc Số điện thoại
              </label>
              <Field
                name="email"
                component={CommonInput}
                type="text"
                tabIndex="1"
                inputRef={input => (this.email = input)}
                onFocus={() => handleInputForIphone('focus')}
                onBlur={() => handleInputForIphone('blur')}
                props={{
                  getInputError: (error, inputName) =>
                    this.getInputError(error, inputName),
                }}
              />
            </div>

            <div className="form-group">
              <label className="form-login-label">Mật khẩu</label>
              <div className="form-group-custom">
                <Field
                  name="password"
                  component={CommonInput}
                  type={this.state.isShowPassword ? 'text' : 'password'}
                  tabIndex="2"
                  inputRef={input => (this.passwordCtr = input)}
                  onFocus={() => handleInputForIphone('focus')}
                  onBlur={() => handleInputForIphone('blur')}
                  props={{
                    getInputError: (error, inputName) =>
                      this.getInputError(error, inputName),
                  }}
                />
                <EyeIcon
                  onClick={() => this.onChangePasswordVisibility()}
                  className="icon icon-show-password"
                />
              </div>
            </div>
            {this.state.isShowLoginSendoAccount && (
              <div
                onClick={e => this.handleLoginSendoAccount()}
                className={styles.guide}>
                <span className={styles.labelGuideDisable}>
                  Đăng nhập bằng <span className={styles.fb}>Facebook</span>{' '}
                  hoặc <spant className={styles.gm}>Gmail</spant>
                </span>
              </div>
            )}
          </div>
          <div className="block-form">
            <div className="form-error">
              {error && <p>{error}</p>}
              {this.state.errorInputText && <p>{this.state.errorInputText}</p>}
              {this.state.errorInputPassword && (
                <p>{this.state.errorInputPassword}</p>
              )}
            </div>
            <div className="text-center">
              <button
                disabled={submitting}
                className={classnames({
                  'btn-primary btn-block btn-round orange': true,
                  [styles.disabled]: submitting,
                })}>
                Đăng nhập
              </button>
            </div>
            <div className={styles.wrap}>
              <a href={forgotPasswordLink} className={styles.forgot}>
                Quên mật khẩu?
              </a>
              <div
                onClick={() => this.props.switchForm(FORM_REGISTER)}
                className={styles.register}>
                Đăng ký
              </div>
            </div>
          </div>
        </div>
        {this.props.enableCheckoutLogin &&
          !this.state.isShowLoginSendoAccount && (
            <div className={`${styles.checkoutWithoutLogin}`}>
              <span className={styles.lineFull} />
              <button
                type="button"
                className={classnames({
                  'btn-primary btn-block btn-round blue-border': true,
                  [styles.btnWithoutLogin]: true,
                })}
                onClick={e => this.handleWithoutLogin(e)}>
                Mua không cần đăng nhập
              </button>
            </div>
          )}
      </form>
    );
  }
}

const LoginForm = reduxForm({
  /**
   * touchOnBlur : marks fields as touched when the blur action is fired. Defaults to true.
   * touchOnChange : marks fields as touched when the change action is fired. Defaults to false.
   */
  form: 'login',
  warn,
  validate,
  touchOnBlur: false,
  // touchOnChange: true,
})(withStyles(styles)(Login));

export default connect(null, dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    toastActions: bindActionCreators(toastActions, dispatch),
  };
})(withStyles(styles)(LoginForm));
