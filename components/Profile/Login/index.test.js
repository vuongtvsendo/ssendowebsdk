import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Login } from './index';
import _identity from 'lodash/identity';

describe('<Login /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      handleSubmit: _identity,
    };
    const wrapper = shallow(<Login {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(Login);
  });
});
