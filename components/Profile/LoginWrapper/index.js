import React, { Component } from 'react';
import { connect } from 'react-redux';
import _startCase from 'lodash/startCase';
import _get from 'lodash/get';
import * as auth from 'sendo-web-sdk/helpers/auth';
import emitter, { IS_LOGGED_IN } from 'sendo-web-sdk/helpers/emitter';
import { bindActionCreators } from 'redux';
import * as userActions from 'sendo-web-sdk/actions/user';

import ModalClassic from 'sendo-web-sdk/modals/Classic';
import Login from '../Login';
import Register from '../Register';
import ForgotPassword from '../ForgotPassword';
import { FORM_LOGIN, FORM_REGISTER, FORM_FORGOT_PASSWORD } from '../const';
import { TITLE_MODAL_LOGIN_BUYNOW } from 'sendo-web-sdk/res/text'; 

export class LoginWrapper extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isModalOpen: false,
      openedForm: props.openedForm || FORM_LOGIN,
    };
    this.toggleModel = this.toggleModel.bind(this);
    this.switchForm = this.switchForm.bind(this);
    this.done = this.done.bind(this);
  }
  toggleModel() {
    let isModalOpen = !this.state.isModalOpen;
    this.setState({
      isModalOpen,
      // @note
      // 1. De reset lai cai form khi mo close va mo lai
      openedForm: this.props.openedForm || FORM_LOGIN,
    });
  }
  done() {
    this.toggleModel();
    emitter.emit(IS_LOGGED_IN, {});
    setTimeout(() => {
      this.props.callback && this.props.callback();
      if (auth.isLoggedIn(this.props.identity)) {
        this.props.userActions.getUserInfoFull();
      }
    });
  }
  switchForm(form) {
    this.setState({
      openedForm: form,
    });
  }
  getTitle() {
    if(this.props.titleModal) {
      return this.props.titleModal
    }
    switch (this.state.openedForm) {
      case FORM_REGISTER:
        return 'Đăng ký';
      case FORM_FORGOT_PASSWORD:
        return 'Quên mật khẩu';
      case FORM_LOGIN:
      default:
        if(this.props.isLoginCheckout) {
          return TITLE_MODAL_LOGIN_BUYNOW;
        }
        return 'Đăng nhập';
    }
  }
  getActionLabel() {
    switch (this.state.openedForm) {
      case FORM_REGISTER:
        return 'Đăng ký';
      case FORM_FORGOT_PASSWORD:
        return 'Quên mật khẩu';
      case FORM_LOGIN:
      default:
        return 'Đăng nhập';
    }
  }
  getForm(enableCheckoutLogin) {
    switch (this.state.openedForm) {
      case FORM_REGISTER:
        /**
         |-------------------------------------------------------------
         | Doi voi truong hop dang ky xong 
         | => dang nhap luon thi co khai niem done 
         |-------------------------------------------------------------
         */
        return <Register switchForm={this.switchForm} done={this.done} />;
      case FORM_FORGOT_PASSWORD:
        /**
         |-------------------------------------------------------------
         | Form Forgot password khong co khai niem done
         |-------------------------------------------------------------
         */
        return <ForgotPassword switchForm={this.switchForm} />;
      case FORM_LOGIN:
      default:
        return (
          <Login
            switchForm={this.switchForm}
            done={this.done}
            enableCheckoutLogin={_get(this.props, 'enableCheckoutLogin', false)}
          />
        );
    }
  }
  render() {
    let childProps = {};
    let event = '';
    if (this.props.event) {
      event = 'on' + _startCase(this.props.event);
    }
    if (auth.isLoggedIn(this.props.identity)) {
      childProps = {
        ...this.props.children.props,
      };
      if (this.props.callback instanceof Function) {
        childProps[event] = this.props.callback;
      }
      return React.cloneElement(this.props.children, childProps);
    }
    childProps[event] = this.toggleModel;
    return (
      <div className="loginWrapper">
        {React.cloneElement(this.props.children, childProps)}
        <ModalClassic
          title={this.getTitle()}
          action={this.getActionLabel()}
          isOpen={this.state.isModalOpen}
          toggle={this.toggleModel}
          className="">
          {this.getForm(_get(this.props.enableCheckoutLogin))}
        </ModalClassic>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      userActions: bindActionCreators(userActions, dispatch),
    };
  }
)(LoginWrapper);
