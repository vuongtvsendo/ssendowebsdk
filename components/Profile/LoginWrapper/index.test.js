import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { LoginWrapper } from './index';
import Login from '../Login';
import Register from '../Register';
import ForgotPassword from '../ForgotPassword';
import { FORM_LOGIN, FORM_REGISTER, FORM_FORGOT_PASSWORD } from '../const';

describe('<LoginWrapper /> component', () => {
  it('It should render Login', () => {
    const context = {
      ...ctx,
    };
    const props = {
      children: [],
    };
    const wrapper = shallow(<LoginWrapper {...props} />, {
      context,
    });
    wrapper.setState({ openedForm: FORM_LOGIN });
    expect(wrapper.instance()).toBeInstanceOf(LoginWrapper);
    expect(wrapper.find(Login).exists()).toEqual(true);
    expect(wrapper.find(Register).exists()).toEqual(false);
    expect(wrapper.find(ForgotPassword).exists()).toEqual(false);
  });
  it('It should render Register', () => {
    const context = {
      ...ctx,
    };
    const props = {
      children: [],
    };
    const wrapper = shallow(<LoginWrapper {...props} />, {
      context,
    });
    wrapper.setState({ openedForm: FORM_REGISTER });
    expect(wrapper.instance()).toBeInstanceOf(LoginWrapper);
    expect(wrapper.find(Register).exists()).toEqual(true);
    expect(wrapper.find(Login).exists()).toEqual(false);
    expect(wrapper.find(ForgotPassword).exists()).toEqual(false);
  });
  it('It should render Forgot password', () => {
    const context = {
      ...ctx,
    };
    const props = {
      children: [],
    };
    const wrapper = shallow(<LoginWrapper {...props} />, {
      context,
    });
    wrapper.setState({ openedForm: FORM_FORGOT_PASSWORD });
    expect(wrapper.instance()).toBeInstanceOf(LoginWrapper);
    expect(wrapper.find(ForgotPassword).exists()).toEqual(true);
    expect(wrapper.find(Login).exists()).toEqual(false);
    expect(wrapper.find(Register).exists()).toEqual(false);
  });
});
