import React, { Component } from 'react';
class CommonInput extends Component {
  componentDidUpdate(prevProps) {
    if (
      this.props.meta.error &&
      this.props.meta.touched &&
      (this.props.meta.touched !== prevProps.meta.touched ||
        this.props.meta.error !== prevProps.meta.error)
    ) {
      this.props.getInputError(this.props.meta.error, this.props.input.name);
    }
    if (
      !this.props.meta.error &&
      this.props.meta.touched &&
      (this.props.meta.touched !== prevProps.meta.touched ||
        this.props.meta.error !== prevProps.meta.error)
    ) {
      this.props.getInputError('', this.props.input.name);
    }
  }
  render() {
    const {
      input,
      type,
      placeholder,
      inputRef,
      meta: { touched, error },
    } = this.props;
    return (
      <div className="form-group">
        <input
          {...input}
          className={`form-control input-standard ${touched && error
            ? 'error'
            : ''}`}
          placeholder={placeholder}
          type={type}
          ref={inputRef}
        />
      </div>
    );
  }
}

export default CommonInput;
