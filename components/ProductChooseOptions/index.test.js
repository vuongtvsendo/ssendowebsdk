import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductChooseOptions } from './index';

describe('<ProductChooseOptions /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        name: 'name1',
      },
      displayProduct: true,
    };
    const wrapper = shallow(<ProductChooseOptions {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductChooseOptions);
    expect(wrapper.find('h3.heading-medium').text()).toEqual(
      props.product.name
    );
  });
});
