import React, { PureComponent } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';

import classnames from 'classnames';
import styles from './productOptions.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import _get from 'lodash/get';

export class ProductOptions extends PureComponent {
  selectOption(event, option) {
    const attribute = _get(this.props, 'groupOption');
    this.props.setSelectedAttr({
      [attribute.search_key]: {
        groupOption: attribute,
        selected: option,
      },
    });
  }

  isActive(option) {
    const attribute = _get(this.props, 'groupOption');
    return (
      _get(this.props, `selected[${attribute.search_key}].selected`) === option
    );
  }
  getClassNameType() {
    const attribute = _get(this.props, 'groupOption', {});
    if (['mau_sac', 'mau_sac_elt'].indexOf(attribute.search_key) !== -1) {
      return 'color';
    }
    return 'rectangle';
  }

  render() {
    return (
      <div className={`${styles.panelOptions} panel`}>
        <h4 className={styles.title}>
          {this.props.title}
        </h4>
        <div className={styles.groupOption}>
          {this.renderAttributeOptions()}
        </div>
      </div>
    );
  }

  renderAttributeOptions() {
    const options = _get(this.props, 'groupOption.value', []);
    if (!(options instanceof Array) || !options.length) {
      return <span />;
    }
    return options.map(option => {
      let classNameType = this.getClassNameType();
      let classNames = {
        [styles.option]: true,
        [styles[classNameType]]: true,
        [styles.active]: this.isActive(option),
      };
      let label = classNameType === 'color' ? '' : option.value;
      let customStyles = {};
      if (classNameType === 'color') {
        customStyles.background = option.background;
      }
      return (
        <div className={styles.item} key={option.option_id}>
          <span
            key={option.option_id}
            onClick={e => this.selectOption(e, option)}
            className={classnames(classNames)}
            style={customStyles}>
            {label}
          </span>
        </div>
      );
    });
  }
}

export default connect(null, dispatch => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
  };
})(withStyles(styles)(ProductOptions));
