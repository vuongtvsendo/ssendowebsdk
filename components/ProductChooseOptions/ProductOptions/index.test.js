import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductOptions } from './index';

describe('<ProductOptions /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<ProductOptions {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductOptions);
  });
});
