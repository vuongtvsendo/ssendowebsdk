import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import styles from './productChooseOptions.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import ReactDOM from 'react-dom';

import Loading from 'sendo-web-sdk/components/Loading';
import ProductOptions from './ProductOptions';
import ModalAttributes from 'sendo-web-sdk/modals/Attributes';
import ModalFullWrap from 'sendo-web-sdk/components/ModalFullWrap';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import Ruler from 'sendo-web-sdk/components/Icons/Ruler';

import _debounce from 'lodash/debounce';

export class ProductChooseOptions extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isCollapseOpen: true,
      scrollHeight: _get(window, 'innerHeight', 500),
    };
    this.handleResize = _debounce(this.handleResize.bind(this), 250);
    this.handleTouchMove = this.handleTouchMove.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    window.addEventListener('touchmove', this.handleTouchMove);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
    window.removeEventListener('touchmove', this.handleTouchMove);
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    node && node.removeEventListener('scroll', this.handleScroll);
  }
  componentDidUpdate() {
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    if (node && !node._isScrollEventAttachted) {
      node.addEventListener('scroll', this.handleScroll);
      node._isScrollEventAttachted = true;
    }
  }
  handleTouchMove(event) {
    let target = event.target;
    if (this.props.isOpen) {
      if (target.className === 'modal fade show') {
        event.preventDefault();
        event.stopPropagation();
        return false;
      }
    }
    return true;
  }
  handleScroll(event) {
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    if (!node) {
      console.log('[BUG] Opps! Why node is not exist.');
    }
    if (node && node.scrollTop + node.clientHeight >= node.scrollHeight) {
      node.scrollTop = node.scrollHeight - node.clientHeight - 1;
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
    if (node && node.scrollTop === 0) {
      node.scrollTop = 1;
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
    return true;
  }
  handleResize() {
    this.setState({ scrollHeight: _get(window, 'innerHeight', 500) });
  }

  setSelectedAttr(attr) {
    this.props.setSelectedAttr(attr);
  }

  render() {
    let { product, sizeGuide, displayProduct, loading } = this.props;
    const attributes = product.attribute || [];
    return (
      <div className={styles.productAttribute}>
        <ModalAttributes
          headingTitle="Chọn thuộc tính"
          isOpen={this.props.isOpen}
          toggle={() => this.props.toggleModalAttributes()}>
          <div
            className={styles.scrolling}
            ref="scroller"
            style={{ height: this.state.scrollHeight }}>
            {loading && <div className={styles.loading}><Loading /></div>}
            {displayProduct &&
              <div className={`${styles.cartHoriz} card-horiz panel`}>
                <img src={product.image} alt={product.name} />
                <div className="caption">
                  <h3 className="heading-medium truncate-medium-2 card-name">
                    {product.name}
                  </h3>
                  <div className="price">
                    <strong className="text-primary current">
                      {helperFunction.formatPrice(product.final_price)}
                    </strong>
                  </div>
                </div>
              </div>}

            {attributes.map((item, index) => {
              return (
                <ProductOptions
                  key={item.attribute_id}
                  setSelectedAttr={attr => this.setSelectedAttr(attr)}
                  groupOption={item}
                  selected={this.props.selectedAttr}
                  className={styles.blockItem}
                  title={item.name}
                />
              );
            })}
            {sizeGuide &&
              sizeGuide.length > 0 &&
              <div className={styles.tableSize}>
                <Ruler width={24} height={24} className={styles.ruler} />
                <Link to="#xem-bang-kich-thuoc" replace>
                  <span onClick={this.toggleModalRuler}>
                    Xem bảng kích thước
                  </span>
                </Link>
              </div>}
          </div>
          <div className={styles.fixedBottom}>
            {this.props.children}
          </div>
        </ModalAttributes>
        {sizeGuide &&
          sizeGuide.length > 0 &&
          <ModalFullWrap
            isOpen={
              _get(this.props, 'location.hash') === '#xem-bang-kich-thuoc'
            }
            title="Bảng kích thước">
            <div className={styles.filterPanel}>
              <div
                className="content"
                dangerouslySetInnerHTML={{ __html: sizeGuide }}
              />
            </div>
          </ModalFullWrap>}

      </div>
    );
  }
}

export default withStyles(styles)(ProductChooseOptions);
