import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { RatingItem } from './index';

describe('<RatingItem /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      rating: {
        media: ['item1'],
      },
    };
    const wrapper = shallow(<RatingItem {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(RatingItem);
  });
});
