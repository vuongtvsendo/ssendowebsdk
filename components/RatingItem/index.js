import React, { PureComponent } from 'react';
import styles from './ratingItem.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import _get from 'lodash/get';
import StarRating from '../StarRating';
import avartarDefault from 'sendo-web-sdk/images/avartar-default.png';

export class RatingItem extends PureComponent {
  handleClick(activeIndex) {
    if (this.props.openGallery) {
      this.props.openGallery(_get(this.props, 'rating.media'), activeIndex);
    }
  }
  render() {
    const { rating } = this.props;
    return (
      <div className={styles.ratingItem}>
        <div className={styles.avatar}>
          <img src={avartarDefault} alt="Hình đại diện" />
        </div>
        <div className={styles.content}>
          <div className={styles.info}>
            <div className={styles.feedbackInfo}>
              <strong className={styles.userName}>{rating.user_name}</strong>
              <time>{rating.update_time}</time>
            </div>
            <div className={styles.rate}>
              <span>{rating.status}</span>
              <StarRating value={rating.star * 20} />
            </div>
          </div>
          <div className={styles.feedback}>
            <p>{rating.comment}</p>
            {rating &&
              rating.media &&
              rating.media.length > 0 &&
              <div className={styles.feedbackImage}>
                {rating.media.map((item, i) => {
                  return (
                    <span
                      className={styles.image}
                      key={i}
                      onClick={() => this.handleClick(i)}>
                      <img src={item.image_50x50} alt="" />
                    </span>
                  );
                })}
              </div>}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(RatingItem);
