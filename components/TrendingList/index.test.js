import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { TrendingList } from './index';

describe('<TrendingList /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<TrendingList {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(TrendingList);
  });
});
