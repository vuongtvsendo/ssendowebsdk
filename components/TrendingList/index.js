import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './trendingList.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Loading from 'sendo-web-sdk/components/Loading';
import productEmptyImg from 'sendo-web-sdk/images/empty-state/products-empty.svg';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import defaultImg from 'sendo-web-sdk/images/default.png';
export class TrendingList extends PureComponent {
  render() {
    const {
      trends,
      isloading,
      isloadingMore,
      initProducts,
      numberFilterActive,
    } = this.props;
    if (!initProducts) {
      return (
        <div className={styles.loadingMoreIcon}>
          <Loading />
        </div>
      );
    }
    if (initProducts && !trends.length) {
      return (
        <section className="panel">
          <div className="content-empty full">
            <div className="image-square">
              <img
                className="img"
                src={productEmptyImg}
                alt="Danh sách xu hướng hiện tại không có"
              />
            </div>
            <strong className="title">
              Danh sách xu hướng hiện tại không có
            </strong>
            {!numberFilterActive &&
              <Link className="btn-primary" to="/">Tiếp tục mua sắm</Link>}
          </div>
        </section>
      );
    }
    return (
      <div className={styles.listingContainer}>
        {isloading && <div className={styles.loadingIcon}><Loading /></div>}
        <div className={styles.listTrendingWrap}>
          {trends.map((trend, i) => {
            let orderHtml = '';
            if (i % 2 !== 0) orderHtml = 'box-trending-flex-order';
            return (
              <Link
                to={`/xu-huong/${trend.url_key}`}
                key={`${trend.url_key}_${i}`}
                className="box-trending-wrap">

                <div className={`box-trending-thumb ${orderHtml}`}>
                  <img
                    className="trending-big-thumb lazyload"
                    data-src={trend.banner_url}
                    src={defaultImg}
                    alt=""
                  />
                  <div className="box-trending-thumb-bottom">
                    <div className="content content-trending">
                      {trend.products.map(product => {
                        return (
                          <div key={product.id} className="item-trending">

                            <div className="image-square wrap-small-image">
                              <img
                                data-src={product.image}
                                src={defaultImg}
                                alt={product.name}
                                className={`lazyload smallImg`}
                              />
                            </div>

                          </div>
                        );
                      })}

                    </div>
                  </div>
                </div>
                <div className="box-trending-text">

                  <h2 className="heading-text text-overflow">
                    {trend.name}
                  </h2>
                  <div className={`truncate-small-3 text-trending`}>
                    <p className="text-desc">
                      {trend.description}
                    </p>
                  </div>
                  <div className="see-more">
                    <span className="text-see-more">
                      Xem tất cả sản phẩm
                    </span>
                    <CaretDown className="icon-see-more" />
                  </div>

                </div>

              </Link>
            );
          })}
        </div>
        <div className={styles.loadingMoreIcon}>
          {isloadingMore && <Loading />}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TrendingList);
