import React from 'react';

export default function OrderIcon(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>
      <path d="M27.292 15.496l-10.8-10.8c-0.431-0.43-1.027-0.696-1.684-0.696-0.003 0-0.006 0-0.008 0h-8.4c-1.32 0-2.4 1.080-2.4 2.4v8.4c0 0.66 0.264 1.26 0.708 1.704l10.8 10.8c0.432 0.432 1.032 0.696 1.692 0.696 0.004 0 0.008 0 0.012 0 0.658 0 1.253-0.271 1.679-0.708l8.4-8.4c0.437-0.427 0.708-1.022 0.708-1.68 0-0.004 0-0.009-0-0.013v0.001c0-0.66-0.276-1.272-0.708-1.704zM8.2 10c-0.996 0-1.8-0.804-1.8-1.8s0.804-1.8 1.8-1.8c0.996 0 1.8 0.804 1.8 1.8s-0.804 1.8-1.8 1.8z" />
    </svg>
  );
}
