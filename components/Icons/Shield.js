import React from 'react';

export default function Shield(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M24,7,10,13.18v9.27C10,31,16,39.05,24,41c8-1.95,14-10,14-18.55V13.18ZM20.89,31.73l-6.22-6.18,2.19-2.18,4,4L31.14,17.17l2.19,2.19Z" />
        </g>
      </g>
    </svg>
  );
}
