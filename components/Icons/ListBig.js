import React from 'react';

export default function ListBig(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M8,28h8V20H8ZM8,38h8V30H8ZM8,18h8V10H8ZM18,28H42V20H18Zm0,10H42V30H18Zm0-28v8H42V10Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
