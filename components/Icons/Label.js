import React from 'react';

export default function Label(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 338.83 48"
      width="338.83"
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path
            data-name="Shape 656 copy 2"
            d="M338.08,21.36,327.31,2.52A5.36,5.36,0,0,0,322.79,0H5.43C2.43,0,0,3.53,0,6.66V42.34A5.55,5.55,0,0,0,5.43,48H322.79a5.39,5.39,0,0,0,4.52-2.52l10.77-17.84A7,7,0,0,0,338.08,21.36Zm-15.24,7.72a4.58,4.58,0,1,1,4.4-4.76c0,.06,0,.12,0,.18A4.49,4.49,0,0,1,322.84,29.08Z"
          />
        </g>
      </g>
    </svg>
  );
}
