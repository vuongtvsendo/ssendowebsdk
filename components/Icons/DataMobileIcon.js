import React from 'react';

export default function DataMobileIcon(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>
      <title>DataMobileIcon</title>
      <path d="M13.333 2.667c7.636 0.007 13.827 5.997 13.827 13.38h-2.513c0-6.045-5.067-10.947-11.313-10.947v-2.433zM13.333 12.397c2.087 0 3.777 1.636 3.771 3.649h-3.771v-3.649zM13.333 7.532c4.859 0.007 8.8 3.82 8.8 8.515h-2.515c0-3.357-2.816-6.081-6.285-6.081v-2.433z" />
      <path d="M10.519 28.617h-6.519v-2.101h4.347v-2.103h-4.347v-2.103h4.347v-2.103h-4.347v-2.103h6.519v10.512zM17.036 22.311h2.173v6.307h-6.519v-10.511h6.519v2.103h-4.347v6.307h2.173v-4.204z" />
    </svg>
  );
}
