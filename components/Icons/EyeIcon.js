import React from 'react';

export default function EyeIcon(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={512}
      height={512}
      viewBox="0 0 512 512"
      {...props}>
      <path d="M256 96c-106.666 0-197.76 66.346-234.667 160 36.907 93.654 128 160 234.667 160s197.76-66.346 234.667-160c-36.906-93.654-128-160-234.667-160zM256 362.666c-58.88 0-106.666-47.786-106.666-106.666s47.786-106.666 106.666-106.666 106.666 47.786 106.666 106.666-47.786 106.666-106.666 106.666zM256 192c-35.413 0-64 28.587-64 64s28.587 64 64 64 64-28.587 64-64-28.587-64-64-64z" />
    </svg>
  );
}
