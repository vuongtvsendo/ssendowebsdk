import MobileIcon from './Mobile';
import AllCate from './AllCate';
import ArrowLeft from './ArrowLeft';
import ArrowRight from './ArrowRight';
import ArrowTopRight from './ArrowRight';
import AvatarFull from './AvartFull';
import Avatar from './Avatar';
import Back from './Back';
import BackToTop from './BackToTop';
import BagIcon from './BagIcon';
import BookIcon from './BookIcon';
import Camera from './Camera';
import CameraIcon from './CameraIcon';
import CaretDown from './CaretDown';
import CaretUp from './CaretUp';
import CarIcon from './CarIcon';
import Cart from './Cart';
import Check from './Check';
import CloseCircle from './CloseCircle';
import Coin from './Coin';
import Confirm from './Confirm';
import Delivery from './Delivery';
import DupCheck from './DupCheck';
import ElectricIcon from './ElectricIcon';
import Facebook from './Facebook';
import FashionStuffIcon from './FashionStuffIcon';
import Filter from './Filter';
import FortyEight from './FortyEight';
import FruitIcon from './FruitIcon';
import Google from './Google';
import Grid from './Grid';
import Hamberger from './Hamberger';
import Hand from './Hand';
import Heart from './Heart';
import HeartFull from './HeartFull';
import HomeStuffIcon from './HomeStuffIcon';
import Hotsale from './Hotsale';
import HouseDecoIcon from './HouseDecoIcon';
import HouseIcon from './HouseIcon';
import IconAddScreen from './IconAddScreen';
import IconBackToTop from './IconBackToTop';
import IconCalendar from './IconCalendar';
import IconCamera from './IconCamera';
import IconClose from './IconClose';
import IconDelete from './IconDelete';
import IconDiamond from './IconDiamond';
import IconDownload from './IconDownload';
import IconFb from './IconFb';
import IconGooglePlus from './IconGooglePlus';
import IconHistory from './IconHistory';
import IconHome from './IconHome';
import IconIdea from './IconIdea';
import IconLogin from './IconLogin';
import IconLogout from './IconLogout';
import IconMoreVert from './IconMoreVert';
import IconSearch from './IconSearch';
import IconShare from './IconShare';
import IconShareIos from './IconShareIos';
import IconStarStroke from './IconStarStroke';
import IconTicketCheck from './IconTicketCheck';
import Info from './Info';
import Install from './Install';
import IsBrandIcon from './IsBrandIcon';
import Label from './Label';
import LaptopIcon from './LaptopIcon';
import LifeIcon from './LifeIcon';
import List from './List';
import ListBig from './ListBig';
import Logo from './Logo';
import Lotus from './Lotus';
import Map from './Map';
import MeatIcon from './MeatIcon';
import MedicIcon from './MedicIcon';
import MenFashionIcon from './MenFashionIcon';
import Minus from './Minus';
import Mobile from './Mobile';
import MomBabyIcon from './MomBabyIcon';
import MoreHor from './MoreHor';
import OrderIcon from './OrderIcon';
import PerfumeIcon from './PerfumeIcon';
import Phone from './Phone';
import Play from './Play';
import Plus from './Plus';
import Process from './Process';
import Promotion from './Promotion';
import Rating from './Rating';
import ReputationIcon from './ReputationIcon';
import Revert from './Revert';
import Ruler from './Ruler';
import Search from './Search';
import Send from './Send';
import ServiceIcon from './ServiceIcon';
import Seven from './Seven';
import Shield from './Shield';
import Shipping from './Shipping';
import ShoeIcon from './ShoeIcon';
import Shop from './Shop';
import ShopPlus from './ShopPlus';
import SoundDeviceIcon from './SoundDeviceIcon';
import SportIcon from './SportIcon';
import Star from './Star';
import Stop from './Stop';
import TechStuffIcon from './TechStuffIcon';
import TiviIcon from './TiviIcon';
import ToyIcon from './ToyIcon';
import Trend from './Trend';
import WatchIcon from './WatchIcon';
import WomenFashionIcon from './WomenFashionIcon';
import DataMobileIcon from './DataMobileIcon';
import GameCartIcon from './GameCartIcon';
import MobileCardIcon from './MobileCardIcon';
import MobileIconMoney from './MobileIconMoney';
import SenpayIcon from './SenpayIcon';
import EyeIcon from './EyeIcon';
import LogoFullIcon from './LogoFullIcon';
import ResendIcon from './ResendIcon';

export default {
  MobileIcon,
  AllCate,
  ArrowLeft,
  ArrowRight,
  ArrowTopRight,
  AvatarFull,
  Avatar,
  Back,
  BackToTop,
  BagIcon,
  BookIcon,
  Camera,
  CameraIcon,
  CaretDown,
  CaretUp,
  CarIcon,
  Cart,
  Check,
  CloseCircle,
  Coin,
  Confirm,
  Delivery,
  DupCheck,
  ElectricIcon,
  Facebook,
  FashionStuffIcon,
  Filter,
  FortyEight,
  FruitIcon,
  Google,
  Grid,
  Hamberger,
  Hand,
  Heart,
  HeartFull,
  HomeStuffIcon,
  Hotsale,
  HouseDecoIcon,
  HouseIcon,
  IconAddScreen,
  IconBackToTop,
  IconCalendar,
  IconCamera,
  IconClose,
  IconDelete,
  IconDiamond,
  IconDownload,
  IconFb,
  IconGooglePlus,
  IconHistory,
  IconHome,
  IconIdea,
  IconLogin,
  IconLogout,
  IconMoreVert,
  IconSearch,
  IconShare,
  IconShareIos,
  IconStarStroke,
  IconTicketCheck,
  Info,
  Install,
  IsBrandIcon,
  Label,
  LaptopIcon,
  LifeIcon,
  List,
  ListBig,
  Logo,
  Lotus,
  Map,
  MeatIcon,
  MedicIcon,
  MenFashionIcon,
  Minus,
  Mobile,
  MomBabyIcon,
  MoreHor,
  OrderIcon,
  PerfumeIcon,
  Phone,
  Play,
  Plus,
  Process,
  Promotion,
  Rating,
  ReputationIcon,
  Revert,
  Ruler,
  Search,
  Send,
  ServiceIcon,
  Seven,
  Shield,
  Shipping,
  ShoeIcon,
  Shop,
  ShopPlus,
  SoundDeviceIcon,
  SportIcon,
  Star,
  Stop,
  TechStuffIcon,
  TiviIcon,
  ToyIcon,
  Trend,
  WatchIcon,
  WomenFashionIcon,
  DataMobileIcon,
  GameCartIcon,
  MobileCardIcon,
  MobileIconMoney,
  SenpayIcon,
  EyeIcon,
  LogoFullIcon,
  ResendIcon,
};
