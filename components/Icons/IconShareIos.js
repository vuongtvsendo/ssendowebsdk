import React from 'react';

export default function IconShareIos(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={32}
      viewBox="0 0 25 32"
      {...props}>
      <path d="M12.522 0l0.671 0.664 4.696 4.637-0.671 0.662-4.1-4.049v18.357h-1.192v-18.357l-4.099 4.049-0.671-0.662 5.366-5.301zM15.503 8.945h8.348v22.657h-22.66v-22.656h8.348v1.192h-7.154v20.273h20.274v-20.274h-7.155v-1.192z" />
    </svg>
  );
}
