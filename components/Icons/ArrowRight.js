import React from 'react';

export default function ArrowRight(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M18.44,30.89,25.31,24l-6.87-6.89L20.56,15l9,9-9,9Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
