import React from 'react';

export default function Rating(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 113.98 17"
      width="113.98"
      height={17}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <polygon points="8.99 0 11.77 5.6 17.98 6.49 13.48 10.85 14.54 17 8.99 14.1 3.43 17 4.49 10.85 0 6.49 6.21 5.6 8.99 0" />
          <polygon points="32.99 0 35.77 5.6 41.98 6.49 37.48 10.85 38.54 17 32.99 14.1 27.43 17 28.49 10.85 24 6.49 30.21 5.6 32.99 0" />
          <polygon points="56.99 0 59.77 5.6 65.98 6.49 61.48 10.85 62.54 17 56.99 14.1 51.43 17 52.49 10.85 48 6.49 54.21 5.6 56.99 0" />
          <polygon points="80.99 0 83.77 5.6 89.98 6.49 85.48 10.85 86.54 17 80.99 14.1 75.43 17 76.49 10.85 72 6.49 78.21 5.6 80.99 0" />
          <polygon points="104.99 0 107.77 5.6 113.98 6.49 109.48 10.85 110.54 17 104.99 14.1 99.43 17 100.49 10.85 96 6.49 102.21 5.6 104.99 0" />
        </g>
      </g>
    </svg>
  );
}
