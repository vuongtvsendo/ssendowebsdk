import React from 'react';

export default function IconMoreVert(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M24,16a4,4,0,1,0-4-4A4,4,0,0,0,24,16Zm0,4a4,4,0,1,0,4,4A4,4,0,0,0,24,20Zm0,12a4,4,0,1,0,4,4A4,4,0,0,0,24,32Z" />
        </g>
      </g>
    </svg>
  );
}
