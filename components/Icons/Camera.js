import React from 'react';

export default function Camera(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H24V24H0Z" fill="none" />
          <path d="M9.55,10.46H14a1,1,0,0,1,1.1,1c0,.38,0,.75,0,1.13s.1.33.34.29a3.07,3.07,0,0,0,.5-.12l3.65-1.22c.29-.1.41,0,.41.29q0,3.19,0,6.37c0,.27-.09.33-.35.26l-4-.92-.08,0c-.44-.06-.5,0-.51.42a4,4,0,0,1-.05.78A1,1,0,0,1,14,19.5H5.22A1.08,1.08,0,0,1,4,18.32c0-.83,0-1.66,0-2.5q0-2.06,0-4.12a1.38,1.38,0,0,1,.22-.8A.92.92,0,0,1,5,10.46Z" />
          <path d="M12.51,9.55A2.53,2.53,0,1,1,15.11,7,2.55,2.55,0,0,1,12.51,9.55Z" />
          <path d="M9.18,7A2.55,2.55,0,0,1,6.59,9.55,2.56,2.56,0,0,1,4,7,2.59,2.59,0,0,1,9.18,7Z" />
        </g>
      </g>
    </svg>
  );
}
