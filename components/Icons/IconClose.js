import React from 'react';

export default function IconClose(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>
      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M38,12.82,35.18,10,24,21.18,12.82,10,10,12.82,21.18,24,10,35.18,12.82,38,24,26.82,35.18,38,38,35.18,26.82,24Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
