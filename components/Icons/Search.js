import React from 'react';

export default function Search(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M31,28H29.42l-.56-.54a13,13,0,1,0-1.4,1.4l.54.56V31L38,41l3-3ZM19,28a9,9,0,1,1,9-9A9,9,0,0,1,19,28Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
