import React from 'react';

export default function Check(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <polygon points="40 15.46 36.37 11.99 18.56 28.95 11.78 22.48 8 26.09 17.77 35.41 17.79 35.4 18.42 36.01 40 15.46" />
        </g>
      </g>
    </svg>
  );
}
