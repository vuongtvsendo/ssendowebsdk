import React from 'react';

export default function MobileCardIcon(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>
      <title>MobileCardIcon</title>
      <path d="M25.333 9.333h-10.667c-0.001 0-0.003 0-0.004 0-0.73 0-1.323 0.592-1.323 1.323 0 0.004 0 0.008 0 0.011v-0.001l-0.007 8c0 0.74 0.593 1.333 1.333 1.333h10.667c0.74 0 1.333-0.593 1.333-1.333v-8c0-0.74-0.593-1.333-1.333-1.333zM25.333 18.667h-10.667v-4h10.667v4zM25.333 12h-10.667v-1.333h10.667v1.333z" />
      <path d="M21.333 8h-2.461v-1.697h-11.077v16.971h11.077v-1.94h2.461v4.969c0 1.673-1.379 3.031-3.077 3.031h-9.845c-1.699 0-3.077-1.357-3.077-3.031v-20.605c0-1.673 1.379-3.031 3.077-3.031h9.845c1.699 0 3.077 1.357 3.077 3.031v2.303zM13.333 28.12c1.021 0 1.847-0.811 1.847-1.817 0-1.005-0.825-1.817-1.847-1.817s-1.847 0.812-1.847 1.817c0 1.007 0.825 1.819 1.847 1.819z" />
    </svg>
  );
}
