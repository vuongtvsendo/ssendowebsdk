import React from 'react';

export default function ArrowTopRight(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M33.9,36.73,16.69,19.52v15.8h-4V12.69H35.31v4H19.52L36.73,33.9Z" />
        </g>
      </g>
    </svg>
  );
}
