import React from 'react';

export default function Coin(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={48} height={48} fill="none" />
          <path d="M24,4A20,20,0,1,0,44,24,20,20,0,0,0,24,4Zm0,38.08A18.08,18.08,0,1,1,42.08,24,18.08,18.08,0,0,1,24,42.08Z" />
          <path d="M36.08,11.92a17.08,17.08,0,1,0,5,12.08A17,17,0,0,0,36.08,11.92ZM25.71,35.28v3.31H22.23V35.1a10.55,10.55,0,0,1-5-1.62l-.46-.29,1.28-3.65.75.52a8.52,8.52,0,0,0,4.58,1.39,3.65,3.65,0,0,0,2.67-.93,2.26,2.26,0,0,0,.7-1.68,4.12,4.12,0,0,0-3.48-3.42,7.65,7.65,0,0,1-6.15-6.61A6.15,6.15,0,0,1,22.29,13V9.42h3.42v3.36A9.74,9.74,0,0,1,30,14.06l.52.29L29.19,18l-.75-.41a7.6,7.6,0,0,0-4-1.1,3.13,3.13,0,0,0-2.26.75,2,2,0,0,0-.58,1.39c0,1.28.87,2,3.83,3.25a7.42,7.42,0,0,1,5.8,7A6.49,6.49,0,0,1,25.71,35.28Z" />
        </g>
      </g>
    </svg>
  );
}
