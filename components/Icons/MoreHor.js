import React from 'react';

export default function MoreHor(props) {
  return (
    <svg viewBox="0 0 48 48" width={48} height={48} {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M12,20a4,4,0,1,0,4,4A4,4,0,0,0,12,20Zm24,0a4,4,0,1,0,4,4A4,4,0,0,0,36,20ZM24,20a4,4,0,1,0,4,4A4,4,0,0,0,24,20Z" />
        </g>
      </g>
    </svg>
  );
}
