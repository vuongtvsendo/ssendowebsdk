import React from 'react';

export default function Filter(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={48} height={48} fill="none" />
          <path d="M27.51,38.26a.66.66,0,0,1-.4.71.67.67,0,0,1-.78-.22L22.7,35.1l-1.9-1.9a1,1,0,0,1-.32-.77V21.77a.83.83,0,0,0-.26-.65L9.34,10.25l-.16-.16A.61.61,0,0,1,9,9.41.64.64,0,0,1,9.6,9H38.17a1.36,1.36,0,0,1,.38,0,.62.62,0,0,1,.32,1,3,3,0,0,1-.25.26L27.78,21.12a.83.83,0,0,0-.27.65V38.16" />
        </g>
      </g>
    </svg>
  );
}
