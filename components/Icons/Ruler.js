import React from 'react';

export default function Ruler(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M42,12H6a4,4,0,0,0-4,4V32a4,4,0,0,0,4,4H42a4,4,0,0,0,4-4V16A4,4,0,0,0,42,12Zm0,20H6V16h4v8h4V16h4v8h4V16h4v8h4V16h4v8h4V16h4Z" />
        </g>
      </g>
    </svg>
  );
}
