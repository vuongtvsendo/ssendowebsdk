import React from 'react';

export default function Hamberger(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M6,36H42V32H6ZM6,26H42V22H6ZM6,12v4H42V12Z" />
        </g>
      </g>
    </svg>
  );
}
