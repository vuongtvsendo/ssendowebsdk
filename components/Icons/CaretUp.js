import React from 'react';

export default function CaretUp(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M17,14,12,9,7,14Z" />
          <path d="M24,24H0V0H24Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
