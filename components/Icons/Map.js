import React from 'react';

export default function Map(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={24} height={24} fill="none" />
          <path d="M17.93,8.26a6.1,6.1,0,0,0-6.05-5c-.42.06-.84.1-1.25.19A6,6,0,0,0,6,9.09a6.79,6.79,0,0,0,.55,2.64A35.54,35.54,0,0,0,11.28,20c.15.21.34.39.51.58a.24.24,0,0,0,.42,0,4.08,4.08,0,0,0,.32-.35c.73-1,1.49-2.06,2.17-3.14a29.5,29.5,0,0,0,2.81-5.54A6.38,6.38,0,0,0,17.93,8.26ZM12,11.39a2.13,2.13,0,1,1,2.12-2.11A2.12,2.12,0,0,1,12,11.39Z" />
        </g>
      </g>
    </svg>
  );
}
