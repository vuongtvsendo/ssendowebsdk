import React from 'react';

export default function List(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path
            d="M24,16"
            fill="none"
            stroke="#000"
            stroke-miterlimit={10}
            stroke-width={2}
          />
          <rect width={48} height={48} fill="none" />
          <rect x={16} y={14} width={24} height={4} />
          <rect x={16} y={22} width={24} height={4} />
          <rect x={16} y={30} width={24} height={4} />
          <rect x={8} y={14} width={4} height={4} />
          <rect x={8} y={22} width={4} height={4} />
          <rect x={8} y={30} width={4} height={4} />
        </g>
      </g>
    </svg>
  );
}
