import React from 'react';

export default function Avatar(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M24,4A20,20,0,1,0,44,24,20,20,0,0,0,24,4Zm0,6a6,6,0,1,1-6,6A6,6,0,0,1,24,10Zm0,28.4A14.4,14.4,0,0,1,12,32c.06-4,8-6.16,12-6.16S35.94,28,36,32A14.4,14.4,0,0,1,24,38.4Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
