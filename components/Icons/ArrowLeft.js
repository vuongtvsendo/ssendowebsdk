import React from 'react';

export default function ArrowLeft(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>
      <path d="M8 16.667l14-14 2.667 2.667-11.333 11.333 11.333 11.333-2.667 2.667z" />
    </svg>
  );
}
