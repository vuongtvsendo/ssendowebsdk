import React from 'react';

export default function IconDelete(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>

      <path d="M23.999 9.333h-16v16c0 1.472 1.195 2.667 2.668 2.667h10.665c1.475 0 2.667-1.195 2.667-2.667v-16zM19.332 3.999h-6.667l-1.332 1.335h-3.335c-0.735 0-1.332 0.597-1.332 1.332v1.335h18.667v-1.335c0-0.735-0.597-1.332-1.335-1.332h-3.332l-1.335-1.335z" />
    </svg>
  );
}
