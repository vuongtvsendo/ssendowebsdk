import React from 'react';

export default function HeartFull(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={48} height={48} fill="none" />
          <path d="M38.16,10.08c-6.93-5.19-14.16,4-14.16,4s-7.23-9.2-14.16-4C1.42,16.39,7.16,29.38,24,39.5,40.84,29.38,46.58,16.39,38.16,10.08Z" />
        </g>
      </g>
    </svg>
  );
}
