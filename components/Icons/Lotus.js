import React from 'react';

export default function Lotus(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H24V24H0Z" fill="none" />
          <path d="M20.89,14a5.77,5.77,0,0,0-2.8,0,9.67,9.67,0,0,1-5.61,3.06A6.74,6.74,0,0,0,20.56,16,8.86,8.86,0,0,1,22,14.38,5.46,5.46,0,0,1,20.89,14Z" />
          <path d="M5.92,14.06a5.77,5.77,0,0,0-2.8,0A5.46,5.46,0,0,1,2,14.38,8.89,8.89,0,0,1,3.44,16a6.74,6.74,0,0,0,8.08,1.15A9.66,9.66,0,0,1,5.92,14.06Z" />
          <path d="M18.78,9.59a9.73,9.73,0,0,1-.19-2,4.75,4.75,0,0,1-.71.27A6.33,6.33,0,0,0,15,9.77a8.35,8.35,0,0,1,.42,3.71,4.41,4.41,0,0,1-1,2.21A7.46,7.46,0,0,0,17.91,13,5,5,0,0,0,18.78,9.59Z" />
          <path d="M12.73,11.82a9.54,9.54,0,0,1-2.44-3.65,8.36,8.36,0,0,0-1.13,5A3.9,3.9,0,0,0,12,16.41a4.22,4.22,0,0,0,1.64-1A3.47,3.47,0,0,0,12.73,11.82Z" />
          <path d="M12,6A6.68,6.68,0,0,0,10.8,7.32a9.38,9.38,0,0,0,2.42,3.89,3.78,3.78,0,0,1,1.33,2.63,4.41,4.41,0,0,0,.1-.48C15.06,10.66,13.21,7.13,12,6Z" />
          <path d="M8.85,9.54A6.89,6.89,0,0,0,6.12,7.91a4.73,4.73,0,0,1-.71-.27,9.66,9.66,0,0,1-.19,2A5,5,0,0,0,6.1,13a7.4,7.4,0,0,0,3.38,2.65A4.73,4.73,0,0,1,8.4,13.26,8.16,8.16,0,0,1,8.85,9.54Z" />
        </g>
      </g>
    </svg>
  );
}
