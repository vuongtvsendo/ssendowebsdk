import React from 'react';

export default function BackToTop(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={48} height={48} fill="none" />
          <path d="M16,22h6V42h4V22h6l-8-8ZM8,6v4H40V6Z" />
        </g>
      </g>
    </svg>
  );
}
