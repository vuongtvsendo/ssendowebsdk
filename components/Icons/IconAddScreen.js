import React from 'react';

export default function IconAddScreen(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>
      <path d="M24 1.347l-13.333-0.013c-1.467 0-2.667 1.2-2.667 2.667v4h2.667v-1.333h13.333v18.667h-13.333v-1.333h-2.667v4c0 1.467 1.2 2.667 2.667 2.667h13.333c1.467 0 2.667-1.2 2.667-2.667v-24c0-1.467-1.2-2.653-2.667-2.653zM13.333 20h2.667v-9.333h-9.333v2.667h4.787l-7.453 7.453 1.88 1.88 7.453-7.453v4.787z" />
    </svg>
  );
}
