import React from 'react';

export default function DupCheck(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M34.26,14.92,31.84,12.5,20.93,23.37l2.43,2.42Zm7.29-2.42L23.35,30.65l-7.19-7.15-2.43,2.42,9.62,9.59L44,14.92ZM4,25.91l9.62,9.59L16,33.08l-9.6-9.59Z" />
        </g>
      </g>
    </svg>
  );
}
