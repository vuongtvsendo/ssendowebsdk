import React from 'react';

export default function Info(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H24V24H0Z" fill="none" />
          <path d="M11,17h2V11H11Zm0-8h2V7H11Z" />
          <path d="M19.6,4.4A10.75,10.75,0,1,0,22.75,12,10.72,10.72,0,0,0,19.6,4.4ZM18.54,18.54A9.25,9.25,0,1,1,21.25,12,9.22,9.22,0,0,1,18.54,18.54Z" />
        </g>
      </g>
    </svg>
  );
}
