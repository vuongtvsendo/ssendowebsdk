import React from 'react';

export default function Stop(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 128 128"
      width={128}
      height={128}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path
            d="M64,42.67"
            fill="none"
            stroke="#000"
            stroke-miterlimit={10}
            stroke-width={2}
          />
          <rect width={128} height={128} fill="none" />
          <rect width={128} height={128} fill="none" />
          <path d="M117.33,64h-2.67A50.67,50.67,0,1,1,99.83,28.17,50.5,50.5,0,0,1,114.67,64H120a56,56,0,1,0-56,56,56,56,0,0,0,56-56Z" />
          <rect x={48} y="42.67" width="10.67" height="42.67" />
          <rect x="69.33" y="42.67" width="10.67" height="42.67" />
        </g>
      </g>
    </svg>
  );
}
