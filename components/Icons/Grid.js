import React from 'react';

export default function Grid(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M6,20H20V6H6Z" />
          <path d="M40,8V18H30V8H40m2-2H28V20H42V6Z" />
          <path d="M18,30V40H8V30H18m2-2H6V42H20V28Z" />
          <path d="M28,42H42V28H28Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
