import React from 'react';

export default function CloseCircle(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M24,4A20,20,0,1,0,44,24,20,20,0,0,0,24,4ZM34,31.18,31.18,34,24,26.82,16.82,34,14,31.18,21.18,24,14,16.82,16.82,14,24,21.18,31.18,14,34,16.82,26.82,24Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
