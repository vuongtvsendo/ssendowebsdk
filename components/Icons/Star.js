import React from 'react';

export default function Star(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <rect width={24} height={24} fill="none" />
          <polygon points="12.29 3.5 15.07 9.1 21.28 9.99 16.79 14.35 17.85 20.5 12.29 17.6 6.74 20.5 7.8 14.35 3.31 9.99 9.52 9.1 12.29 3.5" />
        </g>
      </g>
    </svg>
  );
}
