import React from 'react';

export default function CaretDown(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      width={24}
      height={24}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M7,10l5,5,5-5Z" />
          <path d="M0,0H24V24H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
