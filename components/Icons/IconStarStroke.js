import React from 'react';

export default function IconStarStroke(props) {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}>

      <path d="M16.444 6.536l-3.105 6.616-7.051 1.076 5.105 5.234-1.193 7.318 6.243-3.452 6.246 3.452-1.194-7.319 5.104-5.234-7.050-1.076-3.106-6.616zM16.444 4.444l3.708 7.9 8.292 1.266-5.999 6.151 1.416 8.683-7.418-4.1-7.416 4.1 1.416-8.683-5.999-6.151 8.291-1.266 3.709-7.9z" />
    </svg>
  );
}
