import React from 'react';

export default function Back(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>
      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M0,0H48V48H0Z" fill="none" />
          <path d="M40,22H15.66L26.84,10.82,24,8,8,24,24,40l2.82-2.82L15.66,26H40Z" />
        </g>
      </g>
    </svg>
  );
}
