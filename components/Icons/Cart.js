import React from 'react';

export default function Cart(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 48 48"
      width={48}
      height={48}
      {...props}>

      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M14,36a4,4,0,1,0,4,4A4,4,0,0,0,14,36ZM2,4V8H6l7.2,15.18-2.7,4.9A3.87,3.87,0,0,0,10,30a4,4,0,0,0,4,4H38V30H14.84a.5.5,0,0,1-.5-.5l.06-.24L16.2,26H31.1a4,4,0,0,0,3.5-2.06l7.16-13A2,2,0,0,0,42,10a2,2,0,0,0-2-2H10.42L8.54,4ZM34,36a4,4,0,1,0,4,4A4,4,0,0,0,34,36Z" />
          <path d="M0,0H48V48H0Z" fill="none" />
        </g>
      </g>
    </svg>
  );
}
