import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { AllCateAccordion } from './index';

describe('<AllCateAccordion /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      categories: [],
      isSiteMap: true,
    };
    const wrapper = shallow(<AllCateAccordion {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(AllCateAccordion);
  });
  it('It should render category url one level by key', () => {
    const context = {
      ...ctx,
    };
    const props = {
      categories: [
        [
          {
            id: 1663,
            path: '1/2/1663',
            image:
              'https://media3.scdn.vn/images/ecom/category/cong-nghe-1663.jpg',
            display_order: 0,
            title: 'Điện thoại - Máy tính',
            url_key: 'cong-nghe',
            url_path: 'cong-nghe/',
            child: [
              {
                id: 1664,
                path: '1/2/1663/1664',
                image:
                  'https://media3.scdn.vn/images/ecom/category/thiet-bi-di-dong-1664.jpg',
                display_order: 0,
                title: 'Điện thoại',
                url_key: 'thiet-bi-di-dong',
                url_path: 'cong-nghe/thiet-bi-di-dong/',
                child: [],
              },
            ],
          },
          {
            id: 8,
            path: '1/2/8',
            image:
              'https://media3.scdn.vn/images/ecom/category/thoi-trang-nu-8.jpg',
            display_order: 2,
            title: 'Thời trang nữ',
            url_key: 'thoi-trang-nu',
            url_path: 'thoi-trang-nu/',
            child: [
              {
                id: 2629,
                path: '1/2/8/2629',
                image:
                  'https://media3.scdn.vn/images/ecom/category/thoi-trang-thiet-ke-2629.jpg',
                display_order: 0,
                title: 'Thời trang thiết kế',
                url_key: 'thoi-trang-thiet-ke',
                url_path: 'thoi-trang-nu/thoi-trang-thiet-ke/',
                child: [],
              },
            ],
          },
        ],
      ],
      isSiteMap: true,
    };
    const wrapper = shallow(<AllCateAccordion {...props} />, {
      context,
    });
    expect(wrapper.find('Link').at(0).props().to).toEqual('/thiet-bi-di-dong/');
    expect(wrapper.find('Link').at(1).props().to).toEqual(
      '/thoi-trang-thiet-ke/'
    );
  });
});
