import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import Collapse from 'reactstrap/lib/Collapse';
import classnames from 'classnames';
import styles from './allCateAccordion.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import defaultImg from 'sendo-web-sdk/images/default.png';
import { IconCategories } from 'sendo-web-sdk/res/index_category_list';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import _filter from 'lodash/filter';
export class AllCateAccordion extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cateActiveId: 0,
    };
  }
  toggleAccordion(id) {
    if (id === this.state.cateActiveId) {
      this.setState({ cateActiveId: 0 });
    } else {
      this.setState({ cateActiveId: id });
    }
  }
  isActive(id) {
    return this.state.cateActiveId === id;
  }
  handleParentOnclick(e) {
    e.preventDefault();
    return false;
  }
  render() {
    const { categories, isSiteMap, smallFontSize } = this.props;
    return (
      <div className={styles.allCateAccordion}>
        {categories.length > 0 &&
          categories.map((group, i) => {
            return (
              <div className={styles.groupCate} key={i}>
                <div className={styles.groupParents}>
                  {group.length > 0 &&
                    group.map(parentCate => {
                      const cateBoxClass = classnames({
                        [styles.cateBox]: true,
                        [styles.active]: this.isActive(parentCate.id),
                      });
                      const cateButtonClass = classnames({
                        'btn-primary btn-cate-accordion': true,
                        active: this.isActive(parentCate.id),
                      });
                      if (!parentCate.id) {
                        return null;
                      }
                      if (isSiteMap) {
                        return (
                          <div className={styles.item} key={parentCate.id}>
                            <div
                              className={cateBoxClass}
                              onClick={() =>
                                this.toggleAccordion(parentCate.id)}>
                              <img
                                className="lazyload"
                                src={defaultImg}
                                data-src={parentCate.image}
                                alt={parentCate.title}
                              />
                              <h2 className={styles.cateTitle}>
                                <a
                                  onClick={e => this.handleParentOnclick(e)}
                                  href={normalizeUrl(
                                    parentCate.url_path,
                                    true
                                  )}>
                                  {parentCate.title}
                                </a>
                              </h2>
                            </div>
                          </div>
                        );
                      } else {
                        const Icon = _filter(
                          IconCategories,
                          ic => ic.url_key === parentCate.url_key
                        )[0];
                        let IconComponent = '';
                        if (Icon) IconComponent = Icon.icon;
                        return (
                          <div className={styles.item} key={parentCate.id}>
                            <Link
                              className={cateButtonClass}
                              to={normalizeUrl(parentCate.url_key)}>
                              {IconComponent &&
                                <IconComponent className="svg-btn-cate-accordion icon" />}
                              <span className="text-overflow">
                                {parentCate.title}
                              </span>
                            </Link>
                          </div>
                        );
                      }
                    })}
                </div>
                {isSiteMap &&
                  <div className={styles.groupChild}>
                    {group.length > 0 &&
                      group.map(parentCate => {
                        if (!parentCate.id) {
                          return null;
                        }
                        return (
                          <Collapse
                            isOpen={this.isActive(parentCate.id)}
                            key={parentCate.id}>
                            <div className={styles.collapseInner}>
                              {parentCate.child.length > 0 &&
                                parentCate.child.map(child => {
                                  return (
                                    <div className={styles.item} key={child.id}>
                                      <Link
                                        className={`link-box-cate ${smallFontSize}`}
                                        to={normalizeUrl(child.url_key)}>
                                        <span className="truncate-medium-2">
                                          {child.title}
                                        </span>
                                      </Link>
                                    </div>
                                  );
                                })}
                            </div>
                          </Collapse>
                        );
                      })}
                  </div>}
              </div>
            );
          })}
      </div>
    );
  }
}

export default withStyles(styles)(AllCateAccordion);
