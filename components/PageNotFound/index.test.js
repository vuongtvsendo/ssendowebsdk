import React from 'react';
import { shallow, mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Router } from 'react-router-dom';
import { getInstance as getInstanceHistory } from 'sendo-web-sdk/helpers/history';
import PageNotFound from './index';

const history = getInstanceHistory();

describe('<PageNotFound /> component', () => {
  it('It should allow to click to go home page', () => {
    const context = {
      ...ctx,
      router: {
        route: {
          location: {
            search: '?sortType=rank',
          },
        },
      },
      history: {},
    };
    const wrapper = shallow(<PageNotFound header="Test header 1" />, {
      context,
    });
    expect(wrapper.text()).toEqual('<PageNotFound />');
  });
  // it('It should show correct text', () => {
  //   const context = {
  //     router: {
  //       route: {
  //         location: {
  //           search: '?sortType=rank',
  //         },
  //       },
  //     },
  //     history: {},
  //   };
  //   const wrapper = mount(
  //     <Router history={history}>
  //       <PageNotFound header="Test header 1" />
  //     </Router>,
  //     {
  //       context,
  //     }
  //   );
  //   expect(wrapper.find('.btn-primary').text()).toEqual('/');
  // });
});
