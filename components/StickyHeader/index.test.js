import React from 'react';
import { mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { StickyHeader } from './index';

describe('<StickyHeader /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = mount(<StickyHeader {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(StickyHeader);
    expect(wrapper.ref('sticky').exists()).toEqual(true);
  });
});
