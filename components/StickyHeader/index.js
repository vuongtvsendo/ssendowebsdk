import React, { Component } from 'react';
import { compose } from 'redux';
import _get from 'lodash/get';
import styles from './stickyHeader.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';
import withScrollEvent from 'sendo-web-sdk/helpers/withScrollEvent';
import { connect } from 'react-redux';
export class StickyHeader extends Component {
  element = {};
  state = {
    isHide: false,
    isFixed: false,
  };
  onScroll() {
    if (!_get(this.props, 'smartbanner.isShow', false)) {
      return false;
    }
    this.handleFixed();
  }
  handleFixed() {
    window.requestAnimationFrame(() => {
      if (this.isFixed()) {
        if (!this.state.isFixed) this.setState({ isFixed: true });
      } else {
        if (this.state.isFixed) this.setState({ isFixed: false });
      }
    });
  }
  isFixed() {
    if (!_get(this.props, 'smartbanner.isShow', false)) {
      return true;
    } else {
      if (window.scrollY >= _get(this.props, 'smartbanner.height')) {
        return true;
      }
    }
    return false;
  }
  componentDidMount() {
    this.element = _get(this, 'refs.sticky');
    if (this.element) {
      if (this.isFixed()) this.setState({ isFixed: true });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.smartbanner !== nextProps.smartbanner) {
      this.handleFixed();
    }
  }
  render() {
    let classes = classnames({
      [styles.stickyHeader]: true,
      [styles.hide]: this.state.isHide,
      [styles.fixed]: this.state.isFixed,
    });
    return (
      <div className={classes} ref="sticky">
        {this.props.children}
      </div>
    );
  }
}

export default compose(
  connect(state => {
    return {
      smartbanner: state.smartbanner,
    };
  }),
  withStyles(styles),
  withScrollEvent
)(StickyHeader);
