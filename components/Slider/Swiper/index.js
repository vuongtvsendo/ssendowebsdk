// import Swiper from 'swiper/src/sendo-web-sdk/components/core/core-class';

// // Core Modules
// import Device from 'swiper/src/modules/device/device';
// import Support from 'swiper/src/modules/support/support';
// import Browser from 'swiper/src/modules/browser/browser';
// import Resize from 'swiper/src/modules/resize/resize';
// import Observer from 'swiper/src/modules/observer/observer';

// //IMPORT_COMPONENTS
// import Pagination from 'swiper/src/sendo-web-sdk/components/pagination/pagination';
// import Navigation from 'swiper/src/sendo-web-sdk/components/navigation/navigation';
// import Lazy from 'swiper/src/sendo-web-sdk/components/lazy/lazy';

// Swiper.components = [
//   Device,
//   Support,
//   Browser,
//   Resize,
//   Observer,
//   Pagination,
//   Navigation,
//   Lazy,
//   //INSTALL_COMPONENTS
// ];
import Swiper from 'swiper/dist/js/swiper';
export default Swiper;
