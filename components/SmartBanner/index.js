import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './smartBanner.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import _get from 'lodash/get';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as smartBannerActions from 'sendo-web-sdk/actions/smartbanner';
import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';
import logo from './logo.png';
import { deviceInfo, isInApp } from 'sendo-web-sdk/helpers/common';
import * as cookie from 'sendo-web-sdk/helpers/cookie';
const COOKIE_CLOSED = 'sb-closed';
const COOKIE_INSTALLED = 'sb-installed';

const expiredDate = days => {
  let expires = new Date();
  expires.setDate(expires.getDate() + days);
  return expires;
};

class SmartBanner extends Component {
  static defaultProps = {
    daysHidden: 15,
    daysReminder: 90,
    button: 'Mở',
    title: 'Sendo.vn',
    subTitle: '- Mua sắm đảm bảo',
    description: 'Mua rẻ hơn trên app Sendo.vn',
    link:
      'https://www.sendo.vn/apps/?utm_source=SendoPwa&utm_medium=SmartBanner&utm_term=installment',
  };

  constructor(props) {
    super(props);
    this.state = { isShow: this.isValidate() };
  }
  componentDidMount() {
    if (this.state.isShow) {
      this.show();
    }
  }
  isDeviceValidate() {
    if (deviceInfo().isAndroid()) return true;
    if (!deviceInfo().isSafariIos()) return true;
    return false;
  }
  isValidate() {
    if (
      __SERVER__ ||
      !this.isDeviceValidate() ||
      isInApp() ||
      cookie.get(COOKIE_CLOSED) ||
      cookie.get(COOKIE_INSTALLED)
    ) {
      return false;
    }
    return true;
  }
  hide = () => {
    this.setState({ isShow: false });
  };

  show = () => {
    setTimeout(() => {
      let height = _get(this, 'refs.smartbanner.clientHeight');
      this.props.smartBannerActions.showSmartBanner({ height: height });
    });
  };

  close = () => {
    this.hide();
    cookie.set(COOKIE_CLOSED, 'true', {
      httpOnly: false,
      expires: expiredDate(this.props.daysHidden),
    });
    this.props.smartBannerActions.closeSmartBanner();
    if (this.props.onClose && typeof this.props.onClose === 'function') {
      this.props.onClose();
    }
  };

  install = () => {
    this.hide();
    cookie.set(COOKIE_INSTALLED, 'true', {
      httpOnly: false,
      expires: expiredDate(this.props.daysReminder),
    });
    this.props.smartBannerActions.closeSmartBanner();
    if (this.props.onInstall && typeof this.props.onInstall === 'function') {
      this.props.onInstall();
    }
  };

  render() {
    const smartBannerClass = classnames({
      [styles.smartbanner]: true,
      [styles.show]: this.state.isShow,
    });
    return (
      <div className={smartBannerClass} ref="smartbanner">
        <div className={styles.container}>
          <button
            className={`${styles.btnClose} btn-icon-small`}
            onClick={this.close} aria-label="Button Close Smart Banner">
            <CloseCircle className="icon" />
          </button>
          <div className={styles.mainContent}>
            <img src={logo} className={styles.logo} alt="Sendo.vn" />
            <div className={styles.info}>
              <p className={styles.title}>
                <strong>
                  {this.props.title}
                </strong>
                {this.props.subTitle}
              </p>
              <p className="text-desc text-small">
                {this.props.description}
              </p>
            </div>
          </div>
          <a
            href={this.props.link}
            onClick={this.install}
            className={`${styles.btnOpen} btn-primary`}>
            {this.props.button}
          </a>
        </div>
      </div>
    );
  }
}

export default connect(null, dispatch => {
  return {
    smartBannerActions: bindActionCreators(smartBannerActions, dispatch),
  };
})(withStyles(styles)(SmartBanner));
