import React, { PureComponent } from 'react';
import classnames from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import * as productActions from 'sendo-web-sdk/actions/product';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';
import Loading from 'sendo-web-sdk/components/Loading';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Send from 'sendo-web-sdk/components/Icons/Send';
import styles from './commentInput.css';

export class CommentInputForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '', message: '' };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  postComment = () => {
    const {
      lengthMin,
      lengthMax,
      messageLengthMin,
      messageLengthMax,
      comment,
      cbSendComment,
      identity,
    } = this.props;
    if (this.state.value.length < lengthMin) {
      this.setState({
        message: messageLengthMin,
      });
      return;
    }
    if (this.state.value.length > lengthMax) {
      this.setState({
        message: messageLengthMax,
      });
    }
    let newComment = {
      product_id: comment.product_id,
      content: this.state.value,
      owner_id: identity.customer_id,
      parent_id: comment.comment_id,
    };
    cbSendComment(newComment);
    this.setState({ value: '' });
  };

  render() {
    const posting = _get(this.props, 'product.comment_post_loading');
    const inputClass = classnames({
      [styles.commentInput]: true,
      [styles.posting]: posting,
    });
    const { ariaLabel, placeholder } = this.props;
    const { value, message } = this.state;
    return (
      <div className={inputClass}>
        <div className={styles.inner}>
          <input
            className={styles.input}
            type="text"
            aria-label={ariaLabel}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}
          />
          <LoginWrapper event="click" callback={this.postComment}>
            <button
              className={styles.btnSend}
              aria-label="Send"
              ref={ref => (this.btnSubmit = ref)}>
              <Send className="icon" />
            </button>
          </LoginWrapper>
          {posting &&
            <div className={styles.loading}>
              <Loading small />
            </div>}
        </div>
        <p className={styles.message}>{message}</p>
      </div>
    );
  }
}

CommentInputForm.defaultProps = {
  ariaLabel: 'Question for this product',
  placeholder: 'Đặt câu hỏi cho sản phẩm?',
  lengthMin: 5,
  lengthMax: 1000,
  messageLengthMin: 'Bạn phải nhập tối thiểu 5 ký tự',
  messageLengthMax: 'Bạn chỉ được phép nhập tối đa 1,000 ký tự',
};

CommentInputForm.propTypes = {
  ariaLabel: PropTypes.string,
  placeholder: PropTypes.string,
  lengthMin: PropTypes.number,
  lengthMax: PropTypes.number,
  messageLengthMin: PropTypes.string,
  messageLengthMax: PropTypes.string,
  comment: PropTypes.object.isRequired,
  cbSendComment: PropTypes.func.isRequired,
};
export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(CommentInputForm));
