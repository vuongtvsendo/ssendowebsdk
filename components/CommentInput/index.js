import React, { PureComponent } from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as auth from 'sendo-web-sdk/helpers/auth';
import classnames from 'classnames';
import _get from 'lodash/get';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';
import * as toastActions from 'sendo-web-sdk/actions/toast';

import styles from './commentInput.css';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';
import Loading from 'sendo-web-sdk/components/Loading';
import Send from 'sendo-web-sdk/components/Icons/Send';

export class CommentInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '', message: '', posting: false, disableForm: false };
    this.handleChange = this.handleChange.bind(this);
    this.postComment = this.postComment.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }
  postComment() {
    const { product, identity, parentId } = this.props;
    if (this.input.value.length < 5) {
      this.setState({
        message: 'Bạn phải nhập tối thiểu 5 ký tự',
      });
      return;
    }
    if (this.input.value.length > 1000) {
      this.setState({
        message: 'Bạn chỉ được phép nhập tối đa 1,000 ký tự',
      });
    }
    //@todo Nen move logic ve customer id vo action
    const param = {
      product_id: product.id,
      comment_data: this.input.value,
      admin_id: product.admin_id,
      customer_id: auth.getUserId(identity),
      parent_id: parentId || 0,
    };
    this.setState({ posting: true, disableForm: true });
    this.props.productActions.postProductComment(param).then(data => {
      this.setState({ posting: false, disableForm: false });
      if (_get(data, 'payload.commentPost.err_code') === 0) {
        if (
          parseInt(
            _get(data, 'payload.commentPost.comment_data.parent_id'),
            10
          ) === 0
        ) {
          let scroller = document.getElementById('productCommentModal');
          if (scroller) {
            scroller.scrollTop = 0;
          }
        }
      }
      if (_get(data, 'payload.commentPost.message')) {
        this.props.toastActions.openToast({
          content: _get(data, 'payload.commentPost.message'),
        });
      }
    });

    this.setState({ value: '' });
  }
  handleFocus(e) {
    if (this.props.focusWithBodyFixed) {
      document.body.classList.add('fixed');
    }
    this.setState({ message: '' });
  }
  handleBlur(e) {
    if (this.props.focusWithBodyFixed) {
      document.body.classList.remove('fixed');
    }
  }
  render() {
    const inputClass = classnames({
      [styles.commentInput]: true,
      [styles.posting]: this.state.posting,
    });
    return (
      <div className={inputClass}>
        <div onSubmit={this.handleSubmit} className={styles.inner}>
          {!this.state.disableForm &&
            <input
              className={styles.input}
              type="text"
              aria-label="Question for this product"
              placeholder="Đặt câu hỏi cho sản phẩm?"
              value={this.state.value}
              ref={ref => (this.input = ref)}
              onChange={this.handleChange}
              onFocus={e => this.handleFocus(e)}
              onBlur={e => this.handleBlur(e)}
            />}
          {!this.state.disableForm &&
            <LoginWrapper event="click" callback={this.postComment}>
              <button
                className={styles.btnSend}
                aria-label="Send"
                ref={ref => (this.btnSubmit = ref)}>
                <Send className="icon" />
              </button>
            </LoginWrapper>}
          {this.state.posting &&
            <div className={styles.loading}>
              <Loading small />
            </div>}
        </div>
        <p className={styles.message}>{this.state.message}</p>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(CommentInput));
