import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { CommentInput } from './index';

describe('<CommentInput /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<CommentInput />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(CommentInput);
  });
});
