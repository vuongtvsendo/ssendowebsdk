import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Search } from './index';
import Loading from 'sendo-web-sdk/components/Loading';

describe('<Search /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      keywords: [],
    };
    const wrapper = shallow(<Search {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(Search);
  });
  it('It should render Loading in renderTrending', () => {
    const context = {
      ...ctx,
    };
    const props = {
      keywords: [],
      trending: [],
      loadingSearchTrending: true,
    };
    const wrapper = shallow(<Search {...props} />, {
      context,
    });
    expect(wrapper.find(Loading).exists()).toEqual(true);
  });
  it('It should render Chưa có dữ liệu in renderTrending', () => {
    const context = {
      ...ctx,
    };
    const props = {
      keywords: [],
      trending: [],
      loadingSearchTrending: false,
    };
    const wrapper = shallow(<Search {...props} />, {
      context,
    });
    expect(wrapper.find('p').text()).toEqual('Chưa có dữ liệu!');
  });
});
