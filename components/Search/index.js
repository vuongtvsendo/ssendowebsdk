import React from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Modal from 'reactstrap/lib/Modal';
import Highlighter from 'react-highlight-words';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import qs from 'query-string';
import styles from './search.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import {
  buildSearchUrl,
  normalizeUrl,
  mergeQueryUrl,
  buildShopSearchLink,
} from 'sendo-web-sdk/helpers/url';
import ArrowLeft from 'sendo-web-sdk/components/Icons/ArrowLeft';
import Shop from 'sendo-web-sdk/components/Icons/Shop';
import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';
import IconHistory from 'sendo-web-sdk/components/Icons/IconHistory';
import IconSearch from 'sendo-web-sdk/components/Icons/IconSearch';
import Loading from 'sendo-web-sdk/components/Loading';
import * as searchActions from 'sendo-web-sdk/actions/search';
import { MAX_CATEGORY_NAME_SEARCH_LENGTH } from 'sendo-web-sdk/helpers/const';
import { LABEL_RELATED_TAGS } from 'sendo-web-sdk/res/text';
import _get from 'lodash/get';
import _debounce from 'lodash/debounce';
import _string from 'lodash/string';
import ModalParent from 'sendo-web-sdk/components/Modal';
export class Search extends ModalParent {
  static contextTypes = {
    router: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      isCateSearch: false,
      isShopSearch: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._handleChange = _debounce(this._handleChange.bind(this), 250);
    this.wHeight = _get(window, 'innerHeight', '100%') - 58;
  }
  modalOnOpened() {
    /**
     * If the title is not in the cate, the user should be at the search page
     * So the title will be the keyword on the search page
     */
    if (!this.props.category) {
      this.setState({ value: this.props.title });
      this._handleChange(this.props.title);
    } else {
      this.setState({ isCateSearch: true }); //cate search
    }
    this.setState({ isShopSearch: this.isShopContext() });
    setTimeout(() => {
      this.inputSearch && this.inputSearch.focus();
    });
    this.props.searchActions.getSearchTrending();
  }
  onBack() {
    this.props.toggleSearch();
  }
  onClear() {
    this.setState({ value: '' });
    this.props.searchActions.clearSearchSuggestion();
  }
  toggleCateSearch() {
    this.setState({ isCateSearch: !this.state.isCateSearch });
  }
  toggleShopSearch() {
    this.setState({ isShopSearch: !this.state.isShopSearch });
  }
  handleChange(e) {
    this.setState({ value: e.target.value });
    let inputKeyword = (e.target.value || '').trim();
    this._handleChange(inputKeyword);
  }
  _handleChange(inputKeyword) {
    if (
      this.isShopContext() &&
      _get(this.props, 'shopInfo.shop_name') &&
      this.state.isShopSearch
    ) {
      this.props.searchActions.clearSearchSuggestion();
      return;
    }
    if (!inputKeyword || inputKeyword === '') {
      this.props.searchActions.clearSearchSuggestion();
      return;
    }
    if (inputKeyword.length > 3) {
      this.props.searchActions.getSearchSuggestions({
        q: inputKeyword,
      });
    }
  }
  handleClick(keyword) {
    this.props.closeSearch();
    const { history } = this.context.router;
    const url = buildSearchUrl({ q: keyword });
    this.props.searchActions.addSearchRecentKeyword(keyword);
    history.push(mergeQueryUrl(url, this.props.shopAds));
  }
  handleClickCategory(keyword, category) {
    this.props.closeSearch();
    const { history } = this.context.router;
    const url = buildSearchUrl({
      q: keyword,
      category_id: category.cate_id,
    });
    this.props.searchActions.addSearchRecentKeyword(keyword);
    history.push(mergeQueryUrl(url, this.props.shopAds));
  }
  isShopContext() {
    const shopAlias = _get(this.context, 'router.route.match.params.shopAlias');
    if (!shopAlias) return false;
    return true;
  }
  handleSubmit(e) {
    e.preventDefault();

    if (!this.state.value) {
      return false;
    }

    this.props.closeSearch();
    /**
     |-----------------------------------------------
     | @note: Co 2 truong hop submit cai search nay
     | 1. Trong truong hop search trong cate
     | 2. La search ngoai cate
     |-----------------------------------------------
     */
    const { history } = this.context.router;
    let search = {
      ...qs.parse(history.location.search),
      q: this.state.value,
    };
    // if (this.props.category && this.state.isCateSearch) {
    //   search = { ...search, sortType: 'rank' };
    //   history.replace({
    //     search: qs.stringify(search),
    //   });
    // } else {
    if (this.isShopContext() && this.state.isShopSearch) {
      const shopAlias = _get(
        this.context,
        'router.route.match.params.shopAlias'
      );
      let url = buildShopSearchLink(
        { q: this.state.value, sortType: 'rank' },
        shopAlias
      );
      history.push(url);
    } else if (this.props.category && this.state.isCateSearch) {
      history.replace({
        search: qs.stringify({ ...search, sortType: 'rank' }),
      });
    } else {
      let url = buildSearchUrl({ q: this.state.value, sortType: 'rank' });
      history.push(mergeQueryUrl(url, this.props.shopAds));
    }
    this.props.searchActions.addSearchRecentKeyword(this.state.value);
    return false;
  }
  renderKeywordSuggestions(suggestions, query) {
    const keywords = _get(suggestions, '[0].list');
    if (!keywords) {
      return;
    }
    return keywords.map(keyword => {
      return (
        <li className={styles.item} key={keyword.q}>
          <div onClick={() => this.handleClick(keyword.q)}>
            <Highlighter
              highlightClassName={styles.highlight}
              highlightTag="strong"
              searchWords={[query]}
              autoEscape={true}
              textToHighlight={keyword.q}
            />
          </div>
        </li>
      );
    });
  }

  renderCategorySuggestions(suggestions, query) {
    const categories = _get(suggestions, '[2].list');
    if (!categories) {
      return;
    }
    return categories.map(category => {
      return (
        <li className={styles.item} key={category.cate_name}>
          <div onClick={() => this.handleClickCategory(query, category)}>
            <span>
              <strong className={styles.highlight}>{query}</strong>
              &nbsp;trong&nbsp;
              <strong className={styles.categoryHighlight}>
                {category.cate_name}
              </strong>
            </span>
          </div>
        </li>
      );
    });
  }

  renderRecentKeywords(keywords) {
    if (!keywords.length) {
      return;
    }
    return keywords.map(keyword => {
      return (
        <li className={styles.item} key={keyword}>
          <div
            className={styles.recentKeywords}
            onClick={() => this.handleClick(keyword)}>
            <IconHistory className="icon" />
            <span>{keyword}</span>
          </div>
        </li>
      );
    });
  }

  renderShopSuggestions(suggestions) {
    const shops = _get(suggestions, '[3].list');
    if (!shops) {
      return;
    }
    return shops.map(shop => {
      const url = normalizeUrl('shop/' + shop.username);
      return (
        <div className={styles.tab} key={shop.username}>
          <Link to={url}>
            <Shop className="icon" />
            {shop.q}
          </Link>
        </div>
      );
    });
  }

  renderCateSearch(category) {
    if (!category) {
      return false;
    }
    let filterBtnClass = classnames({
      [styles.tabCate]: true,
      [styles.disabled]: !this.state.isCateSearch,
    });
    let cateName = _string.truncate(category.category_name, {
      length: MAX_CATEGORY_NAME_SEARCH_LENGTH,
      separator: ' ',
    });
    if (cateName.length) {
      return (
        <div className={filterBtnClass} onClick={() => this.toggleCateSearch()}>
          <span>{cateName}</span>
          <CloseCircle className="icon" />
        </div>
      );
    }
    return false;
  }

  renderShopSearch(shopInfo) {
    if (!shopInfo) {
      return false;
    }
    let filterBtnClass = classnames({
      [styles.tabCate]: true,
      [styles.disabled]: !this.state.isShopSearch,
    });
    let shopName = _string.truncate(shopInfo.shop_name, {
      length: MAX_CATEGORY_NAME_SEARCH_LENGTH,
      separator: ' ',
    });
    if (shopName.length) {
      return (
        <div className={filterBtnClass} onClick={() => this.toggleShopSearch()}>
          <span>{shopName}</span>
          <CloseCircle className="icon" />
        </div>
      );
    }
    return false;
  }

  renderTrending(trending, loadingSearchTrending) {
    if (!loadingSearchTrending && !_get(trending, 'length', 0))
      return (
        <div className={styles.loading}>
          <p>Chưa có dữ liệu!</p>
        </div>
      );
    if (loadingSearchTrending) {
      return (
        <div className={styles.loading}>
          <Loading />
        </div>
      );
    }
    return trending.map(trend => {
      return (
        <div
          className={styles.tab + ' ' + styles.noIcon}
          key={trend.q}
          onClick={() => this.handleClick(trend.q)}>
          {trend.q}
        </div>
      );
    });
  }
  renderRelatedKeywords(suggestions) {
    const related_keywords = _get(suggestions, '[1].list', []);
    if (!related_keywords) return false;
    return related_keywords.map(keyword => {
      return (
        <div
          className={styles.tab + ' ' + styles.noIcon}
          key={keyword.q}
          onClick={() => this.handleClick(keyword.q)}>
          {keyword.q}
        </div>
      );
    });
  }

  render() {
    const {
      isOpen,
      suggestions,
      category,
      shopInfo,
      keywords,
      trending,
      loadingSearchTrending,
    } = this.props;
    const keywordSuggestion = this.renderKeywordSuggestions(
      suggestions,
      this.state.value
    );
    const cateSuggestion = this.renderCategorySuggestions(
      suggestions,
      this.state.value
    );
    const recentKeywords = this.renderRecentKeywords(keywords);
    const shopSuggestion = this.renderShopSuggestions(suggestions);
    const searchTrending = this.renderTrending(trending, loadingSearchTrending);
    const relatedKeywords = this.renderRelatedKeywords(suggestions);
    return (
      <Modal
        className={styles.modalDialog + ' modal-full modal-rtl'}
        contentClassName={styles.modalContent}
        isOpen={isOpen}
        onEnter={() => this.modalOnOpened()}
        toggle={() => this.props.toggleSearch()}>
        <header className={styles.header + ' m-b-medium box-shadow'}>
          <button className="btnIcon" onClick={() => this.onBack()}>
            <ArrowLeft className="icon" />
          </button>
          <form className={styles.form} onSubmit={this.handleSubmit}>
            <input
              type="search"
              className={styles.input}
              ref={ref => (this.inputSearch = ref)}
              placeholder="Bạn mua gì hôm nay?"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </form>
          {this.state.value &&
            this.state.value.length > 0 && (
              <button className="btnIcon" onClick={() => this.onClear()}>
                <CloseCircle className="icon" />
              </button>
            )}
          {(!this.state.value || this.state.value.length === 0) &&
            !category &&
            !this.isShopContext() && <IconSearch className="icon" />}
          {!this.isShopContext() && this.renderCateSearch(category)}
          {this.isShopContext() && this.renderShopSearch(shopInfo)}
        </header>
        <div ref="scroller" className={styles.container}>
          <div className={styles.inner}>
            {keywordSuggestion && (
              <div className="panel panel-content">
                <ul>{keywordSuggestion}</ul>
              </div>
            )}
            {!keywordSuggestion &&
              recentKeywords && (
                <div className="panel panel-content">
                  <ul>{recentKeywords}</ul>
                </div>
              )}
            {cateSuggestion && (
              <div className="panel panel-content">
                <ul>{cateSuggestion}</ul>
              </div>
            )}
            {shopSuggestion && (
              <div className={'panel panel-content ' + styles.shopPanel}>
                {shopSuggestion}
              </div>
            )}
            {searchTrending && (
              <div className="panel">
                <div className="panel-heading">Xu hướng tìm kiếm</div>
                <div className={'panel-content ' + styles.shopPanel}>
                  {searchTrending}
                </div>
              </div>
            )}
            {relatedKeywords &&
              relatedKeywords.length > 0 && (
                <div className="panel">
                  <div className="panel-heading">{LABEL_RELATED_TAGS}</div>
                  <div className={'panel-content ' + styles.shopPanel}>
                    {relatedKeywords}
                  </div>
                </div>
              )}
          </div>
        </div>
      </Modal>
    );
  }
}

export default connect(
  state => {
    return {
      suggestions: state.search.suggestions,
      keywords: state.search.keywords,
      trending: state.search.trending,
      loadingSearchTrending: state.search.loading,
    };
  },
  dispatch => {
    return {
      searchActions: bindActionCreators(searchActions, dispatch),
    };
  }
)(withStyles(styles)(Search));
