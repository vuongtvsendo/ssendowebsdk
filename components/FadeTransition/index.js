import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './fade-transition.css';

class FadeTranstion extends Component {
  render() {
    const { children, isActive } = this.props;
    const content = React.Children.only(children);

    return (
      <CSSTransitionGroup
        transitionEnterTimeout={300}
        transitionLeaveTimeout={300}
        transitionAppearTimeout={300}
        transitionAppear={true}
        transitionName={{
          enter: styles.enter,
          enterActive: styles.enterActive,
          leave: styles.leave,
          leaveActive: styles.leaveActive,
        }}>
        {isActive ? content : null}
      </CSSTransitionGroup>
    );
  }
}

FadeTranstion.defaultProps = {
  timeout: 250,
};

export default withStyles(styles)(FadeTranstion);
