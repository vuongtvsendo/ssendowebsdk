import React, { PureComponent } from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import PropTypes from 'prop-types';
import styles from './ripple.css';
import _isElement from 'lodash/isElement';
import _get from 'lodash/get';
export class Ripple extends PureComponent {
  handleClick = event => {
    // incase it is a link
    event.preventDefault();

    // create fake element
    const rippleElement = document.createElement('div');
    const elementRect = event.currentTarget.getBoundingClientRect();

    const containerLeft =
      elementRect.left +
      window.pageXOffset -
      document.documentElement.clientLeft;
    const containerTop =
      elementRect.top + document.body.scrollTop - document.body.clientTop;

    const elementSize = Math.max(elementRect.width, elementRect.height);
    rippleElement.style.left =
      event.pageX - containerLeft - elementSize / 2 + 'px';
    rippleElement.style.top =
      event.pageY - containerTop - elementSize / 2 + 'px';

    rippleElement.style.width = elementSize + 'px';
    rippleElement.style.height = elementSize + 'px';

    const onAnimationEnd = () => {
      rippleElement.removeEventListener('webkitAnimationEnd', onAnimationEnd);
      rippleElement.removeEventListener('animationend', onAnimationEnd);
      rippleElement.parentNode &&
        rippleElement.parentNode.removeChild(rippleElement);

      // callback when done
      this.onFinished();

      if (_isElement(this.containerElement)) {
        this.containerElement.classList.remove(styles.overflow);
      }
    };

    rippleElement.addEventListener('webkitAnimationEnd', onAnimationEnd, false);
    rippleElement.addEventListener('animationend', onAnimationEnd, false);
    _isElement(rippleElement) && rippleElement.classList.add(styles.ripple);

    if (_isElement(this.containerElement)) {
      this.containerElement.classList.add(styles.overflow);
      this.containerElement.appendChild(rippleElement);
    }
  };

  onFinished() {
    const { to, replace } = this.props;
    if (to) {
      const { history } = this.context.router;
      if (/^https?:\/\//.test(to)) {
        return (window.location.href = to);
      }
      if (replace) {
        return history.replace(to);
      } else {
        return history.push(to);
      }
    }
    this.props.onClick && this.props.onClick();
  }

  render() {
    const { tagName: TagName, to } = this.props;
    const ariaLabel = _get(this.props, 'aria-label');
    let additionalAttrs = {};

    if (TagName === 'a') {
      additionalAttrs.href = to;
    }
    if (ariaLabel) {
      additionalAttrs['aria-label'] = ariaLabel;
    }

    return (
      <TagName
        ref={node => (this.containerElement = node)}
        className={this.props.className}
        onClick={this.handleClick}
        {...additionalAttrs}>
        {this.props.children}
      </TagName>
    );
  }
}

Ripple.defaultProps = {
  className: '',
  tagName: 'div',
};

Ripple.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
      replace: PropTypes.func.isRequired,
      createHref: PropTypes.func.isRequired,
    }).isRequired,
  }),
};

export default withStyles(styles)(Ripple);
