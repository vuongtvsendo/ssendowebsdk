import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { StarRating } from './index';

describe('<StarRating /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<StarRating {...props} />, {
      context,
    });
    expect(wrapper.find('Rating')).toHaveLength(2);
  });
});
