import React from 'react';
import styles from './starRating.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Rating from '../Icons/Rating';

export const StarRating = props => {
  const classColor = 'star-' + Math.round(props.value / 20);
  const type = props.type;
  return (
    <div className={styles.stars}>
      <Rating className={styles.bgd + ' ' + styles[type]} />
      <div className={styles.active} style={{ width: props.value + '%' }}>
        <Rating
          className={`${styles.value} ${styles[classColor]} ${styles[type]}`}
        />
      </div>
    </div>
  );
};

export default withStyles(styles)(StarRating);
