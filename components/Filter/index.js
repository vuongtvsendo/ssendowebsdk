import React from 'react';
import PropTypes from 'prop-types';
import styles from './filter.css';
import _get from 'lodash/get';
import _filter from 'lodash/filter';
import _remove from 'lodash/remove';
import debounce from 'lodash/debounce';
import classnames from 'classnames';
import qs from 'query-string';
import Modal from 'reactstrap/lib/Modal';
import ModalBody from 'reactstrap/lib/ModalBody';
import Collapse from 'reactstrap/lib/Collapse';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import FilterBar from 'sendo-web-sdk/components/Filter/FilterBar';
import FilterCheckbox from 'sendo-web-sdk/components/Filter/FilterCheckbox';
import FilterPrice from 'sendo-web-sdk/components/Filter/FilterPrice';
import FilterTag from 'sendo-web-sdk/components/Filter/FilterTag';
import IconClose from 'sendo-web-sdk/components/Icons/IconClose';
import ModalParent from 'sendo-web-sdk/components/Modal';
import { CLASS_MODAL_IGNORE_SCROLL } from 'sendo-web-sdk/helpers/const';

export class Filter extends ModalParent {
  static contextTypes = {
    router: PropTypes.object,
  };
  /**
   * @deprecated Can chuyen tu duy ve Redux
   */
  constructor(props, context) {
    super(props, context);
    this.state = {
      collapseState: [],
      wHeight: _get(window, 'innerHeight', '100vh'),
    };
    this.handleResize = debounce(this.handleResize.bind(this), 100);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }
  handleResize() {
    let wHeight = _get(window, 'innerHeight', '100vh');
    this.setState({ wHeight: wHeight });
  }
  toggleModal() {
    this.setState({ isModalOpen: !this.state.isModalOpen });
    if (this.state.isModalOpen) {
      this.props.revertToOrigin();
    }
    this.setOpenCollapse();
  }
  handleClick(filter) {
    var currentCollapseOpen = this.state.collapseState;
    if (currentCollapseOpen.indexOf(filter) >= 0) {
      let tempArr = _remove(currentCollapseOpen, function(item) {
        return item !== filter;
      });
      this.setState({ collapseState: tempArr });
    } else {
      this.setState({ collapseState: [...currentCollapseOpen, filter] });
    }
  }
  isCollapseOpen(filter) {
    return this.state.collapseState.indexOf(filter) >= 0;
  }
  setOpenCollapse() {
    const origin = qs.parse(_get(this.context, 'router.route.location.search'));
    let temp = [];
    for (var item in origin) {
      if (item === 'gtprice' || item === 'ltprice') {
        temp.push('levelPrice');
      } else {
        temp.push(item);
      }
    }
    this.setState({ collapseState: [...this.state.collapseState, ...temp] });
  }
  applyFilters() {
    // let search = qs.stringify(this.filters);
    // let router = this.context.router;
    // let { history } = router;
    // history.replace({
    //   search: search,
    // });
    this.props.applyFilters();
    this.toggleModal();
  }
  // revertToOrigin() {
  //   for (let p in this.filters) {
  //     delete this.filters[p];
  //   }
  //   const origin = qs.parse(_get(this.context, 'router.route.location.search'));
  //   for (let p in origin) {
  //     this.filters[p] = origin[p];
  //   }
  // }
  resetFilter() {
    this.props.resetFilter();
    this.setState({
      collapseState: [],
    });
    this.toggleModal();
  }

  getDistributeFilters(term) {
    let filters = _get(this.props, 'filters');
    switch (term) {
      case 'GeneralTerm':
        return _filter(
          filters,
          filter => filter.attribute_term === 'GeneralTerm'
        );
      case 'PriceTerm':
        return _filter(
          filters,
          filter => filter.attribute_term === 'PriceTerm'
        );
      default:
        return _filter(
          filters,
          filter =>
            filter.attribute_term !== 'PriceTerm' &&
            filter.attribute_term !== 'GeneralTerm'
        );
    }
  }

  render() {
    const PriceClasses = classnames({
      [styles.filterPanel]: true,
      [styles.active]: this.isCollapseOpen('levelPrice'),
    });
    return (
      <div>
        <Modal
          modalClassName={`modal-filter ${CLASS_MODAL_IGNORE_SCROLL}`}
          isOpen={this.state.isModalOpen}
          toggle={() => this.toggleModal()}>
          <ModalBody>
            <button
              className="btn-icon btn-close-menu"
              onClick={() => this.toggleModal()}>
              <IconClose className="icon" />
            </button>
            <div className={styles.sideBarFiler}>
              <div className={styles.filterHeading}>Bộ lọc sản phẩm</div>
              <div
                ref="scroller"
                className={styles.scrolling}
                style={{ height: this.state.wHeight }}>
                <div className={styles.filterPanel}>
                  <FilterCheckbox
                    items={this.getDistributeFilters('GeneralTerm')}
                  />
                </div>
                <div className={PriceClasses}>
                  <div
                    className={styles.panelTitle}
                    onClick={() => this.handleClick('levelPrice')}>
                    <h4 className={styles.title}>Lọc theo giá</h4>
                    <span className={styles.collapseCtrl} />
                  </div>
                  <Collapse isOpen={this.isCollapseOpen('levelPrice')}>
                    <FilterPrice
                      items={_get(
                        this.getDistributeFilters('PriceTerm'),
                        '[0].attribute_value',
                        []
                      )}
                    />
                  </Collapse>
                </div>
                {this.getDistributeFilters('OtherTerm').map((filter, i) => {
                  const classes = classnames({
                    [styles.filterPanel]: true,
                    [styles.active]: this.isCollapseOpen(filter.attribute_key),
                  });
                  return (
                    filter.attribute_value.length > 0 &&
                    <div className={classes} key={i}>
                      <div
                        className={styles.panelTitle}
                        onClick={() => this.handleClick(filter.attribute_key)}>
                        <h4 className={styles.title}>
                          {filter.attribute_name}
                        </h4>
                        <span className={styles.collapseCtrl} />
                      </div>
                      <Collapse
                        isOpen={this.isCollapseOpen(filter.attribute_key)}>
                        <div className={styles.collapseInner}>
                          <FilterTag
                            items={filter.attribute_value}
                            type={filter.attribute_term}
                            searchKey={filter.attribute_key}
                          />
                        </div>
                      </Collapse>
                    </div>
                  );
                })}
              </div>

              <div className={styles.filtersFooter}>
                <button
                  className={`${styles.btnFilter} btn-secondary`}
                  onClick={() => this.resetFilter()}>
                  Thiết lập lại
                </button>
                <button
                  className={`${styles.btnFilter} btn-primary`}
                  onClick={() => this.applyFilters()}>
                  Áp dụng
                </button>
              </div>
            </div>
          </ModalBody>
        </Modal>
        <FilterBar
          openModal={() => this.toggleModal()}
          quantity={this.props.numberFilterActive}
          listView={this.props.listView}
          toggleListView={this.props.toggleListView}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Filter);
