import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Filter } from './index';

describe('<Filter /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      filters: [
        { attribute_term: 'GeneralTerm', attribute_value: '' },
        { attribute_term: 'PriceTerm', attribute_value: '' },
        { attribute_term: 'OtherTerm', attribute_value: '' },
      ],
    };
    const wrapper = shallow(<Filter {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(Filter);
  });
});
