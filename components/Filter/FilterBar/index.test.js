import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { FilterBar } from './index';
import ListBig from 'sendo-web-sdk/components/Icons/ListBig';
import Grid from 'sendo-web-sdk/components/Icons/Grid';

describe('<FilterBar /> component', () => {
  let props = {
    listView: false,
  };
  it('it should be ok with ListBig', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<FilterBar {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterBar);
    expect(wrapper.find(ListBig).exists()).toEqual(true);
  });
  it('it should be ok with Grid', () => {
    const context = {
      ...ctx,
    };
    props.listView = true;
    const wrapper = shallow(<FilterBar {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterBar);
    expect(wrapper.find(Grid).exists()).toEqual(true);
  });
});
