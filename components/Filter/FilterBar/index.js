import React, { Component } from 'react';
import classnames from 'classnames';
import Ripple from 'sendo-web-sdk/components/Ripple';
import styles from './filterBar.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Filter from 'sendo-web-sdk/components/Icons/Filter';
import ListBig from 'sendo-web-sdk/components/Icons/ListBig';
import Grid from 'sendo-web-sdk/components/Icons/Grid';
// import withScrollEvent from 'sendo-web-sdk/helpers/withScrollEvent';

class FilterBar extends Component {
  // state = {
  //   isShow: true,
  // };
  // onScrollDown() {
  //   window.requestAnimationFrame(() => {
  //     this.setState({
  //       isShow: false,
  //     });
  //   });
  // }
  // onScrollUp() {
  //   window.requestAnimationFrame(() => {
  //     this.setState({
  //       isShow: true,
  //     });
  //   });
  // }
  render() {
    let classes = classnames({
      [styles.footerStikyBar]: true,
      [styles.footerStikyShow]: true, // this.state.isShow,
    });
    return (
      <div className={classes}>
        <div className={styles.filterBar}>
          <Ripple
            className={styles.filterCtrl}
            onClick={() => this.props.openModal()}>
            <Filter className={styles.iconFilter} />
            <span>Bộ lọc</span>
            {this.props.quantity > 0 &&
              <span className={styles.quantity}>{this.props.quantity}</span>}
          </Ripple>
          <div className={styles.viewType}>
            <Ripple
              tagName="button"
              className={styles.btnView}
              aria-label="Switch view type"
              onClick={this.props.toggleListView}>
              {this.renderIcon()}
            </Ripple>
          </div>
        </div>
      </div>
    );
  }
  renderIcon() {
    if (this.props.listView) {
      return <Grid className={styles.iconList} />;
    }
    return <ListBig className={styles.iconList} />;
  }
}
export { FilterBar };
export default withStyles(styles)(FilterBar);
