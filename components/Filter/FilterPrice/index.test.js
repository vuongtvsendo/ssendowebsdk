import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { FilterPrice } from './index';
import _identity from 'lodash/identity';

describe('<FilterPrice /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
      filters: {},
    };
    const props = {
      items: [{ name: 'item1' }, { name: 'item2' }, { name: 'item3' }],
      isSelectedItem: _identity,
    };
    const wrapper = shallow(<FilterPrice {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterPrice);
  });
  it('it should render TextPrice ok', () => {
    const context = {
      ...ctx,
      filters: {},
    };
    const props = {
      items: [
        {
          gtprice: -1,
          ltprice: '150000',
        },
        {
          gtprice: '150000',
          ltprice: '200000',
        },
        {
          gtprice: '200000',
          ltprice: '300000',
        },
        {
          gtprice: '300000',
          ltprice: '400000',
        },
        {
          gtprice: '400000',
          ltprice: '500000',
        },
        {
          gtprice: '500000',
          ltprice: -1,
        },
      ],
      isSelectedItem: _identity,
    };
    const wrapper = shallow(<FilterPrice {...props} />, {
      context,
    });
    expect(wrapper.find('.col-4').at(0).find('span').at(0).text()).toEqual(
      'Dưới 150k'
    );
    expect(wrapper.find('.col-4').at(1).find('span').at(0).text()).toEqual(
      '150k ~ 200k'
    );
    expect(
      wrapper
        .find('.col-4')
        .at(props.items.length - 1)
        .find('span')
        .at(0)
        .text()
    ).toEqual('Trên 500k');
  });
});
