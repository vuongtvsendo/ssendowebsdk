import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import classnames from 'classnames';
import styles from './filterPrice.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import withFilterContext from 'sendo-web-sdk/helpers/withFilterContext';
import * as helperFunction from 'sendo-web-sdk/helpers/common';

export class FilterPrice extends Component {
  /**
   * @deprecated Can chuyen tu duy ve Redux
   */
  static contextTypes = {
    filters: PropTypes.object,
  };
  onChangePrice(evt, typePrice) {
    var value = evt.target.value;
    const { setFilter } = this.props;
    setFilter(typePrice, value.replace(/[^0-9]/g, ''));
    return true;
  }
  onClickPrice(evt, value) {
    const { handleClickItem } = this.props;
    handleClickItem(value);
    return true;
  }
  renderTextPrice(item) {
    let textPrice = `${helperFunction.friendlyNumber(
      item.gtprice
    )} ~ ${helperFunction.friendlyNumber(item.ltprice)}`;
    if (item.gtprice === -1) {
      textPrice = `Dưới ${helperFunction.friendlyNumber(item.ltprice)}`;
    }
    if (item.ltprice === -1) {
      textPrice = `Trên ${helperFunction.friendlyNumber(item.gtprice)}`;
    }
    return textPrice;
  }
  render() {
    const { items, isSelectedItem } = this.props;
    const gtprice = this.context.filters.gtprice === -1
      ? ''
      : this.context.filters.gtprice || '';
    const ltprice = this.context.filters.ltprice === -1
      ? ''
      : this.context.filters.ltprice || '';
    return (
      <div>
        <div className={`${styles.inputGroupPrice} row`}>
          <div className="col-5">
            <input
              onChange={evt => this.onChangePrice(evt, 'gtprice')}
              className={`${styles.inputPrice}`}
              placeholder="Thấp nhất"
              value={gtprice}
            />
          </div>
          <span className={`${styles.text} col-2`}>đến</span>
          <div className="col-5">
            <input
              onChange={evt => this.onChangePrice(evt, 'ltprice')}
              className={`${styles.inputPrice}`}
              placeholder="cao nhất"
              value={ltprice}
            />
          </div>
        </div>
        {items.length > 0 &&
          <div className={`${styles.priceFilters} row`}>
            {items.map((item, i) => {
              const isSelected = isSelectedItem(item);
              const classes = classnames({
                [styles.chooseLabel]: true,
                [styles.selected]: isSelected,
              });
              return (
                <div
                  onClick={evt => this.onClickPrice(evt, item)}
                  className="col-4"
                  key={i}>
                  <span className={classes}>
                    {this.renderTextPrice(item)}
                  </span>
                </div>
              );
            })}
          </div>}
      </div>
    );
  }
}

export default compose(withStyles(styles), withFilterContext)(FilterPrice);
