import { Component } from 'react';
import qs from 'query-string';
import _get from 'lodash/get';

class Common extends Component {
  constructor(props, context) {
    super(props, context);
    this.countFilters = this.countFilters.bind(this);
  }
  checkPriceObj(search) {
    const ltprice = _get(search, 'ltprice');
    const gtprice = _get(search, 'gtprice');
    if (ltprice && !gtprice) {
      search['gtprice'] = -1;
    }
    if (!ltprice && gtprice) {
      search['ltprice'] = -1;
    }
    return search;
  }
  getOrigin() {
    return this.checkPriceObj(
      qs.parse(_get(this.context, 'router.route.location.search'))
    );
  }
  getSearch() {
    let search = this.checkPriceObj(qs.parse(this.props.location.search));
    let category_id = _get(this.props, 'metadata.category_id', 0);
    if (category_id) {
      search.category_id = category_id;
    }
    let url = _get(this.context, 'router.route.match.url', '');
    if (/\.htm$/.test(url)) {
      let qsRoute = _get(this.context, 'router.route.match.params', {});
      search = { ...search, ...qsRoute, is_htm: 1 };
    }
    return search;
  }
  applyFilters() {
    let filters = { ...this.filters };
    for (let p in filters) {
      if (filters.hasOwnProperty(p)) {
        if (filters[p] === '' || filters[p] === -1) {
          delete filters[p];
        }
      }
    }
    let search = qs.stringify(filters);
    let router = this.context.router;
    let { history } = router;
    history.replace({
      search: search,
    });
  }
  revertToOrigin() {
    for (let p in this.filters) {
      delete this.filters[p];
    }
    const origin = this.getOrigin(this.context);
    for (let p in origin) {
      this.filters[p] = origin[p];
    }
  }

  countFilters() {
    let total = 0;
    const origin = this.getOrigin(this.context);
    let filters = this.props.filters;
    let listKeys = {};
    filters.map(filter => {
      return (listKeys[filter.attribute_key] = true);
    });
    for (let p in origin) {
      if (listKeys.hasOwnProperty(p)) {
        total = total + 1;
      }
      /**
       * WARNING
       * Day la hard code
       * Gia dinh rang khi filter theo gia thi luon co 1 cap 'gtprice' và 'ltprice'
       * Nen chi can kiem tra ton tai mot phan tu
       */
      if (p === 'gtprice') {
        total = total + 1;
      }
    }
    return total;
  }

  resetFilter() {
    const origin = this.getOrigin(this.context);
    let filters = this.props.filters;
    let listKeys = {};
    filters.map(filter => {
      return (listKeys[filter.attribute_key] = true);
    });
    for (let p in origin) {
      if (listKeys.hasOwnProperty(p)) {
        delete origin[p];
      }
      if (p === 'gtprice' || p === 'ltprice') {
        delete origin[p];
      }
    }
    for (let p in this.filters) {
      delete this.filters[p];
    }
    let search = qs.stringify(origin);
    let router = this.context.router;
    let { history } = router;
    history.replace({
      search: search,
    });
  }
}

export default Common;
