import React, { Component } from 'react';
import { compose } from 'redux';
import classnames from 'classnames';
import styles from './filterTag.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import withFilterContext from 'sendo-web-sdk/helpers/withFilterContext';

import Check from 'sendo-web-sdk/components/Icons/Check';

export class FilterTag extends Component {
  render() {
    const arrSupportedTypes = [
      'BrandTerm',
      'ColorTerm',
      'DefaultTerm',
      'CategoryTerm',
    ];
    const { type, items } = this.props;
    if (arrSupportedTypes.indexOf(type) === -1) {
      return (
        <div>
          <label>
            {type} is not support.
          </label>
        </div>
      );
    }
    return (
      <div className={`${styles[type]} ${styles.groupOptions}`}>
        {items.map((item, index) => this.renderByType(item, index))}
      </div>
    );
  }
  renderByType(item, index) {
    const { type, searchKey, handleClickItem, isSelectedItem } = this.props;
    switch (type) {
      case 'BrandTerm': {
        const isSelected = isSelectedItem(item.option_id, searchKey);
        const classes = classnames({
          'choose-label': true,
          'choose-label-img': true,
          active: isSelected,
        });
        return (
          <div className={styles.item} key={index}>
            <span
              className={classes}
              onClick={() => handleClickItem(item.option_id, searchKey)}>
              <img
                className={styles.logo}
                src={item.background}
                alt={item.option_name}
              />
            </span>
          </div>
        );
      }
      case 'ColorTerm': {
        const isSelected = isSelectedItem(item.option_id, searchKey);
        const classes = classnames({
          'choose-label-circle': true,
          active: isSelected,
        });
        const bgd = item.color_hexRgb
          ? '#' + item.color_hexRgb
          : `url(${item.image})`;
        return (
          <div className={styles.item} key={index}>
            <span
              title={item.option_name}
              className={classes}
              style={{ background: bgd }}
              onClick={() => handleClickItem(item.option_id, searchKey)}
            />
          </div>
        );
      }
      case 'DefaultTerm':
      case 'CategoryTerm': {
        const isSelected = isSelectedItem(item.option_id, searchKey);
        const classes = classnames({
          'choose-label': true,
          active: isSelected,
        });
        return (
          <div className={styles.item} key={index}>
            <span
              className={classes}
              onClick={() => handleClickItem(item.option_id, searchKey)}>
              {item.option_name}
            </span>
          </div>
        );
      }

      case 'Checkbox':
        const isSelected = isSelectedItem(item.option_id, searchKey);
        const classes = classnames({
          [styles.checkbox]: true,
          [styles.selected]: isSelected,
        });
        return (
          <label className={classes} key={index}>
            <input
              type="checkbox"
              checked={isSelected}
              onClick={() => handleClickItem(item.option_id, searchKey)}
              className={styles.input}
            />
            <span className={styles.boxCheck}>
              <Check className={styles.iconCheck} />
            </span>
            <span className={styles.text}>{item.option_name}</span>
          </label>
        );
      default:
        return (
          <label key={index}>
            Detail of {type} is not support
          </label>
        );
    }
  }
}

export default compose(withStyles(styles), withFilterContext)(FilterTag);
