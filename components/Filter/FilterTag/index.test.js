import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { FilterTag } from './index';
import _identity from 'lodash/identity';

describe('<FilterTag /> component', () => {
  let props = {
    items: [{ name: 'item1' }, { name: 'item2' }, { name: 'item3' }],
    isSelectedItem: _identity,
    type: 'BrandTerm',
  };
  it('it should be ok BrandTerm', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<FilterTag {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterTag);
    expect(wrapper.find('.choose-label-img').exists()).toEqual(true);
  });
  it('it should be ok ColorTerm', () => {
    const context = {
      ...ctx,
    };
    props.type = 'ColorTerm';
    const wrapper = shallow(<FilterTag {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterTag);
    expect(wrapper.find('.choose-label-circle').exists()).toEqual(true);
  });
});
