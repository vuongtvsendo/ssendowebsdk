import React, { Component } from 'react';
import { compose } from 'redux';
import classnames from 'classnames';
import styles from './FilterCheckbox.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import withFilterContext from 'sendo-web-sdk/helpers/withFilterContext';
import Check from 'sendo-web-sdk/components/Icons/Check';
import Install from 'sendo-web-sdk/components/Icons/Install';
import Revert from 'sendo-web-sdk/components/Icons/Revert';
import IconFreeShip from 'sendo-web-sdk/components/Icons/IconFreeShip';
import Coin from 'sendo-web-sdk/components/Icons/Coin';
import IsBrandIcon from 'sendo-web-sdk/components/Icons/IsBrandIcon';
import IconCamera from 'sendo-web-sdk/components/Icons/IconCamera';
import ReputationIcon from 'sendo-web-sdk/components/Icons/ReputationIcon';

const iconComponent = type => {
  switch (type) {
    case 'install':
      return <Install className={`${styles.icon} ${styles.install}`} />;
    case 'revert':
      return <Revert className={`${styles.icon} ${styles.revert}`} />;
    case 'delivery':
      return (
        <IconFreeShip className={`${styles.iconDelivery} icon-free-ship`} />
      );
    case 'coin':
      return <Coin className={`${styles.icon} ${styles.coin}`} />;
    case 'IconCamera':
      return <IconCamera className={`${styles.icon} ${styles.video}`} />;
    case 'shopcheck':
      return (
        <ReputationIcon className={`${styles.iconLarge} ${styles.shopcheck}`} />
      );
    case 'hhch':
      return <IsBrandIcon className={`${styles.iconLarge} ${styles.hhch}`} />;
    default:
      return;
  }
};

export class FilterCheckbox extends Component {
  render() {
    var { items, handleClickItem, isSelectedItem } = this.props;
    return (
      <div>
        {items.map((item, i) => {
          const isChecked = isSelectedItem(item.attribute_value);
          const classes = classnames({
            'checkbox-custom': true,
            checked: isChecked,
          });
          return (
            <label className={classes} key={i}>
              <input
                type="checkbox"
                checked={isChecked}
                onClick={() => handleClickItem(item.attribute_value)}
                className={'input-cb-custom'}
              />
              <span className={'box-check'}>
                <Check className={'icon-check'} />
              </span>
              {iconComponent(item.attribute_icon)}
              <span className={styles.text}>
                {item.attribute_label}
              </span>
            </label>
          );
        })}
      </div>
    );
  }
}

export default compose(withStyles(styles), withFilterContext)(FilterCheckbox);
