import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { FilterCheckbox } from './index';
import _identity from 'lodash/identity';

describe('<Filter /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [{ name: 'item1' }, { name: 'item2' }, { name: 'item3' }],
      isSelectedItem: _identity,
      handleClickItem: _identity,
    };
    const wrapper = shallow(<FilterCheckbox {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(FilterCheckbox);
    expect(wrapper.find('.box-check').exists()).toEqual(true);
  });
});
