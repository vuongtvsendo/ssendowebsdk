import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import styles from './productList.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import _get from 'lodash/get';
import Product from 'sendo-web-sdk/components/ProductCard';
import ProductCardListView from 'sendo-web-sdk/components/ProductCardListView';
import ProductSkeleton from 'sendo-web-sdk/components/ProductSkeleton';
import Loading from 'sendo-web-sdk/components/Loading';
import productEmptyImg from 'sendo-web-sdk/images/empty-state/products-empty.svg';
import { TEXT_PRODUCT_EMPTY } from '../../res/text';
export class ProductList extends PureComponent {
  initSkeletion() {
    let result = [];
    for (var i = 0; i < 6; i++) {
      result.push(
        <div className="item" key={i}>
          <ProductSkeleton />
        </div>
      );
    }
    return result;
  }
  resetFilter() {
    if (_get(this, 'props.resetFilter') instanceof Function) {
      this.props.resetFilter();
    }
  }
  // shouldComponentUpdate(nextProps) {
  //   return (
  //     nextProps.isloading !== this.props.isloading ||
  //     nextProps.isloadingMore !== this.props.isloadingMore ||
  //     (nextProps.products.length === 0 &&
  //       nextProps.products.length !== this.props.products.length) ||
  //     nextProps.listView !== this.props.listView
  //   );
  // }

  render() {
    const {
      products,
      listView,
      isloading,
      redirecting,
      isloadingMore,
      initProducts,
      numberFilterActive,
      isPreventLoadOldItems,
    } = this.props;
    const classes = classnames({
      [styles.productList]: true,
      'loading-full': true,
      'product-gridview-2': true,
      'product-listview': listView,
      'product-gridview': true,
      'loading-full-effect': isloading,
    });
    if ((!initProducts && !products.length) || isPreventLoadOldItems) {
      return <div className={classes}>{this.initSkeletion()}</div>;
    }
    if (initProducts && !products.length && !isloading && !redirecting) {
      return (
        <section className="panel">
          <div className="content-empty full">
            <div className="image-square">
              <img
                className="img"
                src={productEmptyImg}
                alt="Sản phẩm tìm kiếm hiện tại không có"
              />
            </div>
            <strong className="title">{TEXT_PRODUCT_EMPTY}</strong>
            {!!numberFilterActive &&
              numberFilterActive !== 0 && (
                <button
                  className="btn-primary"
                  onClick={() => this.resetFilter()}>
                  Thiết lập lại bộ lọc
                </button>
              )}
            {!numberFilterActive && (
              <Link className="btn-primary" to="/">
                Tiếp tục mua sắm
              </Link>
            )}
          </div>
        </section>
      );
    }
    return (
      <div className={styles.listingContainer}>
        {isloading && (
          <div className="loading-wrapper">
            <Loading />
          </div>
        )}
        <div className={classes}>
          {products.map((product, i) => this.renderProduct(product, i))}
        </div>
        <div className="loading-more-wrapper">
          {!isloading && isloadingMore && <Loading />}
        </div>
      </div>
    );
  }
  getProductKey(prd) {
    return prd.uid || prd.id || prd.product_id;
  }
  renderProduct(product, i) {
    const { listView, cateMetadata, isTrackInviewport } = this.props;
    const classes = classnames({
      item: true,
      [styles.listItem]: listView,
    });
    return (
      <div key={this.getProductKey(product)} className={classes}>
        {listView && (
          <ProductCardListView
            isTrackInviewport={isTrackInviewport}
            dataIndex={i}
            product={product}
          />
        )}
        {!listView && (
          <Product
            isTrackInviewport={isTrackInviewport}
            dataIndex={i}
            product={product}
            cateMetadata={cateMetadata}
          />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(ProductList);
