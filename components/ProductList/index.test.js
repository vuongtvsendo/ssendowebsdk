import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductList } from './index';
import ProductSkeleton from 'sendo-web-sdk/components/ProductSkeleton';
import { TEXT_PRODUCT_EMPTY } from '../../res/text';

describe('<ProductOptions /> component', () => {
  let props = {
    products: [],
    listView: false,
    isloading: false,
    isloadingMore: false,
    initProducts: false,
    numberFilterActive: 0,
  };
  it('It should render with Skeletion', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<ProductList {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductList);
    expect(wrapper.find(ProductSkeleton).exists()).toEqual(true);
  });
  it('It should render with ProductEmpty', () => {
    const context = {
      ...ctx,
    };
    props = { ...props, initProducts: true };
    const wrapper = shallow(<ProductList {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductList);
    expect(wrapper.find('.content-empty').find('.title').text()).toEqual(
      TEXT_PRODUCT_EMPTY
    );
  });
  it('It should render with Products', () => {
    const context = {
      ...ctx,
    };
    props = {
      ...props,
      products: [{ id: 1 }],
      isloading: true,
    };
    const wrapper = shallow(<ProductList {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductList);
    expect(wrapper.find(ProductSkeleton).exists()).toEqual(false);
    expect(wrapper.find('.content-empty').exists()).toEqual(false);
  });
});
