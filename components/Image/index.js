import React, { PureComponent } from 'react';
import { normalizeImgUrl } from 'sendo-web-sdk/helpers/url';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import defaultImg from 'sendo-web-sdk/images/default.png';

export class Image extends PureComponent {
  buildSrc(props) {
    if (!props.src) {
      if (props.default) {
        return props.default;
      }
      return defaultImg;
    }
    return normalizeImgUrl(props.src);
  }
  render() {
    return (
      <img
        {...this.props}
        src={this.buildSrc(this.props)}
        onError={e => onImgError(e)}
        alt={this.props.alt}
      />
    );
  }
}

export default Image;
