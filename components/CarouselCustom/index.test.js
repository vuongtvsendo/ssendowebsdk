import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { CarouselCustom } from './index';

describe('<CarouselCustom /> component', () => {
  const Item = ({ children }) =>
    <div className="item">
      {children}
    </div>;

  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<CarouselCustom />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(CarouselCustom);
  });

  it('should render with correct number children', () => {
    const Test = (
      <CarouselCustom>
        <Item>item 1</Item>
        <Item>item 2</Item>
        <Item>item 3</Item>
      </CarouselCustom>
    );

    const wrapper = shallow(Test);
    expect(wrapper.find('Item').length).toEqual(3);
  });

  it('should render with correct order', () => {
    const Test = (
      <CarouselCustom>
        <Item>item 1</Item>
        <Item>item 2</Item>
        <Item>item 3</Item>
      </CarouselCustom>
    );

    const expectedItemsText = ['item 1', 'item 2', 'item 3'];

    const wrapper = shallow(Test);
    wrapper.find('Item').forEach((item, index) => {
      expect(item.contains(expectedItemsText[index])).toEqual(true);
    });
  });
});
