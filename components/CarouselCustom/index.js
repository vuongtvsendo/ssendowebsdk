import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './carouselCustom.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import _get from 'lodash/get';
import { withRouter } from 'react-router';

const injectClassName = node => {
  let props = {
    ...node.props,
    className: node.props.className + ' ' + styles.item,
    ref: 'child_' + node.key,
  };

  return React.cloneElement(node, props);
};

class CarouselCustom extends Component {
  componentDidMount() {
    this.props.onRef && this.props.onRef(this);
    if (this.props.isScroll) {
      setTimeout(() => this.handScrollIfElementActive(), 200);
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.categoryLevel === 1 || this.props.categoryLevel === 1) {
      if (this.props.activeCategory !== prevProps.activeCategory) {
        this.refs.carousel.scrollLeft = 0;
      }
      return;
    }
    if (this.props.isScroll) {
      if (_get(this.props, 'history.action') === 'POP') {
        setTimeout(() => this.handScrollIfElementActive(), 200);
      }
    } else {
      if (this.props.location !== prevProps.location)
        this.refs.carousel.scrollLeft = 0;
    }
  }
  handScrollIfElementActive = () => {
    let self = this;
    React.Children.map(this.props.children, (child, index) => {
      React.Children.map(child.props.children, (e, i) => {
        if (e.props && e.props.isActive()) {
          let dom = ReactDOM.findDOMNode(self.refs['child_' + child.key]);
          if (dom) {
            let left = dom.getBoundingClientRect().left;
            let width = dom.getBoundingClientRect().width;
            self.scrollLeft(left - (window.innerWidth / 2 - width / 2));
          }
        }
      });
    });
  };
  componentWillUnmount() {
    this.props.onRef && this.props.onRef(null);
    this.refs.carousel.scrollLeft = 0;
  }
  scrollLeft(value = 0) {
    this.refs.carousel.scrollLeft += value;
  }
  render() {
    return (
      <div
        className={`${styles.carousel} ${this.props.className}`}
        ref="carousel">
        <div className={styles.inner}>
          {React.Children.map(this.props.children, injectClassName)}
        </div>
      </div>
    );
  }
}

export { CarouselCustom };
export default withRouter(withStyles(styles)(CarouselCustom));
