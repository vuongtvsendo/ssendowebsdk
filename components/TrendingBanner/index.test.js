import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { TrendingBanner } from './index';

describe('<TrendingBanner /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<TrendingBanner {...props} />, {
      context,
    });
    expect(wrapper.find('.bannerTrending')).toHaveLength(1);
  });
});
