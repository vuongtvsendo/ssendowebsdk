import React from 'react';
import bannerTrendingImg from 'sendo-web-sdk/images/banners/banner_trending.jpg';
import styles from './banner.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

export function TrendingBanner(props) {
  return (
    <div className="bannerTrending">
      <img
        className={styles.bannerTrendingImg}
        src={bannerTrendingImg}
        alt=""
      />
    </div>
  );
}

export default withStyles(styles)(TrendingBanner);
