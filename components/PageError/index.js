import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import withStyles from 'sendo-web-sdk/helpers/withStyles';
import HeaderSecondary from 'sendo-web-sdk/components/Header/HeaderSecondary';

import styles from './styles.css';
import PageErrorImg from 'sendo-web-sdk/images/empty-state/500.svg';

class PageError extends Component {
  getHeader() {
    return this.props.header || 'Địa chỉ không hợp lệ';
  }
  render() {
    return (
      <div className={styles.pageerror}>
        <section>
          <HeaderSecondary title={this.getHeader()} />
        </section>
        <section>
          <div className="content-empty full">
            <div className="image-square">
              <img className="img" src={PageErrorImg} alt={this.getHeader()} />
            </div>
            <strong className="title">{this.getHeader()}</strong>
            <div className="message">
              <p>Địa chỉ URL bạn yêu cầu không tìm thấy trên server.</p>
              <p>
                Có thể bạn gõ sai địa chỉ hoặc dữ liệu này đã bị xóa khỏi server
              </p>
            </div>
            <Link className="btn-primary" to="/">Tiếp tục mua sắm</Link>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(styles)(PageError);
