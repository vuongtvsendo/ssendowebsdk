import React from 'react';
import { shallow, mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Router } from 'react-router-dom';
import { getInstance as getInstanceHistory } from 'sendo-web-sdk/helpers/history';
import PageError from './index';

const history = getInstanceHistory();

describe('<PageError /> component', () => {
  it('It should allow to click to go home page', () => {
    const context = {
      ...ctx,
      router: {
        route: {
          location: {
            search: '?sortType=rank',
          },
        },
      },
      history: {},
    };
    const wrapper = shallow(<PageError header="Test header 1" />, {
      context,
    });
    expect(wrapper.text()).toEqual('<PageError />');
  });
});
