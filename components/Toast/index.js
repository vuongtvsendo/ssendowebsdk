import React, { Component } from 'react';
import styles from './toast.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as toastAction from 'sendo-web-sdk/actions/toast';

export class Toast extends Component {
  constructor(props) {
    super(props);
    this.time = this.props.time || 3000;
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen && nextProps.isOpen) {
      const { autoClose } = this.props.toast;
      if (typeof autoClose === 'undefined' || autoClose) {
        setTimeout(() => {
          this.props.toastAction.closeToast();
        }, this.time);
      }
    }
  }
  render() {
    const { isOpen } = this.props;
    const classToast = classnames({
      [styles.toast]: true,
      toast: true,
      open: isOpen,
    });
    return (
      <div className={classToast}>
        <div className="content">
          <p
            className="text truncate-medium-2"
            dangerouslySetInnerHTML={{ __html: this.props.toast.content }}
          />
          <button
            className="close"
            onClick={() => {
              this.props.toastAction.closeToast();
            }}>
            Đóng
          </button>
        </div>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      toast: state.toast.toastState,
    };
  },
  dispatch => {
    return {
      toastAction: bindActionCreators(toastAction, dispatch),
    };
  }
)(withStyles(styles)(Toast));
