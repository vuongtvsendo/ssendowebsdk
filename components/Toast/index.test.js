import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Toast } from './index';

describe('<Toast /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      toast: {
        content: '',
      },
    };
    const wrapper = shallow(<Toast {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(Toast);
    expect(wrapper.find('.close').text()).toEqual('Đóng');
  });
});
