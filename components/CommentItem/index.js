import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as productActions from 'sendo-web-sdk/actions/product';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as auth from 'sendo-web-sdk/helpers/auth';
import styles from './commentItem.css';
import _get from 'lodash/get';

import ConfirmPopover from 'sendo-web-sdk/modals/ConfirmPopover';
import CommentInput from '../CommentInput';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import avartarDefault from 'sendo-web-sdk/images/avartar-default.png';
export class CommentItem extends Component {
  state = { confirmPopver: false };
  currentDelete = {};
  toogleInput(id) {
    this.props.handleToogle(id);
  }
  deleteComment(comment, index) {
    this.setState({ confirmPopver: true });
    this.currentDelete = { ...comment, index: index };
  }
  handleCancel() {
    this.setState({ confirmPopver: false });
  }
  handleDelete() {
    this.setState({ confirmPopver: false });
    this.props.productActions
      .deleteProductComment({
        customer_id: this.currentDelete.customer_id,
        id: this.currentDelete.id,
        product_id: this.currentDelete.product_id,
        index: this.currentDelete.index,
      })
      .then(data => {
        this.props.toastActions.openToast({
          content: _get(
            data,
            'payload.comment.message',
            'Có lỗi xảy ra. Vui lòng thử lại.'
          ),
        });
      });
  }
  render() {
    const {
      product,
      comment,
      isActiveReply,
      disableReply,
      disableDelete,
    } = this.props;
    const inputClass = classnames({
      [styles.inputWrap]: true,
      [styles.show]: isActiveReply,
    });
    return (
      <div className={`${styles.commentGroup} commentGroup`}>
        <div className={styles.mainComment}>
          {this.renderCommentItem(comment, disableReply, disableDelete)}
        </div>
        {comment.sub &&
          comment.sub.length > 0 &&
          <div className={styles.subListComment}>
            {comment.sub.map((sub, index) => {
              return this.renderCommentItem(sub, true, disableDelete, index);
            })}
          </div>}
        <div className={inputClass}>
          <CommentInput
            focusWithBodyFixed
            product={product}
            parentId={comment.id}
            comment={comment}
          />
        </div>
        <ConfirmPopover
          title="Xác nhận"
          open={this.state.confirmPopver}
          onCancel={() => this.handleCancel()}
          onOk={() => this.handleDelete()}>
          Bạn có muốn xóa bình luận này?
        </ConfirmPopover>
      </div>
    );
  }
  renderCommentItem(comment, disableReply, disableDelete, index = 0) {
    const isSubComment = !(parseInt(_get(comment, 'parent_id'), 10) === 0);
    const commentClass = classnames({
      [styles.commentItem]: true,
      [styles.subComment]: isSubComment,
    });
    return (
      <div className={commentClass} key={comment.id}>
        <div className={styles.avatar}>
          <img
            className="lazyload"
            onError={e => onImgError(e, avartarDefault)}
            data-src={comment.customer_logo}
            alt="Hình đại diện"
          />
        </div>
        <div className={styles.content}>
          <strong className={styles.userName}>
            {comment.is_shop && <span className={styles.shopLabel}>Shop</span>}
            {comment.customer_name}
          </strong>
          <div className={styles.feedback}>
            <p>{comment.data}</p>
          </div>
          <div className={styles.info}>
            <time>{comment.time}</time>
            <div className={styles.groupAction}>
              {!disableReply &&
                <button
                  className={styles.reply}
                  onClick={() => this.toogleInput(comment.id)}>
                  Trả lời
                </button>}
              {!disableDelete &&
                auth.getUserId(this.props.identity) === comment.customer_id &&
                <button
                  className={styles.delete}
                  onClick={() => this.deleteComment(comment, index)}>
                  Xóa
                </button>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(CommentItem));
