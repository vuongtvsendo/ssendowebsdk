import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as shopActions from 'sendo-web-sdk/actions/shop';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as auth from 'sendo-web-sdk/helpers/auth';
import { formatTimeDisplay } from 'sendo-web-sdk/helpers/common';
import styles from './commentItem.css';
import _get from 'lodash/get';

import ConfirmPopover from 'sendo-web-sdk/modals/ConfirmPopover';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import avartarDefault from 'sendo-web-sdk/images/avartar-default.png';

export class CommentItemForm extends Component {
  state = { confirmPopver: false };
  currentDelete = {};

  deleteComment(comment, isReply) {
    this.setState({ confirmPopver: true });
    this.currentDelete = { ...comment, isReply: isReply };
  }
  handleCancel() {
    this.setState({ confirmPopver: false });
  }
  handleDelete() {
    this.setState({ confirmPopver: false });
    let deleteComment = {
      comment_id: this.currentDelete.comment_id,
      isReply: this.currentDelete.isReply,
      parentId: this.currentDelete.parentId,
    };
    this.props.shopActions.setDeleteShopComment(deleteComment);
    this.props.shopActions.deleteShopComment(deleteComment).then(data => {
      this.props.toastActions.openToast({
        content: _get(
          data,
          'payload.comment.message',
          'Có lỗi xảy ra. Vui lòng thử lại.'
        ),
      });
    });
  }
  render() {
    const { comment, disableDelete } = this.props;
    return (
      <div className="commentGroup">
        <div className={styles.mainComment}>
          {this.renderCommentItem(comment, disableDelete)}
        </div>
        {comment.reply &&
          comment.reply.length > 0 &&
          <div className={styles.subListComment}>
            {comment.reply.map(reply => {
              reply = { ...reply, ...{ parentId: comment.comment_id } };
              return this.renderCommentItem(reply, disableDelete, true);
            })}
          </div>}
        <ConfirmPopover
          title="Xác nhận"
          open={this.state.confirmPopver}
          onCancel={() => this.handleCancel()}
          onOk={() => this.handleDelete()}>
          Bạn có muốn xóa bình luận này?
        </ConfirmPopover>
      </div>
    );
  }
  renderCommentItem(comment, disableDelete, isReply = false) {
    const commentClass = classnames({
      [styles.commentItem]: true,
      [styles.subComment]: isReply,
    });
    return (
      <div className={commentClass} key={comment.comment_id}>
        <div className={styles.avatar}>
          <img
            className="lazyload"
            onError={e => onImgError(e, avartarDefault)}
            data-src={comment.customer_logo}
            alt=""
          />
        </div>
        <div className={styles.content}>
          <strong className={styles.userName}>
            {comment.is_shop && <span className={styles.shopLabel}>Shop</span>}
            {comment.customer_name}
          </strong>
          <div className={styles.feedback}>
            <p>{comment.content}</p>
          </div>
          <div className={styles.info}>
            <time>{formatTimeDisplay(comment.time_update)}</time>
            <div className={styles.groupAction}>
              {!disableDelete &&
                auth.getUserId(this.props.identity) === comment.customer_id &&
                <button
                  className={styles.delete}
                  onClick={() => this.deleteComment(comment, isReply)}>
                  Xóa
                </button>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      shopActions: bindActionCreators(shopActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(CommentItemForm));
