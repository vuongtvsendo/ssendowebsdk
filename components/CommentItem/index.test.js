import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { CommentItem } from './index';

describe('<CommentItem /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {},
      comment: {},
    };
    const wrapper = shallow(<CommentItem {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(CommentItem);
  });
});
