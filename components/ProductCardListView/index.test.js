import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductCardListView } from './index';

describe('<ProductCardListView /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {},
    };
    const wrapper = shallow(<ProductCardListView {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductCardListView);
  });
});
