import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { EventList } from './index';

describe('<EventList /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<EventList {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(EventList);
  });
});
