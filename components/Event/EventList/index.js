import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './trendingList.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Loading from 'sendo-web-sdk/components/Loading';
import productEmptyImg from 'sendo-web-sdk/images/empty-state/products-empty.svg';
import EventCard from 'sendo-web-sdk/components/Event/EventCard';

export class EventList extends PureComponent {
  render() {
    const {
      events,
      isloading,
      isloadingMore,
      initProducts,
      numberFilterActive,
    } = this.props;
    if (!initProducts) {
      return (
        <div className={styles.loadingMoreIcon}>
          <Loading />
        </div>
      );
    }
    if (initProducts && !events.length) {
      return (
        <section className="panel">
          <div className="content-empty full">
            <div className="image-square">
              <img
                className="img"
                src={productEmptyImg}
                alt="Danh sách sự kiện hiện tại không có"
              />
            </div>
            <strong className="title">
              Danh sách sự kiện hiện tại không có
            </strong>
            {!numberFilterActive &&
              <Link className="btn-primary" to="/">Tiếp tục mua sắm</Link>}
          </div>
        </section>
      );
    }
    return (
      <div className={styles.listingContainer}>
        {isloading && <div className={styles.loadingIcon}><Loading /></div>}
        <div className="product-gridview product-gridview-2 event-listview">
          {events.map((event, i) => {
            return <EventCard event={event} key={event.id} />;
          })}
        </div>
        <div className={styles.loadingMoreIcon}>
          {isloadingMore && <Loading />}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(EventList);
