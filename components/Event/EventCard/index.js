import React, { PureComponent } from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productCard.css';

import defaultImg from 'sendo-web-sdk/images/default.png';
import AppLink from 'sendo-web-sdk/helpers/webview/Link';
import { detectDirectUrlEvent } from 'sendo-web-sdk/helpers/url';

export class EventCard extends PureComponent {
  render() {
    const { event } = this.props;
    return (
      <div className="item event">
        <AppLink
          isDirectLink={detectDirectUrlEvent(event.campaign_url)}
          to={event.campaign_url}
          className="product-card-wrap">
          <div className="product-card event">
            <div className="thumbnail">
              <figure className="image-square">
                {!event.event_active &&
                  <div className="badge">
                    <span className="text">Đã kết thúc</span>
                  </div>}
                <img
                  className={`lazyload ${!event.event_active ? 'blur' : ''}`}
                  src={defaultImg}
                  data-src={event.banner_url}
                  alt=""
                />
              </figure>
            </div>
            <div className="caption">
              <div
                className={`round-border-label ${!event.event_active
                  ? 'gray'
                  : 'red'}`}>
                {event.start_date} - {event.end_date}
              </div>
            </div>
          </div>
        </AppLink>
      </div>
    );
  }
}

export default withStyles(styles)(EventCard);
