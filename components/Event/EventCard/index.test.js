import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { EventCard } from './index';

describe('<EventCard /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      event: {
        event_active: false,
        campaign_url: '',
      },
    };
    const wrapper = shallow(<EventCard {...props} />, {
      context,
    });
    expect(wrapper.find('.badge').exists()).toEqual(true);
    expect(wrapper.find('.gray').exists()).toEqual(true);
    expect(wrapper.find('.blur').exists()).toEqual(true);
  });
});
