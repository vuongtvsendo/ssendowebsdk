import React from 'react';
import styles from './banner.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { normalizeImgUrl } from 'sendo-web-sdk/helpers/url';

export function EventBanner(props) {
  return (
    <div className={styles.bannerEvent}>
      <img
        className={`${styles.bannerEventImg}`}
        src={normalizeImgUrl(props.imgSrc)}
        alt=""
      />
    </div>
  );
}

export default withStyles(styles)(EventBanner);
