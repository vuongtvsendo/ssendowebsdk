import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { EventBanner } from './index';
import startsWith from 'lodash/startsWith';

describe('<EventBanner /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      imgSrc: 'http://media3.scdn.vn/img2/2018/1_30/O0E1yC.png',
    };
    const wrapper = shallow(<EventBanner {...props} />, {
      context,
    });
    expect(startsWith(wrapper.find('img').at(0).props().src, '//')).toEqual(
      true
    );
  });
});
