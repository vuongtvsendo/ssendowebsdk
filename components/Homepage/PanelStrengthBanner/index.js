import React, { PureComponent } from 'react';
import { buildExternalSendoUrl } from 'sendo-web-sdk/helpers/url';

import styles from './panelStrengthBanner.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import banner from './strength.png';

class PanelStrengthBanner extends PureComponent {
  render() {
    return (
      <a
        href={buildExternalSendoUrl('chi-dai-sen-do/')}
        className={styles.panelStrengthBanner}>
        <img
          data-src={banner}
          className="lazyload"
          alt="Siêu rẻ, siêu nhanh, siêu an toàn"
        />
      </a>
    );
  }
}

export default withStyles(styles)(PanelStrengthBanner);
