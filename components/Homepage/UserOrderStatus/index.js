import React, { PureComponent } from 'react';

import styles from './userOrderStatus.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import * as helperFunction from 'sendo-web-sdk/helpers/common';
import _get from 'lodash/get';
import { buildExternalSendoUrl } from 'sendo-web-sdk/helpers/url';

import StarFull from 'sendo-web-sdk/components/Icons/Star';
import StarStroke from 'sendo-web-sdk/components/Icons/IconStarStroke';
import Back from 'sendo-web-sdk/components/Icons/Back';
import Process from 'sendo-web-sdk/components/Icons/Process';
import Confirm from 'sendo-web-sdk/components/Icons/Confirm';
import Shipping from 'sendo-web-sdk/components/Icons/Shipping';
import Check from 'sendo-web-sdk/components/Icons/Check';
import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';

export class UserOrderStatus extends PureComponent {
  state = { isShow: true };
  handleClose() {
    this.setState({ isShow: false });
  }
  render() {
    const { orderStatus } = this.props;

    const product = _get(orderStatus, `[0].product_info`, {});
    const steppers = _get(orderStatus, `[0].list_status`, []);
    const status = _get(orderStatus, `[0].status`);
    const message = _get(orderStatus, `[0].rating_status`);
    const orderUrl = _get(orderStatus, `[0].order_url`);
    const orderStatusNote = _get(orderStatus, `[0].status_note`);
    const orderRatingStar = _get(orderStatus, `[0].rating_star`, 0);

    if (!this.state.isShow) {
      return <div />;
    }
    return (
      <div className={styles.userOrderStatus}>
        <div className="box-toggle">
          <div className="inner">
            <button className="btn-close">
              <CloseCircle
                className="icon"
                onClick={() => this.handleClose()}
              />
            </button>
            <a className="content" href={buildExternalSendoUrl(orderUrl)}>
              {(status === 'ecom_complete' || status === 'ecom_pod') &&
                <div className={styles.orderMessage}>
                  "<span dangerouslySetInnerHTML={{ __html: message }} />"
                </div>}
              {steppers &&
                steppers.length > 0 &&
                <div className={`${styles.stepper} stepper stepper-4`}>
                  {steppers.map((step, i) => {
                    let iconForwardClass = i === 0
                      ? 'icon icon-forward hidden'
                      : 'icon icon-forward';
                    let stepClass = !step.passed ? 'step' : 'step passed';
                    stepClass = !step.is_active
                      ? stepClass
                      : stepClass + ' active';
                    return (
                      <div className={stepClass} key={i}>
                        <Back className={iconForwardClass} />
                        <span className="wrap-icon">
                          {this.renderIcon(step.icon)}
                        </span>
                        <span className="text">{step.title}</span>
                      </div>
                    );
                  })}
                </div>}
              {product &&
                <div className={`${styles.cardHoriz} card-horiz`}>
                  <img src={product.image} alt={product.name} />
                  <div className="caption">
                    <h3 className="heading-medium truncate-medium-2 card-name">
                      {product.name}
                    </h3>
                    <div className="price">
                      <strong className="text-primary current">
                        {helperFunction.formatPrice(product.final_price)}
                      </strong>
                    </div>
                  </div>
                </div>}

              {(status === 'ecom_complete' || status === 'ecom_pod') &&
                <div className={styles.rating}>
                  {this.renderRating(orderRatingStar)}
                </div>}
              {steppers &&
                steppers.length === 0 &&
                orderStatusNote &&
                <div className={styles.orderNote}>
                  <span dangerouslySetInnerHTML={{ __html: orderStatusNote }} />
                </div>}
            </a>
          </div>
        </div>
      </div>
    );
  }
  renderRating(numStar = 0) {
    var strokeStart = 5 - numStar;
    var starRating = [];
    if (numStar) {
      for (let i = 0; i < numStar; i++) {
        starRating.push(<StarFull className={styles.iconStar} />);
      }
    }
    if (strokeStart) {
      for (let i = 0; i < strokeStart; i++) {
        starRating.push(<StarStroke className={styles.iconStar} />);
      }
    }
    return starRating;
  }
  renderIcon(icon) {
    switch (icon) {
      case 'New':
        return <Process className="icon icon-inverse" />;
      case 'Processing':
        return <Confirm className="icon icon-inverse" />;
      case 'Delivering':
        return <Shipping className="icon icon-inverse" />;
      default:
        return <Check className="icon icon-inverse" />;
    }
  }
}

export default withStyles(styles)(UserOrderStatus);
