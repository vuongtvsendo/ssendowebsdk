import React from 'react';
import { shallow } from 'enzyme';
import { ctx, mount } from 'sendo-web-sdk/utils/testing';
import { UserOrderStatus } from './index';

describe('<UserOrderStatus /> component', () => {
  const defaultProps = {
    categories: [],
    steppers: [],
  };
  it('it should render ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      ...defaultProps,
    };
    const wrapper = shallow(<UserOrderStatus {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(UserOrderStatus);
  });

  it('it should show order status ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      ...defaultProps,
      orderStatus: [
        {
          status_note: '<span>This is message of an order.</span>',
        },
      ],
    };
    const wrapper = mount(<UserOrderStatus {...props} />, {
      context,
    });
    expect(wrapper.find('span').last().text()).toEqual(
      'This is message of an order.'
    );
  });
});
