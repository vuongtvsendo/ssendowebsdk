import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import styles from './recommendCategories.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';

import _get from 'lodash/get';

import defaultImg from 'sendo-web-sdk/images/default.png';
import { LABEL_RECOMMEND_CATEGORY } from 'sendo-web-sdk/res/text';
export class RecommendCategories extends PureComponent {
  render() {
    const { categories } = this.props;
    const large = _get(categories, 'firstSection[0]');
    const smalls = _get(categories, 'secondSection');
    const others = _get(categories, 'thirdSection');
    return (
      <div className={styles.recommendCategories}>
        <div className="panel-heading">
          <h2>{LABEL_RECOMMEND_CATEGORY}</h2>
        </div>
        <div className={styles.boxImage}>
          <div className={styles.main}>
            {large &&
              <Link
                className="card-cate"
                key={large.url_path}
                to={normalizeUrl(large.url_path)}>
                <div className="image-square">
                  <img
                    className="lazyload"
                    src={defaultImg}
                    data-src={large.image}
                    alt={large.name}
                    onError={e => onImgError(e)}
                  />
                  <div className="caption">
                    <h3 className="text-normal text-overflow">{large.name}</h3>
                  </div>
                </div>
              </Link>}
          </div>
          <div className={styles.sub}>
            {smalls &&
              smalls.length > 0 &&
              smalls.map(small => {
                return (
                  <Link
                    className="card-cate"
                    to={normalizeUrl(small.url_path)}
                    key={small.url_path}>
                    <div className="image-square">
                      <img
                        className="lazyload"
                        src={defaultImg}
                        data-src={small.image}
                        alt={small.name}
                        onError={e => onImgError(e)}
                      />
                      <div className="caption">
                        <h3 className="text-normal text-overflow">
                          {small.name}
                        </h3>
                      </div>
                    </div>
                  </Link>
                );
              })}
          </div>
        </div>
        {others &&
          others.length > 0 &&
          <div className={styles.linkCates}>
            {others.map((other, i) => {
              return (
                <Link
                  key={other.url_path}
                  className="link-box text-overflow"
                  to={normalizeUrl(other.url_path)}>
                  {other.name}
                </Link>
              );
            })}
          </div>}
      </div>
    );
  }
}

export default withStyles(styles)(RecommendCategories);
