import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { RecommendCategories } from './index';
import { LABEL_RECOMMEND_CATEGORY } from 'sendo-web-sdk/res/text';

describe('<ProductCateRecommend /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      categories: [],
    };
    const wrapper = shallow(<RecommendCategories {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(RecommendCategories);
    expect(wrapper.find('h2').text()).toEqual(LABEL_RECOMMEND_CATEGORY);
  });
});
