import React from 'react';
import _get from 'lodash/get';
import Modal from 'reactstrap/lib/Modal';
import styles from './mainMenu.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as auth from 'sendo-web-sdk/helpers/auth';
import Ripple from 'sendo-web-sdk/components/Ripple';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';
import { FORM_LOGIN, FORM_REGISTER } from 'sendo-web-sdk/components/Profile/const';

import AvartFull from 'sendo-web-sdk/components/Icons/AvartFull';
import IconLogout from 'sendo-web-sdk/components/Icons/IconLogout';
import IconClose from 'sendo-web-sdk/components/Icons/IconClose';
import { shortCutLink, extraLink } from 'sendo-web-sdk/res/mainmenu';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import ModalParent from 'sendo-web-sdk/components/Modal';
import avartarDefault from 'sendo-web-sdk/images/avartar-default.gif';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import { CLASS_MODAL_IGNORE_SCROLL } from 'sendo-web-sdk/helpers/const';
export class MainMenu extends ModalParent {
  toggleMenu() {
    if (_get(this, 'props.toggleMenu') instanceof Function) {
      this.props.toggleMenu();
    }
  }
  userLogout() {
    if (_get(this, 'props.userLogout') instanceof Function) {
      this.props.userLogout();
      this.props.closeMenu();
      this.props.toastActions.openToast({
        content: 'Bạn đã đăng xuất thành công',
      });
    }
  }
  onLoginSuccess() {
    this.props.closeMenu();
  }
  render() {
    const { isOpen, identity } = this.props;
    return (
      <Modal
        modalClassName={`modal-left-menu ${CLASS_MODAL_IGNORE_SCROLL}`}
        isOpen={isOpen}
        toggle={() => this.props.toggleMenu()}>
        <button
          className="btn-icon btn-close-menu"
          onClick={() => this.props.toggleMenu()}>
          <IconClose className="icon" />
        </button>
        {this.renderUserInfo()}
        <section ref="scroller" className={styles.listMenu}>
          <div className={styles.inner}>
            <ul className={styles.shortCutLink}>
              {shortCutLink &&
                shortCutLink.length > 0 &&
                shortCutLink.map((menu, i) => {
                  return this.renderMenuItem(menu, i);
                })}
            </ul>
            <ul className={styles.extraLink}>
              {extraLink &&
                extraLink.length > 0 &&
                extraLink.map((menu, i) => {
                  if (!auth.isLoggedIn(identity) && menu.requireLogin) {
                    return false;
                  }
                  return this.renderExtraLink(menu, i);
                })}
            </ul>
            {auth.isLoggedIn(identity) &&
              <ul className={styles.userControl}>
                <li>
                  <Ripple
                    className={styles.menuLink}
                    onClick={() => {
                      this.userLogout();
                    }}>
                    <IconLogout className={`${styles.menuIcon} icon`} />
                    <span className={styles.title}>Đăng xuất</span>
                  </Ripple>
                </li>
              </ul>}
          </div>
        </section>
      </Modal>
    );
  }
  renderMenuItem(menu, i) {
    if (menu.link === '') {
      return (
        <li key={i}>
          <Ripple
            className={styles.menuLink}
            onClick={() => {
              this.toggleMenu();
            }}>
            <menu.icon className={`${styles.menuIcon} icon`} />
            <span className={styles.title}>{menu.title}</span>
            {menu.label && <span className={styles.label}>{menu.label}</span>}
          </Ripple>
        </li>
      );
    }
    if (menu.link.indexOf('https://') >= 0) {
      return (
        <li key={i}>
          <Ripple tagName="a" to={menu.link} className={styles.menuLink}>
            <menu.icon className={`${styles.menuIcon} icon`} />
            <span className={styles.title}>{menu.title}</span>
            {menu.label && <span className={styles.label}>{menu.label}</span>}
          </Ripple>
        </li>
      );
    }
    if (menu.link === '/sitemap/') {
      return (
        <li key={i}>
          <Ripple
            tagName="a"
            to={menu.link}
            className={`${styles.menuLink} ${styles.allcate}`}>
            <menu.icon className={`${styles.menuIcon} icon`} />
            <span className={styles.title}>{menu.title}</span>
          </Ripple>
        </li>
      );
    }
    return (
      <li key={i}>
        <Ripple tagName="a" to={menu.link} className={styles.menuLink}>
          <menu.icon className={`${styles.menuIcon} icon`} />
          <span className={styles.title}>{menu.title}</span>
        </Ripple>
      </li>
    );
  }
  renderExtraLink(menu, i) {
    if (menu.link.indexOf('https://') >= 0) {
      return (
        <li key={i}>
          <Ripple tagName="a" to={menu.link} className={styles.menuLink}>
            <menu.icon className={`${styles.menuIcon} icon`} />
            <span className={styles.title}>{menu.title}</span>
          </Ripple>
        </li>
      );
    }
    return (
      <li key={i}>
        <Ripple tagName="a" to={menu.link} className={styles.menuLink}>
          <menu.icon className={`${styles.menuIcon} icon`} />
          <span className={styles.title}>{menu.title}</span>
        </Ripple>
      </li>
    );
  }
  renderUserInfo() {
    const { identity } = this.props;
    if (auth.isLoggedIn(identity)) {
      return (
        <header className={styles.userInfo} itemType="http://schema.org/Person">
          <div className={styles.avatar}>
            <img
              itemProp="image"
              src={auth.getAvatar(identity)}
              alt={auth.getFullname(identity)}
              onError={e => onImgError(e, avartarDefault)}
            />
          </div>
          <div className={styles.info}>
            <strong className={styles.username} itemProp="name">
              {auth.getFullname(identity)}
            </strong>
          </div>
        </header>
      );
    } else {
      return (
        <header className={styles.userInfo}>
          <div className={styles.avatar}>
            <AvartFull className={`${styles.iconDefault} icon`} />
          </div>
          <div className={styles.info}>
            <LoginWrapper
              event="click"
              openedForm={FORM_LOGIN}
              callback={() => this.onLoginSuccess()}>
              <strong className={styles.textLogin}>Đăng nhập</strong>
            </LoginWrapper>
            <LoginWrapper
              event="click"
              openedForm={FORM_REGISTER}
              callback={() => this.onLoginSuccess()}>
              <strong className={styles.textRegister}>Đăng ký</strong>
            </LoginWrapper>
          </div>
        </header>
      );
    }
  }
}

export default connect(null, dispatch => {
  return {
    toastActions: bindActionCreators(toastActions, dispatch),
  };
})(withStyles(styles)(MainMenu));
