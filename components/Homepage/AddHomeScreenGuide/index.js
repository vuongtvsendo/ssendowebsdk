import React, { PureComponent } from 'react';
import Modal from 'reactstrap/lib/Modal';
import classnames from 'classnames';
import styles from './addHomeScreenGuide.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import ArrowLeft from 'sendo-web-sdk/components/Icons/ArrowLeft';
import IconShareIos from 'sendo-web-sdk/components/Icons/IconShareIos';
import { getMobileOperatingSystem } from 'sendo-web-sdk/helpers/common';
class AddHomeScreenGuide extends PureComponent {
  handleClick() {
    this.props.toggleAddGuide();
  }
  isAndroid() {
    return getMobileOperatingSystem() === 'Android';
  }
  isIos() {
    return getMobileOperatingSystem() === 'iOS';
  }
  render() {
    const componentClassname = classnames({
      [styles.addHomeScreenGuide]: true,
      [styles.android]: this.isAndroid(),
      [styles.iOS]: this.isIos(),
    });
    return (
      <Modal
        modalClassName="modal-full"
        isOpen={this.props.isOpen}
        toggle={() => this.props.toggleAddGuide()}>
        <div className={componentClassname}>
          <button
            className="btn-icon"
            aria-label="Close Modal"
            onClick={() => this.handleClick()}>
            <ArrowLeft className="icon" />
          </button>
          {this.isAndroid() &&
            <div className={styles.description}>
              <h2 className={styles.title}>Thêm sendo.vn vào màn hình chính</h2>
              <p className={`${styles.guide} text-desc`}>
                Ấn &#8942; để hiển thị menu trình duyệt và chọn{' '}
                <strong>"Thêm vào màn hình chính"</strong> để truy cập{' '}
                <strong>Sendo.vn</strong>{' '}
                nhanh nhất
              </p>
            </div>}
          {this.isIos() &&
            <div className={styles.description}>
              <h2 className={styles.title}>Thêm sendo.vn vào màn hình chính</h2>
              <p className={`${styles.guide} text-desc`}>
                Ấn <IconShareIos className="icon" /> để hiển thị menu
                trình duyệt và chọn{' '}
                <strong>"Add home Screen"</strong> để truy cập{' '}
                <strong>Sendo.vn</strong>{' '}
                nhanh nhất
              </p>
            </div>}
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(AddHomeScreenGuide);
