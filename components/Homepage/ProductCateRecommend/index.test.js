import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductCateRecommend } from './index';
import { LABEL_PRODUCT_CATE_RECOMMEND } from 'sendo-web-sdk/res/text';

describe('<ProductCateRecommend /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      recommendProducts: {
        products: [],
      },
    };
    const wrapper = shallow(<ProductCateRecommend {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductCateRecommend);
    expect(wrapper.find('.heading-large').text()).toEqual(
      LABEL_PRODUCT_CATE_RECOMMEND
    );
  });
});
