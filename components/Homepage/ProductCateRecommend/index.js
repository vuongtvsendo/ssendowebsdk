import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import styles from './productCateRecommend.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import _get from 'lodash/get';
import Loading from 'sendo-web-sdk/components/Loading';
import Product from 'sendo-web-sdk/components/ProductCard';
import { colorsArray } from 'sendo-web-sdk/res/colors';
// import _get from 'lodash/get';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';

import { LABEL_PRODUCT_CATE_RECOMMEND } from 'sendo-web-sdk/res/text';

const NUMBER_CATE_BLOCK = 6;
const NUMBER_PROD_FIRST_BLOCK = 6;
const NUMBER_PROD_SECOND_BLOCK = 16;

const loopColor = (colors, index) => {
  return colors[index % colors.length];
};
export class ProductCateRecommend extends PureComponent {
  renderProducts(offset, length) {
    const { products } = this.props.recommendProducts;
    if (!products.length) return;
    const listRender = products.slice(offset, length || products.length);
    return (
      <div className="product-gridview product-gridview-2">
        {listRender.map(product =>
          <div key={product.uid || product.id} className="item">
            <Product
              className={styles.product}
              key={product.id}
              product={product}
            />
          </div>
        )}
      </div>
    );
  }
  renderCategory(offset, length) {
    const { categories } = this.props;
    if (!categories.length) return;
    const listRender = categories.slice(offset, length || categories.length);
    return listRender.map((cate, i) =>
      <div className={styles.linkBoxWrap} key={cate.url_path}>
        <Link
          className="link-box-cate"
          to={normalizeUrl(cate.url_path)}
          style={{ backgroundColor: loopColor(colorsArray, i + offset) }}>
          <span className="truncate-medium-2">{cate.name}</span>
        </Link>
      </div>
    );
  }
  render() {
    return (
      <div className={styles.productCateRecommend}>
        <h3 className="heading-large">{LABEL_PRODUCT_CATE_RECOMMEND}</h3>

        {this.renderProducts(0, NUMBER_PROD_FIRST_BLOCK)}

        {_get(this.props, 'recommendProducts.products.length') >=
          NUMBER_PROD_FIRST_BLOCK &&
          <div className={styles.cateWrap}>
            {this.renderCategory(0, NUMBER_CATE_BLOCK)}
          </div>}

        {this.renderProducts(
          NUMBER_PROD_FIRST_BLOCK,
          NUMBER_PROD_FIRST_BLOCK + NUMBER_PROD_SECOND_BLOCK
        )}

        {_get(this.props, 'recommendProducts.products.length') >=
          NUMBER_PROD_FIRST_BLOCK + NUMBER_PROD_SECOND_BLOCK &&
          <div className={styles.cateWrap}>
            {this.renderCategory(NUMBER_CATE_BLOCK)}
          </div>}

        {this.renderProducts(
          NUMBER_PROD_FIRST_BLOCK + NUMBER_PROD_SECOND_BLOCK
        )}

        {!this.props.isOutOfItem &&
          <div className={styles.loadingMoreIcon}>
            {_get(this.props, 'recommendProducts.loading') && <Loading />}
          </div>}

      </div>
    );
  }
}

export default withStyles(styles)(ProductCateRecommend);
