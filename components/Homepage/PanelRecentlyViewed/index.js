import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import styles from './panelRecentlyViewed.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { onImgError } from 'sendo-web-sdk/helpers/img';

import ProductCardWrap from 'sendo-web-sdk/components/ProductCardWrap';
import ArrowRight from 'sendo-web-sdk/components/Icons/ArrowRight';
import { LABEL_RECENTLY_VIEWED } from 'sendo-web-sdk/res/text';
import { PRODUCT_RECENTLY_ROUTER } from 'sendo-web-sdk/helpers/const';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';

export class PanelRecentlyViewed extends PureComponent {
  render() {
    const { recentProducts } = this.props;
    return (
      <div className={styles.panelRecentlyViewed}>
        <Link
          className={styles.text}
          to={normalizeUrl(PRODUCT_RECENTLY_ROUTER)}
          aria-label="Open Recently">
          {LABEL_RECENTLY_VIEWED}
        </Link>
        <div className={styles.right}>
          <div className={styles.thumbs}>
            {this.renderProducts(recentProducts)}
          </div>
          <Link
            to={normalizeUrl(PRODUCT_RECENTLY_ROUTER)}
            aria-label="Open Recently">
            <ArrowRight className="icon" />
          </Link>
        </div>
      </div>
    );
  }
  renderProducts(recentProducts) {
    let products = [];
    let nums = recentProducts.length - 5;
    for (var i = recentProducts.length - 1; i >= 0 && i > nums; i--) {
      let product = recentProducts[i].value;
      products.push(
        <ProductCardWrap
          className={styles.thumb}
          product={product}
          key={product.id}>
          <img
            src={product.image || ''}
            data-src={product.image || ''}
            alt={product.name}
            onError={e => onImgError(e)}
          />
        </ProductCardWrap>
      );
    }
    return products;
  }
}

export default withStyles(styles)(PanelRecentlyViewed);
