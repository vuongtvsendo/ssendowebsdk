import React from 'react';
import { shallow, mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { PanelRecentlyViewed } from './index';
import { LABEL_RECENTLY_VIEWED } from 'sendo-web-sdk/res/text';
import { MemoryRouter as Router } from 'react-router-dom';

describe('<PanelRecentlyViewed /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      recentProducts: [],
    };
    const wrapper = shallow(<PanelRecentlyViewed {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(PanelRecentlyViewed);
  });
  it('it should be ok text url', () => {
    const context = {
      ...ctx,
    };
    const props = {
      recentProducts: [],
    };
    const wrapper = mount(
      <Router>
        <PanelRecentlyViewed {...props} />
      </Router>,
      {
        context,
      }
    );
    expect(wrapper.find('Link').first().text()).toEqual(LABEL_RECENTLY_VIEWED);
  });
});
