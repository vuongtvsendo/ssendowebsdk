import React, { PureComponent } from 'react';
import Ripple from 'sendo-web-sdk/components/Ripple';
import _get from 'lodash/get';
import styles from './homeRich.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import AllCate from 'sendo-web-sdk/components/Icons/AllCate';
import Trend from 'sendo-web-sdk/components/Icons/Trend';
import Promotion from 'sendo-web-sdk/components/Icons/Promotion';
import Hotsale from 'sendo-web-sdk/components/Icons/Hotsale';
import bannerDefault from './banner-default.svg';
import Loading from 'sendo-web-sdk/components/Loading';
import {
  buildExternalSendoUrl,
  normalizeImgUrl,
} from 'sendo-web-sdk/helpers/url';
import LaptopIcon from 'sendo-web-sdk/components/Icons/LaptopIcon';
export class HomeRich extends PureComponent {
  handleLoadImg(e) {
    e.target.style.opacity = '1';
  }
  componentDidMount() {
    this.initSlider();
  }
  componentWillUnmount() {
    if (this.swiperInstance) {
      try {
        this.swiperInstance.destroy();
      } catch (e) {
        console.error('Loi tai qua trinh destroy cua Swiper:', e);
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      _get(prevProps, 'banners') &&
      this.banners !== _get(prevProps, 'banners')
    ) {
      this.initSlider();
    }
  }
  initSlider() {
    if (this.isInitializing) {
      return;
    }
    if (this.swiperInstance) {
      try {
        this.swiperInstance.destroy();
      } catch (e) {
        console.error('Loi tai qua trinh destroy cua Swiper:', e);
      }
    }
    const { banners } = this.props;
    if (!banners || !banners.length) {
      return;
    }
    const sliderSetting = {
      pagination: {
        el: '.swiper-pagination',
      },
      lazy: true,
    };
    this.isInitializing = true;
    import('sendo-web-sdk/components/Slider/Swiper').then(Swiper => {
      this.swiperInstance = new Swiper.default('.swiper-container', {
        ...sliderSetting,
      });
      setTimeout(() => {
        this.isInitializing = false;
      }, 1000);
    });
  }
  render() {
    const { banners } = this.props;
    return (
      <div className={styles.homeRich}>
        <div className={styles.sliderWrap}>
          <div className={styles.bannerLoading}>
            <img src={bannerDefault} alt="Sendo.vn" />
          </div>
          {banners.length > 0 && (
            <div className="swiper-container">
              <div className="swiper-wrapper">
                {banners.map((banner, index) => {
                  return (
                    <a
                      className="swiper-slide"
                      key={index}
                      href={buildExternalSendoUrl(banner.url)}>
                      <img
                        className="swiper-lazy"
                        onLoad={e => this.handleLoadImg(e)}
                        data-src={normalizeImgUrl(banner.image)}
                        alt={banner.title}
                        style={{ opacity: 0 }}
                      />
                      {index > 0 && (
                        <div className="swiper-lazy-preloader">
                          <Loading />
                        </div>
                      )}
                    </a>
                  );
                })}
              </div>
              <div className="swiper-pagination" />
            </div>
          )}
        </div>
        <div className={styles.shortcutLink}>
          <Ripple tagName="a" className="shortcut" to="/sitemap/">
            <span className="icon-wrap">
              <AllCate
                className={`${
                  styles.iconAllcate
                } icon icon-xlarge icon-inverse`}
              />
            </span>
            <strong className="name">Danh mục</strong>
          </Ripple>

          <Ripple tagName="a" className="shortcut" to="/xu-huong/">
            <span className="icon-wrap trend">
              <Trend
                className={`${styles.iconTrend} icon icon-xlarge icon-inverse`}
              />
            </span>
            <strong className="name">Xu hướng</strong>
          </Ripple>
          <Ripple tagName="a" className="shortcut" to="/khuyen-mai/">
            <span className="icon-wrap promotion">
              <Promotion
                className={`${
                  styles.iconPromotion
                } icon icon-xlarge icon-inverse`}
              />
            </span>
            <strong className="name">Khuyến mãi</strong>
          </Ripple>
          <Ripple tagName="a" className="shortcut" to="/ban-chay/">
            <span className="icon-wrap hotsale">
              <Hotsale
                className={`${
                  styles.iconHotsale
                } icon icon-xlarge icon-inverse`}
              />
            </span>
            <strong className="name">Bán chạy</strong>
          </Ripple>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(HomeRich);
