import React from 'react';
import { shallow, mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HomeRich } from './index';

describe('<HomeRich /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      banners: ['banner1'],
    };
    const wrapper = shallow(<HomeRich {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(HomeRich);
    expect(wrapper.find('.swiper-container').exists()).toEqual(true);
  });
  it('it should unmount ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      banners: [],
    };
    const wrapper = mount(<HomeRich {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(HomeRich);
    expect(wrapper.find('.swiper-container').exists()).toEqual(false);
  });
});
