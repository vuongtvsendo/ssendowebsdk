import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

import styles from './panelLoginBanner.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';

import ArrowRight from 'sendo-web-sdk/components/Icons/ArrowRight';
import IconLogin from 'sendo-web-sdk/components/Icons/IconLogin';
import { LABEL_LOGIN, TEXT_PANEL_LOGIN_BANNER } from 'sendo-web-sdk/res/text';

export class PanelLoginBanner extends Component {
  render() {
    return (
      <LoginWrapper event="click">
        <div className={styles.panelLoginBanner}>
          <IconLogin className={styles.iconBgd} />
          <div className={styles.text}>
            <h3>{LABEL_LOGIN}</h3>
            <em className="text-desc">{TEXT_PANEL_LOGIN_BANNER}</em>
          </div>
          <ArrowRight className="icon" />
        </div>
      </LoginWrapper>
    );
  }
}

export default withStyles(styles)(PanelLoginBanner);
