import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { PanelLoginBanner } from './index';
import { LABEL_LOGIN, TEXT_PANEL_LOGIN_BANNER } from 'sendo-web-sdk/res/text';

describe('<PanelLoginBanner /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<PanelLoginBanner />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(PanelLoginBanner);
    expect(wrapper.find('h3').text()).toEqual(LABEL_LOGIN);
    expect(wrapper.find('em').text()).toEqual(TEXT_PANEL_LOGIN_BANNER);
  });
});
