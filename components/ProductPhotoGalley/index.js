import React, { Component } from 'react';
import styles from './productPhotoGallery.css';
import classnames from 'classnames';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import ReactPlayer from 'react-player';
import _get from 'lodash/get';

import CarouselCustom from '../CarouselCustom';
import defaultImg from '../../images/default.png';

import ArrowRight from '../Icons/ArrowRight';
import Play from '../Icons/Play';

export class ProductPhotoGalley extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: defaultImg,
      currentVideo: '',
      currentIndex: props.activeIndex,
      totalMedia: 0,
      isPlayVideo: false,
    };
  }

  componentDidMount() {
    //stupid code :(
    if (
      _get(this.props.media, `[${this.state.currentIndex}].type`) === 'video'
    ) {
      this.setState({
        isPlayVideo: true,
        currentVideo: _get(
          this.props.media,
          `[${this.state.currentIndex}].video_url`
        ),
      });
    }
    this.setState({
      currentImage: _get(
        this.props.media,
        `[${this.state.currentIndex}].image`
      ),
      totalMedia: this.props.media.length,
    });
  }
  onChangeImage(image, index) {
    this.setState({
      currentImage: image,
      currentIndex: index,
      isPlayVideo: false,
    });
  }
  prev() {
    this.navigatorGallery('prev');
  }
  next() {
    this.navigatorGallery('next');
  }
  navigatorGallery(direction) {
    const { currentIndex, totalMedia } = this.state;
    const { media } = this.props;
    var nextIndex;
    if (direction === 'next') {
      nextIndex = (currentIndex + 1) % totalMedia;
    }
    if (direction === 'prev') {
      nextIndex = (currentIndex + totalMedia - 1) % totalMedia;
    }
    const nextImage = _get(media, `[${nextIndex}].image`);

    this.setState({
      currentImage: nextImage,
      currentIndex: nextIndex,
    });

    // Kiem tra item ke tiep co phai la video hay khong
    if (_get(media, `[${nextIndex}].type`) === 'video') {
      this.setState({
        isPlayVideo: true,
        currentVideo: _get(media, `[${nextIndex}].video_url`),
      });
    } else {
      if (this.state.isPlayVideo) {
        this.setState({
          isPlayVideo: false,
        });
      }
    }

    // this.carousel.scrollLeft(50);
  }
  playVideo(video, index) {
    this.setState({
      isPlayVideo: true,
      currentIndex: index,
      currentVideo: video.video_url,
    });
  }
  render() {
    const { media, alt } = this.props;
    return (
      <div className={styles.productPhotoGallery}>
        <div className={styles.main}>
          {!this.state.isPlayVideo && (
            <img
              className={styles.currentImage}
              src={this.state.currentImage}
              alt={alt}
            />
          )}

          {this.state.isPlayVideo && (
            <ReactPlayer
              className={styles.videoContainer}
              url={this.state.currentVideo}
              playing
              controls
              width={_get(window, 'innerWidth', 640)}
            />
          )}

          <div className={styles.navCtrl}>
            <button className={styles.btnPrev} onClick={() => this.prev()}>
              <ArrowRight className={styles.icon} />
            </button>
            <button className={styles.btnNext} onClick={() => this.next()}>
              <ArrowRight className={styles.icon} />
            </button>
          </div>
        </div>
        <footer className={styles.thumbnails}>
          <CarouselCustom onRef={ref => (this.carousel = ref)}>
            {media.map((item, i) => {
              const classes = classnames({
                [styles.item]: true,
                [styles.active]: i === this.state.currentIndex,
              });
              if (item.type === 'video') {
                return (
                  <div
                    key={i}
                    className={`${classes} ${styles.videoThumb}`}
                    onClick={() => this.playVideo(item, i)}>
                    <span className={styles.midle}>
                      <img src={item.video_thumb} alt="Thumbnail Video" />
                    </span>
                    <Play className={styles.iconPlay} />
                  </div>
                );
              }
              return (
                <div
                  key={i}
                  className={classes}
                  onClick={() => this.onChangeImage(item.image, i)}>
                  <img src={item.image_50x50} alt="Thumbnail" />
                </div>
              );
            })}
          </CarouselCustom>
        </footer>
      </div>
    );
  }
}

export default withStyles(styles)(ProductPhotoGalley);
