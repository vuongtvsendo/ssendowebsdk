import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductPhotoGalley } from './index';

describe('<ProductPhotoGalley /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      media: [],
    };
    const wrapper = shallow(<ProductPhotoGalley {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductPhotoGalley);
  });
});
