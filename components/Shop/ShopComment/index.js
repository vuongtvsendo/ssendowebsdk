import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import CommentItemForm from 'sendo-web-sdk/components/CommentItem/commentItem';
import CommentInputForm from 'sendo-web-sdk/components/CommentInput/commentInput';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as shopActions from 'sendo-web-sdk/actions/shop';
import Loading from 'sendo-web-sdk/components/Loading';
import styles from './index.css';
import Meta from 'sendo-web-sdk/helpers/meta';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import commentEmpty from 'sendo-web-sdk/images/empty-state/comment-empty.svg';

export class ShopComment extends PureComponent {
  sentComment = newComment => {
    // TODO: show toast message
    this.props.shopActions.setNewShopComment(newComment);
    this.props.shopActions.postShopComment(newComment).then(data => {
      this.props.toastActions.openToast({
        content: _get(
          data,
          'payload.comment.message',
          'Có lỗi xảy ra. Vui lòng thử lại.'
        ),
      });
    });
  };

  renderShopComment(shopComment) {
    const product = _get(shopComment, 'product', null);
    let comment = _get(shopComment, 'comment', null);
    const totalReply = _get(comment, 'total_reply', 0);
    const productCommentUrl = normalizeUrl(
      _get(product, 'cat_path', '') + '#hoi-dap'
    );
    comment = { ...comment, ...{ product_id: product.id } };
    return (
      <div className="panel round shop-comment" key={comment.comment_id}>
        <div className="panel-heading custom-1">
          <div className="card-horiz">
            <img
              src={_get(product, 'image', '')}
              alt={_get(product, 'name', '')}
              onError={e => onImgError(e)}
            />
            <div className="caption">
              <h3 className="heading-medium truncate-medium-2 card-name">
                {_get(product, 'name', '')}
              </h3>
              <div className="product-price">
                {_get(product, 'final_price') && (
                  <strong className="current">
                    {helperFunction.formatPrice(_get(product, 'final_price'))}
                  </strong>
                )}
                {_get(product, 'promotion_percent') > 0 && (
                  <span className="old">
                    {helperFunction.formatPrice(_get(product, 'final_price'))}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="panel no-shadow no-margin-bottom">
          <div className="panel-content">
            {comment && (
              <section>
                <CommentItemForm
                  disableDelete={false}
                  key={comment.comment_id}
                  comment={comment}
                />
              </section>
            )}
            {totalReply > 1 && (
              <Link className="see-more" to={productCommentUrl} replace>
                <span className="text-see-more">Xem thêm</span>
                <CaretDown className="icon-see-more" />
              </Link>
            )}
            <CommentInputForm
              comment={comment}
              cbSendComment={this.sentComment}
            />
          </div>
        </div>
      </div>
    );
  }

  renderCommentEmpty() {
    return (
      <div className="content-empty small">
        <div className="image-square">
          <img
            className="img"
            src={commentEmpty}
            alt="Shop hiện chưa có hỏi đáp"
          />
        </div>
        <strong className="title small">Shop hiện chưa có hỏi đáp.</strong>
      </div>
    );
  }

  renderLoading() {
    return (
      <div className="loading-more-wrapper">
        <Loading />
      </div>
    );
  }

  render() {
    const {
      shopInfo,
      shopLoading,
      shopLoadingMore,
      breadcrumb,
      init,
    } = this.props;
    const shopComments = _get(this.props, 'shopComments', []);
    const shopName = _get(shopInfo, 'shop_name', '');
    const title = `Hỏi đáp: Shop ${shopName} tư vấn thông tin sản phẩm! | Sendo.vn`;
    const description = `Trang hỏi đáp của người dùng dành cho Shop ${shopName} tại Sendo. Gửi câu hỏi cho shop, giải đáp thắc mắc và tư vấn mua hàng.`;
    const keywords = `Hỏi đáp ${shopName} , sendo shop.`;
    return (
      <div className="wrapper-shop">
        <Meta
          title={title}
          description={description}
          keywords={keywords}
          index={true}
          follow={true}
          breadcrumb={breadcrumb}
        />
        {shopComments &&
          shopComments.length > 0 &&
          shopComments.map(shopComment => {
            return this.renderShopComment(shopComment);
          })}
        {(shopLoading || shopLoadingMore) && this.renderLoading()}
        {shopComments &&
          shopComments.length === 0 &&
          init &&
          !shopLoading &&
          this.renderCommentEmpty()}
      </div>
    );
  }
}

ShopComment.propTypes = {
  shopComments: PropTypes.array.isRequired,
  shopInfo: PropTypes.object.isRequired,
  shopLoading: PropTypes.bool,
  shopLoadingMore: PropTypes.bool,
  breadcrumb: PropTypes.array,
};

export default connect(
  state => {
    return {};
  },
  dispatch => {
    return {
      shopActions: bindActionCreators(shopActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
    };
  }
)(withStyles(styles)(ShopComment));
