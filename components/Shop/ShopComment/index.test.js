import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ShopComment } from './index';

describe('<ShopComment /> component', () => {
  it('it should be render comment OK', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopInfo: {},
      shopComments: [
        {
          product: {
            product_id: 8405291,
            product_url: '',
            product_name: 'Đầm len dáng suông 1',
            product_image:
              'https://media3.scdn.vn/img2/2018/1_5/s9cExe_simg_02d57e_50x50_maxb.jpg',
            time_update: '07:45 | 23/01/2018',
          },
          comment: {
            comment_id: 1,
            customer_id: 2016112344,
            customer_name: 'H*** Thanh',
            customer_logo:
              'https://media3.scdn.vn/images/apps/icon_user_default_simg_c7d04b_35x35_maxb.png',
            content: 'Chất len gì đây shop. Cho mình xem ảnh thật nha',
            time_update: '07:45 | 23/01/2018',
            reply: [
              {
                id: 2,
                customer_id: 2016112344,
                customer_name: 'T*** Quyen',
                customer_logo:
                  'https://media3.scdn.vn/img/2014/7_23/avatar_user_2jb6apgefrjn9_simg_c7d04b_35x35_maxb.gif',
                content:
                  'Chị gửi lại sản phẩm nhé em? chị không hài lòng khi shop em bán hàng như kiểu lừa khách ',
                time_update: '07:45 | 23/01/2018',
                parent_id: 1,
              },
            ],
            totalReply: 10,
          },
        },
      ],
      loading: false,
      shopLoadingMore: false,
    };
    const wrapper = shallow(<ShopComment {...props} />, {
      context,
    });
    expect(wrapper.find('.wrapper-shop')).toHaveLength(1);
    expect(wrapper.find('.shop-comment')).toHaveLength(1);
  });
  it('it should be render loading OK', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopInfo: {},
      shopComments: [],
      loading: true,
      shopLoadingMore: false,
    };
    const wrapper = shallow(<ShopComment {...props} />, {
      context,
    });
    expect(wrapper.find('.wrapper-shop')).toHaveLength(1);
    expect(wrapper.find('.shop-comment')).toHaveLength(0);
  });
});
