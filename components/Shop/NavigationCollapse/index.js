import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import { Collapse } from 'reactstrap';
import { Link } from 'react-router-dom';
import ArrowRight from 'sendo-web-sdk/components/Icons/ArrowRight';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './styles.css';

export class NavigationCollapse extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeId: 0,
    };
  }
  toggleAccordion(id) {
    if (id === this.state.activeId) {
      this.setState({ activeId: 0 });
    } else {
      this.setState({ activeId: id });
    }
  }
  isActive(id) {
    return this.state.activeId === id;
  }
  getLinkCate(cate) {
    const baseUrl = _get(this.props, 'baseUrl', '');
    let cateUrl = normalizeUrl(baseUrl + '/' + cate.url_path);
    return cateUrl;
  }
  render() {
    const { categories } = this.props;

    return (
      <div className={styles.menuListCategory}>
        <ul className="list-category-vertical">
          {categories.map(cate => {
            return (
              <li
                className={`li-parent ${this.isActive(cate.id) ? 'open' : ''}`}
                key={cate.id}>
                <h3
                  className="title"
                  onClick={() => {
                    this.toggleAccordion(cate.id);
                  }}>
                  {cate.title}
                </h3>
                <Collapse isOpen={this.isActive(cate.id)}>
                  <ul className="ul-child">
                    {_get(cate, 'child.length', 0) > 0 &&
                      cate.child.map(sub => {
                        return (
                          <li className="li-child" key={sub.id}>
                            <Link to={this.getLinkCate(sub)}>
                              <span>{sub.title}</span>
                              <ArrowRight className="icon" />
                            </Link>
                          </li>
                        );
                      })}

                  </ul>
                </Collapse>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default withStyles(styles)(NavigationCollapse);
