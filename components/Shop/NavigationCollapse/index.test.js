import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { NavigationCollapse } from './index';

describe('<NavigationCollapse /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      categories: [
        {
          id: 1663,
          path: '1/2/1663',
          image:
            'https://media3.scdn.vn/images/ecom/category/cong-nghe-1663.jpg',
          image_mob: '',
          title: 'Điện thoại - Máy tính bảng',
          url_key: 'cong-nghe',
          url_path: 'cong-nghe/',
          display_order: 0,
          child: [
            {
              id: 1664,
              path: '1/2/1663/1664',
              image:
                'https://media3.scdn.vn/images/ecom/category/thiet-bi-di-dong-1664.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/cong-nghe/thiet-bi-di-dong.jpg',
              title: 'Điện thoại mới',
              url_key: 'thiet-bi-di-dong',
              url_path: 'cong-nghe/thiet-bi-di-dong/',
              display_order: 0,
              child: [
                {
                  id: 1665,
                  path: '1/2/1663/1664/1665',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/dien-thoai-di-dong-1665.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/1665.jpg',
                  title: 'Điện thoại phổ thông',
                  url_key: 'dien-thoai-di-dong',
                  url_path: 'cong-nghe/thiet-bi-di-dong/dien-thoai-di-dong/',
                  display_order: 1,
                  child: [],
                },
              ],
            },
            {
              id: 2339,
              path: '1/2/1663/2339',
              image:
                'https://media3.scdn.vn/images/ecom/category/may-da-qua-su-dung-2339.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/cong-nghe/may-da-qua-su-dung.jpg',
              title: 'Điện thoại cũ',
              url_key: 'may-da-qua-su-dung',
              url_path: 'cong-nghe/may-da-qua-su-dung/',
              display_order: 4,
              child: [
                {
                  id: 2360,
                  path: '1/2/1663/2339/2360',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/dien-thoai-cu-2360.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/2360.jpg',
                  title: 'Nokia cổ',
                  url_key: 'dien-thoai-cu',
                  url_path: 'cong-nghe/may-da-qua-su-dung/dien-thoai-cu/',
                  display_order: 1,
                  child: [],
                },
              ],
            },
          ],
        },
        {
          id: 8,
          path: '1/2/8',
          image:
            'https://media3.scdn.vn/images/ecom/category/thoi-trang-nu-8.jpg',
          image_mob: '',
          title: 'Thời trang nữ',
          url_key: 'thoi-trang-nu',
          url_path: 'thoi-trang-nu/',
          display_order: 2,
          child: [
            {
              id: 26,
              path: '1/2/8/26',
              image: 'https://media3.scdn.vn/images/ecom/category/dam-26.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/thoi-trang-nu/dam.jpg',
              title: 'Đầm, váy',
              url_key: 'dam',
              url_path: 'thoi-trang-nu/dam/',
              display_order: 1,
              child: [
                {
                  id: 2483,
                  path: '1/2/8/26/2483',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/dam-len-2483.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/2483.jpg',
                  title: 'Đầm len',
                  url_key: 'dam-len',
                  url_path: 'thoi-trang-nu/dam/dam-len/',
                  display_order: 16,
                  child: [],
                },
                {
                  id: 33,
                  path: '1/2/8/26/33',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/dam-vay-khac-33.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/33.jpg',
                  title: 'Đầm, váy khác',
                  url_key: 'dam-vay-khac',
                  url_path: 'thoi-trang-nu/dam/dam-vay-khac/',
                  display_order: 17,
                  child: [],
                },
              ],
            },
            {
              id: 9,
              path: '1/2/8/9',
              image: 'https://media3.scdn.vn/images/ecom/category/ao-nu-9.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/thoi-trang-nu/ao-nu.jpg',
              title: 'Áo nữ',
              url_key: 'ao-nu',
              url_path: 'thoi-trang-nu/ao-nu/',
              display_order: 2,
              child: [
                {
                  id: 647,
                  path: '1/2/8/9/647',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/ao-len-647.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/647.jpg',
                  title: 'Áo len',
                  url_key: 'ao-len',
                  url_path: 'thoi-trang-nu/ao-nu/ao-len/',
                  display_order: 13,
                  child: [],
                },
              ],
            },
            {
              id: 18,
              path: '1/2/8/18',
              image:
                'https://media3.scdn.vn/images/ecom/category/quan-nu-18.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/thoi-trang-nu/quan-nu.jpg',
              title: 'Quần nữ',
              url_key: 'quan-nu',
              url_path: 'thoi-trang-nu/quan-nu/',
              display_order: 4,
              child: [
                {
                  id: 23,
                  path: '1/2/8/18/23',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/quan-legging-23.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/23.jpg',
                  title: 'Quần legging',
                  url_key: 'quan-legging',
                  url_path: 'thoi-trang-nu/quan-nu/quan-legging/',
                  display_order: 8,
                  child: [],
                },
              ],
            },
            {
              id: 52,
              path: '1/2/8/52',
              image:
                'https://media3.scdn.vn/images/ecom/category/do-lot-va-ngu-52.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/thoi-trang-nu/do-lot-va-ngu.jpg',
              title: 'Đồ lót',
              url_key: 'do-lot-va-ngu',
              url_path: 'thoi-trang-nu/do-lot-va-ngu/',
              display_order: 6,
              child: [
                {
                  id: 53,
                  path: '1/2/8/52/53',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/ao-nguc-53.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/53.jpg',
                  title: 'Áo ngực',
                  url_key: 'ao-nguc',
                  url_path: 'thoi-trang-nu/do-lot-va-ngu/ao-nguc/',
                  display_order: 0,
                  child: [],
                },
                {
                  id: 61,
                  path: '1/2/8/52/61',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/do-lot-dinh-hinh-61.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/61.jpg',
                  title: 'Đồ lót định hình',
                  url_key: 'do-lot-dinh-hinh',
                  url_path: 'thoi-trang-nu/do-lot-va-ngu/do-lot-dinh-hinh/',
                  display_order: 0,
                  child: [],
                },
              ],
            },
            {
              id: 1938,
              path: '1/2/8/1938',
              image:
                'https://media3.scdn.vn/images/ecom/category/do-mac-nha-do-ngu-1938.jpg',
              image_mob:
                'https://media3.scdn.vn/images/ecom/img-nav-cate/thoi-trang-nu/do-mac-nha-do-ngu.jpg',
              title: 'Đồ mặc nhà, đồ ngủ',
              url_key: 'do-mac-nha-do-ngu',
              url_path: 'thoi-trang-nu/do-mac-nha-do-ngu/',
              display_order: 8,
              child: [
                {
                  id: 1943,
                  path: '1/2/8/1938/1943',
                  image:
                    'https://media3.scdn.vn/images/ecom/category/do-ngu-bo-1943.jpg',
                  image_mob:
                    'https://media3.scdn.vn/images/ecom/category/1943.jpg',
                  title: 'Đồ ngủ bộ',
                  url_key: 'do-ngu-bo',
                  url_path: 'thoi-trang-nu/do-mac-nha-do-ngu/do-ngu-bo/',
                  display_order: 0,
                  child: [],
                },
              ],
            },
          ],
        },
      ],
    };
    const wrapper = shallow(<NavigationCollapse {...props} />, {
      context,
    });
    expect(wrapper.find('.list-category-vertical')).toHaveLength(1);
    expect(wrapper.find('.li-parent')).toHaveLength(2);
    expect(wrapper.find('.li-child')).toHaveLength(7);
  });
});
