import React, { PureComponent } from 'react';
import styles from './styles.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';
import _get from 'lodash/get';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';

import * as helperFunction from 'sendo-web-sdk/helpers/common';
import { TEXT_SHOP_NORATING } from 'sendo-web-sdk/res/text';
import ReputationIcon from 'sendo-web-sdk/components/Icons/ReputationIcon';
import HeartFull from 'sendo-web-sdk/components/Icons/HeartFull';
import Lotus from 'sendo-web-sdk/components/Icons/Lotus';
import StarRating from 'sendo-web-sdk/components/StarRating';
import Image from 'sendo-web-sdk/components/Image';
import shopAvatarDefault from 'sendo-web-sdk/images/shop-default.jpg';

export class ShopRich extends PureComponent {
  getRatingInfo(shopInfo) {
    const rating_info = _get(shopInfo, 'rating_info', {});
    let rating = {};
    rating.ratingNumber = rating_info.total_rat || 0;
    rating.ratingStar = rating_info.percent_star || 0;
    rating.ratingPercent = rating.ratingStar / 5 * 100;
    return rating;
  }
  toggleWishlist() {
    if (_get(this, 'props.toggleWishlist') instanceof Function) {
      this.props.toggleWishlist();
    }
  }
  render() {
    const { shopInfo } = this.props;
    const ratingInfo = this.getRatingInfo(shopInfo);
    const btnWishlistClass = classnames({
      'btn-icon-small': true,
      active: _get(shopInfo, 'is_liked', 0),
    });
    return (
      <div className={styles.shopRich}>
        <div className={styles.banner}>
          <Image src={shopInfo.shop_cover} alt={shopInfo.shop_name} />
        </div>

        <div className={styles.logoBlock}>
          <div className={styles.logo}>
            <div className={styles.logoInner}>
              <Image
                src={shopInfo.logo_120x60}
                default={shopAvatarDefault}
                alt={shopInfo.shop_name}
              />
            </div>
            {_get(shopInfo, 'is_certified', 0) > 0 && (
              <ReputationIcon className={`${styles.iconUytin} icon`} />
            )}
          </div>
          {shopInfo.score > 0 && (
            <div className={styles.shopPoint}>
              <span className="text-small">Điểm uy tín: </span>
              <strong className="text-small">
                {helperFunction.formatNumber(shopInfo.score)}
              </strong>
              {shopInfo.lotus !== 0 && (
                <span className={styles.lotusPoint}>
                  {this.renderLotus(shopInfo.lotus, shopInfo.lotus_class)}
                </span>
              )}
            </div>
          )}
          <div className={styles.btnWishlist}>
            <LoginWrapper event="click" callback={e => this.toggleWishlist(e)}>
              <button className={btnWishlistClass} aria-label="Toggle Wishlist">
                <HeartFull className="icon" />
              </button>
            </LoginWrapper>
          </div>
        </div>

        <div className={`${styles.chatInfo} text-small`}>
          <div className={styles.inner}>
            <div className={styles.item}>
              <span className="text-desc">Tỷ lệ phản hồi chat: </span>
              <strong className="text-primary">93%</strong>
            </div>
            <div className={styles.item}>
              <span className="text-desc">Thời gian phản hồi: </span>
              <strong className="text-primary">Trong ngày</strong>
            </div>
          </div>
        </div>

        <div className={`${styles.shopStatistics} text-small`}>
          <div className={styles.item}>
            {ratingInfo.ratingNumber > 0 && (
              <div className={styles.rating}>
                <div className={styles.ratingStar}>
                  <strong>{ratingInfo.ratingStar}</strong>
                  <StarRating value={ratingInfo.ratingPercent} />
                </div>
                <p>
                  <span className="text-desc">Đánh giá </span>
                  <strong>
                    ({helperFunction.formatNumber(ratingInfo.ratingNumber)})
                  </strong>
                </p>
              </div>
            )}
            {ratingInfo.ratingNumber === 0 && (
              <div className={styles.rating}>
                <span className="text-desc">Chưa có đánh giá </span>
              </div>
            )}
          </div>
          <div className={styles.item}>
            <div className={styles.product}>
              <p>
                <strong>
                  {helperFunction.formatNumber(shopInfo.product_total) || 0}
                </strong>
              </p>
              <p>
                <span className="text-desc">Sản phẩm</span>
              </p>
            </div>
          </div>
          <div className={styles.item}>
            <div className={styles.favor}>
              <p>
                <strong>
                  {helperFunction.formatNumber(shopInfo.favorite_total) || 0}
                </strong>
              </p>
              <p>
                <span className="text-desc">Yêu thích</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
  renderLotus(number, color) {
    let icons = [];
    for (var i = 0; i < number; i++) {
      const iconClass = classnames({
        [styles.iconLotus]: true,
        [styles.yellow]: color === 'yellow',
        [styles.red]: color === 'red',
        [styles.purple]: color === 'purple',
      });
      icons.push(<Lotus key={i} className={iconClass} />);
    }
    return icons;
  }
}

export default withStyles(styles)(ShopRich);
