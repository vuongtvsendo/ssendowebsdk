import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Mobile from 'sendo-web-sdk/components/Icons/Mobile';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import Meta from 'sendo-web-sdk/helpers/meta';
import Loading from 'sendo-web-sdk/components/Loading';

import styles from './index.css';
export class ShopDetail extends PureComponent {
  render() {
    const { shopInfo, shopLoading } = this.props;
    const shippingInfo = _get(this.props, 'shopInfo.shipping_info', []);
    let shopName = _get(this.props, 'shopInfo.shop_name', '');
    const title = `Thông tin, địa chỉ Shop ${shopName} | Sendo.vn`;
    const description = `Shop ${shopName} –  – Thông tin, địa chỉ ${shopName}  tại Sendo - Sàn TMĐT chính thức của tập đoàn FPT.`;
    const keywords = `Shop ${shopName} –  – Thông tin, địa chỉ Shop ${shopName}  tại Sendo - Sàn TMĐT chính thức của tập đoàn FPT.`;
    return (
      <div className="wrapper-shop">
        {shopLoading && (
          <div className="loading-more-wrapper">
            <Loading />
          </div>
        )}
        {!shopLoading && (
          <div>
            <Meta
              title={title}
              description={description}
              keywords={keywords}
              index={true}
              follow={true}
              breadcrumb={this.props.breadcrumb}
            />
            {shopInfo.shop_slogan && <p>"{shopInfo.shop_slogan}"</p>}
            <div className="panel round">
              <div className="panel-heading">
                <h2>Hoạt động</h2>
              </div>
              <div className="panel-heading custom-1">
                <span>Thời gian hoạt động:</span>
                <span className="text-desc">{shopInfo.created_at_str}</span>
              </div>
              <div className="panel-heading custom-1">
                <span>Thời gian xử lý đơn hàng:</span>
                <span className="text-desc">{shopInfo.response_time}</span>
              </div>
            </div>
            {shippingInfo &&
              shippingInfo.length > 0 && (
                <div className="panel round">
                  <div className="panel-heading">
                    <h2>Hỗ trợ phí vận chuyển</h2>
                  </div>
                  {shippingInfo.map(s => {
                    return (
                      <div key={s.position} className="panel-heading custom-1">
                        <span>
                          Mức {s.position}:{' '}
                          <span className="text-shipping">
                            {helperFunction.formatPrice(s.seller_support_fee)}
                          </span>{' '}
                          cho đơn hàng từ
                        </span>
                        <span className="text-primary">
                          {helperFunction.formatPrice(s.order_amount)}
                        </span>
                      </div>
                    );
                  })}
                </div>
              )}
            {shopInfo.app_dis_count_percent > 0 && (
              <div className="panel round">
                <div className="panel-heading">
                  <h2>Giảm giá trên App</h2>
                  <span className="discount-label discount-label-app-reverse">
                    <Mobile className="icon-medium" />
                    {shopInfo.app_dis_count_percent} %
                  </span>
                </div>
              </div>
            )}
            <div className="panel round">
              <div className="panel-heading">
                <h2>Thông tin shop</h2>
              </div>
              <div className="panel-heading custom-1">
                <div className="content">
                  <span>Địa chỉ:</span>
                  <span className="text-desc">{shopInfo.warehouse}</span>
                </div>
              </div>
              <div className="panel-heading custom-1">
                <div className="content">
                  <span>Điện thoại:</span>
                  <span className="text-desc">{shopInfo.telephone}</span>
                </div>
              </div>
              <div className="panel-heading custom-1">
                <div className="content">
                  <span>Email:</span>
                  <span className="text-desc">{shopInfo.email}</span>
                </div>
              </div>
              <div className="panel-heading custom-1">
                <div className="content">
                  <span>Website:</span>
                  <a href={shopInfo.website}>
                    <span className="text-desc">{shopInfo.website}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ShopDetail.propTypes = {
  shopInfo: PropTypes.object.isRequired,
  shopLoading: PropTypes.bool,
};

export default withStyles(styles)(ShopDetail);
