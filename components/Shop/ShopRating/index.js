import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import ProductRating from 'sendo-web-sdk/components/Product/ProductRating';
import Loading from 'sendo-web-sdk/components/Loading';
import Meta from 'sendo-web-sdk/helpers/meta';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import ratingEmpty from 'sendo-web-sdk/images/empty-state/rating-empty.svg';
import styles from './index.css';

export class ShopRating extends PureComponent {
  renderShopRating(shopRating) {
    return (
      <div className="panel round shop-rating" key={shopRating.rating_id}>
        <div className="panel-heading custom-1">
          <div className="card-horiz">
            <img
              src={_get(shopRating, 'image', '')}
              alt={_get(shopRating, 'product_name', '')}
              onError={e => onImgError(e)}
            />
            <div className="caption">
              <h3 className="heading-medium truncate-medium-2 card-name">
                {_get(shopRating, 'product_name', '')}
              </h3>
              <div className="product-price">
                {_get(shopRating, 'price') && (
                  <strong className="current">
                    {helperFunction.formatPrice(_get(shopRating, 'price'))}
                  </strong>
                )}
              </div>
            </div>
          </div>
        </div>
        <section>
          <ProductRating
            ratingInfo={{}}
            ratings={[shopRating]}
            location={{}}
            isShowHeader={false}
          />
        </section>
      </div>
    );
  }

  renderLoading(length, totalRating) {
    return (
      <div className={styles.loadingMoreIcon}>
        {length > 0 &&
          totalRating > 0 && (
            <p
              className={
                styles.infoPaging
              }>{`Tổng số ${length}/${totalRating}`}</p>
          )}
        <Loading />
      </div>
    );
  }

  renderEmptyRating() {
    return (
      <div className="content-empty small">
        <div className="image-square">
          <img
            className="img"
            src={ratingEmpty}
            alt="Shop hiện chưa có đánh giá"
          />
        </div>
        <strong className="title small">Shop hiện chưa có đánh giá.</strong>
      </div>
    );
  }
  render() {
    const {
      shopInfo,
      shopRatings,
      shopLoading,
      shopLoadingMore,
      breadcrumb,
      totalRating,
      init,
    } = this.props;
    const shopName = _get(shopInfo, 'shop_name', '');
    const title = `Đánh giá Shop ${shopName} trên Sendo.vn | Sendo.vn`;
    const description = `Lịch sử đánh giá và phản hồi về sản phẩm, dịch vụ của Shop ${shopName} trên Sendo.vn của các khách hàng đã từng mua hàng.`;
    const keywords = `Đánh giá phản hồi ${shopName}, sendo shop`;
    return (
      <div className="wrapper-shop">
        <Meta
          title={title}
          description={description}
          keywords={keywords}
          index={true}
          follow={true}
          breadcrumb={breadcrumb}
        />
        {shopRatings.length > 0 &&
          shopRatings.map(shopRating => {
            return this.renderShopRating(shopRating);
          })}
        {(shopLoading || shopLoadingMore) &&
          this.renderLoading(shopRatings.length, totalRating)}
        {shopRatings.length === 0 &&
          init &&
          !shopLoading &&
          this.renderEmptyRating()}
      </div>
    );
  }
}

export default withStyles(styles)(ShopRating);
