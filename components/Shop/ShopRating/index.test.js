import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ShopRating } from './index';

describe('<ShopRating /> component', () => {
  it('it should be render rating OK', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopRatings: [
        {
          rating_id: 1474122296,
          address: 'Hồ Chí Minh',
          comment: 'Sản phẩm gần giống mô tả',
          image: 'https://media3.scdn.vn/img2/2017/12_12/j6DrHk.jpg',
          customer_id: 2016112344,
          star: 3,
          status: 'Bình thường',
          type: 3,
          update_time: '09:53 | 31/01/2018',
          mob_update_time: 1517367194,
          user_name: 'N*** Trịnh',
          crr_star: 3,
          cs_star: 0,
          quantity: 0,
          price: 300000,
          arr_image: [],
          product_name: 'Xe hơi điều khiển từ xa',
          product_path: 'xe-hoi-dieu-khien-tu-xa-8582024.html',
          media: [],
        },
      ],
      totalRating: 1,
      loading: false,
      shopLoadingMore: false,
    };
    const wrapper = shallow(<ShopRating {...props} />, {
      context,
    });
    expect(wrapper.find('.wrapper-shop')).toHaveLength(1);
    expect(wrapper.find('.shop-rating')).toHaveLength(1);
  });
  it('it should be render loading OK', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopRatings: [],
      totalRating: 1,
      loading: true,
      shopLoadingMore: false,
    };
    const wrapper = shallow(<ShopRating {...props} />, {
      context,
    });
    expect(wrapper.find('.wrapper-shop')).toHaveLength(1);
    expect(wrapper.find('.shop-rating')).toHaveLength(0);
  });
});
