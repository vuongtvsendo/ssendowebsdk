import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductBenefit } from './index';
import { LABEL_PRODUCT_BENEFIT } from 'sendo-web-sdk/res/text';
import Collapse from 'reactstrap/lib/Collapse';

describe('<ProductBenefit /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      benefits: [],
      discount: {
        list_discount: ['item1'],
      },
    };
    const wrapper = shallow(<ProductBenefit {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductBenefit);
    expect(wrapper.find('.panel-heading h2').text()).toEqual(
      LABEL_PRODUCT_BENEFIT
    );
    expect(wrapper.find(Collapse).exists()).toEqual(true);
  });
});
