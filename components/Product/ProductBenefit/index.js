import React, { PureComponent } from 'react';
import styles from './productBenefit.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';
import _get from 'lodash/get';

import { Modal } from 'reactstrap';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import Collapse from 'reactstrap/lib/Collapse';

// import LabelIcon from '../LabelIcon';
import Revert from 'sendo-web-sdk/components/Icons/Revert';
import DupCheck from 'sendo-web-sdk/components/Icons/DupCheck';
import Shield from 'sendo-web-sdk/components/Icons/Shield';
import Install from 'sendo-web-sdk/components/Icons/Install';
import FortyEight from 'sendo-web-sdk/components/Icons/FortyEight';
import Hand from 'sendo-web-sdk/components/Icons/Hand';
import CaretUp from 'sendo-web-sdk/components/Icons/CaretUp';
import Info from 'sendo-web-sdk/components/Icons/Info';
import Seven from 'sendo-web-sdk/components/Icons/Seven';
import Label from 'sendo-web-sdk/components/Icons/Label';

import { LABEL_PRODUCT_BENEFIT } from 'sendo-web-sdk/res/text';

export class ProductBenefit extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      collapse: true,
      benefitModalOpen: false,
      currentPopover: { tooltip_title: '', tooltip_content: '' },
    };
  }
  collapseToogle() {
    this.setState({ collapse: !this.state.collapse });
  }
  openPopover(content) {
    this.setState({
      benefitModalOpen: true,
      currentPopover: content,
    });
  }
  togglePopover() {
    this.setState({ benefitModalOpen: !this.state.benefitModalOpen });
  }
  closePopover() {
    this.setState({ benefitModalOpen: false });
  }
  render() {
    const { benefits, discount } = this.props;
    const collapseClass = classnames({
      [styles.promotion]: true,
      [styles.collapseClose]: !this.state.collapse,
    });
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>{LABEL_PRODUCT_BENEFIT}</h2>
        </div>

        <div className="panel-content no-padding-bottom">
          <div className={`${styles.groupLabel} row`}>
            {benefits.map((benefit, i) => {
              return (
                <div className="col-6" key={i}>
                  {this.renderLabelIcon(benefit)}
                </div>
              );
            })}
          </div>
          {discount.list_discount &&
            discount.list_discount.length > 0 &&
            <div className={collapseClass}>
              <div
                className={styles.collapseCtrl}
                onClick={() => this.collapseToogle()}>
                <span className={styles.text}>
                  Mua với giá ưu đãi nhất:
                  {discount &&
                    <strong>
                      {helperFunction.formatPrice(
                        _get(discount, 'total_min_price', 0)
                      )}
                    </strong>}
                </span>
                <CaretUp className={styles.iconCollapse} />
              </div>

              {discount &&
                discount.list_discount &&
                <Collapse isOpen={this.state.collapse}>
                  <ul className={styles.listBenefit}>
                    {discount.list_discount.map((item, i) => {
                      return (
                        <li
                          key={i}
                          className={styles.promotionItem}
                          onClick={() => this.openPopover(item)}>
                          <Info className={`${styles.iconInfo} icon-small`} />
                          <em className={styles.promotionText}>
                            {item.title}
                          </em>
                          <strong className={styles.promotionValue}>
                            -{helperFunction.formatPrice(item.discount)}
                          </strong>
                        </li>
                      );
                    })}
                  </ul>
                </Collapse>}
            </div>}

        </div>

        <Modal
          className={styles.modalPopover}
          modalClassName={styles.modalPopoverRoot}
          contentClassName={styles.modalPopoverContent}
          isOpen={this.state.benefitModalOpen}
          toggle={() => this.togglePopover()}>
          <header className={styles.popoverHeader}>
            {this.state.currentPopover.tooltip_title}
          </header>
          <div className={styles.content}>
            <div
              className={styles.scrollContainner}
              dangerouslySetInnerHTML={{
                __html: this.state.currentPopover.tooltip_content,
              }}
            />
          </div>
          <footer className={styles.popoverFooter}>
            <button
              className={styles.btnClose}
              onClick={() => this.closePopover()}>
              Đóng
            </button>
          </footer>
        </Modal>

      </div>
    );
  }
  renderLabelIcon(benefit) {
    return (
      <div
        className={styles.labelIcon}
        onClick={() => this.openPopover(benefit)}>
        <Label className={styles.bgdIcon} fill={benefit.color} />
        <span className={styles.labelText}>{benefit.title}</span>
        <span className={styles.icon}>{this.renderIcon(benefit.icon)}</span>
      </div>
    );
  }
  renderIcon(icon) {
    switch (icon) {
      case 'Revert':
        return <Revert />;
      case 'Seven':
        return <Seven />;
      case 'Shield':
        return <Shield />;
      case 'Install':
        return <Install />;
      case 'DupCheck':
        return <DupCheck />;
      case 'Hande':
        return <Hand />;
      default:
        return <FortyEight />;
    }
  }
}

export default withStyles(styles)(ProductBenefit);
