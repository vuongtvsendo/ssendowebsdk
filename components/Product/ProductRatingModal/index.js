import React, { PureComponent } from 'react';
import reactDOM from 'react-dom';
import classnames from 'classnames';
import styles from './productRatingModal.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';

import _get from 'lodash/get';
import _debounce from 'lodash/debounce';
import { Modal } from 'reactstrap';
import RatingChart from 'sendo-web-sdk/components/RatingChart';
import RatingItem from 'sendo-web-sdk/components/RatingItem';
import Loading from 'sendo-web-sdk/components/Loading';
import ProductPhotoGalley from 'sendo-web-sdk/components/ProductPhotoGalley';
import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';
import IconClose from 'sendo-web-sdk/components/Icons/IconClose';

export class ProductRatingModal extends PureComponent {
  currentRatingPage = 1;
  isLoadingMore = false;
  isOutOfItem = false;

  constructor(props) {
    super(props);
    this.state = {
      filterImage: 0,
      isOpenGallery: false,
      galleryData: [],
      activeIndex: 0,
      filterLoading: false,
    };
    this.handleScroll = _debounce(this.handleScroll.bind(this), 50);
    this.openGallery = this.openGallery.bind(this);
  }
  componentDidMount() {
    this.props.productActions.getProductRating(this.props.product, {
      is_rating_image: this.state.filterImage,
    });
    this.container.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    if (this.state.filterImage === 1) {
      this.handleFilter();
    }
    this.container.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll() {
    if (this.isOutOfItem) return;
    window.requestAnimationFrame(() => {
      var container = reactDOM.findDOMNode(this.container);
      var scroller = reactDOM.findDOMNode(this.scroller);
      this.debouncing = false;
      if (container && scroller) {
        let containerHeight = container.clientHeight;
        let scrollHeight = scroller.clientHeight;
        if (
          scrollHeight <= container.scrollTop + containerHeight &&
          !this.isLoadingMore
        ) {
          this.loadRating().then(() => {
            this.isLoadingMore = false;
          });
          this.isLoadingMore = true;
        }
      }
    });
  }
  loadRating() {
    let paging = ++this.currentRatingPage;
    return this.props.productActions
      .getProductRatingMore(this.props.product, {
        p: paging,
        is_rating_image: this.state.filterImage,
      })
      .then(data => {
        this.isOutOfItem = _get(data, 'payload.ratings.length', 0) === 0;
      });
  }
  handleFilter() {
    this.isOutOfItem = false;
    let filterImage = this.state.filterImage ? 0 : 1;
    this.currentRatingPage = 1;
    this.props.productActions
      .getProductRating(this.props.product, {
        is_rating_image: filterImage,
      })
      .then(data => {
        this.setState({ filterLoading: false });
      });
    this.setState({ filterImage: filterImage, filterLoading: true });
  }
  closeGallery() {
    this.setState({ isOpenGallery: false });
  }
  openGallery(media, indexActive) {
    this.setState({
      isOpenGallery: true,
      galleryData: media,
      activeIndex: indexActive,
    });
  }
  render() {
    const classesFilter = classnames({
      [styles.btnFilter]: true,
      [styles.active]: this.state.filterImage === 1,
    });
    return (
      <div
        className={styles.productRatingModal}
        ref={ref => (this.container = ref)}>
        {this.state.filterLoading &&
          <div className={styles.filterLoading}>
            <Loading />
          </div>}
        <div className={styles.content} ref={ref => (this.scroller = ref)}>
          <div className={styles.ratingReport}>
            <RatingChart rating={_get(this.props, 'product.rating_info')} />
            <div className={styles.btnFilterWrap}>
              <button
                className={classesFilter}
                onClick={() => this.handleFilter()}>
                <span className="text-small">Đánh giá hình ảnh</span>{' '}
                <IconClose className="icon icon-small icon-inverse" />
              </button>
            </div>
          </div>
          <div className={styles.ratingList}>
            {this.props.product &&
              _get(this.props, 'product.ratings').map(rating => {
                return (
                  <RatingItem
                    openGallery={this.openGallery}
                    key={rating.rating_id}
                    rating={rating}
                  />
                );
              })}
          </div>
          <div className={styles.loadingMoreIcon}>
            <p className={styles.infoPaging}>{`Tổng số ${_get(
              this.props,
              'product.ratings'
            ).length}/${_get(
              this.props,
              'product.rating_info.total_rated'
            )}`}</p>
            {_get(this.props, 'product.rating_loading') && <Loading />}
          </div>
        </div>
        <Modal modalClassName="modal-full" isOpen={this.state.isOpenGallery}>
          <button
            className={styles.btnClose}
            onClick={() => this.closeGallery()}>
            <CloseCircle className={`${styles.iconClose} icon`} />
          </button>
          <ProductPhotoGalley
            activeIndex={this.state.activeIndex}
            media={this.state.galleryData}
          />
        </Modal>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      product: state.product.productDetail,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
    };
  }
)(withStyles(styles)(ProductRatingModal));
