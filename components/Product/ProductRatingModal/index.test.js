import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductRatingModal } from './index';
import RatingItem from 'sendo-web-sdk/components/RatingItem';

describe('<ProductRatingModal /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        ratings: [{ rating_id: 0 }, { rating_id: 1 }, { rating_id: 2 }],
      },
    };
    const wrapper = shallow(<ProductRatingModal {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductRatingModal);
    expect(wrapper.find(RatingItem)).toHaveLength(props.product.ratings.length);
  });
});
