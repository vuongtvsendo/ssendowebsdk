import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductShop } from './index';

describe('<ProductShop /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopInfo: {},
    };
    const wrapper = shallow(<ProductShop {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductShop);
  });
});
