import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './productShop.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as helperFunction from 'sendo-web-sdk/helpers/common';

import classnames from 'classnames';
import Phone from 'sendo-web-sdk/components/Icons/Phone';
import ReputationIcon from 'sendo-web-sdk/components/Icons/ReputationIcon';
import Lotus from 'sendo-web-sdk/components/Icons/Lotus';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { TEXT_SHOP_NORATING } from 'sendo-web-sdk/res/text';
import Image from 'sendo-web-sdk/components/Image';
import shopAvatarDefault from 'sendo-web-sdk/images/shop-default.jpg';

export class ProductShop extends PureComponent {
  renderLotus(number, color) {
    for (var i = 0; i < number; i++) {
      const iconClass = classnames({
        [styles.iconLotus]: true,
        [styles.yellow]: color === 'yellow',
        [styles.red]: color === 'red',
        [styles.purple]: color === 'purple',
      });
      return <Lotus className={iconClass} />;
    }
  }
  render() {
    const {
      shop_logo,
      shop_url,
      shop_name,
      lotus,
      lotus_class,
      phone_number,
      good_review_percent,
      score,
      is_certified,
      warehourse_region_name,
    } = this.props.shopInfo;

    return (
      <div className={`${styles.productShop} panel`}>
        <Link
          to={normalizeUrl(shop_url)}
          className={`${styles.shopName} text-overflow`}>
          <em className={styles.gray}>Được bán bởi:&nbsp;</em>
          <strong>{shop_name}</strong>
        </Link>
        <div className={styles.wrap}>
          <Link to={normalizeUrl(shop_url)} className={styles.shopLogo}>
            <Image
              src={shop_logo}
              alt={shop_name}
              default={shopAvatarDefault}
            />
            {is_certified > 0 && (
              <ReputationIcon className={styles.shopUytin} />
            )}
          </Link>
          <Link to={normalizeUrl(shop_url)} className={styles.shopInfo}>
            <p>
              Điểm uy tín:&nbsp;
              <strong>
                {score !== 0
                  ? helperFunction.formatNumber(score)
                  : TEXT_SHOP_NORATING}
              </strong>
              {score !== 0 && (
                <span className={styles.lotusPoint}>
                  {this.renderLotus(lotus, lotus_class)}
                </span>
              )}
            </p>
            {score !== 0 && (
              <p>
                Đơn hàng tốt:&nbsp;
                <strong>
                  {good_review_percent && good_review_percent.toFixed(2)}%
                </strong>
              </p>
            )}
            <p>
              Kho hàng:&nbsp;
              <strong className={styles.gray}>{warehourse_region_name}</strong>
            </p>
          </Link>
        </div>
        <a
          href={`tel:${phone_number}`}
          className={styles.shopPhone}
          aria-label="Shop's phone number">
          <Phone className={styles.iconPhone} />
        </a>
      </div>
    );
  }
}

export default withStyles(styles)(ProductShop);
