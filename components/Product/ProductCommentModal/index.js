import React, { PureComponent } from 'react';
import reactDOM from 'react-dom';
// import classnames from 'classnames';
import styles from './productCommentModal.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';

import _get from 'lodash/get';
import _debounce from 'lodash/debounce';
import CommentItem from 'sendo-web-sdk/components/CommentItem';
import Loading from 'sendo-web-sdk/components/Loading';
import commentEmpty from 'sendo-web-sdk/images/empty-state/comment-empty.svg';

export class ProductCommentModal extends PureComponent {
  currentCommentPage = 1;
  isLoadingMore = false;
  isOutOfItem = false;

  constructor(props) {
    super(props);
    this.state = { currentReplyActive: '' };
    this.handleScroll = _debounce(this.handleScroll.bind(this), 50);
  }
  componentDidMount() {
    this.props.productActions.getProductComment(this.props.product);
    this.container.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    this.container.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    if (this.isOutOfItem) return;
    window.requestAnimationFrame(() => {
      var container = reactDOM.findDOMNode(this.container);
      var scroller = reactDOM.findDOMNode(this.scroller);
      this.debouncing = false;
      if (container && scroller) {
        let containerHeight = container.clientHeight;
        let scrollHeight = scroller.clientHeight;
        if (
          scrollHeight <= container.scrollTop + containerHeight &&
          !this.isLoadingMore
        ) {
          this.loadComment().then(() => {
            this.isLoadingMore = false;
          });
          this.isLoadingMore = true;
        }
      }
    });
  }
  loadComment() {
    let paging = ++this.currentCommentPage;
    return this.props.productActions
      .getProductCommentMore(this.props.product, {
        p: paging,
      })
      .then(data => {
        this.isOutOfItem = _get(data, 'payload.comments.data.length', 0) === 0;
      });
  }
  toogleInputReply(id) {
    this.setState({ currentReplyActive: id });
  }

  render() {
    const comments = _get(this.props, 'product.comments', []);
    const totalComment = _get(this.props, 'product.total_comment', 0);
    const isLoading = _get(this.props, 'product.comment_loading');
    return (
      <div
        id="productCommentModal"
        className={styles.productCommentModal}
        ref={ref => (this.container = ref)}>
        <div className={styles.content} ref={ref => (this.scroller = ref)}>
          <div className={styles.commentList}>
            {!comments.length > 0 &&
              <div className="content-empty full">
                <div className="image-square">
                  <img
                    className="img"
                    src={commentEmpty}
                    alt="Sản phẩm hiện chưa có hỏi đáp"
                  />
                </div>
                <strong className="title">
                  Sản phẩm hiện chưa có hỏi đáp.
                </strong>
                <p className="message">
                  Đặt câu hỏi ngay cho Shop để có thêm thông tin
                </p>
              </div>}
            {comments.length > 0 &&
              comments.map(comment => {
                return (
                  <CommentItem
                    handleToogle={id => this.toogleInputReply(id)}
                    isActiveReply={this.state.currentReplyActive === comment.id}
                    key={comment.id}
                    comment={comment}
                    product={this.props.product}
                  />
                );
              })}
          </div>
          {comments.length > 0 &&
            <div className={styles.loadingMoreIcon}>
              <p
                className={
                  styles.infoPaging
                }>{`Tổng số ${comments.length}/${totalComment}`}</p>
              {isLoading && <Loading />}
            </div>}
        </div>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      product: state.product.productDetail,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
    };
  }
)(withStyles(styles)(ProductCommentModal));
