import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductCommentModal } from './index';
import CommentItem from 'sendo-web-sdk/components/CommentItem';

describe('<ProductCommentModal /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        comments: [
          { id: 'item1' },
          { id: 'item2' },
          { id: 'item3' },
          { id: 'item4' },
        ],
      },
    };
    const wrapper = shallow(<ProductCommentModal {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductCommentModal);
    expect(wrapper.find(CommentItem).exists()).toEqual(true);
  });
});
