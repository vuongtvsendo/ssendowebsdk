import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import RatingChart from 'sendo-web-sdk/components/RatingChart';
import RatingItem from 'sendo-web-sdk/components/RatingItem';
import ModalFullWrap from 'sendo-web-sdk/components/ModalFullWrap';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import ProductRatingModal from 'sendo-web-sdk/components/Product/ProductRatingModal';
import styles from './productRating.css';

class ProductRating extends PureComponent {
  render() {
    const { location, ratingInfo, ratings } = this.props;
    const isShowHeader = _get(this.props, 'isShowHeader', true);
    return (
      <div
        className={`panel ${!isShowHeader
          ? 'no-shadow no-margin-bottom'
          : ''}`}>
        {isShowHeader &&
          <div className="panel-heading">
            <h2>Đánh giá</h2>
          </div>}
        <div className="panel-content no-padding-bottom">
          {isShowHeader &&
            <div className={styles.ratingReport}>
              <RatingChart rating={ratingInfo} />
            </div>}
          <Link to="#danh-gia" replace>
            {ratings.map((rating, i) => {
              if (i < 3) {
                return <RatingItem key={rating.rating_id} rating={rating} />;
              } else {
                return false;
              }
            })}
          </Link>
          {ratings.length > 3 &&
            <Link className="see-more" to="#danh-gia" replace>
              <div className="more-effect" />
              <span className="text-see-more">
                Xem thêm
              </span>
              <CaretDown className="icon-see-more" />
            </Link>}

        </div>
        <ModalFullWrap
          title="Đánh giá phản hồi"
          isOpen={location.hash === '#danh-gia'}>
          <ProductRatingModal />
        </ModalFullWrap>
      </div>
    );
  }
}

export default withStyles(styles)(ProductRating);
