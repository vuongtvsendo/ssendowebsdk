import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import ProductRating from './index';

describe('<ProductRating /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const ratings = [];
    const location = {
      hash: '#danh-gia',
    };
    const wrapper = shallow(
      <ProductRating ratings={ratings} location={location} />,
      {
        context,
      }
    );
    expect(wrapper.text()).toEqual('<ProductRating />');
  });
});
