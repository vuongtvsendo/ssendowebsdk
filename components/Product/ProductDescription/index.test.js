import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductDescription } from './index';
import { LABEL_PRODUCT_DESCRIPTION } from 'sendo-web-sdk/res/text';

describe('<ProductDescription /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      location: {},
    };
    const wrapper = shallow(<ProductDescription {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductDescription);
    expect(wrapper.find('.panel-heading').text()).toEqual(
      LABEL_PRODUCT_DESCRIPTION
    );
  });
  it('It should render short_description if not empty', () => {
    const context = {
      ...ctx,
    };
    const props = {
      location: {},
      decsription: 'description',
      shortDescription: 'shortDescription',
    };
    const wrapper = shallow(<ProductDescription {...props} />, {
      context,
    });
    expect(wrapper.find('.panel-content > div').html()).toEqual(
      '<div>' + props.shortDescription + '</div>'
    );
  });
  it('It should render description if short_description empty', () => {
    const context = {
      ...ctx,
    };
    const props = {
      location: {},
      decsription: 'description',
      shortDescription: '',
    };
    const wrapper = shallow(<ProductDescription {...props} />, {
      context,
    });
    expect(wrapper.find('.panel-content > div').html()).toEqual(
      '<div>' + props.decsription + '</div>'
    );
  });
});
