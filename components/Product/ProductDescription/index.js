import React, { PureComponent } from 'react';
import styles from './productDescription.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { Link } from 'react-router-dom';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import ModalFullWrap from 'sendo-web-sdk/components/ModalFullWrap';
import { normalize as normalizeHtml } from 'sendo-web-sdk/helpers/html';
import { LABEL_PRODUCT_DESCRIPTION } from 'sendo-web-sdk/res/text';

const PRODUCT_DESCRIPTION_HTML_ID = 'pd-html';
export class ProductDescription extends PureComponent {
  removImgeWithHeight() {
    var container = document.getElementById(PRODUCT_DESCRIPTION_HTML_ID);
    if (!container) {
      return;
    }
    var imgs = container.getElementsByTagName('img');
    if (!imgs || !imgs.length) {
      return;
    }
    for (var i = 0; i < imgs.length; i++) {
      let img = imgs[i];
      img.removeAttribute('width');
      img.removeAttribute('height');
    }
  }
  onEnter() {
    setTimeout(() => this.removImgeWithHeight(), 1000);
  }
  isOpenModalProductDescription() {
    const { location } = this.props;
    return location.hash === '#chi-tiet-san-pham';
  }
  render() {
    const { decsription, shortDescription } = this.props;
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>{LABEL_PRODUCT_DESCRIPTION}</h2>
        </div>
        <div className="panel-content no-padding-bottom">
          <div
            className={styles.shortDescription}
            dangerouslySetInnerHTML={{
              __html: shortDescription
                ? shortDescription
                : normalizeHtml(decsription),
            }}
          />
          <Link className="see-more" to="#chi-tiet-san-pham" replace>
            <div className="more-effect" />
            <span className="text-see-more">
              Xem thêm
            </span>
            <CaretDown className="icon-see-more" />
          </Link>
        </div>
        <ModalFullWrap
          title="Chi tiết sản phẩm"
          onEnter={() => this.onEnter()}
          isOpen={this.isOpenModalProductDescription()}>
          <main className={styles.modalScrollContainer}>
            {this.isOpenModalProductDescription() &&
              <div
                id={PRODUCT_DESCRIPTION_HTML_ID}
                className={styles.editor}
                dangerouslySetInnerHTML={{ __html: normalizeHtml(decsription) }}
              />}
          </main>

        </ModalFullWrap>
      </div>
    );
  }
}

export default withStyles(styles)(ProductDescription);
