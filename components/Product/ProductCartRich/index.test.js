import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductCartRich } from './index';

describe('<ProductBenefit /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        name: '',
        promotion_percent: 0,
        final_price: 0,
        price: 0,
        discount_app: 0,
        order_count: 0,
        media: ['media1'],
        brand_id: 1,
        rating_info: '',
        quantity: 0,
        isFavorite: 0,
      },
    };
    const wrapper = shallow(<ProductCartRich {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductCartRich);
    expect(wrapper.find('.swiper-container').exists()).toEqual(true);
  });
});
