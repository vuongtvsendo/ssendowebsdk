import React, { PureComponent } from 'react';
import ReactDOMServer from 'react-dom/server';
import classnames from 'classnames';
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { ShareButtons, generateShareIcon } from 'react-share';
// import Slider from 'react-slick';
import Loading from 'sendo-web-sdk/components/Loading';
import styles from './productCartRich.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import * as helperFunction from 'sendo-web-sdk/helpers/common';
import * as auth from 'sendo-web-sdk/helpers/auth';
import { fbShare } from 'sendo-web-sdk/utils/facebook';
import _get from 'lodash/get';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'sendo-web-sdk/actions/product';

import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';

import StarRating from 'sendo-web-sdk/components/StarRating';
import Mobile from 'sendo-web-sdk/components/Icons/Mobile';
import IsBrandIcon from 'sendo-web-sdk/components/Icons/IsBrandIcon';
import IconShare from 'sendo-web-sdk/components/Icons/IconShare';
import HeartFull from 'sendo-web-sdk/components/Icons/HeartFull';
import CloseCircle from 'sendo-web-sdk/components/Icons/CloseCircle';
import Play from 'sendo-web-sdk/components/Icons/Play';
import IconCamera from 'sendo-web-sdk/components/Icons/IconCamera';
import IconFreeShip from 'sendo-web-sdk/components/Icons/IconFreeShip';
import ProductPhotoGalley from 'sendo-web-sdk/components/ProductPhotoGalley';
import emitter, {
  CLICK_WISHLIST,
  CLICK_REMOVE_WISHLIST,
} from 'sendo-web-sdk/helpers/emitter';

const { FacebookShareButton, GooglePlusShareButton } = ShareButtons;
const FacebookIcon = generateShareIcon('facebook');
const GooglePlusIcon = generateShareIcon('google');

export class ProductCartRich extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      galleryModalOpen: false,
      galleryActiveIndex: 0,
      wishlistLoading: false,
    };
    this.toggleModel = this.toggleModel.bind(this);
    this.openShareModal = this.openShareModal.bind(this);
    this.swiperInitialized = false;
  }
  getOriginImage() {
    return this.props.product.image || this.props.product.img_url;
  }
  getImage(product) {
    if (!product.media || !product.media[0]) {
      return;
    }
    return product.media[0]['image_500x500'];
  }
  toggleModel() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  openGallery(index) {
    this.setState({
      galleryModalOpen: true,
      galleryActiveIndex: index,
    });
  }
  closeGallery() {
    this.setState({
      galleryModalOpen: false,
    });
  }
  toggleWishlist(e) {
    let productId = _get(this.props, 'product.id');
    this.setState({ wishlistLoading: true });
    this.props.productActions.isProductFavorite(productId).then(() => {
      let isFavorite = _get(this.props, 'product.isFavorite', -1);
      if (isFavorite) {
        this.props.productActions.removeToWishlist(productId).then(() => {
          this.setState({ wishlistLoading: false });
        });
        emitter.emit(CLICK_REMOVE_WISHLIST, e, this.props.product);
        return;
      }
      this.props.productActions.addToWishlist(productId).then(() => {
        this.setState({ wishlistLoading: false });
      });
      emitter.emit(CLICK_WISHLIST, e, this.props.product);
    });
  }
  checkValidBtnWishlist() {
    if (
      this.state.wishlistLoading ||
      _get(this.props, 'product.isFavorite', -1) === -1
    ) {
      if (!auth.isLoggedIn(this.props.identity)) {
        return false;
      }
      return true;
    }
    return false;
  }
  componentWillUnmount() {
    if (this.swiperInstance) {
      try {
        this.swiperInstance.destroy();
      } catch (e) {
        console.error('Loi tai qua trinh destroy cua Swiper:', e);
      }
    }
  }
  componentDidUpdate(prevProps) {
    this.initSlider(prevProps);
  }
  initSlider(prevProps) {
    if (
      _get(prevProps, 'product.media') &&
      this.media !== _get(prevProps, 'product.media')
    ) {
      if (this.isInitializing) {
        return;
      }
      if (this.swiperInstance) {
        try {
          this.swiperInstance.destroy();
        } catch (e) {
          console.error('Loi tai qua trinh destroy cua Swiper:', e);
        }
      }
      const sliderSetting = {
        pagination: {
          el: '.swiper-pagination',
          renderBullet: (index, className) => {
            if (this.isVideoIndex(index)) {
              return ReactDOMServer.renderToString(
                <IconCamera className={`${styles.camera} ${className}`} />
              );
            } else {
              return ReactDOMServer.renderToString(
                <span className={`${styles.dot} ${className}`} />
              );
            }
          },
        },
        lazy: true,
      };
      this.isInitializing = true;
      import('sendo-web-sdk/components/Slider/Swiper').then(Swiper => {
        this.swiperInstance = new Swiper.default('.swiper-container', {
          ...sliderSetting,
        });
        this.isInitializing = false;
      });
      this.media = _get(prevProps, 'product.media');
    }
  }
  hasSlider() {
    const { media } = this.props.product;
    return media && media.length;
  }
  openShareModal(event) {
    fbShare(_get(this.props, 'product.cat_path'));
  }
  isVideoIndex(index) {
    let type = _get(this.props, `product.media[${index}].type`);
    return type === 'video';
  }
  handleLoadImg(e, i) {
    e.target.style.opacity = '1';
    /*
    | Sau khi load xong tấm hình đầu tiên thì hiển thị slider
    */
    if (i === 0) {
      setTimeout(() => {
        if (
          document.getElementById('thumbnailSwiperContainer') &&
          document.getElementById('productSliderWrap')
        ) {
          document
            .getElementById('thumbnailSwiperContainer')
            .removeAttribute('style');
          document.getElementById('productSliderWrap').removeAttribute('style');
        }
      }, 100);
    }
  }
  render() {
    const {
      name,
      promotion_percent,
      final_price,
      price,
      discount_app,
      order_count,
      media,
      brand_id,
      rating_info,
      quantity,
      isFavorite,
      campaign_list,
    } = this.props.product;
    const favorClass = classnames({
      [styles.btnIcon]: true,
      'btn-icon': true,
      [styles.isInvalid]: this.checkValidBtnWishlist(),
      [styles.active]: isFavorite,
    });
    return (
      <div className={`${styles.productCartRich} panel`}>
        <div className={styles.thumbnail}>
          <div
            className={styles.sliderWrap}
            id="productSliderWrap"
            style={{ backgroundImage: `url(${this.getOriginImage()})` }}>
            {!this.hasSlider() &&
              <figure className={styles.figure}>
                {this.getImage(this.props.product) &&
                  <img
                    className={`${styles.img}`}
                    src={this.getImage(this.props.product)}
                    alt={name}
                  />}
              </figure>}

            {this.hasSlider() &&
              <div
                className="swiper-container"
                id="thumbnailSwiperContainer"
                style={{ opacity: 0 }}>
                <div className="swiper-wrapper">
                  {media.map((item, i) => {
                    const classes = classnames({
                      'swiper-slide': true,
                      [styles.figure]: false,
                    });
                    if (item.type === 'video') {
                      return (
                        <div
                          key={i}
                          className={`${classes} ${styles.videoThumb}`}
                          onClick={() => this.openGallery(i)}>
                          {i === 0 &&
                            <div className="image-square">
                              <img
                                src={item.video_thumb}
                                alt={name}
                                onLoad={e => this.handleLoadImg(e, i)}
                                style={{ opacity: 0 }}
                              />
                            </div>}
                          {i > 0 &&
                            <div className="image-square">
                              <img
                                className="swiper-lazy"
                                data-src={item.video_thumb}
                                alt={name}
                                onLoad={e => this.handleLoadImg(e, i)}
                                style={{ opacity: 0 }}
                              />
                            </div>}
                          <Play className={styles.iconPlay} />
                          {i > 0 &&
                            <div className="swiper-lazy-preloader">
                              <Loading />
                            </div>}
                        </div>
                      );
                    }
                    return (
                      <div
                        key={i}
                        className={classes}
                        onClick={() => this.openGallery(i)}>
                        {i === 0 &&
                          <img
                            onLoad={e => this.handleLoadImg(e, i)}
                            src={item.image_500x500}
                            alt={name}
                            style={{ opacity: 0 }}
                          />}
                        {i > 0 &&
                          <img
                            className="swiper-lazy"
                            onLoad={e => this.handleLoadImg(e, i)}
                            data-src={item.image_500x500}
                            alt={name}
                            style={{ opacity: 0 }}
                          />}
                        {i > 0 &&
                          <div className="swiper-lazy-preloader">
                            <Loading />
                          </div>}

                      </div>
                    );
                  })}
                </div>
                <div className="swiper-pagination" />
              </div>}
          </div>

          {brand_id > 0 && <IsBrandIcon className={styles.hhch} />}

          <div className={styles.social}>
            <LoginWrapper event="click" callback={e => this.toggleWishlist(e)}>
              <button className={favorClass} aria-label="Toggle Wishlist">
                <HeartFull className="icon" />
              </button>
            </LoginWrapper>
            <button
              className={`${styles.btnIcon} btn-icon`}
              aria-label="Share"
              onClick={this.openShareModal}>
              <IconShare className="icon" />
            </button>
          </div>
        </div>
        <div className={styles.caption}>
          <h1 className={styles.productName}>{name}</h1>

          <div className={styles.groupPrice}>
            <div className={`product-price ${styles.price}`}>
              <strong className="current text-large">
                {helperFunction.formatPrice(final_price)}
              </strong>
              {promotion_percent > 0 &&
                <span className="old text-medium">
                  {helperFunction.formatPrice(price)}
                </span>}
            </div>
            <span className="group-discount-horizontal">
              {discount_app > 0 &&
                <span className="discount-label discount-label-app">
                  <Mobile className="icon-small" />
                  -{discount_app} %
                </span>}
              {promotion_percent > 0 &&
                <span className="discount-label">
                  -{promotion_percent} %
                </span>}
            </span>
          </div>
          {quantity > 0 &&
            quantity < 10 &&
            <p className={styles.countDown}>
              Chỉ còn <strong>{quantity}</strong> sản phẩm
            </p>}

          <div className={styles.productInfo}>
            {rating_info &&
              rating_info.total_rated > 0 &&
              <span className={styles.rate}>
                <StarRating value={rating_info.percent_number} />
                <span className={styles.rateNumber}>
                  <span>( {rating_info.total_rated} )</span> lượt đánh giá
                </span>
              </span>}
            {rating_info &&
              !rating_info.total_rated > 0 &&
              <span className={styles.rate + ' text-small'}>
                Chưa có đánh giá
              </span>}
            {order_count > 0 &&
              <span className={styles.order}>
                ( {helperFunction.friendlyNumber(order_count, 1)} )
                {' '}
                <span className="text-main">lượt mua</span>
              </span>}
          </div>
        </div>
        {_get(campaign_list, 'length', 0) > 0 &&
          campaign_list.map(item =>
            <div className="freeship-campaign-box">
              <IconFreeShip className="icon icon-free-ship" />
              <p className="info" dangerouslySetInnerHTML={{ __html: item }} />
            </div>
          )}
        <Modal modalClassName="modal-full" isOpen={this.state.galleryModalOpen}>
          <button
            className={styles.btnClose}
            onClick={() => this.closeGallery()}>
            <CloseCircle className={`${styles.iconClose} icon`} />
          </button>
          <ProductPhotoGalley
            activeIndex={this.state.galleryActiveIndex}
            media={media}
            alt={name}
          />

        </Modal>

        <Modal isOpen={this.state.isOpen} toggle={this.toggleModel}>
          <ModalBody>
            <h1>Chia sẻ với bạn bè</h1>
            <FacebookShareButton url="https://www.sendo.vn">
              <FacebookIcon size={32} round />
            </FacebookShareButton>
            <GooglePlusShareButton>
              <GooglePlusIcon size={32} round />
            </GooglePlusShareButton>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleModel}>Đóng</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default connect(
  state => {
    return {
      identity: state.user.identity,
    };
  },
  dispatch => {
    return {
      productActions: bindActionCreators(productActions, dispatch),
    };
  }
)(withStyles(styles)(ProductCartRich));
