import React, { PureComponent } from 'react';
import styles from './productRecommend.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import defaultImg from 'sendo-web-sdk/images/default.png';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import ProductCardWrap from 'sendo-web-sdk/components/ProductCardWrap';

const MAX_PRODUCTS_RECOMMENTS = 15;
class ProductRecommend extends PureComponent {
  render() {
    const { recommends, productOriginId } = this.props;
    let count = 0;
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>Có thể bạn quan tâm</h2>
        </div>
        <div className="panel-content">
          <div className="product-gridview product-gridview-3">
            {recommends.map(product => {
              if (
                !product.cat_path ||
                product.id === productOriginId ||
                count === MAX_PRODUCTS_RECOMMENTS
              ) {
                return null;
              }
              count++;
              return (
                <div key={product.uid || product.id} className="item">
                  <ProductCardWrap product={product}>
                    {this.renderProductCartBasic(product)}
                  </ProductCardWrap>
                </div>
              );
            })}
          </div>

        </div>
      </div>
    );
  }
  renderProductCartBasic(product) {
    return (
      <div className="product-card product-card-basic">
        <div className="thumbnail">
          <figure className="image-square">
            <img
              src={defaultImg}
              data-src={product.image}
              className="lazyload"
              alt={product.name}
            />
          </figure>
        </div>
        <div className="caption">
          <h3 className="heading-medium text-overflow">
            {' '}{product.name}
          </h3>
          <div className="product-price">
            <strong className="current">
              {helperFunction.formatPrice(product.final_price)}
            </strong>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ProductRecommend);
