import React from 'react';
import { mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import ProductRecommend from './index';

describe('<ProductRecommend /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
      insertCss: () => {},
    };
    const recommends = [];
    const wrapper = mount(<ProductRecommend recommends={recommends} />, {
      context,
    });
    expect(wrapper.find('.panel-heading').text()).toEqual(
      'Có thể bạn quan tâm'
    );
  });
});
