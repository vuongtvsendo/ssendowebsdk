import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductComment } from './index';
import { LABEL_PRODUCT_COMMENT } from 'sendo-web-sdk/res/text';
import CommentItem from 'sendo-web-sdk/components/CommentItem';

describe('<ProductComment /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      comments: [
        { id: 'item1' },
        { id: 'item2' },
        { id: 'item3' },
        { id: 'item4' },
      ],
      location: {},
    };
    const wrapper = shallow(<ProductComment {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductComment);
    expect(wrapper.find('h2').text()).toEqual(LABEL_PRODUCT_COMMENT);
    expect(wrapper.find(CommentItem)).toHaveLength(3);
    expect(wrapper.find('span').text()).toEqual('Xem thêm');
  });
});
