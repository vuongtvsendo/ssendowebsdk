import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './productComment.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import CommentItem from 'sendo-web-sdk/components/CommentItem';
import CommentInput from 'sendo-web-sdk/components/CommentInput';
import ModalFullWrap from 'sendo-web-sdk/components/ModalFullWrap';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import ProductCommentModal from 'sendo-web-sdk/components/Product/ProductCommentModal';
import commentEmpty from 'sendo-web-sdk/images/empty-state/comment-empty.svg';
import { LABEL_PRODUCT_COMMENT } from 'sendo-web-sdk/res/text';

export class ProductComment extends PureComponent {
  render() {
    const { product, comments, location } = this.props;
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>{LABEL_PRODUCT_COMMENT}</h2>
        </div>
        <div
          className={
            'panel-content' +
            (comments && comments.length > 3 ? ' no-padding-bottom' : '')
          }>
          <div className={styles.comments}>
            <Link to="#hoi-dap" replace>
              {comments.length > 0 &&
                comments.map((comment, i) => {
                  if (i < 3) {
                    return (
                      <CommentItem
                        disableReply="true"
                        disableDelete="true"
                        key={comment.id}
                        comment={comment}
                      />
                    );
                  } else {
                    return false;
                  }
                })}
            </Link>

            {comments.length === 0 &&
              <div className="content-empty small">
                <div className="image-square">
                  <img
                    className="img"
                    src={commentEmpty}
                    alt="Sản phẩm hiện chưa có hỏi đáp"
                  />
                </div>
                <strong className="title small">
                  Sản phẩm hiện chưa có hỏi đáp.
                </strong>
              </div>}
          </div>
          <CommentInput product={product} />
          {comments &&
            comments.length > 3 &&
            <Link className="see-more" to="#hoi-dap" replace>
              <span className="text-see-more">Xem thêm</span>
              <CaretDown className="icon-see-more" />
            </Link>}
        </div>
        <ModalFullWrap title="Hỏi đáp" isOpen={location.hash === '#hoi-dap'}>
          <ProductCommentModal />
          <div className={styles.inputComment}>
            <CommentInput focusWithBodyFixed product={product} key={'ddddd'} />
          </div>
        </ModalFullWrap>
      </div>
    );
  }
}

export default withStyles(styles)(ProductComment);
