import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './productTags.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { LABEL_RELATED_TAGS } from 'sendo-web-sdk/res/text';

export class ProductTags extends PureComponent {
  render() {
    const { tags } = this.props;
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>{LABEL_RELATED_TAGS}</h2>
        </div>
        <div className="panel-content no-padding-bottom">
          <div className={`${styles.content} row`}>
            {tags.map((tag, index) => {
              return (
                <div key={index} className="col-6">
                  <Link
                    to={normalizeUrl(tag.link)}
                    className={`${styles.productTag} tag default gray`}>
                    <span className="text-overflow">{tag.title}</span>
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ProductTags);
