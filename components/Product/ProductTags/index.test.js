import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductTags } from './index';

describe('<ProductTags /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      tags: [],
    };
    const wrapper = shallow(<ProductTags {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductTags);
    expect(wrapper.find('.row').exists()).toEqual(true);
  });
});
