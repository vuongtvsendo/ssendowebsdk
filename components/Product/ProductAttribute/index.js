import React, { Component } from 'react';
import styles from './style.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';

import ArrowRight from 'sendo-web-sdk/components/Icons/ArrowRight';

export class ProductAttribute extends Component {
  render() {
    let { product, selectedAttr, labelArrAttr, isValidAttr } = this.props;
    return (
      <div className="panel">
        {product &&
          selectedAttr &&
          labelArrAttr.length &&
          <div className="panel-heading no-border">
            <h2 className={styles.headingText + ' text-with-circle'}>
              <strong>Chọn:</strong>&nbsp;
              {labelArrAttr.join(', ').toLowerCase()}
            </h2>
            <ArrowRight className="icon" />
          </div>}

        {product &&
          selectedAttr &&
          Object.keys(selectedAttr).length > 0 &&
          isValidAttr &&
          <div className="panel-heading no-border">
            <div className={styles.listAttrSelected}>
              {Object.keys(selectedAttr).map(key => {
                const item = selectedAttr[key];
                const customStyles = {};
                let classNames = 'option';
                let classNameType = styles.option;
                let label = item.selected.value;
                if (
                  item.groupOption.search_key === 'mau_sac' ||
                  item.groupOption.search_key === 'mau_sac_elt'
                ) {
                  label = '';
                  classNameType = 'circle';
                  customStyles.background = item.selected.background;
                  classNames = {
                    [styles[classNameType]]: true,
                  };
                }
                return (
                  <div className={styles.attr} key={item.selected.option_id}>
                    <p className={classnames(classNames)} style={customStyles}>
                      {label}
                    </p>
                    <p className={styles.labelAttr}>{item.groupOption.name}</p>
                  </div>
                );
              })}
            </div>
          </div>}
      </div>
    );
  }
}

export default withStyles(styles)(ProductAttribute);
