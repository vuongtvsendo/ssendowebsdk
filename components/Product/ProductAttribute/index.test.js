import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductAttribute } from './index';

describe('<ProductAttribute /> component', () => {
  it('It should render text-with-circle', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        id: '123',
      },
      selectedAttr: {
        label: {},
      },
      labelArrAttr: ['test'],
      isValidAttr: false,
    };
    const wrapper = shallow(<ProductAttribute {...props} />, {
      context,
    });
    expect(wrapper.find('.text-with-circle').exists()).toEqual(true);
  });
  it('It should render attributes', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {
        id: '123',
      },
      selectedAttr: {
        kich_thuoc_1: {
          groupOption: {
            attribute_id: 298,
            name: 'Kích thước',
            product_option: '7266441_298',
            show_required: 1,
            type: 'Option',
            value: [
              {
                option_id: 816,
                value: 'M',
                product_option_id: '7266441_816',
              },
            ],
            search_key: 'kich_thuoc_1',
          },
          selected: {
            option_id: 816,
            value: 'M',
            product_option_id: '7266441_816',
          },
        },
      },
      labelArrAttr: ['test'],
      isValidAttr: true,
    };
    const wrapper = shallow(<ProductAttribute {...props} />, {
      context,
    });
    expect(wrapper.find('.option').exists()).toEqual(true);
  });
});
