import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductShippingModal } from './index';

describe('<ProductShippingModal /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<ProductShippingModal {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductShippingModal);
  });
});
