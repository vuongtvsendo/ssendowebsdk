import React, { PureComponent } from 'react';
import classnames from 'classnames';
import styles from './productShippingModal.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import _get from 'lodash/get';
import _find from 'lodash/find';

import IconFreeShip from 'sendo-web-sdk/components/Icons/IconFreeShip';
import Map from 'sendo-web-sdk/components/Icons/Map';
import Loading from 'sendo-web-sdk/components/Loading';
import carrierEmpty from 'sendo-web-sdk/images/empty-state/carrier-empty.svg';
import emitter, { CLICK_CARRIER } from 'sendo-web-sdk/helpers/emitter';

export class ProductShippingModal extends PureComponent {
  getGeolocation() {
    if (_get(this, 'props.onGetGeolocation') instanceof Function) {
      this.props.onGetGeolocation();
    }
  }

  changeCarrier(e, carrier) {
    if (_get(this, 'props.onChangeCarrier') instanceof Function) {
      this.props.onChangeCarrier(carrier);
    }
    emitter.emit(CLICK_CARRIER, e, this.props.product);
  }

  changeDestination(e) {
    const region = _find(
      this.props.allCity,
      region =>
        parseInt(_get(region, 'region_code', 1), 10) ===
        parseInt(e.target.value, 10)
    );
    if (_get(this, 'props.onChangeDestination') instanceof Function) {
      this.props.onChangeDestination(region);
    }
  }

  handleClickConfirm(e) {
    if (_get(this, 'props.onClickConfirmShipping') instanceof Function) {
      this.props.onClickConfirmShipping();
    }
  }

  renderCarrier(carriers, is_cod) {
    return carriers.map(carrier => {
      return (
        <tr
          className={`groupCarrier_${carrier.group_id}`}
          key={carrier.carrier_id}
          onClick={e => this.changeCarrier(e, carrier)}>
          <td className={styles.logo}>
            <div className={styles.customRadio}>
              <input
                type="radio"
                value={carrier.carrier_id}
                checked={
                  this.props.currentCarrier.carrier_id === carrier.carrier_id
                }
              />
              <span className={styles.cirle} />
              <img
                alt={carrier.carrier_name}
                src={carrier.carrier_logo_url}
                onError={e => onImgError(e)}
              />
            </div>
          </td>
          <td className={styles.time}>
            {carrier.carrier_code === 'ecom_shipping_shop_tvc'
              ? 'Thỏa thuận'
              : carrier.deliver_duration_note}
          </td>
          {is_cod &&
            <td className={styles.deliveryFee}>
              {helperFunction.formatPrice(carrier.delivery_fee_discount)}
            </td>}

          <td className={styles.finalFee}>
            {carrier.carrier_code === 'ecom_shipping_shop_tvc'
              ? 'Thỏa thuận'
              : helperFunction.formatPrice(
                  carrier.delivery_fee_discount + carrier.cod_fee_discount
                )}
          </td>
        </tr>
      );
    });
  }

  render() {
    const { allCity, product } = this.props;
    const selfTransport = _get(product, 'shop_info.self_transport', []);
    const isShopSelfTransport = _get(
      product,
      'shop_info.is_self_transport',
      false
    );
    const group_carrier = _get(product, 'shipingInfo.group_carrier', []);
    const is_cod = _get(product, 'shipingInfo.has_cod_fee');
    const colSpanTitle = is_cod ? 4 : 3;
    const headerColSpan = is_cod ? 2 : 1;
    const destinationClass = classnames({
      [styles.destination]: true,
      [styles.loading]: this.props.loadingGetLocation,
    });
    let valueSelected = _get(this.props, 'currentDestination.region_code');
    if (isShopSelfTransport) {
      const city = _find(selfTransport, { region_code: valueSelected });
      if (!city) {
        valueSelected = 0;
      }
    }
    if (!group_carrier) {
      return <div className={styles.loadingIcon}><Loading /></div>;
    }

    return (
      <div className={styles.productShippingModal}>
        <section className={destinationClass}>
          {this.props.loadingGetLocation &&
            <div className={styles.loadingIcon}><Loading /></div>}
          <div className={styles.shopStore}>
            <IconFreeShip className="icon icon-free-ship" />
            <span> Vận chuyển từ:&nbsp;</span>
            <strong className={styles.location}>
              {_get(product, 'shop_info.warehourse_region_name')}
            </strong>
          </div>
          <div className={styles.destinationInfo}>
            <p>Giao hàng đến:</p>
            <div className={styles.controls}>
              <div className={styles.customSelect}>
                <span className={styles.customSelectLabel}>
                  {_get(this.props, 'currentDestination.region_name')}
                </span>
                <select
                  value={valueSelected}
                  onChange={e => this.changeDestination(e)}>
                  <option disabled value={0} key={0}>
                    Chọn tỉnh/thành phố...
                  </option>
                  {!isShopSelfTransport &&
                    allCity &&
                    allCity.length > 0 &&
                    allCity.map(city => {
                      return (
                        <option value={city.region_code} key={city.region_code}>
                          {city.region_name}
                        </option>
                      );
                    })}
                  {isShopSelfTransport &&
                    selfTransport.length > 0 &&
                    selfTransport.map(item => {
                      return (
                        <option value={item.region_code} key={item.region_code}>
                          {item.region_name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <div
                className={styles.getLocation}
                onClick={() => this.getGeolocation()}>
                <Map className={styles.iconMap} />
                <span className={styles.text}>Sử dụng vị trí hiện tại</span>
              </div>
            </div>
          </div>
        </section>

        <section className={styles.deliveryInfo}>
          <header className={styles.heading}>Chọn nhà vận chuyển</header>
          <div className={styles.tableWrap}>
            {!_get(group_carrier, '[0].carriers[0].carrier_id') &&
              <div className="content-empty small">
                <div className="image-square">
                  <img
                    className="img"
                    src={carrierEmpty}
                    alt="Rất tiếc, shop không hỗ trợ vận chuyển"
                    onError={e => onImgError(e)}
                  />
                </div>
                <strong className="title small">
                  Rất tiếc, shop không hỗ trợ vận chuyển ở{' '}
                  {_get(this.props, 'currentDestination.region_name')}
                </strong>
              </div>}
            {_get(group_carrier, '[0].carriers[0].carrier_id') &&
              <table className={styles.table}>
                <tbody>
                  <tr>
                    <th rowSpan={headerColSpan}>Nhà vận chuyển</th>
                    <th rowSpan={headerColSpan} width="20%">Thời gian</th>
                    <th colSpan={headerColSpan}>Phí vận chuyển</th>
                  </tr>
                  {is_cod &&
                    <tr className={styles.subHeading}>
                      <th>TT trực tuyến</th>
                      <th>TT nhận hàng</th>
                    </tr>}

                  {group_carrier.map((delivery, index) => {
                    return [
                      <tr className={`groupCarrier_${index}`}>
                        <td
                          className={styles.groupCarrierTitle}
                          colSpan={colSpanTitle}>
                          {delivery.title}
                        </td>
                      </tr>,
                      this.renderCarrier(delivery.carriers, is_cod),
                    ];
                  })}
                </tbody>
              </table>}
          </div>
        </section>

        <section className={styles.buttonSection}>
          <div className={styles.btnWrap}>
            <button
              className={`${styles.btnShipping} btn-primary`}
              onClick={e => this.handleClickConfirm(e)}>
              Xác nhận
            </button>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(styles)(ProductShippingModal);
