import React, { PureComponent } from 'react';
import styles from './productShipping.css';
import { Collapse } from 'reactstrap';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as geolocationActions from 'sendo-web-sdk/actions/geolocation';
import * as productActions from 'sendo-web-sdk/actions/product';
import * as toastActions from 'sendo-web-sdk/actions/toast';
import * as userActions from 'sendo-web-sdk/actions/user';
import * as helperFunction from 'sendo-web-sdk/helpers/common';

import _get from 'lodash/get';
import _find from 'lodash/find';
import _filter from 'lodash/filter';

import IconFreeShip from 'sendo-web-sdk/components/Icons/IconFreeShip';
import ArrowRight from 'sendo-web-sdk/components/Icons/ArrowRight';
import CaretDown from 'sendo-web-sdk/components/Icons/CaretDown';
import ProductShippingModal from 'sendo-web-sdk/components/Product/ProductShippingModal';

import ModalFullWrap from 'sendo-web-sdk/components/ModalFullWrap';

export class ProductShipping extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      shopPromotionCollapse: true,
      isOpenModalShipping: false,
      loadingGetLocation: false,
      isDestinationSelected: false,
      currentDestination: null,
      currentCarrier: null,
      allCity: this.props.allCity,
      userLocation: this.props.userLocation,
      product: this.props.product,
    };
  }

  onChangeCarrier(carrier) {
    this.setState({
      currentCarrier: carrier,
    });
  }

  onChangeDestination(region) {
    this.setState({ currentDestination: region, isDestinationSelected: true });
    this.getShipingInfo(region.region_code);
  }

  onClickConfirmShipping() {
    this.closeModalShipping(2);
  }

  onGetGeolocation() {
    if (!navigator.geolocation) {
      return;
    }
    this.setState({
      loadingGetLocation: true,
    });
    const options = {
      enableHighAccuracy: true,
      timeout: 2000,
      maximumAge: 0,
    };
    const success = position => {
      let latitude = position.coords.latitude;
      let longitude = position.coords.longitude;
      this.props.geolocationActions
        .getCityByLatlng(`${latitude},${longitude}`)
        .then(data => {
          let currentDestination = {
            region_code: _get(this.props, 'userLocation.region_code'),
            region_name: _get(this.props, 'userLocation.region_name'),
          };
          this.setState({
            currentDestination: currentDestination,
            loadingGetLocation: false,
            isDestinationSelected: true,
          });
          this.getShipingInfo(currentDestination.region_code);
        });
    };
    const error = error => {
      this.setState({
        loadingGetLocation: false,
      });
      switch (error.code) {
        case error.PERMISSION_DENIED:
        case error.POSITION_UNAVAILABLE:
        case error.TIMEOUT:
        default:
          this.props.toastActions.openToast({
            content: 'Lấy thông tin vị trí không thành công',
          });
          break;
      }
      return;
    };
    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  getCurrentLocation(param, defaultVal) {
    let identityLocation = _get(this.props, `userLocation.${param}`);
    return !!identityLocation ? identityLocation : defaultVal;
  }

  getShipingInfo(regionCode) {
    this.props.productActions.getShippingInfo(this.props.product, {
      to: regionCode,
    });
  }

  viewMoreShopPromotion() {
    this.setState({ shopPromotionCollapse: true });
  }

  openModalShipping() {
    this.setState({ isOpenModalShipping: true });
  }

  closeModalShipping(position) {
    if (position === 1) {
      this.setState({
        currentCarrier: this.defaultCarrier,
        currentDestination: this.defaultDestination,
      });
    } else if (position === 2) {
      this.props.productActions.setShippingCarrier(this.state.currentCarrier);
      this.props.productActions.setShippingDestination(
        this.state.currentDestination
      );
      this.defaultCarrier = this.state.currentCarrier;
      this.defaultDestination = this.state.currentDestination;
      let location = {
        city_id: _get(this.state, 'currentDestination.region_code', 1),
        city_name: _get(
          this.state,
          'currentDestination.region_name',
          'Hồ Chí Minh'
        ),
      };
      this.props.userActions.setUserLocation(location);
    }
    this.setState({ isOpenModalShipping: false });
  }

  onClickBtnBack(position) {
    this.closeModalShipping(position);
  }

  componentDidMount() {
    this.props.geolocationActions.getAllCity();
    let regionCode = _get(this.state, 'userLocation.city_id', 1);
    this.getShipingInfo(regionCode);
  }
  getDefaultCarrier(groups) {
    let defautCarrier = {};
    if (groups) {
      for (var i = 0; i <= groups.length; i++) {
        var carriers = _get(groups, `[${i}].carriers`, null);
        if (carriers) {
          let groupFiltered = carriers.filter(item => item.sort_wage === -1);
          defautCarrier = _get(groupFiltered, '[0]', null);
        }
      }
    }
    if (!defautCarrier) {
      defautCarrier = _get(groups, '[0].carriers[0]', null);
    }
    return defautCarrier;
  }

  componentWillReceiveProps(nextProps) {
    let groupCarrier = _get(
      nextProps,
      'product.shipingInfo.group_carrier',
      null
    );
    if (nextProps.allCity.length) {
      this.setState({ allCity: nextProps.allCity });
    }
    if (nextProps.product.shipingInfo) {
      if (!this.state.currentCarrier) {
        let carrier = this.getDefaultCarrier(groupCarrier);
        if (carrier) {
          this.setState({ currentCarrier: carrier });
          this.defaultCarrier = carrier;
          this.props.productActions.setShippingCarrier(carrier);
        }
      }
    }
    if (nextProps.allCity.length && nextProps.userLocation) {
      if (!this.state.currentDestination) {
        let regionCode = _get(nextProps, 'userLocation.city_id', 1);
        let destination = _find(nextProps.allCity, { region_code: regionCode });
        this.setState({ currentDestination: destination });
        this.defaultDestination = destination;
        this.props.productActions.setShippingDestination(destination);
      }
    }
    if (this.state.isDestinationSelected) {
      let carrier = this.getDefaultCarrier(groupCarrier);
      this.setState({
        currentCarrier: carrier,
        isDestinationSelected: false,
      });
    }
  }
  render() {
    const { product } = this.props;
    const shopInfo = _get(product, 'shop_info', {});
    const shopPromotionList = _get(
      shopInfo,
      'delivery_info.shop_discount_delivery'
    );
    const regionName = _get(
      this.state.currentDestination,
      'region_name',
      'Vui lòng chọn tỉnh thành'
    );
    return (
      <div className="panel">
        <div className="panel-heading" onClick={() => this.openModalShipping()}>
          <h2>
            Giao hàng đến:&nbsp;
            <strong className={`${styles.textInfo} text-info`}>
              {regionName}
            </strong>
          </h2>
          <ArrowRight className="icon" />
        </div>
        <div className="panel-content">
          <div className={styles.promotion}>
            <div
              className={styles.deliveryInfo}
              onClick={() => this.openModalShipping()}>
              <div className={styles.infoCarrier}>
                {this.renderShippingInfo()}
              </div>
            </div>
            <Collapse
              className={styles.deliveryInfo}
              isOpen={this.state.shopPromotionCollapse}>
              {shopPromotionList &&
                shopPromotionList.length > 0 &&
                <ul className={styles.shopPromotionList}>
                  {shopPromotionList.map((item, i) => {
                    return (
                      <li
                        className={styles.shopPromotionItem}
                        key={i}
                        dangerouslySetInnerHTML={{ __html: item }}
                      />
                    );
                  })}
                </ul>}
            </Collapse>
            {!this.state.shopPromotionCollapse &&
              shopPromotionList &&
              shopPromotionList.length > 0 &&
              <div
                className="see-more"
                onClick={() => this.viewMoreShopPromotion()}>
                <span className="text-see-more">
                  +{shopPromotionList.length} Khuyến mãi khác từ shop
                </span>
                <CaretDown className="icon-see-more" />
              </div>}
          </div>
        </div>
        <ModalFullWrap
          isOpen={this.state.isOpenModalShipping}
          onClickBtnBack={() => this.onClickBtnBack(1)}
          title="Thông tin giao hàng">
          <ProductShippingModal
            loadingGetLocation={this.state.loadingGetLocation}
            currentCarrier={this.state.currentCarrier}
            currentDestination={this.state.currentDestination}
            product={this.props.product}
            allCity={this.state.allCity}
            userLocation={this.state.userLocation}
            onGetGeolocation={f => this.onGetGeolocation()}
            onChangeCarrier={carrier => this.onChangeCarrier(carrier)}
            onChangeDestination={region => this.onChangeDestination(region)}
            onClickConfirmShipping={f => this.onClickConfirmShipping()}
          />
        </ModalFullWrap>
      </div>
    );
  }
  renderShippingInfo() {
    const { currentCarrier } = this.state;
    const selfTransport = _get(
      this.props,
      'product.shop_info.self_transport',
      []
    );
    const isShopSelfTransport = _get(
      this.props,
      'product.shop_info.is_self_transport',
      false
    );
    // Shop Tu Van Chuyen
    if (isShopSelfTransport) {
      if (!this.getCurrentLocation('city_id', 0)) {
        return (
          <p className={styles.messageDeli}>
            <IconFreeShip className="icon icon-free-ship" />{' '}
            <span>
              Vui lòng chọn nhà vận chuyển để biết thêm thông tin!
            </span>
          </p>
        );
      }

      let locationMatch = _filter(selfTransport, item => {
        return item.region_id === this.getCurrentLocation('city_id', 0);
      });
      if (locationMatch.length) {
        return (
          <p>
            Vị trí nhận hàng của bạn ở{' '}
            <strong>
              {_get(this.state.currentDestination, 'region_name', '')}
            </strong>. Sản
            phẩm này do shop tự vận chuyển. Shop sẽ liên hệ báo phí vận chuyển
            và thời gian giao hàng.
          </p>
        );
      } else {
        return (
          <p>
            Rất tiếc, shop không hỗ trợ vận chuyển ở{' '}
            <strong>
              {_get(this.state.currentDestination, 'region_name', '')}
            </strong>
          </p>
        );
      }
    }
    // Shop van chuyen qua nvc
    if (!isShopSelfTransport && currentCarrier) {
      if (currentCarrier.delivery_fee_discount === 0) {
        return (
          <p>
            <strong className="text-shipping">Miễn phí vận chuyển</strong>
            {' '}trong{' '}
            <strong className="text-desc">
              {currentCarrier.deliver_duration_note}
            </strong>
            {' '} bởi{' '}
            <strong className="text-desc">
              {currentCarrier.carrier_name}
            </strong>
          </p>
        );
      }
      return (
        <p>
          Trong{' '}
          <strong className="text-desc">
            {currentCarrier.deliver_duration_note}
          </strong>
          {' '} với{' '}
          <strong className="uppercase text-shipping">
            {helperFunction.formatPrice(currentCarrier.delivery_fee_discount)}
          </strong>
          {' '} bởi{' '}
          <strong className="text-desc">
            {currentCarrier.carrier_name}
          </strong>
        </p>
      );
    } else {
      return (
        <p className={styles.messageDeli}>
          <IconFreeShip className="icon icon-free-ship" />{' '}
          <span>
            Vui lòng chọn nhà vận chuyển để biết thêm thông tin!
          </span>
        </p>
      );
    }
  }
}

export default connect(
  state => {
    return {
      userLocation: state.user.location,
      allCity: state.location.allCity,
      product: state.product.productDetail,
    };
  },
  dispatch => {
    return {
      geolocationActions: bindActionCreators(geolocationActions, dispatch),
      productActions: bindActionCreators(productActions, dispatch),
      toastActions: bindActionCreators(toastActions, dispatch),
      userActions: bindActionCreators(userActions, dispatch),
    };
  }
)(withStyles(styles)(ProductShipping));
