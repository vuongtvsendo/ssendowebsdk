import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductShipping } from './index';

describe('<ProductShipping /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      product: {},
    };
    const wrapper = shallow(<ProductShipping {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ProductShipping);
  });
});
