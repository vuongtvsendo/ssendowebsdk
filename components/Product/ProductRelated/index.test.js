import React from 'react';
import { mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import ProductRelated from './index';

describe('<ProductRelated /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const relateds = [];
    const wrapper = mount(<ProductRelated relateds={relateds} />, {
      context,
    });
    expect(wrapper.find('.panel-heading').text()).toEqual('Sản phẩm liên quan');
  });
});
