import React, { PureComponent } from 'react';
import styles from './productRelated.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
// import { Link } from 'react-router-dom';

import ProductCardWrap from 'sendo-web-sdk/components/ProductCardWrap';

class ProductRelated extends PureComponent {
  render() {
    const { relateds, productOriginId } = this.props;
    return (
      <div className="panel">
        <div className="panel-heading">
          <h2>Sản phẩm liên quan</h2>
        </div>
        <div className="panel-content">
          <div className={styles.products}>
            {relateds.map(product => {
              if (!product.cat_path || product.id === productOriginId) {
                return null;
              }
              return (
                <div className={styles.item} key={product.uid || product.id}>
                  <ProductCardWrap product={product}>
                    <span className={styles.box}>
                      <img src={product.image} alt={product.name} />
                    </span>
                  </ProductCardWrap>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ProductRelated);
