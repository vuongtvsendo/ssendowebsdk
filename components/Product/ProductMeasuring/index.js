import React, { PureComponent } from 'react';
import Collapse from 'reactstrap/lib/Collapse';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productMeasuring.css';

import imageSize from './img-size.png';

class ProductMesuring extends PureComponent {
  render() {
    const { isOpen } = this.props;
    return (
      <div>
        <Collapse isOpen={isOpen}>
          <table width="100%">
            <thead>
              <tr>
                <td>
                  <strong>Size Shop</strong>
                  <br />chuẩn<br />
                  <strong>Việt Nam</strong>
                </td>
                <td><strong>Vòng ngực</strong></td>
                <td><strong>Vòng eo</strong></td>
                <td><strong>Vòng hông</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><strong>M</strong></td>
                <td>65 - 79</td>
                <td>54 - 64</td>
                <td>65 - 79</td>
              </tr>
              <tr>
                <td><strong>L</strong></td>
                <td>65 - 79</td>
                <td>54 - 64</td>
                <td>65 - 79</td>
              </tr>
              <tr>
                <td><strong>XL</strong></td>
                <td>65 - 79</td>
                <td>54 - 64</td>
                <td>65 - 79</td>
              </tr>
            </tbody>
          </table>

          <div className={styles.howToMeasure}>
            <div className={styles.imgContainer}>
              <img src={imageSize} alt="Thumbnail Measuring" />
            </div>
            <div className={styles.infoContainer}>
              <strong>Cách đo chiều dài bàn chân</strong>
              <ul>
                <li>
                  Đặt bàn chân lên tờ giấy trắng, đánh dấu điểm đầu ngón chân
                  dài nhất và điểm gót chân
                </li>
                <li>
                  Chiều dài giữa 2 điểm vừa đo ( X ) là kích thước bàn chân của
                  bạn
                </li>
              </ul>
            </div>
          </div>
          <div className={styles.note}>
            <strong>Lưu ý</strong>
            <p>
              - Mỗi size giày châu Á thường lệch nhau khoảng 5mm chiều dài và
              4mm chiều ngang
            </p>
            <p>
              - Thông số size giày sẽ thay đổi theo kết cấu bàn chân, theo từng
              mẫu và chất liệu sản phẩm
            </p>
            <p>
              - Những dạng giày thời trang bít mũi hoặc túm mũi nên trừ hao mũi
              giày đến đầu ngón chân khoảng 1-2cm
            </p>
          </div>
        </Collapse>
      </div>
    );
  }
}

export default withStyles(styles)(ProductMesuring);
