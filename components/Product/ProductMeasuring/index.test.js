import React from 'react';
import { mount } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import ProductMeasuring from './index';

describe('<ProductMeasuring /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const wrapper = mount(<ProductMeasuring />, {
      context,
    });
    expect(wrapper.html()).toBeTruthy();
  });
});
