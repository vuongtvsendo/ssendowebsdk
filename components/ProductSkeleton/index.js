import React from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productSkeleton.css';

const ProductSkeleton = () => {
  return (
    <div className="skeleton">
      <div className="skeleton-thumbnail effect-1" />
      <div className="skeleton-line skeleton-lineMd effect-1" />
      <div className="skeleton-line skeleton-lineLg effect-1" />
      <div className="skeleton-line skeleton-lineSm effect-1" />
    </div>
  );
};

export default withStyles(styles)(ProductSkeleton);
