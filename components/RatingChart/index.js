import React, { PureComponent } from 'react';
import styles from './ratingChart.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import StarRating from '../StarRating';

export class RatingChart extends PureComponent {
  render() {
    const { rating } = this.props;
    return (
      <div className={styles.ratingChart}>
        <div className={styles.chart}>
          <div className={styles.average}>
            <div className={styles.number}>
              <strong>
                {rating.percent_star && rating.percent_star.toFixed(1)}
              </strong>
              /
              <span>5</span>
            </div>
            <div className={styles.starRating}>
              <StarRating value={rating.percent_number} type="big" />
            </div>
            <p>( {rating.total_rated} lượt đánh giá)</p>
          </div>
          <div className={styles.details}>
            <div className={styles.item}>
              <StarRating value="100" />
              <span className={styles.progessBar}>
                <span
                  style={{ width: rating.percent_star5 + '%' }}
                  className={`${styles.value} ${styles.star5}`}
                />
              </span>
              <strong className={styles.quantity}>
                {rating.star5}
              </strong>
            </div>

            <div className={styles.item}>
              <StarRating value="80" />
              <span className={styles.progessBar}>
                <span
                  style={{ width: rating.percent_star4 + '%' }}
                  className={`${styles.value} ${styles.star4}`}
                />
              </span>
              <strong className={styles.quantity}>
                {rating.star4}
              </strong>
            </div>

            <div className={styles.item}>
              <StarRating value="60" />
              <span className={styles.progessBar}>
                <span
                  style={{ width: rating.percent_star3 + '%' }}
                  className={`${styles.value} ${styles.star3}`}
                />
              </span>
              <strong className={styles.quantity}>
                {rating.star3}
              </strong>
            </div>

            <div className={styles.item}>
              <StarRating value="40" />
              <span className={styles.progessBar}>
                <span
                  style={{ width: rating.percent_star2 + '%' }}
                  className={`${styles.value} ${styles.star2}`}
                />
              </span>
              <strong className={styles.quantity}>
                {rating.star2}
              </strong>
            </div>

            <div className={styles.item}>
              <StarRating value="20" />
              <span className={styles.progessBar}>
                <span
                  style={{ width: rating.percent_star1 + '%' }}
                  className={`${styles.value} ${styles.star1}`}
                />
              </span>
              <strong className={styles.quantity}>
                {rating.star1}
              </strong>
            </div>

          </div>
        </div>
        <p className={styles.decsription}>
          Đây là thông tin người mua đánh giá shop bán sản phẩm này có đúng mô
          tả không.
        </p>
      </div>
    );
  }
}

export default withStyles(styles)(RatingChart);
