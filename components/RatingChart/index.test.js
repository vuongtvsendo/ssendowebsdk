import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { RatingChart } from './index';

describe('<RatingChart /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      rating: {},
    };
    const wrapper = shallow(<RatingChart {...props} />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(RatingChart);
  });
});
