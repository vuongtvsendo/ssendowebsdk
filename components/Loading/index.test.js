import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Loading } from './index';

describe('<Loading /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<Loading {...props} />, {
      context,
    });
    expect(wrapper.find('span')).toHaveLength(1);
  });
});
