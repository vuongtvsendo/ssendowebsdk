import React from 'react';
import classnames from 'classnames';
import styles from './styles.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Loading from 'sendo-web-sdk/components/Loading';

export const PageLoading = props => {
  const loadingClass = classnames({
    [styles.pageLoading]: true,
    [styles.open]: props.isOpen,
  });
  return (
    <div className={loadingClass}>
      <Loading />
    </div>
  );
};

export default withStyles(styles)(PageLoading);
