import React from 'react';
import classnames from 'classnames';
import styles from './loading.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

export const Loading = props => {
  const loadingClass = classnames({
    loading: true,
    small: props.small,
    large: props.large,
    autowidth: props.autowidth,
  });
  return (
    <div className={loadingClass}>
      <span className={styles.loader} />
    </div>
  );
};

export default withStyles(styles)(Loading);
