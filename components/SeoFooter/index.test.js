import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { SeoFooter } from './index';

describe('<SeoFooter /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      data: {
        title: 'title',
        description: 'description',
      },
      isLoading: false,
    };
    const wrapper = shallow(<SeoFooter {...props} />, {
      context,
    });

    expect(wrapper.instance()).toBeInstanceOf(SeoFooter);
    expect(wrapper.html()).toContain('title');
    expect(wrapper.html()).toContain('description');
  });
});
