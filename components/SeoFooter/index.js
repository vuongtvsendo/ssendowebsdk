import React, { PureComponent } from 'react';
import styles from './styles.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

export class SeoFooter extends PureComponent {
  constructor(props) {
    super(props);
    this.createMarkup = this.createMarkup.bind(this);
  }
  createMarkup(data) {
    return { __html: data };
  }
  render() {
    const { data, isLoading } = this.props;
    if (!data || isLoading) {
      return <div />;
    }
    return (
      <div className={styles.seoContentWrapper}>
        <div className={styles.seoContent}>
          <div
            className="title"
            dangerouslySetInnerHTML={this.createMarkup(data.title)}
          />
          <div
            className="description"
            dangerouslySetInnerHTML={this.createMarkup(data.description)}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SeoFooter);
