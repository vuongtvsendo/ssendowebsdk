import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ShopCartItem } from './index';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';

const productObj = {
  id: '0b2bb09f3acbdae43a4ae450ebbb6de1',
  name: 'đầm ren tay dài',
  cat_path: 'san-pham/dam-ren-tay-dai-7654777',
  stock_status: 1,
  image:
    'https://media3.scdn.vn/img2/2017/11_7/J8HjPW_simg_02d57e_50x50_maxb.jpg',
  shop_id: 67050,
  price: 300000,
  final_price: 300000,
  qty: 1,
  quantity: 0,
  is_promotion: 0,
  hash: '0b2bb09f3acbdae43a4ae450ebbb6de1',
  attribute: [
    {
      attribute_id: 284,
      name: 'Màu sắc',
      product_option: '7654777_284',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 628,
          color_id: 16,
          product_option_id: '7654777_628',
          name: 'Đỏ',
          color_hex_rgb: 'ff0000',
          image: '',
          background: '#ff0000',
        },
      ],
      search_key: 'mau_sac',
    },
    {
      attribute_id: 298,
      name: 'Kích thước',
      product_option: '7654777_298',
      show_required: 1,
      type: 'Option',
      value: [
        {
          option_id: 816,
          value: 'M',
          product_option_id: '7654777_816',
        },
      ],
      search_key: 'kich_thuoc_1',
    },
  ],
  cart_attribute: [
    {
      option_id: 628,
      search_key: 'mau_sac',
      value: 'Đỏ',
      product_option_id: '7654777_628',
      color_hex_rgb: 'ff0000',
      image: '',
      background: '#ff0000',
      checkout_name: 'options[7654777_284][]',
      checkout_value: '7654777_628',
    },
    {
      option_id: 816,
      search_key: 'kich_thuoc_1',
      value: 'M',
      product_option_id: '7654777_816',
      checkout_name: 'options[7654777_298][]',
      checkout_value: '7654777_816',
    },
  ],
};

describe('<ShopCartItem /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopCart: {
        product_list: [],
      },
      deleteState: '',
    };
    const wrapper = shallow(<ShopCartItem {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(ShopCartItem);
  });
  it('it should render Thanh Toán enable if all products are out of stock', () => {
    const context = {
      ...ctx,
    };
    const props = {
      shopCart: {
        product_list: [productObj],
      },
      productSelected: '',
      deleteState: '',
    };
    const wrapper = shallow(<ShopCartItem {...props} />, {
      context,
    });
    expect(wrapper.find(LoginWrapper).exists()).toEqual(true);
  });
  it('it should render Thanh Toán disable if all products are NOT out of stock', () => {
    const context = {
      ...ctx,
    };
    productObj.stock_status = 0;
    const props = {
      shopCart: {
        product_list: [productObj],
      },
      productSelected: '',
      deleteState: '',
    };
    const wrapper = shallow(<ShopCartItem {...props} />, {
      context,
    });
    expect(wrapper.find(LoginWrapper).exists()).toEqual(false);
  });
});
