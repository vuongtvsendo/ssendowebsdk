import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import ProductCardWrap from 'sendo-web-sdk/components/ProductCardWrap';
import _get from 'lodash/get';
import classnames from 'classnames';
import styles from './shopCartItem.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import * as helperFunction from 'sendo-web-sdk/helpers/common';
import IconIdea from 'sendo-web-sdk/components/Icons/IconIdea';
import ReputationIcon from 'sendo-web-sdk/components/Icons/ReputationIcon';
import Check from 'sendo-web-sdk/components/Icons/Check';
import { buildExternalSendoUrl, mergeQueryUrl } from 'sendo-web-sdk/helpers/url';
import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';
import { paramsForCheckoutLink } from 'sendo-web-sdk/helpers/formatData';
import { ENABLE_BUYNOW_NOT_LOGIN } from 'sendo-web-sdk/helpers/const';

export class ShopCartItem extends PureComponent {
  handleCheckoutShopCart(url) {
    if (url) {
      let params = paramsForCheckoutLink();
      url = mergeQueryUrl(url, params);
      window.location.href = buildExternalSendoUrl(url);
    }
  }
  checkIsAllProductOutOfStock() {
    let isAllOutOfStock = true;
    const productList = _get(this.props, 'shopCart.product_list', []);
    for (let i = 0; i < productList.length; i++) {
      if (productList[i].stock_status === 1) {
        isAllOutOfStock = false;
        break;
      }
    }
    return isAllOutOfStock;
  }
  render() {
    const { shopCart, deleteState } = this.props;
    const classShopCartItem = classnames({
      [styles.shopCartItem]: true,
      [styles.deleteState]: deleteState,
    });
    const radioClass = classnames({
      'radio-face': true,
      selected: this.props.cartSelected,
    });
    return (
      <div className={classShopCartItem}>
        <header className={styles.shopInfo}>
          {deleteState &&
            <span
              className={radioClass}
              onClick={() => this.props.selectCart(shopCart)}
            />}

          <div className={styles.shopAvatar}>
            <img src={shopCart.shop_logo} alt={shopCart.shop_name} />
          </div>
          <h3 className={styles.shopName}>
            <span className={styles.shopName}>{shopCart.shop_name}</span>
            {shopCart.is_certified > 0 &&
              <ReputationIcon className={styles.iconShopUyTin} />}
          </h3>
        </header>
        <main>
          {this.renderCartItem(shopCart.product_list)}
        </main>
        <footer className={styles.footer}>
          <div className={styles.cartTotal}>
            <div className={styles.cartAmount}>
              <strong>Tổng tiền:</strong>
              <strong className="text-primary">
                {helperFunction.formatPrice(shopCart.total_order)}
              </strong>
            </div>
            <div className={styles.checkoutBtn}>
              {!this.checkIsAllProductOutOfStock() &&
                <LoginWrapper
                  event="click"
                  enableCheckoutLogin={ENABLE_BUYNOW_NOT_LOGIN}
                  isLoginCheckout="true"
                  callback={() =>
                    this.handleCheckoutShopCart(shopCart.url_checkout)}>
                  <button className="btn-primary">
                    Thanh toán
                  </button>
                </LoginWrapper>}
            </div>
          </div>
          {_get(shopCart, 'extra_bonus', false) &&
            <div className={styles.shopPromotion}>
              <p className={styles.bonusInfo}>
                <IconIdea className="icon" />
                <em
                  dangerouslySetInnerHTML={{ __html: shopCart.extra_bonus }}
                />
              </p>
            </div>}
        </footer>
      </div>
    );
  }
  renderCartItem(products) {
    const { deleteState } = this.props;
    return products.map(product => {
      const checkboxClass = classnames({
        'checkbox-face': true,
        selected: this.props.productSelected.indexOf(product) >= 0,
      });
      const cartItemClass = classnames({
        [styles.cartItem]: true,
        [styles.outOfStock]: product.stock_status === 0,
      });
      return (
        <div className={styles.cartItemWrapper} key={product.hash}>
          {deleteState &&
            <span
              className={checkboxClass}
              onClick={() =>
                this.props.onClickCheckbox(product, this.props.shopCart)}>
              <Check className="checkbox-face-icon" />
            </span>}
          <div className={cartItemClass} key={product.hash}>
            <ProductCardWrap product={product} className="card-horiz">
              <span className={styles.imageWrap}>
                <img src={product.image} alt={product.name} />
                {product.stock_status === 0 &&
                  <span className={styles.imgMask}>Hết hàng</span>}
              </span>
              <div className="caption">
                <h3 className="heading-medium truncate-medium-2 card-name">
                  {product.name}
                </h3>
                <div className="price">
                  <strong className="text-primary current">
                    {helperFunction.formatPrice(product.final_price)}
                  </strong>
                  {_get(product, 'is_promotion', 0) > 0 &&
                    <em className="text-small old">
                      {helperFunction.formatPrice(product.price)}
                    </em>}
                </div>
              </div>
            </ProductCardWrap>

            <div
              className={styles.attrs}
              onClick={() => {
                this.props.onUpdateCartItem(product);
              }}>
              <div className={styles.options}>
                {_get(product, 'cart_attribute.length', 0) > 0 &&
                  product.cart_attribute.map((attr, i) => {
                    return (
                      <div className={styles.option} key={i}>
                        {this.renderOptionLabel(attr)}
                      </div>
                    );
                  })}

                {/* <div className={styles.option}>
                {this.renderOptionLabel()}
                <span className="choose-lable-circle" />
              </div>
              <div className={styles.option}>
                <span className="choose-lable">S</span>
              </div>
              <div className={styles.option}>
                <span className="choose-lable">25 kg</span>
              </div> */}
              </div>
              <div className={styles.quantity}>
                <span className={styles.text}>Số lượng</span>
                <span className="selectbox-face">{product.qty}</span>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }
  renderOptionLabel(attr) {
    if (attr.search_key === 'mau_sac' || attr.search_key === 'mau_sac_lte') {
      return (
        <span
          className="choose-label-circle"
          style={{ background: attr.background }}
        />
      );
    } else {
      return <span className="choose-label">{attr.value}</span>;
    }
  }
  renderSelect(number) {
    var quantity = _get(this.props, 'product.quantity');
    quantity = quantity > 0 ? quantity : 100;
    var options = [];
    for (let i = 1; i <= quantity; i++) {
      options.push(i);
    }
    return options;
  }
}

export default withRouter(withStyles(styles)(ShopCartItem));
