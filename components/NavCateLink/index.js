import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './navCateLink.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import CarouselCustom from '../CarouselCustom';

class NavCateLink extends PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  };
  static childContextTypes = {
    router: PropTypes.object.isRequired,
  };

  buildLink(item) {
    return '/' + this.props.urlType + '/' + item.url_path;
  }
  handleActive(item) {
    if (this.props.sortPath === item.url_path) {
      return true;
    }
    return false;
  }
  onClickLink(e, item) {
    if (this.props.sortPath === item.url_path) e.preventDefault();
  }
  render() {
    let { items } = this.props;
    return (
      <div className={styles.sort}>
        <div className={styles.carousel}>
          <CarouselCustom
            isScroll={true}
            className={styles.carousel}
            history={this.props.history}>
            {items.map((item, i) => {
              let singleItemFirst = '';
              if (i === 0) singleItemFirst = styles['singleItemFirst'];
              return (
                <div key={i} className={`${styles.customItem}`}>
                  <NavLink
                    to={this.buildLink(item)}
                    activeClassName="active"
                    className={styles.singleItem + ' ' + singleItemFirst}
                    isActive={() => this.handleActive(item)}
                    onClick={e => this.onClickLink(e, item)}>
                    {item.name}
                  </NavLink>
                </div>
              );
            })}
          </CarouselCustom>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(NavCateLink);
