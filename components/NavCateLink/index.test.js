import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { mount, render } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import NavCateLink from './index';

describe('<NavCateLink /> component', () => {
  const items = [
    {
      name: 'test1',
      urlType: '',
      url_path: '?sortType=rank',
    },
    {
      name: 'test2',
      urlType: '',
      url_path: '?sortType=test2',
    },
  ];
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    // http://airbnb.io/enzyme/docs/api/render.html
    const wrapper = render(
      <Router>
        <NavCateLink sortPath="?sortType=rank" items={items} />
      </Router>,
      {
        context,
      }
    );
    expect(wrapper.find('a')).toHaveLength(2);
    // https://github.com/cheeriojs/cheerio#eq-i-
    expect(wrapper.find('a').eq(0).text()).toEqual('test1');
    expect(wrapper.find('a').eq(1).text()).toEqual('test2');
  });
  it('should be ok at first rendering', () => {
    const context = {
      ...ctx,
    };
    const wrapper = mount(
      <Router>
        <NavCateLink sortPath="?sortType=rank" items={items} />
      </Router>,
      {
        context,
      }
    );
    expect(wrapper.find('NavLink').at(0).props().isActive()).toEqual(true);
    expect(wrapper.find('NavLink').at(1).props().isActive()).toEqual(false);
  });
  it('should be ok on another', () => {
    const context = {
      ...ctx,
    };
    const wrapper = mount(
      <Router>
        <NavCateLink sortPath="?sortType=test2" items={items} />
      </Router>,
      {
        context,
      }
    );
    expect(wrapper.find('NavLink').at(0).props().isActive()).toEqual(false);
    expect(wrapper.find('NavLink').at(1).props().isActive()).toEqual(true);
  });
});
