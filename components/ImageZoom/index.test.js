import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ImageZoom } from './index';

describe('<ImageZoom /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<ImageZoom />, {
      context,
    });
    expect(wrapper.instance()).toBeInstanceOf(ImageZoom);
  });
});
