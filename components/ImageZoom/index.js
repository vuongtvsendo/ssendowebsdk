import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './imageZoom.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import * as Hammer from 'hammerjs';

export class ImageZoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isZooming: false,
      scaleValue: 1,
    };
    this.originWidth = 0;
    this.originHeight = 0;
    this.scaleRatio = 1;
  }
  componentDidMount() {
    this.hammer = Hammer(this.container, { domEvents: true });
  }
  toogleZoom(e) {
    this.setState({
      isZooming: !this.state.isZooming,
      scaleValue: this.state.isZooming ? this.scaleRatio : 1,
    });
  }
  onLoad(e) {
    this.originWidth = e.currentTarget.naturalWidth;
    this.originHeight = e.currentTarget.naturalHeight;
    this.scaleRatio = this.originWidth / e.currentTarget.clientWidth;
  }

  render() {
    const classContainer = classnames({
      [styles.container]: true,
      [styles.zooming]: this.state.isZooming,
    });
    return (
      <div
        onClick={e => this.toogleZoom(e)}
        className={classContainer}
        ref={ref => (this.container = ref)}>
        <img
          style={{ transform: `scale(${this.state.scaleValue})` }}
          className={styles.image}
          src={this.props.src}
          onLoad={e => this.onLoad(e)}
          ref={ref => (this.image = ref)}
        />
      </div>
    );
  }
}

export default withStyles(styles)(ImageZoom);
