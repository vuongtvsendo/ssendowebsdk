import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';
import CarouselCustom from '../CarouselCustom';
import styles from './carouselCateLink.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { normalizeUrl, mergeQueryUrl } from 'sendo-web-sdk/helpers/url';
import IconCheck from 'sendo-web-sdk/components/Icons/Check';

import { colorsArray } from 'sendo-web-sdk/res/colors';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import defaultImg from 'sendo-web-sdk/images/default.png';
import classnames from 'classnames';
import _get from 'lodash/get';
const loopColor = (colors, index) => {
  return colors[index % colors.length];
};

export class CarouselCateLink extends PureComponent {
  handleActive(item) {
    if (this.props.activeCategory === item.id) {
      return true;
    }
    return false;
  }
  onClickLink(e, item) {
    if (this.props.activeCategory === item.id) e.preventDefault();
  }
  checkIsScroll() {
    const isScroll = _get(this.props, 'isScroll', true);
    const categoryLevel = _get(this.props, 'categoryLevel', 0);
    if (categoryLevel === 1) return false;
    return isScroll;
  }
  buildLink(url) {
    if (!_get(this.props, 'shopAds.shop_ads')) {
      const shopAlias = _get(this.props, 'match.params.shopAlias');
      if (shopAlias) {
        return normalizeUrl(`shop/${shopAlias}/${url}`);
      }
      return url;
    }

    return mergeQueryUrl(url, this.props.shopAds);
  }
  render() {
    const { items, isHideImage } = this.props;
    const categoryLevel = _get(this.props, 'categoryLevel', 0);
    const activeCategory = _get(this.props, 'activeCategory');
    const classNavLink = classnames({
      item: true,
      'no-img': isHideImage,
    });
    return (
      <div className={`${styles.carousel} ${this.props.className}`}>
        {categoryLevel === 1 && (
          <h2 className={`text-small ${styles.heading}`}>Danh mục</h2>
        )}
        <CarouselCustom
          isScroll={this.checkIsScroll()}
          className={styles.carousel}
          history={this.props.history}
          categoryLevel={categoryLevel}
          activeCategory={activeCategory}>
          {items.map((item, i) => {
            let cover_active = '',
              style = { backgroundColor: loopColor(colorsArray, i) };
            if (categoryLevel === 1) style = {};
            if (this.handleActive(item) && !isHideImage)
              cover_active = (
                <span className={styles.activeCover}>
                  <IconCheck className="icon iconActiveCategory" />
                </span>
              );
            return (
              <div
                key={item.id}
                className={`${
                  categoryLevel === 1 ? 'box-cate-parent' : 'box-cate-child'
                }`}>
                <NavLink
                  to={normalizeUrl(this.buildLink(item.url_key))}
                  activeClassName="active"
                  key={item.id}
                  className={classNavLink}
                  style={style}
                  isActive={() => this.handleActive(item)}
                  onClick={e => this.onClickLink(e, item)}>
                  {cover_active}
                  {!isHideImage && (
                    <img
                      className="item-img lazyload"
                      src={defaultImg}
                      data-src={item.cat_image}
                      onError={e => onImgError(e)}
                      alt={item.name}
                    />
                  )}
                  <span className="item-title truncate-small-2">
                    {item.name}
                  </span>
                </NavLink>
              </div>
            );
          })}
        </CarouselCustom>
      </div>
    );
  }
}

export default withStyles(styles)(CarouselCateLink);
