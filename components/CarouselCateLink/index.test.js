import React from 'react';
import sinon from 'sinon';
import { shallow, mount } from 'sendo-web-sdk/utils/testing';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { CarouselCateLink } from './index';

describe('<CarouselCateLink /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [],
    };
    const wrapper = shallow(<CarouselCateLink {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(CarouselCateLink);
  });
  it('it should render box-cate-parent class if categoryLevel is 1', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [
        {
          id: 2629,
          name: 'Thời trang thiết kế',
          url_path: 'thoi-trang-nu/thoi-trang-thiet-ke/',
          url_key: 'thoi-trang-thiet-ke',
          cat_image:
            'https://media3.scdn.vn/images/ecom/category/2629_simg_02d57e_50x50_maxb.jpg',
        },
      ],
      categoryLevel: 1,
      isScroll: true,
    };
    const wrapper = shallow(<CarouselCateLink {...props} />, {
      context,
    });
    expect(wrapper.find('.box-cate-parent').exists()).toEqual(true);
  });
  it('it should render box-cate-child class if categoryLevel is not 1', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [
        {
          id: 2629,
          name: 'Thời trang thiết kế',
          url_path: 'thoi-trang-nu/thoi-trang-thiet-ke/',
          url_key: 'thoi-trang-thiet-ke',
          cat_image:
            'https://media3.scdn.vn/images/ecom/category/2629_simg_02d57e_50x50_maxb.jpg',
        },
      ],
      categoryLevel: 2,
      isScroll: true,
    };
    const wrapper = shallow(<CarouselCateLink {...props} />, {
      context,
    });
    expect(wrapper.find('.box-cate-child').exists()).toEqual(true);
  });
  it('it should return false in checkIsScroll if categoryLevel is 1', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [
        {
          id: 2629,
          name: 'Thời trang thiết kế',
          url_path: 'thoi-trang-nu/thoi-trang-thiet-ke/',
          url_key: 'thoi-trang-thiet-ke',
          cat_image:
            'https://media3.scdn.vn/images/ecom/category/2629_simg_02d57e_50x50_maxb.jpg',
        },
      ],
      categoryLevel: 1,
      isScroll: true,
    };
    const spyGetCategoryLevel = sinon.spy(
      CarouselCateLink.prototype,
      'checkIsScroll'
    );
    mount(<CarouselCateLink {...props} />, {
      context,
    });
    expect(spyGetCategoryLevel.returnValues[0]).toEqual(false);
  });
});
