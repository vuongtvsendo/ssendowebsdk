import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Offline } from './index';

describe('<Offline /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {};
    const wrapper = shallow(<Offline {...props} />, {
      context,
    });
    expect(wrapper.find('.title').text()).toEqual('Không thể kết nối Internet');
  });
});
