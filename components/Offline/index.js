import React, { Component } from 'react';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import HeaderSecondary from 'sendo-web-sdk/components/Header/HeaderSecondary';
import styles from './offline.css';
import offlineImg from 'sendo-web-sdk/images/empty-state/offline.svg';

export class Offline extends Component {
  render() {
    return (
      <div className={styles.offlinePage}>
        <section>
          <HeaderSecondary title="Không thể kết nối Internet" />
        </section>
        <section>
          <div className="content-empty full">
            <div className="image-square">
              <img
                className="img"
                src={offlineImg}
                alt="Không thể kết nối Internet"
              />
            </div>

            <strong className="title">Không thể kết nối Internet</strong>
            <p className="message">
              Sendo.vn không thể hiển thị nội dung vì máy bạn chưa có kết nối
              Internet. Vui lòng kiểm tra lại đường truyền!
            </p>
          </div>
        </section>
      </div>
    );
  }
}

export default withStyles(styles)(Offline);
