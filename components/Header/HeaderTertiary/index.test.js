import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HeaderTertiary } from './index';

describe('<HeaderTertiary /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      history: {},
      location: {},
      title: 'title',
      sub: 'sub',
      cartQuantity: 1,
    };
    const wrapper = shallow(<HeaderTertiary {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(HeaderTertiary);
    expect(wrapper.find('.circle-number').text()).toEqual(
      props.cartQuantity + ''
    );
  });
});
