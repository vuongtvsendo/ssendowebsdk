import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import styles from './headerTertiary.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import Cart from 'sendo-web-sdk/components/Icons/Cart';
import AllCate from 'sendo-web-sdk/components/Icons/AllCate';
import Hamberger from 'sendo-web-sdk/components/Icons/Hamberger';
import { CART_ROUTER } from 'sendo-web-sdk/helpers/const';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';

export class HeaderTertiary extends PureComponent {
  toggleMenu() {
    if (_get(this, 'props.toggleMenu') instanceof Function) {
      this.props.toggleMenu();
    }
  }
  toggleSearch() {
    if (_get(this, 'props.toggleSearch') instanceof Function) {
      this.props.toggleSearch();
    }
  }
  render() {
    return (
      <header className="header header-tertiary">
        <Link className="btn-icon" to="/sitemap" aria-label="Categories">
          <AllCate className="icon icon-inverse" />
        </Link>
        <div className="search-bar-fake">
          <button
            className="btn-icon-small"
            aria-label="Toggle Navigation"
            onClick={() => this.toggleMenu()}>
            <Hamberger className="icon" />
          </button>
          <div className={styles.searchBar} onClick={() => this.toggleSearch()}>
            <span className="text">Bạn mua gì hôm nay ?</span>
          </div>
        </div>
        <Link
          className="btn-icon circle-number-outer"
          to={normalizeUrl(CART_ROUTER)}
          aria-label="Open cart">
          <Cart className="icon icon-inverse" />
          {this.props.cartQuantity > 0 &&
            <span className="circle-number">{this.props.cartQuantity}</span>}
        </Link>
      </header>
    );
  }
}

export default withRouter(withStyles(styles)(HeaderTertiary));
