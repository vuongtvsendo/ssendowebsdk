import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import _get from 'lodash/get';
import { connect } from 'react-redux';
import styles from './headerSecondary.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import Search from 'sendo-web-sdk/components/Search';
import qs from 'query-string';
import { fbShare } from 'sendo-web-sdk/utils/facebook';
import MoreVerticalNav from 'sendo-web-sdk/components/Header/MoreVerticalNav';
import Ripple from 'sendo-web-sdk/components/Ripple';
import ArrowLeft from 'sendo-web-sdk/components/Icons/ArrowLeft';
import IconSearch from 'sendo-web-sdk/components/Icons/IconSearch';
import Cart from 'sendo-web-sdk/components/Icons/Cart';
import IconShare from 'sendo-web-sdk/components/Icons/IconShare';
import IconMoreVert from 'sendo-web-sdk/components/Icons/IconMoreVert';
import { CART_ROUTER } from 'sendo-web-sdk/helpers/const';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
import { isHistoryEmpty } from 'sendo-web-sdk/helpers/history';
import { getShopAdsParams } from 'sendo-web-sdk/helpers/formatData';
export class HeaderSecondary extends Component {
  constructor(props) {
    super(props);
    this.state = { isMoreVertOpen: false };
  }
  static propTypes = {
    history: PropTypes.object.isRequired,
  };
  onBack() {
    const { history } = this.props;
    if (isHistoryEmpty(this.props.historyLength)) {
      history.replace('/');
    } else {
      history.goBack();
    }
  }
  toggleSearch() {
    this.setState({ isOpenSearch: !this.state.isOpenSearch });
  }
  closeSearch() {
    this.setState({ isOpenSearch: false });
  }
  getSearchQuery() {
    const search = qs.parse(this.props.location.search);
    return search.q || '';
  }
  moreVertToggle() {
    this.setState({ isMoreVertOpen: !this.state.isMoreVertOpen });
  }
  openMoreVert() {
    this.setState({ isMoreVertOpen: true });
  }
  openShareModal(event) {
    const url = _get(this.props, 'shareUrl');
    fbShare(url);
  }
  render() {
    return (
      <header className="header">
        <Ripple
          tagName="button"
          className="btn-icon"
          aria-label="Previous page">
          <ArrowLeft
            className="icon icon-inverse"
            onClick={() => this.onBack()}
          />
        </Ripple>
        <h1 className="title-page text-overflow">
          <strong>{this.props.title}</strong>
          {this.props.sub && (
            <span className="title-sub-page text-info">{this.props.sub}</span>
          )}
        </h1>
        {/* button share facebook */}
        {_get(this.props, 'shareUrl', null) && (
          <Ripple
            tagName="button"
            className="btn-icon-small"
            aria-label="Share Facebook"
            onClick={e => this.openShareModal(e)}>
            <IconShare className="icon icon-inverse" />
          </Ripple>
        )}

        {/* button search */}
        {!_get(this.props, 'hideSearch', null) && (
          <Ripple
            tagName="button"
            className="btn-icon-small"
            aria-label="Open Search"
            onClick={() => this.toggleSearch()}>
            <IconSearch className="icon icon-inverse" />
          </Ripple>
        )}

        {/* button cart */}
        {!_get(this.props, 'hideCart', null) && (
          <Ripple
            tagName="a"
            className="btn-icon-small circle-number-outer"
            to={normalizeUrl(CART_ROUTER)}
            aria-label="Open Cart">
            <Cart className="icon icon-inverse" />
            {this.props.cartQuantity > 0 && (
              <span className="circle-number">{this.props.cartQuantity}</span>
            )}
          </Ripple>
        )}

        {/* button more navigation */}
        {!_get(this.props, 'hideMoreNav', null) && (
          <Ripple
            tagName="button"
            className="btn-icon-small"
            aria-label="Toggle more navigation"
            onClick={() => this.openMoreVert()}>
            <IconMoreVert className="icon icon-inverse" />
          </Ripple>
        )}
        {!_get(this.props, 'hideMoreNav', null) && (
          <MoreVerticalNav
            moreVertToggle={() => this.moreVertToggle()}
            isOpen={this.state.isMoreVertOpen}
          />
        )}

        {/* modal search */}
        {!_get(this.props, 'hideSearch', null) && (
          <Search
            isOpen={this.state.isOpenSearch}
            category={this.props.category}
            shopAds={getShopAdsParams(_get(this.props, 'location', {}))}
            title={this.getSearchQuery()}
            closeSearch={() => this.closeSearch()}
            toggleSearch={() => this.toggleSearch()}
            shopInfo={this.props.shopInfo}
          />
        )}
      </header>
    );
  }
}

// export default withRouter(withStyles(styles)(HeaderSecondary));
export default connect(state => {
  return {
    historyLength: state.historydata.historyLength,
  };
})(withRouter(withStyles(styles)(HeaderSecondary)));
