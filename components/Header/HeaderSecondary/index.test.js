import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HeaderSecondary } from './index';

describe('<HeaderSecondary /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      history: {},
      location: {},
      title: 'title',
      sub: 'sub',
      cartQuantity: 1,
    };
    const wrapper = shallow(<HeaderSecondary {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(HeaderSecondary);
    expect(wrapper.find('.title-page strong').text()).toEqual(props.title);
    expect(wrapper.find('.title-sub-page').text()).toEqual(props.sub);
  });
});
