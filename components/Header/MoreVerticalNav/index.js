import React, { PureComponent } from 'react';
import { buildExternalSendoUrl, normalizeUrl } from 'sendo-web-sdk/helpers/url';
import _get from 'lodash/get';
import _filter from 'lodash/filter';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './moreVerticalNav.css';
import Modal from 'reactstrap/lib/Modal';
import Ripple from 'sendo-web-sdk/components/Ripple';
import { PRODUCT_RECENTLY_ROUTER } from 'sendo-web-sdk/helpers/const';
var dataMenu = [
  {
    title: 'Trang chủ',
    url: '/',
  },
  {
    title: 'Sản phẩm vừa xem',
    url: normalizeUrl(PRODUCT_RECENTLY_ROUTER),
  },
  {
    title: 'Sản phẩm yêu thích',
    url: buildExternalSendoUrl('thong-tin-tai-khoan/san-pham-quan-tam/'),
  },
];
export class MoreVerticalNav extends PureComponent {
  getDataMenu() {
    const { pathname } = window.location;
    let menuItems = _filter(dataMenu, item => {
      return item.url !== pathname;
    });
    return menuItems;
  }
  render() {
    const menuData = this.getDataMenu();
    return (
      <Modal
        className={styles.moreVerticalNav}
        contentClassName={styles.modalContent}
        modalClassName="more-nav-vert"
        toggle={() => this.props.moreVertToggle()}
        isOpen={this.props.isOpen}>
        <div className={styles.moreNav}>
          <ul>
            {_get(menuData, 'length', 0) > 0 &&
              menuData.map(item => {
                return (
                  <li key={item.url}>
                    <Ripple
                      tagName="a"
                      className={styles.menuItem}
                      to={item.url}>
                      {item.title}
                    </Ripple>
                  </li>
                );
              })}

          </ul>
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(MoreVerticalNav);
