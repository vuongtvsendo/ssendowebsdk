import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { MoreVerticalNav } from './index';

describe('<MoreVerticalNav /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<MoreVerticalNav />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(MoreVerticalNav);
  });
});
