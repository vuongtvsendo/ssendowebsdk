import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import styles from './headerDetail.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import Ripple from 'sendo-web-sdk/components/Ripple';
import MoreVerticalNav from 'sendo-web-sdk/components/Header/MoreVerticalNav';

import ArrowLeft from 'sendo-web-sdk/components/Icons/ArrowLeft';
import Cart from 'sendo-web-sdk/components/Icons/Cart';
import IconMoreVert from 'sendo-web-sdk/components/Icons/IconMoreVert';
import { CART_ROUTER } from 'sendo-web-sdk/helpers/const';
import { normalizeUrl, mergeQueryUrl } from 'sendo-web-sdk/helpers/url';
import { isHistoryEmpty } from 'sendo-web-sdk/helpers/history';
import { getShopAdsParams } from 'sendo-web-sdk/helpers/formatData';

export class HeaderDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { isMoreVertOpen: false };
  }
  static propTypes = {
    history: PropTypes.object.isRequired,
  };
  onBack() {
    const { history, category } = this.props;
    if (isHistoryEmpty(this.props.historyLength)) {
      let url_key = _get(category, `[${category.length - 1}].url_key`, false);
      if (url_key) {
        const search = _get(this.props, 'history.location');
        url_key = mergeQueryUrl(url_key, getShopAdsParams(search, true));
        history.replace(normalizeUrl(url_key));
      }
    } else {
      history.goBack();
    }
  }
  moreVertToggle() {
    this.setState({ isMoreVertOpen: !this.state.isMoreVertOpen });
  }
  openMoreVert() {
    this.setState({ isMoreVertOpen: true });
  }
  render() {
    return (
      <header className={`${styles.headerDetail} header header-detail`}>
        {this.renderBackButton()}
        <h2 className="title-page text-overflow">
          {this.props.sku && <span>Mã: {this.props.sku}</span>}
        </h2>
        <div className="group-btn">
          <Ripple
            tagName="a"
            className="btn-icon-small circle-number-outer"
            to={normalizeUrl(CART_ROUTER)}
            aria-label="Open Cart">
            <Cart className="icon icon-inverse" />
            {this.props.cartQuantity > 0 &&
              <span className="circle-number">{this.props.cartQuantity}</span>}
          </Ripple>
          <Ripple
            tagName="button"
            className="btn-icon-small"
            aria-label="Toggle more navigation"
            onClick={() => this.openMoreVert()}>
            <IconMoreVert className="icon icon-inverse" />
          </Ripple>
        </div>
        <MoreVerticalNav
          moreVertToggle={() => this.moreVertToggle()}
          isOpen={this.state.isMoreVertOpen}
        />
      </header>
    );
  }
  renderBackButton() {
    const { cateMetadata, category } = this.props;
    let cateName = _get(category, `[${category.length - 1}].title`, '');
    if (isHistoryEmpty(this.props.historyLength)) {
      return (
        <Ripple
          tagName="button"
          className="btn-back-cate"
          onClick={() => this.onBack()}
          aria-label="Go to categories page">
          <ArrowLeft className="icon" />
          <span className="text-overflow">{cateName}</span>
        </Ripple>
      );
    } else if (cateMetadata && cateMetadata.category_name) {
      return (
        <Ripple
          tagName="button"
          className="btn-back-cate"
          onClick={() => this.onBack()}
          aria-label="Go to categories page">
          <ArrowLeft className="icon" />
          <span className="text-overflow">
            {cateMetadata.category_name || ''}
          </span>
        </Ripple>
      );
    }

    return (
      <Ripple
        tagName="button"
        className="btn-icon btn-icon-back"
        onClick={() => this.onBack()}
        aria-label="Previous page">
        <ArrowLeft className="icon icon-inverse" />
      </Ripple>
    );
  }
}

// export default withRouter(withStyles(styles)(HeaderDetail));
export default connect(state => {
  return {
    historyLength: state.historydata.historyLength,
  };
})(withRouter(withStyles(styles)(HeaderDetail)));
