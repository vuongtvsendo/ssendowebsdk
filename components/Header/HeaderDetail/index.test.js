import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HeaderDetail } from './index';

describe('<HeaderDetail /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      history: {},
      category: [],
      cateMetadata: {},
      cartQuantity: 1,
    };
    const wrapper = shallow(<HeaderDetail {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(HeaderDetail);
    expect(wrapper.find('.circle-number').text()).toEqual(
      props.cartQuantity + ''
    );
  });
});
