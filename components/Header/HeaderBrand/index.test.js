import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HeaderBrand } from './index';

describe('<HeaderBrand /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      cartQuantity: 1,
    };
    const wrapper = shallow(<HeaderBrand {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(HeaderBrand);
    expect(wrapper.find('.circle-number').text()).toEqual(
      props.cartQuantity + ''
    );
  });
});
