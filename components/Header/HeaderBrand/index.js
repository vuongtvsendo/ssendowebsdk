import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import classnames from 'classnames';
// import { Link } from 'react-router-dom';
import _get from 'lodash/get';
import styles from './headerBrand.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import Ripple from 'sendo-web-sdk/components/Ripple';
import Cart from 'sendo-web-sdk/components/Icons/Cart';
import IconAddScreen from 'sendo-web-sdk/components/Icons/IconAddScreen';
import Hamberger from 'sendo-web-sdk/components/Icons/Hamberger';
import IconSearch from 'sendo-web-sdk/components/Icons/IconSearch';
import imgLogo from 'sendo-web-sdk/images/logo.svg';
import { getMobileOperatingSystem, getBrowser } from 'sendo-web-sdk/helpers/common';
import { MOBILE_OS_IOS, BROWSER_SAFARI, CART_ROUTER } from 'sendo-web-sdk/helpers/const';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
export class HeaderBrand extends PureComponent {
  toggleMenu() {
    if (_get(this, 'props.toggleMenu') instanceof Function) {
      this.props.toggleMenu();
    }
  }
  toggleSearch() {
    if (_get(this, 'props.toggleSearch') instanceof Function) {
      this.props.toggleSearch();
    }
  }
  toggleAddGuide() {
    if (_get(this, 'props.toggleAddGuide') instanceof Function) {
      this.props.toggleAddGuide();
    }
  }

  canShowUserGuide() {
    let browser = getBrowser();
    let isShow = true;
    if (
      getMobileOperatingSystem() === MOBILE_OS_IOS &&
      browser !== BROWSER_SAFARI
    ) {
      isShow = false;
    }
    return isShow;
  }
  render() {
    const btnClassname = classnames({
      [styles.btnGuide]: true,
      'btn-icon-small': true,
      [styles.android]: getMobileOperatingSystem() === 'Android',
      [styles.iOS]: getMobileOperatingSystem() === 'iOS',
      [styles.canShowUserGuide]: this.canShowUserGuide(),
    });
    return (
      <header className="header-brand">
        <div className="header-brand-logo">
          <Ripple
            tagName="button"
            aria-label="Toggle Navigation"
            className="btn-icon btn-menu-main"
            onClick={() => this.toggleMenu()}>
            <Hamberger className="icon icon-inverse" />
          </Ripple>
          <img
            className="logo"
            src={imgLogo}
            alt="SENDO.VN - Mua sắm trực tuyến uy tín số 1 trong giao dịch, giá tốt nhất thị trường"
          />
          <Ripple
            tagName="button"
            className={btnClassname}
            onClick={() => this.toggleAddGuide()}
            aria-label="Open Userguide add homescreen">
            <IconAddScreen className="icon icon-inverse" />
          </Ripple>

          <Ripple
            tagName="a"
            className="btn-icon-small circle-number-outer"
            to={normalizeUrl(CART_ROUTER)}
            aria-label="Open Cart">
            <Cart className="icon icon-inverse" />
            {this.props.cartQuantity > 0 &&
              <span className="circle-number">{this.props.cartQuantity}</span>}
          </Ripple>
        </div>
        <div className="header-brand-search">
          <div className="search-bar-fake" onClick={() => this.toggleSearch()}>
            <span className="text">Bạn mua gì hôm nay ?</span>
            <IconSearch className="icon" />
          </div>
        </div>
      </header>
    );
  }
}

export default withRouter(withStyles(styles)(HeaderBrand));
