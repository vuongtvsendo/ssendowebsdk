import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import { compose } from 'redux';
import styles from './swapHeader.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import classnames from 'classnames';
import withScrollEvent from 'sendo-web-sdk/helpers/withScrollEvent';

const HEADER_HEIGHT_DEFAUTL = 96;
export class SwapHeader extends PureComponent {
  state = {
    isHide: true,
    height: HEADER_HEIGHT_DEFAUTL,
  };
  onScroll() {
    if (window.scrollY < this.state.height) {
      window.requestAnimationFrame(() => {
        this.setState({
          isHide: true,
        });
      });
    } else {
      if (!this.state.isHide) return;
      window.requestAnimationFrame(() => {
        this.setState({
          isHide: false,
        });
      });
    }
  }
  // onScrollDown() {
  //   window.requestAnimationFrame(() => {
  //     this.setState({
  //       isHide: false,
  //     });
  //   });
  // }
  // onScrollUp() {
  //   window.requestAnimationFrame(() => {
  //     this.setState({
  //       isHide: false,
  //     });
  //   });
  // }
  componentDidMount() {
    if (this.refs.sticky) {
      setTimeout(() =>
        this.setState({
          height: _get(this, 'refs.sticky.clientHeight', HEADER_HEIGHT_DEFAUTL),
        })
      );
    }
  }
  render() {
    let classes = classnames({
      [styles.stickyHeader]: true,
      [styles.hide]: this.state.isHide,
    });
    return (
      <div className={classes} ref="sticky">
        {this.props.children}
      </div>
    );
  }
}

export default compose(withStyles(styles), withScrollEvent)(SwapHeader);
