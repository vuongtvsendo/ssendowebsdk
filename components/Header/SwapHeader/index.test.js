import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { SwapHeader } from './index';

describe('<SwapHeader /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const wrapper = shallow(<SwapHeader />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(SwapHeader);
  });
});
