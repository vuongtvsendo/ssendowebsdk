import React, { Component } from 'react';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import classnames from 'classnames';
import styles from './headerSubView.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';

import Ripple from 'sendo-web-sdk/components/Ripple';

import ArrowLeft from 'sendo-web-sdk/components/Icons/ArrowLeft';
import IconDelete from 'sendo-web-sdk/components/Icons/IconDelete';
import { isHistoryEmpty } from 'sendo-web-sdk/helpers/history';

export class HeaderSubView extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
  };
  onBack() {
    const { history } = this.props;
    if (this.props.deleteState) {
      this.props.onToggleDelete();
      return;
    }
    if (isHistoryEmpty(this.props.historyLength)) {
      history.replace('/');
    } else {
      history.goBack();
    }
  }
  onToggleDelete() {
    if (_get(this, 'props.onToggleDelete') instanceof Function) {
      this.props.onToggleDelete();
    }
  }
  onDelete() {
    if (_get(this, 'props.onDelete') instanceof Function) {
      this.props.onDelete();
    }
  }
  render() {
    const { deleteState, totalCount } = this.props;
    const headerClass = classnames({
      [styles.header]: true,
      [styles.deleteState]: deleteState,
    });
    return (
      <header className={headerClass}>
        <Ripple
          tagName="button"
          className={`${styles.btnIcon} ${styles.iconBack}`}
          aria-label="Previous page"
          onClick={() => this.onBack()}>
          <ArrowLeft className={`${styles.icon}`} />
        </Ripple>
        <h1 className={`${styles.title}`}>
          {this.props.title}{' '}
          {this.props.sub && (
            <span className={styles.sub}>{this.props.sub}</span>
          )}
        </h1>
        <div className={styles.groupBtn}>
          {!deleteState &&
            totalCount > 0 && (
              <Ripple
                tagName="button"
                className={styles.btnIcon}
                aria-label="Delete Cart"
                onClick={() => this.onToggleDelete()}>
                <IconDelete className={styles.icon} />
              </Ripple>
            )}
          {deleteState && (
            <Ripple
              tagName="button"
              className={styles.btnIcon}
              onClick={() => this.onDelete()}>
              <span className={styles.textDelete}>Xóa</span>
            </Ripple>
          )}
        </div>
      </header>
    );
  }
}

// export default withRouter(withStyles(styles)(HeaderSubView));
export default connect(state => {
  return {
    historyLength: state.historydata.historyLength,
  };
})(withRouter(withStyles(styles)(HeaderSubView)));
