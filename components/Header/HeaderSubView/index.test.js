import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { HeaderSubView } from './index';

describe('<HeaderSubView /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      history: {},
      deleteState: '',
      sub: 'sub',
      title: 'title',
    };
    const wrapper = shallow(<HeaderSubView {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(HeaderSubView);
    expect(wrapper.find('h1').text()).toEqual(props.title + ' ' + props.sub);
  });
});
