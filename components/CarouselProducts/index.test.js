import React from 'react';
import { shallow } from 'sendo-web-sdk/utils/testing';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { CarouselProducts } from './index';

describe('<CarouselProducts /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      items: [],
    };
    const wrapper = shallow(<CarouselProducts {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(CarouselProducts);
  });
});
