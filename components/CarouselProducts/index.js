import React, { PureComponent } from 'react';
import CarouselCustom from '../CarouselCustom';
import styles from './style.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { normalizeUrl, mergeQueryUrl } from 'sendo-web-sdk/helpers/url';

import { colorsArray } from 'sendo-web-sdk/res/colors';
import { onImgError } from 'sendo-web-sdk/helpers/img';
import defaultImg from 'sendo-web-sdk/images/default.png';
import ProductCardWrap from 'sendo-web-sdk/components/ProductCardWrap';
import * as helperFunction from 'sendo-web-sdk/helpers/common';

export class CarouselProducts extends PureComponent {
  renderProductCartBasic(product) {
    return (
      <div className="product-card product-card-basic">
        <div className="thumbnail">
          <figure className="image-square">
            <img
              src={defaultImg}
              data-src={product.image}
              className="lazyload"
              alt={product.name}
            />
          </figure>
        </div>
        <div className="caption">
          <h3 className="heading-medium text-overflow"> {product.name}</h3>
          <div className="product-price">
            <strong className="current">
              {helperFunction.formatPrice(product.final_price)}
            </strong>
          </div>
        </div>
      </div>
    );
  }
  render() {
    const { items, cateMetadata, isTrackInviewport } = this.props;
    return (
      <div className={styles.carousel}>
        <CarouselCustom
          className={`${styles.carousel} ${styles.carouselProduct}`}>
          {items.map((item, i) => {
            return (
              <div key={i}>
                <ProductCardWrap
                  isHorizontalSensor={true}
                  isTrackInviewport={isTrackInviewport}
                  product={item}
                  dataIndex={i}
                  cateMetadata={cateMetadata}>
                  {this.renderProductCartBasic(item)}
                </ProductCardWrap>
              </div>
            );
          })}
        </CarouselCustom>
      </div>
    );
  }
}

export default withStyles(styles)(CarouselProducts);
