import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CarouselCustom from 'sendo-web-sdk/components/CarouselCustom';
import { NavLink } from 'react-router-dom';
import styles from './sort.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import qs from 'query-string';
import _merge from 'lodash/merge';
import _get from 'lodash/get';

class Sort extends PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  };
  static childContextTypes = {
    router: PropTypes.object.isRequired,
  };

  buildLink(item) {
    let location = this.context.router.route.location;
    let searchObj = qs.parse(location.search);
    searchObj = _merge(searchObj, item.search);
    let search = qs.stringify(searchObj);
    return location.pathname + '?' + search;
  }
  getSortType() {
    let location = this.context.router.route.location;
    let searchObj = qs.parse(location.search);
    return searchObj.sortType;
  }
  handleActive(item, defaultActiveType) {
    /**
     |------------------------------------------------
     | Cho nay code chua tot, can review lai
     |------------------------------------------------
     */
    let sortType = this.getSortType();

    if (!sortType && defaultActiveType) {
      sortType = defaultActiveType;
    }
    /**
     * Xu ly truong hop search trong cate
     */
    if (sortType === item.search.sortType) {
      return true;
    }
    return false;
  }
  checkIsScroll() {
    if (!this.getSortType()) return false;
    return true;
  }
  render() {
    const { items, defaultActiveType } = this.props;
    const customClass = _get(this.props, 'customClass', '');

    if (items.length > 4) {
      return (
        <CarouselCustom
          className={`${styles.sortTabs} custom-tab ${customClass}`}
          isScroll={this.checkIsScroll()}
          history={this.props.history}>
          {this.renderListSort(items, defaultActiveType)}
        </CarouselCustom>
      );
    }
    return (
      <div className={`custom-tab ${customClass}`}>
        {this.renderListSort(items, defaultActiveType)}
      </div>
    );
  }
  renderListSort(items, defaultActiveType) {
    return items.map((item, i) => (
      <div key={i} className={styles.itemWrap}>
        <NavLink
          to={this.buildLink(item)}
          replace
          activeClassName="active"
          className="item"
          isActive={() => this.handleActive(item, defaultActiveType)}>
          {item.name}
        </NavLink>
      </div>
    ));
  }
}

export { Sort };
export default withStyles(styles)(Sort);
