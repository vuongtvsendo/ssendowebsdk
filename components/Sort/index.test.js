import React from 'react';
import { shallow } from 'enzyme';
import { Sort } from './index';
//isolate
describe('<Sort /> component', () => {
  const props = {
    defaultActiveType: 'rank',
    items: [
      {
        name: 'name 1',
        search: {
          sortType: 'rank',
        },
      },
      {
        name: 'name 2',
        search: {
          sortType: 'vasup_desc',
        },
      },
    ],
  };
  it('should be ok at first rendering', () => {
    const context = {
      router: {
        route: {
          location: {
            search: '?sortType=rank',
          },
        },
      },
    };
    const wrapper = shallow(<Sort {...props} />, { context });
    expect(
      wrapper
        .find('NavLink')
        .at(0)
        .props()
        .isActive()
    ).toEqual(true);
  });
  it('should be ok on another', () => {
    const context = {
      router: {
        route: {
          location: {
            search: '?sortType=vasup_desc',
          },
        },
      },
    };
    const wrapper = shallow(<Sort {...props} />, { context });
    expect(
      wrapper
        .find('NavLink')
        .at(1)
        .props()
        .isActive()
    ).toEqual(true);
  });
  it('should be ok with default active sort type', () => {
    const context = {
      router: {
        route: {
          location: {
            search: '',
          },
        },
      },
    };
    const wrapper = shallow(
      <Sort {...props} />,
      { context }
    );
    expect(
      wrapper
        .find('NavLink')
        .at(0)
        .props()
        .isActive()
    ).toEqual(true);
  });
});
