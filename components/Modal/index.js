import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import { CLASS_MODAL_IGNORE_SCROLL } from 'sendo-web-sdk/helpers/const';

export class Common extends PureComponent {
  constructor(props, context) {
    super(props, context);
    this.handleTouchMove = this.handleTouchMove.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.state = {
      isModalOpen: true,
    };
  }
  componentDidMount() {
    window.addEventListener('touchmove', this.handleTouchMove);
  }
  componentWillUnmount() {
    window.removeEventListener('touchmove', this.handleTouchMove);
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    node && node.removeEventListener('scroll', this.handleScroll);
  }
  componentDidUpdate() {
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    if (node && !node._isScrollEventAttachted) {
      node.addEventListener('scroll', this.handleScroll);
      node._isScrollEventAttachted = true;
    }
  }
  handleTouchMove(event) {
    let target = event.target;
    if (this.state.isModalOpen || this.props.isOpen) {
      if (
        target.className === 'modal fade show' ||
        target.classList.contains(CLASS_MODAL_IGNORE_SCROLL)
      ) {
        event.preventDefault();
        event.stopPropagation();
        return false;
      }
    }
    return true;
  }
  handleScroll(event) {
    let node = ReactDOM.findDOMNode(this.refs.scroller);
    if (!node) {
      console.log('[BUG] Opps! Why node is not exist.');
    }
    if (node && node.scrollTop + node.clientHeight >= node.scrollHeight) {
      node.scrollTop = node.scrollHeight - node.clientHeight - 1;
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
    if (node && node.scrollTop === 0) {
      node.scrollTop = 1;
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
    return true;
  }
  render() {
    return <div>Sẽ được override bởi component thừa kế</div>;
  }
}

export default Common;
