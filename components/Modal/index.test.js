import React from 'react';
import { shallow } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { Common } from './index';

describe('<Common /> component', () => {
  it('it should be ok', () => {
    const context = {
      ...ctx,
    };
    const props = {
      title: 'Modal title',
    };
    const wrapper = shallow(<Common {...props} />, {
      context,
    });
    const inst = wrapper.instance();
    expect(inst).toBeInstanceOf(Common);
    expect(wrapper.find('div').text()).toEqual(
      'Sẽ được override bởi component thừa kế'
    );
  });
});
