import React, { PureComponent } from 'react';
import styles from './styles.css';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import { htmlDecode } from 'sendo-web-sdk/helpers/html';

export class CmsContent extends PureComponent {
  constructor(props) {
    super(props);
    this.createMarkup = this.createMarkup.bind(this);
  }
  createMarkup(html) {
    return {
      __html: htmlDecode(html),
    };
  }
  createScript(input) {
    if (!input) {
      return;
    }
    const script = document.createElement('script');
    const scriptText = document.createTextNode(input);
    script.appendChild(scriptText);
    document.body.appendChild(script);
  }

  componentDidMount() {
    if (!this.props.js) {
      this.createScript(this.props.js);
    }
  }

  render() {
    let { html, css } = this.props;
    return (
      <div>
        {css &&
          <style
            dangerouslySetInnerHTML={{
              __html: css,
            }}
          />}
        {html &&
          <div className={styles.cmsContentWrapper}>
            <div
              className={styles.cmsContent}
              dangerouslySetInnerHTML={this.createMarkup(html)}
            />
          </div>}
      </div>
    );
  }
}

export default withStyles(styles)(CmsContent);
