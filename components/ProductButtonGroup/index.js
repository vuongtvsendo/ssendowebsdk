import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import withStyles from 'sendo-web-sdk/helpers/withStyles';
import styles from './productButtonGroup.css';

import LoginWrapper from 'sendo-web-sdk/components/Profile/LoginWrapper';
import Cart from 'sendo-web-sdk/components/Icons/Cart';
import Plus from 'sendo-web-sdk/components/Icons/Plus';
import { ENABLE_BUYNOW_NOT_LOGIN } from 'sendo-web-sdk/helpers/const';

export class ProductButtonGroup extends PureComponent {
  onChangeAmount(e) {
    if (_get(this, 'props.onChangeAmount') instanceof Function) {
      this.props.onChangeAmount(e.target.value);
    }
  }
  onClickAddToCart() {
    if (_get(this, 'props.onClickAddToCart') instanceof Function) {
      this.props.onClickAddToCart();
    }
  }
  onClickBuyNow() {
    if (_get(this, 'props.onClickBuyNow') instanceof Function) {
      this.props.onClickBuyNow();
    }
  }
  onClickConfirm() {
    if (_get(this, 'props.onClickConfirm') instanceof Function) {
      this.props.onClickConfirm();
    }
  }
  render() {
    const { stockStatus } = this.props;
    return (
      <div className={`${styles.productButtonGroup}`}>
        {stockStatus === 0 &&
          <div className={styles.outOfStock + ' ' + styles.fixedBottom}>
            <span>Sản phẩm hết hàng</span>
          </div>}
        <div className={styles.groupControl}>
          <div className={styles.customSelect}>
            <select
              className={styles.selectbox}
              value={this.props.defaultAmount || 1}
              onChange={e => this.onChangeAmount(e)}
              aria-label="Quantity">
              {this.renderSelect()}
            </select>
          </div>
          {this.renderButtonGroup()}

        </div>
      </div>
    );
  }
  renderButtonGroup() {
    const { isValidAttr, actionFlow } = this.props;
    if (isValidAttr) {
      if (actionFlow && actionFlow !== 'chooseAttr') {
        if (actionFlow === 'addToCart') {
          return (
            <div className={styles.btnWrap}>
              <button
                className={styles.btnBuy}
                onClick={e => this.onClickConfirm(e)}>
                Xác nhận
              </button>
            </div>
          );
        }
        return (
          <div className={styles.btnWrap}>
            <LoginWrapper
              event="click"
              callback={e => this.onClickConfirm(e)}
              isLoginCheckout="true"
              enableCheckoutLogin={ENABLE_BUYNOW_NOT_LOGIN}>
              <button className={styles.btnBuy}>
                Xác nhận
              </button>
            </LoginWrapper>
          </div>
        );
      } else {
        return (
          <div className={styles.btnWrap}>
            <button
              className={styles.btnCart}
              aria-label="Add to cart"
              onClick={e => this.onClickAddToCart(e)}>
              <Plus className={styles.iconCart} />
              <Cart className={styles.iconCart} />
            </button>
            <LoginWrapper
              event="click"
              callback={e => this.onClickBuyNow(e)}
              isLoginCheckout="true"
              enableCheckoutLogin={ENABLE_BUYNOW_NOT_LOGIN}>
              <button className={styles.btnBuy}>
                Mua ngay
              </button>
            </LoginWrapper>
          </div>
        );
      }
    } else {
      if (actionFlow && actionFlow !== 'chooseAttr') {
        return (
          <div className={styles.btnWrap}>
            <button
              className={styles.btnBuy}
              onClick={e => this.onClickConfirm(e)}>
              Xác nhận
            </button>
          </div>
        );
      } else {
        return (
          <div className={styles.btnWrap}>
            <button
              className={styles.btnCart}
              aria-label="Add to cart"
              onClick={e => this.onClickAddToCart(e)}>
              <Plus className={styles.iconCart} />
              <Cart className={styles.iconCart} />
            </button>
            <button
              className={styles.btnBuy}
              onClick={e => this.onClickBuyNow(e)}>
              Mua ngay
            </button>
          </div>
        );
      }
    }
  }
  renderSelect() {
    var quantity = _get(this.props, 'productQuantity', 0) || 100;
    quantity = Math.min(quantity, 100);
    var options = [];
    for (let i = 1; i <= quantity; i++) {
      options.push(<option key={i} value={i}>{i}</option>);
    }
    return options;
  }
}

export default withStyles(styles)(ProductButtonGroup);
