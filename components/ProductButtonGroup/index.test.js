import React from 'react';
import { shallow, render } from 'enzyme';
import { ctx } from 'sendo-web-sdk/utils/testing';
import { ProductButtonGroup } from './index';

describe('<ProductButtonGroup /> component', () => {
  it('It should render without error', () => {
    const context = {
      ...ctx,
    };
    const props = {
      stockStatus: 0,
    };
    const wrapper = shallow(<ProductButtonGroup {...props} />, {
      context,
    });
    expect(wrapper.find('span').first().text()).toEqual('Sản phẩm hết hàng');
  });
  it('It should render correct Amount', () => {
    const context = {
      ...ctx,
    };
    const props = {
      stockStatus: 0,
      defaultAmount: 10,
    };
    const wrapper = render(<ProductButtonGroup {...props} />, {
      context,
    });
    expect(wrapper.find('select').val()).toEqual('10');
  });
});
