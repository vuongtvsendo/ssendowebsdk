/**
 * Day la ham dac biet va rat ton chi phi de thay text cho phu hop
 * Ham nay nen dc fix o dau SMC, va ca dau API truoc khi ghi xuong du lieu
 * @param html
 * @param options
 * @returns {string|*}
 */
export const normalize = (html, options = {}) => {
  html = html || '';
  html = html.split('src="http://media3.scdn.vn').join('src="//media3.scdn.vn');
  return html;
};

export const htmlDecode = text => {
  var entities = {
    amp: '&',
    apos: "'",
    lt: '<',
    gt: '>',
    quot: '"',
    nbsp: '\xa0',
  };
  var entityPattern = /&([a-z]+);/gi;
  // A single replace pass with a static RegExp is faster than a loop
  return text.replace(entityPattern, function(match, entity) {
    entity = entity.toLowerCase();
    if (entities.hasOwnProperty(entity)) {
      return entities[entity];
    }
    // return original string if there is no matching entity (no replace)
    return match;
  });
};
