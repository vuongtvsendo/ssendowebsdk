import * as common from '../common';
describe('sendo-web-sdk/helpers/auth', () => {
  describe('test function removeEndsSlash', () => {
    it('should return correct value', () => {
      expect(common.removeEndsSlash('abc/')).toEqual('abc');
    });
  });
  describe('test function friendlyNumber', () => {
    it('should return correct value', () => {
      expect(common.friendlyNumber(99000)).toEqual('99k');
      expect(common.friendlyNumber(999000)).toEqual('999k');
      expect(common.friendlyNumber(10000000)).toEqual('10m');
      // expect(common.friendlyNumber(10000000000)).toEqual('10t');
    });
  });
  describe('test function formatNumber', () => {
    it('should return correct value', () => {
      expect(common.formatNumber(99000)).toEqual('99,000');
      expect(common.formatNumber(999000)).toEqual('999,000');
    });
  });
  describe('test function formatPrice', () => {
    it('should return correct value', () => {
      expect(
        common.formatPrice(() => {
          return 100000;
        })
      ).toEqual('100,000 đ');
    });
    it('should return correct Free', () => {
      expect(common.formatPrice(0)).toEqual('Miễn phí');
    });
  });
  describe('test function formatTimeDisplay', () => {
    it('should return correct value by input value is null,', () => {
      const epochTime = null;
      expect(common.formatTimeDisplay(epochTime)).toEqual('');
    });
    it('should return correct value by input value is undefined,', () => {
      const epochTime = undefined;
      expect(common.formatTimeDisplay(epochTime)).toEqual('');
    });
    it('should return correct value by input value is dsdsdsdsdsd,', () => {
      const epochTime = 'abcxyz';
      expect(common.formatTimeDisplay(epochTime)).toEqual('');
    });
    it('should return correct value by input value is 1477297243', () => {
      const epochTime = 1477297243;
      expect(common.formatTimeDisplay(epochTime)).toEqual('15:20 | 24/10/2016');
    });
  });
});
