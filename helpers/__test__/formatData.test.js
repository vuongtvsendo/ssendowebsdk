import * as formatData from '../formatData';
import sinon from 'sinon';
import * as cookie from 'sendo-web-sdk/helpers/cookie';
import emitter from 'sendo-web-sdk/helpers/emitter';
import {
  SESSION_CONTEXT_CATEGORIES,
  LISTING_PRODUCT_PER_PAGE,
} from 'sendo-web-sdk/helpers/const';
describe('sendo-web-sdk/helpers/formatData', () => {
  afterEach(function() {
    cookie.get.restore();
    emitter.getSession.restore();
  });
  beforeEach(function() {
    sinon.stub(cookie, 'get');
    cookie.get.withArgs('listing_algo').returns('algo');
    cookie.get.withArgs('tracking_id').returns('tracking');
    sinon
      .stub(emitter, 'getSession')
      .returns({ session_key: 1234, context: SESSION_CONTEXT_CATEGORIES });
  });
  describe('test function buildSourceInfoString', () => {
    it('should return correct value by source_info', () => {
      const product = {
        product_type: 2,
        num_result_per_page: 30,
        index: 1,
        productMetadata: {
          special_res: 2,
          experiment_id: 4,
        },
      };
      expect(formatData.buildSourceInfoString(product)).toEqual(
        'pwa_' + LISTING_PRODUCT_PER_PAGE + '_1234_tracking_4_algo_2_1_2'
      );
    });
  });
});
