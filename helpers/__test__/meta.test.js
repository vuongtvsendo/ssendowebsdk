import React from 'react';
// import sinon from 'sinon';
import { shallow } from 'sendo-web-sdk/utils/testing';
import Meta from '../meta';

describe('<Meta /> component', () => {
  it('should return correctly description', () => {
    const props = {
      description: 'description',
      ogDescription: 'ogDescription',
    };

    var warraper = shallow(<Meta {...props} />);

    expect(
      warraper.find('meta[property="og:description"]').first().props().content
    ).toEqual('ogDescription');

    expect(
      warraper.find('meta[name="description"]').first().props().content
    ).toEqual('description');
  });

  it('should return equa correct image meta', () => {
    const props = {
      image: [1, 2, 3],
    };

    var warraper = shallow(<Meta {...props} />);

    expect(warraper.find('meta[property="og:image"]')).toHaveLength(3);
  });

  it('should return equa correct image meta', () => {
    const props = {
      image: 'image url',
    };

    var warraper = shallow(<Meta {...props} />);

    expect(warraper.find('meta[property="og:image"]')).toHaveLength(1);
  });
});
