import sinon from 'sinon';
import * as base from '../base';
import * as url from '../url';

describe('sendo-web-sdk/helpers/url', () => {
  afterEach(function() {
    if (base.getLocation.restore instanceof Function) {
      base.getLocation.restore();
    }
  });
  describe('test function serverBaseDomain', () => {
    it('should return correct value', () => {
      expect(url.serverBaseDomain('localhost', true)).toEqual('localhost');
    });
    it('should return correct value', () => {
      expect(url.serverBaseDomain('mpilot.sendo.vn', true)).toEqual(
        '.mpilot.sendo.vn'
      );
    });
    it('should return correct value', () => {
      expect(url.serverBaseDomain('m.sendo.vn', true)).toEqual('.m.sendo.vn');
    });
    it('should return correct value', () => {
      expect(url.serverBaseDomain('www.sendo.vn', true)).toEqual('.sendo.vn');
    });
  });
  describe('test function getBaseDomain', () => {
    it('should return correct value', () => {
      //callsFake === returns
      sinon.stub(base, 'getLocation').callsFake(() => ({
        hostname: 'localhost',
      }));
      expect(url.getBaseDomain()).toEqual('localhost');
    });
  });
  describe('test function getBaseDomain on pilot.sendo.vn', () => {
    it('should return correct value', () => {
      sinon.stub(base, 'getLocation').returns({
        hostname: 'pilot.sendo.vn',
      });
      expect(url.getBaseDomain()).toEqual('pilot.sendo.vn');
    });
  });
  describe('test function getBaseDomain on www.sendo.vn', () => {
    it('should return correct value', () => {
      sinon.stub(base, 'getLocation').returns({
        hostname: 'www.sendo.vn',
      });
      expect(url.getBaseDomain()).toEqual('sendo.vn');
    });
  });
  describe('test function getBaseDomain(true) on www.sendo.vn', () => {
    it('should return correct value', () => {
      sinon.stub(base, 'getLocation').returns({
        hostname: 'www.sendo.vn',
      });
      expect(url.getBaseDomain(true)).toEqual('.sendo.vn');
    });
  });
  describe('test function getBaseDomain(true) on pilot.sendo.vn', () => {
    it('should return correct value', () => {
      sinon.stub(base, 'getLocation').returns({
        hostname: 'pilot.sendo.vn',
      });
      expect(url.getBaseDomain(true)).toEqual('.pilot.sendo.vn');
    });
  });
  describe('test function getBaseDomain(true) on localhost', () => {
    it('should return correct value', () => {
      sinon.stub(base, 'getLocation').returns({
        hostname: 'localhost',
      });
      expect(url.getBaseDomain(true)).toEqual('localhost');
    });
  });
  describe('test function normalizeImgUrl(url) on localhost', () => {
    it('should return correct value', () => {
      expect(
        url.normalizeImgUrl('http://media3.scdn.vn/img2/2017/12_7/CdBJ9x.png')
      ).toEqual('//media3.scdn.vn/img2/2017/12_7/CdBJ9x.png');
      expect(
        url.normalizeImgUrl('https://media3.scdn.vn/img2/2017/12_7/CdBJ9x.png')
      ).toEqual('//media3.scdn.vn/img2/2017/12_7/CdBJ9x.png');
    });
  });
  describe('test function normalizeUrl()', () => {
    it('should return correct value with many url', () => {
      expect(url.normalizeUrl('/tim-kiem?q=iphone')).toEqual(
        '/tim-kiem/?q=iphone'
      );
      expect(url.normalizeUrl('tim-kiem?q=iphone')).toEqual(
        '/tim-kiem/?q=iphone'
      );
      expect(
        url.normalizeUrl('http://localhost:3000/tim-kiem?q=iphone', true)
      ).toEqual('http://localhost:3000/tim-kiem/?q=iphone');
      expect(
        url.normalizeUrl('http://www.sendo.vn/tim-kiem?q=iphone', true)
      ).toEqual('http://www.sendo.vn/tim-kiem/?q=iphone');
      expect(
        url.normalizeUrl('https://www.sendo.vn/tim-kiem?q=iphone')
      ).toEqual('/tim-kiem/?q=iphone');
      expect(url.normalizeUrl('http://sendo.vn/tim-kiem?q=iphone')).toEqual(
        '/tim-kiem/?q=iphone'
      );
    });
    it('validate *.htm || *.html', () => {
      expect(url.normalizeUrl('/san-pham-133.html/?q=iphone')).toEqual(
        '/san-pham-133.html?q=iphone'
      );
    });
  });
  describe('test function fetchCategorySlugFromUrl', () => {
    it('should return correct value', () => {
      const param = {
        cate1: 'thoi-trang-nu',
        cate2: 'ao-vay',
      };
      expect(url.fetchCategorySlugFromUrl(param)).toEqual(
        param.cate1 + '/' + param.cate2
      );
    });
  });
  describe('test function addEndSlash', () => {
    it('should return correct value', () => {
      var href1 = '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906';
      expect(url.addEndSlash(href1)).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/'
      );

      var href2 =
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906?';
      expect(url.addEndSlash(href2)).toEqual(
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?'
      );

      var href3 =
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?';
      expect(url.addEndSlash(href3)).toEqual(
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?'
      );

      var href4 =
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906?a=1&b=2';
      expect(url.addEndSlash(href4)).toEqual(
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1&b=2'
      );

      var href5 =
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1&b=2';
      expect(url.addEndSlash(href5)).toEqual(
        'http://localhost:3000/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1&b=2'
      );
    });
  });

  describe('test function mergeQueryUrl', () => {
    it('should return correct value', () => {
      var href1 = '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906';
      expect(url.mergeQueryUrl(href1)).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/'
      );

      var href2 = '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906?';
      expect(url.mergeQueryUrl(href2)).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?'
      );

      var href3 = '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?';
      expect(url.mergeQueryUrl(href3)).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?'
      );
    });
  });

  describe('test function mergeQueryUrl', () => {
    it('should return correct value', () => {
      var href6 = '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1';
      expect(url.mergeQueryUrl(href6, { q: 'iphone' })).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1&q=iphone'
      );
    });
  });

  describe('test function mergeQueryUrl', () => {
    it('should return correct value', () => {
      var href6 = 'san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906';
      expect(url.mergeQueryUrl(href6, { a: 1, q: 'iphone' })).toEqual(
        '/san-pham/vong-tay-ngoc-trai-thap-eiffel-3535906/?a=1&q=iphone'
      );
    });
  });

  describe('test function isSendoUrl', () => {
    it('should return correct value', () => {
      let href =
        'https://www.google.com/url?q=https%3A%2F%2Fsendo.vn%2Fmat-kinh-nam-nu-porsche-mua-1-tang-1-7180773.html&sa=D&sntz=1&usg=AFQjCNGhmzWxC5zalhQ-C1thOEeedKxfIQ';
      expect(url.isSendoUrl(href)).toEqual(false);
    });
    it('should return correct value', () => {
      let href = 'https://www.sendo.vn/style-guide#Button_Style';
      expect(url.isSendoUrl(href)).toEqual(true);
      href = 'https://sendo.vn/style-guide#Button_Style';
      expect(url.isSendoUrl(href)).toEqual(true);
    });
  });

  describe('test function buildStaticUrl', () => {
    var PUBLIC_URL = process.env.PUBLIC_URL;
    beforeEach(() => {
      process.env.PUBLIC_URL = '';
    });
    it("should return correct value when PUBLIC_URL=''", () => {
      var exprectUrl = '/icons/sendo-notification.png';
      expect(url.buildStaticUrl('/icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
      expect(url.buildStaticUrl('icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
    });
    afterEach(() => {
      process.env.PUBLIC_URL = PUBLIC_URL;
    });
  });
  describe('test function buildStaticUrl', () => {
    var PUBLIC_URL = process.env.PUBLIC_URL;
    beforeEach(() => {
      process.env.PUBLIC_URL = '/';
    });
    it("should return correct value when PUBLIC_URL='/'", () => {
      var exprectUrl = '/icons/sendo-notification.png';
      expect(url.buildStaticUrl('/icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
      expect(url.buildStaticUrl('icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
    });
    afterEach(() => {
      process.env.PUBLIC_URL = PUBLIC_URL;
    });
  });
  describe('test function buildStaticUrl', () => {
    var PUBLIC_URL = process.env.PUBLIC_URL;
    beforeEach(() => {
      process.env.PUBLIC_URL = '//static.sendo.vn';
    });
    it("should return correct value when PUBLIC_URL='//static.sendo.vn'", () => {
      var exprectUrl = '//static.sendo.vn/icons/sendo-notification.png';
      expect(url.buildStaticUrl('/icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
      expect(url.buildStaticUrl('icons/sendo-notification.png')).toEqual(
        exprectUrl
      );
    });
    afterEach(() => {
      process.env.PUBLIC_URL = PUBLIC_URL;
    });
  });
  describe('test function serialize', () => {
    it('should return correct value', () => {
      var params = { q: 'test', order: { id: 'asc' } };
      var expectValue = 'q=test&order[id]=asc';
      expect(decodeURIComponent(url.serialize(params))).toEqual(expectValue);
    });
  });
  describe('test function detectDirectUrlEvent', () => {
    it('should return correct value', () => {
      let campaign_url = '/su-kien/giam-sap-san-tet-hoanh-trang/';
      expect(url.detectDirectUrlEvent(campaign_url)).toEqual(true);
      campaign_url = '/event/giam-sap-san-tet-hoanh-trang/';
      expect(url.detectDirectUrlEvent(campaign_url)).toEqual(false);
    });
  });
});
