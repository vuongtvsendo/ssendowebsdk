import { htmlDecode } from '../html';

describe('sendo-web-sdk/helpers/html', () => {
  describe('test function htmlDecode', () => {
    it('should return valid value', () => {
      expect(htmlDecode("<img src='myimage.jpg'>")).toEqual(
        "<img src='myimage.jpg'>"
      );
      expect(htmlDecode("&lt;img src='myimage.jpg'&gt;")).toEqual(
        "<img src='myimage.jpg'>"
      );
    });
  });
});
