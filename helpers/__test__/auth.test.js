import * as auth from '../auth';
describe('sendo-web-sdk/helpers/auth', () => {
  describe('test function getUserId', () => {
    it('should return correct value by user_id', () => {
      var identity = {
        user_id: 1,
      };
      expect(auth.getUserId(identity)).toEqual(1);
    });
    it('should return correct value by customer_id', () => {
      var identity = {
        customer_id: 1,
      };
      expect(auth.getUserId(identity)).toEqual(1);
    });
  });
  describe('test function getTrackingUserId', () => {
    it('should return correct value by fpt_id', () => {
      var identity = {
        fpt_id: 1,
      };
      expect(auth.getTrackingUserId(identity)).toEqual(1);
    });
  });
  it('should return correct value', () => {
    var identity = {
      user_id: 1,
      customer_id: 1,
    };
    expect(auth.isLoggedIn(identity)).toEqual(true);
  });
  it('should return correct value', () => {
    var identity = {};
    expect(auth.isLoggedIn(identity)).toEqual(false);
  });
  it('should return correct UserFullName', () => {
    var identity = {
      firstname: null,
      first_name: 'First',
      lastname: null,
      last_name: 'Last',
      fullname: 'First Last',
      full_name: 'First Last',
    };
    expect(auth.getFullname(identity)).toEqual('First Last');
  });
});
