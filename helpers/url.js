import * as Cookies from 'sendo-web-sdk/helpers/cookie';
import {
  JWT_COOKIE_NAME,
  STATUS_OK,
  STATUS_REDIRECT,
} from 'sendo-web-sdk/helpers/const';
import { getInstance as getInstanceHistory } from 'sendo-web-sdk/helpers/history';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import _defaultsDeep from 'lodash/defaultsDeep';
import { isInArray } from 'sendo-web-sdk/helpers/common';
import _endsWith from 'lodash/endsWith';
import qs from 'query-string';
import { getLocation } from './base';

const detectRedirect = res => {
  // Trong truong hop manually redirect
  if (res.type === 'opaqueredirect' && res.status === 0) {
    return true;
  }
  // Chua thay truong hop nay xay ra
  if (isInArray(STATUS_REDIRECT, res.status)) {
    return true;
  }
  return false;
};
const guessRedirectUrl = res => {
  if (res.headers.get('location')) {
    return res.headers.get('location');
  }
  if (res.headers.get('x-location')) {
    return res.headers.get('x-location');
  }
  return getBaseUrl();
};

export const isSendoUrl = url => {
  var regex = /^https?:\/\/([^.]+\.)?sendo\.vn/;
  return regex.test(url);
};

/**
 * Note
 * 1. Function nay dc cap nhat de tach biet giua viet ghi cookie cho
 *    domain: pilot.sendo.vn = .pilot.sendo.vn
 *    domain: www.sendo.vn = .sendo.vn
 * @param {String} hostname
 * @param {String} withDot
 */
export const serverBaseDomain = (hostname, withDot = false) => {
  let arrHostnames = hostname.split('.');
  if (arrHostnames.length === 1) {
    return arrHostnames[0];
  }
  var prefix = '';
  if (arrHostnames.length > 2 && arrHostnames[0] === 'www') {
    arrHostnames = arrHostnames.splice(arrHostnames.length - 2);
  }
  if (withDot) {
    prefix = '.';
  }
  var base = arrHostnames.join('.');
  return prefix + base;
};

export const getBaseDomain = (withDot = false) => {
  const location = getLocation();
  const hostname = location.hostname;
  return serverBaseDomain(hostname, withDot);
};

export const getBaseUrl = () => {
  const location = getLocation();
  const BASE_URL = location.protocol + '//' + location.host;
  return BASE_URL;
};

export const normalizeUrl = (url, absolute = false, endSlash = true) => {
  /**
   |---------------------------------------------------------------
   | ! IMPORTANT
   | 1. Logic nay nen dc fix o dau API
   |--------------------------------------------------------------- 
   */
  url = (url || '').replace(new RegExp('&amp;', 'g'), '&');
  const hasProtocol = /^https?:\/\//.test(url);
  var relativeUrl = (url[0] !== '/' ? '/' : '') + url;
  var result = relativeUrl;
  if (absolute) {
    if (hasProtocol) {
      result = url;
    } else {
      result = getBaseUrl() + relativeUrl;
    }
  } else if (hasProtocol) {
    result = url.replace(/^https?:\/\/[^/]+\//, '/');
  }
  return endSlash ? addEndSlash(result) : result;
};

export const normalizeImgUrl = (url, options = {}) => {
  if (url) {
    url = url.replace(/^https?:\/\//, '//');
  }
  return url;
};

export const addEndSlash = url => {
  if (/\.html?\//.test(url)) {
    return url.replace(/(\.html?)\//, '$1');
  }
  if (/\.html?/.test(url)) {
    return url;
  }
  if (/\/(\?|#)/.test(url)) {
    return url;
  }
  if (/\?|#/.test(url)) {
    return url.replace(/(\?|#)/, '/$1');
  }
  if (/\/$/.test(url)) {
    return url;
  }
  return url + '/';
};

export const mergeQueryUrl = (url, source_info = {}) => {
  url = normalizeUrl(url, false);
  let queryOld = qs.extract(url);
  let search = qs.parse(queryOld);
  for (const item in source_info) {
    if (!source_info[item]) {
      delete source_info[item];
    }
  }
  var queryNew = qs.stringify({
    ...search,
    ...source_info,
  });
  if (!queryNew) {
    return url;
  }
  if (!queryOld) {
    return url + '?' + queryNew;
  }
  return url.replace(queryOld, queryNew);
};

export const buildStaticUrl = (path, query = {}) => {
  path = normalizeUrl(path, false, false);
  var PUBLIC_URL = process.env.PUBLIC_URL || '';
  if (_endsWith(PUBLIC_URL, '/')) {
    PUBLIC_URL = PUBLIC_URL.substr(0, PUBLIC_URL.length - 1);
  }
  return `${PUBLIC_URL}${path}`;
};

export const buildExternalSendoUrl = (pathname, search = {}) => {
  const hasProtocol = /^https?:\/\//.test(pathname);
  if (hasProtocol) {
    return pathname;
  }
  let url = process.env.APP_EXTERNAL_SENDO_URL;
  if (pathname && pathname[0] !== '/') {
    url += '/' + pathname;
  } else {
    url += pathname;
  }
  if (Object.keys(search).length) {
    url += '?' + qs.stringify(search);
  }
  return url;
};

export const getAPIUrl = pathname => {
  if (/^https?:\/\//.test(pathname)) {
    return pathname;
  }
  let apiUrl = getBaseUrl() + process.env.APP_API_URL;
  if (pathname[0] !== '/') {
    apiUrl += '/';
  }
  apiUrl += pathname;
  return apiUrl;
};

export const buildToFormData = params => {
  if (!window.FormData) {
    console.log('FormData does not supported');
    return params;
  }
  let form = new window.FormData();
  for (var name in params) {
    form.append(name, params[name]);
  }
  return form;
};

export const fetchJSON = (url, options = {}) => {
  /**
   |-----------------------------------------------------------
   | De dam bao tuong thich giua www.sendo.vn va m.sendo.vn
   | Va de khong bi loi:
   | https://stackoverflow.com/questions/21177387/caution-provisional-headers-are-shown-in-chrome-debugger?rq=1
   |-----------------------------------------------------------
   */
  options = _defaultsDeep(
    {
      credentials: 'include',
      headers: {
        'x-requested-with': 'XMLHttpRequest',
      },
    },
    options
  );
  if (options.disableXMLHttpRequest) {
    delete options.headers['x-requested-with'];
  }
  // Handle JSON format data
  const method = (options.method || '').toLowerCase();
  if (options.body && ['post', 'put'].indexOf(method) !== -1) {
    if (
      typeof options.body === 'object' &&
      !(options.body instanceof window.FormData)
    ) {
      console.log(options.body);
      options.body = JSON.stringify(options.body);
    }
    if (typeof options.body === 'string') {
      options.headers['Accept'] = 'application/json, text/plain, */*';
      options.headers['Content-Type'] = 'application/json';
    }
  }
  return fetch(getAPIUrl(url), options).then(res => {
    if (res.status >= 500) {
      return Promise.reject(res);
    }
    if (res.status >= 400) {
      throw new Error(res.text());
    }
    /**
     |-----------------------------------------------------------------------
     | Detect if user try to redirect by header
     |-----------------------------------------------------------------------
     */
    if (detectRedirect(res)) {
      const redirect = guessRedirectUrl(res);
      /**
       |-----------------------------------------------------------------------
       | Phai doi resolve roi moi redirect de tranh leak memory
       |-----------------------------------------------------------------------
       */
      if (redirect) {
        // 1. Redirect from TOP
        window.location.href = normalizeUrl(redirect);

        // Ham nay khong co y nghia o ung dung
        return Promise.reject(res);
        // Xem xet de bo luon
      }
    }
    return res.json().then(
      body => {
        const redirect = _get(body, 'result.meta_data.redirect');
        const statusCode = _get(body, 'status.code', STATUS_OK);
        if (redirect && isInArray(STATUS_REDIRECT, statusCode)) {
          // 2. Redirect from APP
          const hasProtocol = /^https?:\/\//.test(redirect);
          if (hasProtocol) {
            window.location.href = normalizeUrl(redirect, true);
            return Promise.reject(body);
          } else {
            const history = getInstanceHistory();
            history.replace(normalizeUrl(redirect));
            return Promise.reject(body);
          }
        }
        if (statusCode >= 400) {
          return Promise.reject(body);
        }
        return Promise.resolve(body);
      },
      error => {
        return Promise.reject(error);
      }
    );
  });
};

export const fetchWithToken = (url, options = {}) => {
  var jwt = Cookies.get(JWT_COOKIE_NAME);
  if (!jwt || jwt === 'undefined' || jwt === 'null') {
    jwt = '';
  }
  /**
   |----------------------------------------------------------------
   | Bo sung 'x-requested-with': 'XMLHttpRequest' de work voi ham
   | /general/login/getSession/
   |----------------------------------------------------------------
   */
  options = _defaultsDeep(
    {
      headers: {
        Authorization: jwt,
      },
    },
    options
  );
  return fetchJSON(url, options);
};

export const fetchCategorySlugFromUrl = (category_param = {}) => {
  let match = '';
  if (typeof category_param.cate1 !== 'undefined')
    match += category_param.cate1 + '/';
  if (typeof category_param.cate2 !== 'undefined')
    match += category_param.cate2;

  return match;
};

// https://stackoverflow.com/questions/1714786/query-string-encoding-of-a-javascript-object
export const serialize = (obj, prefix) => {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + '[' + p + ']' : p,
        v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v)
      );
    }
  }
  return str.join('&');
};
export const encodeUrl = (url = null) => {
  if (!url) return encodeURIComponent(_get(window, 'location.href'));
  return encodeURIComponent(url);
};

export const detectDirectUrlEvent = url_path => {
  return url_path.indexOf('su-kien/') > -1;
};

/**
 * @deprecated Should not use this function any more @phutp
 * @see Should use buildAPIUrl('tim-kiem')
 * @param {*} search
 */
export const buildSearchUrl = (search = {}) => {
  return normalizeUrl('tim-kiem?' + qs.stringify(search));
};

export const buildAPIUrl = (url, search = {}) => {
  let apiUrl = normalizeUrl(url, true, false);
  if (search && !_isEmpty(search)) {
    apiUrl += '?' + qs.stringify(search);
  }
  return apiUrl;
};

export const buildShopSearchLink = (query, shopAlias) => {
  return normalizeUrl(`shop/${shopAlias}/tim-kiem/?${qs.stringify(query)}`);
};

export const buildShopProductLink = (productUrl, shopAlias) => {
  if (!shopAlias) return productUrl;
  return `shop/${shopAlias}/${productUrl}`;
};

export const getShopAlias = shopUrl => {
  if (!shopUrl) return null;
  return shopUrl.replace('shop/', '');
};
