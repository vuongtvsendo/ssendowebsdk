import React, { Component } from 'react';
import PropTypes from 'prop-types';

function withFilterContext(Element) {
  class FilterContextCompnent extends Component {
    /**
     * @deprecated Can chuyen tu duy ve Redux
     */
    static contextTypes = {
      filters: PropTypes.object,
    };
    handleClick(value, searchKey, options = {}) {
      let filters = this.context.filters;
      if (this.isSelected(value, searchKey)) {
        if (typeof value !== 'object') {
          let r = new RegExp(`,${value}|^${value},?`);
          filters[searchKey] = filters[searchKey].replace(r, '');
          if (filters[searchKey].length === 0) {
            delete filters[searchKey];
          }
        } else {
          for (let prop in value) {
            if (value.hasOwnProperty(prop)) {
              delete filters[prop];
            }
          }
        }
      } else {
        if (typeof value !== 'object') {
          if (!filters[searchKey] || !filters[searchKey].length) {
            filters[searchKey] = [];
          } else {
            filters[searchKey] = filters[searchKey].split(/,\s+/);
          }
          filters[searchKey].push(value);
          filters[searchKey] = filters[searchKey].join(',');
        } else {
          for (let prop in value) {
            if (value.hasOwnProperty(prop)) {
              filters[prop] = value[prop];
            }
          }
        }
      }
      this.forceUpdate();
    }
    isSelected(value, searchKey) {
      let filters = this.context.filters;
      if (typeof value !== 'object') {
        // Regex de search cai value co xuat hien hay khong
        // 1. Bat dau la dau cai chuoi hay la dau ,
        // 2. Ket thuc la cuoi chuoi hoac la dau ,
        let r = new RegExp(`(^|,)${value}(,|$)`);
        return r.test(filters[searchKey] || '');
      } else {
        for (let prop in value) {
          if (!value.hasOwnProperty(prop)) {
            return false;
          }
          // eslint-disable-next-line
          if (filters[prop] != value[prop]) {
            return false;
          }
        }
      }
      return true;
    }
    setFilter(searchKey, value) {
      let filters = this.context.filters;
      filters[searchKey] = value;
      this.forceUpdate();
    }

    render() {
      return (
        <Element
          {...this.props}
          handleClickItem={this.handleClick.bind(this)}
          isSelectedItem={this.isSelected.bind(this)}
          setFilter={this.setFilter.bind(this)}
          ref="child"
        />
      );
    }
  }
  return FilterContextCompnent;
}

export default withFilterContext;
