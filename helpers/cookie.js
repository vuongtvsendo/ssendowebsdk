import Cookies from 'js-cookie';
import { getBaseDomain } from 'sendo-web-sdk/helpers/url';
import { MAX_COOKIE_EXPIRED } from 'sendo-web-sdk/helpers/const';

export const get = name => {
  return Cookies.get(name);
};

/**
 * @note by @phutp
 * 1. Co 2 gia tri cua options là: Secure & HttpOnly can dc can nhac cho nay.
 * 1.1 Tuy nhien neu ung dung da co the HttpOnly thi dau server se handle
 * 1.2 Chung ta chi can withCredentail option khi send API ve server la dc.
 * 2. Cho phep khong truyen domain vao thi su dung domain hien tai.
 * @param {String} name 
 * @param {String} value 
 * @param {Object} options 
 */
export const set = (name, value, options = {}) => {
  options = {
    domain: getBaseDomain(true),
    expires: MAX_COOKIE_EXPIRED,
    path: '/',
    ...options,
  };
  if( options.hasOwnProperty('domain') && !options.domain ) {
    delete options.domain;
  }
  return Cookies.set(name, value, options);
};

export const remove = (name, options = {}) => {
  options = {
    domain: getBaseDomain(true),
    path: '/',
    ...options,
  };
  if( options.hasOwnProperty('domain') && !options.domain ) {
    delete options.domain;
  }
  return Cookies.remove(name, options);
};
