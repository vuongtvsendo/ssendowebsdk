import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isInApp } from 'sendo-web-sdk/helpers/common';
import { normalizeUrl } from 'sendo-web-sdk/helpers/url';
export default class CustomLink extends Component {
  render() {
    const { isDirectLink, ...props } = this.props;
    if (isInApp() || isDirectLink) {
      return (
        <a href={normalizeUrl(props.to, true)} {...props}>
          {props.children}
        </a>
      );
    }
    return <Link {...props}>{props.children}</Link>;
  }
}
