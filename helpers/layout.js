import * as css from 'sendo-web-sdk/helpers/css';
export const handleInputForIphone = (state = 'focus') => {
  switch (state) {
    case 'blur':
      setTimeout(() => css.fixCssFormAfter(), 20);
      break;
    default:
      setTimeout(() => css.fixCssFormBefore(), 20);
      break;
  }
  return null;
};
