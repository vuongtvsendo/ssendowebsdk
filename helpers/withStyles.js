import React, { Component } from 'react';
import PropTypes from 'prop-types';
import hoistStatics from 'hoist-non-react-statics';

const contextTypes = {
  insertCss: PropTypes.func,
};

function withStyles(...styles) {
  return function wrapWithStyles(ComposedComponent) {
    // if (process.env.NODE_ENV === 'test' && process.env.BABEL_ENV === 'test') {
    //   return ComposedComponent;
    // }
    class WithStyles extends Component {
      componentWillMount() {
        if (
          process.env.BABEL_ENV === 'test' &&
          process.env.NODE_ENV === 'test'
        ) {
          return;
        }
        this.removeCss = this.context.insertCss.apply(undefined, styles);
      }

      componentWillUnmount() {
        if (process.env.NODE_ENV !== 'production') {
          setTimeout(this.removeCss, 0);
        }
      }

      render() {
        return <ComposedComponent {...this.props} />;
      }
    }

    const displayName =
      ComposedComponent.displayName || ComposedComponent.name || 'Component';

    WithStyles.displayName = `WithStyles(${displayName})`;
    WithStyles.contextTypes = contextTypes;
    WithStyles.ComposedComponent = ComposedComponent;

    return hoistStatics(WithStyles, ComposedComponent);
  };
}

export default withStyles;
