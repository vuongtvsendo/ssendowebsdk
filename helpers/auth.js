import store from '../store';
import _get from 'lodash/get';
import * as userAction from 'sendo-web-sdk/actions/user';
import { normalizeImgUrl } from 'sendo-web-sdk/helpers/url';
import avartarDefault from 'sendo-web-sdk/images/avartar-default.gif';

/**
 * Hien khong thay function nay dc su dung
 * @param {Function} f 
 */
export const loginWrapper = f => {
  const state = store.getState();
  const identity = _get(state, 'user.identity', {});
  if (!isLoggedIn(identity)) {
    userAction.login()(store.dispatch).then(f).catch(f);
    return;
  }
  f(identity);
};

export const isHasSessionInfo = identity => {
  identity = identity || {};
  return identity.session_id;
};

export const isLoggedIn = identity => {
  return !!getUserId(identity);
};

/**
 * Dung cho cac truong hop trong app
 * @param {Object} identity 
 */
export const getUserId = identity => {
  identity = identity || {};
  return identity.user_id || identity.customer_id || identity.fpt_id;
};
export const getFptId = identity => {
  identity = identity || {};
  return identity.fpt_id;
};

export const getFullname = identity => {
  identity = identity || {};
  const firstName = getFirstname(identity);
  const lastName = getLastname(identity);
  if (firstName && lastName) {
    return `${firstName} ${lastName}`;
  } else if (lastName) {
    return lastName;
  } else {
    return identity.fullname || identity.full_name;
  }
};

export const getFirstname = identity => {
  identity = identity || {};
  return identity.firstname || identity.first_name;
};

export const getLastname = identity => {
  identity = identity || {};
  return identity.lastname || identity.last_name;
};

export const getAvatar = identity => {
  identity = identity || {};
  var url = identity.avatar || identity.avatar_url;
  if (url) {
    url = normalizeImgUrl(url);
  } else {
    url = avartarDefault
  }
  return url;
};

/**
 * Dung cho R&D
 * @param {Object} identity 
 */
export const getTrackingUserId = identity => {
  identity = identity || {};
  return identity.fpt_id;
};
