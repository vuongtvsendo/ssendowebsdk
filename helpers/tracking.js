import emitter, { PAGE_VIEW } from 'sendo-web-sdk/helpers/emitter';

export const emitPageView = (context, updateSession = false) => {
  if (!updateSession) {
    emitter.saveSession({
      context: context,
      requestInfo: {},
    });
  }
  emitter.emit(PAGE_VIEW);
};
