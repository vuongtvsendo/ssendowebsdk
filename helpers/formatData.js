import _get from 'lodash/get';
import _values from 'lodash/values';
import qs from 'query-string';
import * as Cookies from 'sendo-web-sdk/helpers/cookie';
import {
  LISTING_PRODUCT_PER_PAGE,
  SESSION_CONTEXT_CATEGORIES,
  SENDO_PLATFORM,
} from 'sendo-web-sdk/helpers/const';
import emitter from 'sendo-web-sdk/helpers/emitter';
/**
   |---------------------------------------------------------------
   | ! attrsSectected
   | 1. tham số nhận vào là object lúc select attributes của product
   |    => function sẽ trả về format của options mà bên API cần
   |--------------------------------------------------------------- 
   */
export const formatOptionsForCheckout = attrsSectected => {
  let options = {};
  for (var attr in attrsSectected) {
    let groupOption = _get(attrsSectected, `[${attr}].groupOption`, {});
    let selected = _get(attrsSectected, `[${attr}].selected`, {});
    options[groupOption.product_option] = [_get(selected, 'product_option_id')];
  }
  return options;
};

export const paramsForCheckoutLink = (params = {}) => {
  return {
    sendo_platform: SENDO_PLATFORM,
    ...params,
  };
};

export const buildSourceInfoString = product => {
  if (emitter.getSession().context !== SESSION_CONTEXT_CATEGORIES) {
    return '';
  }
  let source_info = {
    sendo_platform: SENDO_PLATFORM,
    num_result_per_page: LISTING_PRODUCT_PER_PAGE,
    session_key: emitter.getSession().session_key,
    user_id: Cookies.get('tracking_id') || '',
    experiment_id: _get(product, 'productMetadata.experiment_id', 0),
    listing_algo: Cookies.get('listing_algo') || 'default',
    special_res: _get(product, 'productMetadata.special_res', 0),
    result_position: _get(product, 'index', 0),
    product_type: _get(product, 'product_type', 2),
  };
  return _values(source_info).join('_');
};

export const getShopAdsParams = (location, hasShopId = false) => {
  const search = qs.parse(_get(location, 'search', {}));
  let shopAds = {};
  shopAds.shop_ads = search.shop_ads;
  if (hasShopId) {
    shopAds.admin_shop_ads = search.admin_shop_ads;
  }
  return shopAds;
};
