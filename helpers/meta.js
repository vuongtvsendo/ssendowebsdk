import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import _isArray from 'lodash/isArray';
import { FB_APP_ID } from 'sendo-web-sdk/helpers/const';
import { buildStaticUrl, normalizeUrl } from 'sendo-web-sdk/helpers/url';

export default class Meta extends PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  };
  getTitle(isOg) {
    if (isOg && this.props.OgTitle) {
      return this.props.OgTitle;
    }
    return this.props.title || _get(this.props, 'product.name', '');
  }
  getDescription(isOg) {
    if (isOg && this.props.ogDescription) {
      return this.props.ogDescription;
    }
    return (
      this.props.description ||
      _get(this.props, 'product.short_description', '')
    );
  }
  renderOgImage() {
    if (_isArray(this.props.image) && this.props.image.length > 0) {
      return this.props.image.map((image, i) => {
        return <meta key={i} property="og:image" content={image} />;
      });
    }
    if (this.props.image) {
      return <meta property="og:image" content={this.props.image} />;
    }
    return (
      <meta property="og:image" content={_get(this.props, 'product.image')} />
    );
  }
  getKeywords() {
    return this.props.keywords;
  }
  getIndexFollow() {
    let ret = [];
    if (this.props.index === false) {
      ret.push('NOINDEX');
    } else {
      ret.push('INDEX');
    }
    if (this.props.follow === false) {
      ret.push('NOFOLLOW');
    } else {
      ret.push('FOLLOW');
    }
    return ret.join(',');
  }
  getBreadcrumbSchema() {
    if (!this.props.breadcrumb) {
      return;
    }
    let breadcrumb = {
      '@context': 'http://schema.org',
      '@type': 'BreadcrumbList',
    };
    let itemListElement = [
      {
        '@type': 'ListItem',
        position: 1,
        item: {
          '@id': this.getBaseUrl(),
          name: 'Trang chủ',
        },
      },
    ];
    if (this.props.breadcrumb instanceof Array) {
      for (let i = 0; i < this.props.breadcrumb.length; i++) {
        let item = this.props.breadcrumb[i];
        itemListElement.push({
          '@type': 'ListItem',
          position: i + 2,
          item: {
            '@id': this.getBaseUrl() + '/' + item.id,
            name: item.name,
          },
        });
      }
    }
    breadcrumb.itemListElement = itemListElement;
    return breadcrumb;
  }
  getProductSchema() {
    if (!this.props.product) {
      return;
    }
    const product = this.props.product;
    let schema = {
      '@context': 'http://schema.org/',
      '@type': 'Product',
      name: product.name,
      image: [product.image],
      description: product.short_description,
      sku: product.sku,
      brand: {
        '@type': 'Thing',
        name: _get(product, 'brand_info.name', 'N/A'),
      },
      aggregateRating: {
        '@type': 'AggregateRating',
        ratingValue: _get(product, 'rating_info.percent_star', 0),
        reviewCount: _get(product, 'rating_info.total_rated', 0),
      },
      offers: {
        '@type': 'Offer',
        priceCurrency: 'VND',
        price: product.price,
        seller: {
          '@type': 'Organization',
          name: _get(product, 'shop_info.shop_name', 'N/A'),
        },
      },
    };
    return schema;
  }
  getVideo(mediaList) {
    for (let media of mediaList) {
      if (media.type === 'video') {
        return media;
      }
    }
    return null;
  }
  getVideoSchema() {
    const { product } = this.props;
    const video = this.getVideo(_get(product, 'media', []));
    if (!product || video) {
      return;
    }
    let schema = {
      '@context': 'http://schema.org',
      '@type': 'VideoObject',
      name: _get(product, 'name', ''),
      description: _get(product, 'short_description', ''),
      thumbnailUrl: _get(video, 'video_thumb', ''),
      uploadDate: 'N/A',
      duration: 'N/A',
      publisher: {
        '@type': 'Organization',
        name: _get(product, 'shop_info.shop_name', ''),
        logo: {
          '@type': 'ImageObject',
          url: _get(product, 'shop_info.shop_logo', ''),
          width: 100,
          height: 100,
        },
      },
      contentUrl: _get(video, 'video_url', ''),
      embedUrl: _get(video, 'video_url', ''),
      interactionCount: 'N/A',
    };
    return schema;
  }
  getCanonicalUrl() {
    if (this.props.urlCanonical) {
      return normalizeUrl(this.props.urlCanonical, true);
    }
    let currentUrl = _get(this.context, 'router.route.location.pathname');
    return normalizeUrl(currentUrl, true);
  }
  getBaseUrl() {
    /**
     |----------------------------------------------------------
     | 1. Do ca 2 dc xu ly o server side va client side
     | Nen xu dung gia tri nay phai het suc than trong
     | Chi co 2 props hien tai dc support la: protocal va host (include: port)
     | 2. Should move to helper
     |----------------------------------------------------------
     */
    return window.location.protocol + '//' + window.location.host;
  }
  render() {
    const breadcrumbSchema = this.getBreadcrumbSchema();
    const productSchema = this.getProductSchema();
    const videoSchema = this.getVideoSchema();
    return (
      <Helmet>
        {/**
          |---------------------------------------------------------------------
          | General
          |---------------------------------------------------------------------
        */}
        <link rel="dns-prefetch" href={this.getBaseUrl()} />
        <meta charset="UTF-8" />
        <meta http-equiv="content-language" content="vi" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1.0,minimum-scale=1, maximum-scale=1, user-scalable=no"
        />
        <title>{this.props.title}</title>
        <meta name="description" content={this.getDescription()} />
        {/**
          |---------------------------------------------------------------------
          | Nhung meta cua mobile se dc detect dua tren agent de render cho hop l
          |---------------------------------------------------------------------
        */}
        <meta name="theme-color" content="#e5101d" />
        <meta name="msapplication-navbutton-color" content="#e5101d" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#e5101d" />
        {/**
          |---------------------------------------------------------------------
          | Mobile App - Android
          |---------------------------------------------------------------------
        */}
        {/*
        <meta
          property="al:android:url"
          content="android-app://com.sendo/sendo/sendo.vn/"
        />
        <meta property="al:android:package" content="com.sendo" />
        <meta property="al:android:app_name" content="Sendo.vn App" />
        <link rel="alternate" href="android-app://com.sendo/sendo/sendo.vn/" />
        */}
        {/**
          |---------------------------------------------------------------------
          | Mobile App - IOs
          |---------------------------------------------------------------------
        */}
        {/*
        <meta
          property="al:ios:url"
          content="ios-app://940313804/sendo/sendo.vn/"
        />
        <meta property="al:ios:app_store_id" content="940313804" />
        <meta property="al:ios:app_name" content="Sendo.vn App" />
        <link rel="alternate" href="ios-app://940313804/www.sendo.vn/" />
        */}
        {/**
          |---------------------------------------------------------------------
          | SEO 
          |---------------------------------------------------------------------
        */}
        <meta name="robots" content={this.getIndexFollow()} />
        <meta name="revisit" content="1 days" />
        <meta name="geo.placename" content="Vietnamese" />
        <meta name="geo.region" content="VN" />
        <meta name="dc.creator" content="Sendo" />
        <meta name="generator" content="Sendo.vn" />
        <meta name="tt_category_id" content="1002592" />
        <meta name="tt_article_id" content="1002592" />
        <meta name="msvalidate.01" content="43C7783BB799E592C63714B2FADC730F" />
        {/**
          |---------------------------------------------------------------------
          | Open Graph - Facebook Share
          |---------------------------------------------------------------------
        */}
        <meta property="fb:app_id" content={FB_APP_ID} />
        <meta property="og:url" content={this.getCanonicalUrl()} />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={this.getTitle(true)} />
        <meta property="og:description" content={this.getDescription(true)} />
        {this.renderOgImage()}
        <meta property="og:image:width" content="640" />
        <meta property="og:image:height" content="442" />

        <meta property="keywords" content={this.getKeywords()} />
        {/**
          |---------------------------------------------------------------------
          | Specifying a Webpage Icon for Web Clip
          |---------------------------------------------------------------------
        */}
        <link
          rel="apple-touch-icon"
          href={buildStaticUrl('/icons/apple-touch-icon-152x152.png')}
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={buildStaticUrl('/icons/touch-icon-iphone-retina-180x180.png')}
        />
        {/* Specifying a Launch Screen Image */}
        <link
          rel="apple-touch-startup-image"
          href={buildStaticUrl('/icons/icon-large-512x512.png')}
        />
        {/* Adding a Launch Icon Title */}
        <meta name="apple-mobile-web-app-title" content="Sendo.vn" />
        {/* Hiding Safari User Interface Components */}
        <meta name="apple-mobile-web-app-capable" content="yes" />

        {/**
          |---------------------------------------------------------------------
          | Others
          |---------------------------------------------------------------------
        */}
        <link rel="canonical" href={this.getCanonicalUrl()} />
        <link rel="alternate" hreflang="x-default" href={this.getBaseUrl()} />
        <link rel="alternate" hreflang="vi" href={this.getBaseUrl()} />
        <link
          href="https://plus.google.com/+SendoVnOfficial/"
          rel="publisher"
        />
        <link
          href="https://plus.google.com/+SendoVnOfficial/posts"
          rel="author"
        />
        {/**
          |---------------------------------------------------------------------
          | JSON-LD
          |---------------------------------------------------------------------
        */}
        {breadcrumbSchema &&
          <script type="application/ld+json">
            {JSON.stringify(breadcrumbSchema)}
          </script>}
        {productSchema &&
          <script type="application/ld+json">
            {JSON.stringify(productSchema)}
          </script>}
        {videoSchema &&
          <script type="application/ld+json">
            {JSON.stringify(videoSchema)}
          </script>}
        {/**
          |---------------------------------------------------------------------
          | Override by child components
          |---------------------------------------------------------------------
        */}
        {this.props.children}
      </Helmet>
    );
  }
}
