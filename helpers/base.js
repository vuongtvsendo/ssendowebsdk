import UAParser from 'ua-parser-js';
import _get from 'lodash/get';
/**
 |-------------------------------------------------------
 | Chu y day la nhung function quan trong.
 |------------------------------------------------------- 
 */

export const getLocation = () => {
  return window.location;
};

export const getDeviceInfo = () => {
  if (!_get(window, 'deviceInfo')) {
    const parser = new UAParser();
    window.deviceInfo = parser.getResult();
  }
  return window.deviceInfo;
};

export const importScriptOnce = (src, id, opts = {}) => {
  opts = {
    async: true,
    ...opts,
  };
  return new Promise(resolve => {
    var d = document;
    if (d.getElementById(id)) resolve();
    var s, t;
    s = document.createElement('script');
    for (var k in opts) {
      s[k] = opts[k];
    }
    s.type = 'text/javascript';
    s.src = src;
    s.onload = resolve;
    s.onerror = resolve;
    t = document.getElementsByTagName('script')[0];
    t.parentNode.insertBefore(s, t);
  });
};
