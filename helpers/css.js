export const fixCssFormBefore = () => {
  document.body.classList.add('fixed');
};

export const fixCssFormAfter = () => {
  document.body.classList.remove('fixed');
};
