import React, { Component } from 'react';

function withScrollEvent(Element) {
  class ScrollEventCompnent extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleScroll = this.handleScroll.bind(this);
    }
    lastScrollTop = 0;
    isScrollDown = false;
    iCounter = 0;
    componentDidMount() {
      window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll(evt) {
      let counter = ++this.iCounter;
      if (!this.debouncing) {
        window.requestAnimationFrame(() => {
          this.debouncing = false;
          var ref = this.refs.child;
          if (ref && ref.onScroll instanceof Function) {
            if (ref.onScroll(evt) === false) {
              return;
            }
          }
          /**
           |-------------------------------------------
           | Optimize for mobile
           |-------------------------------------------
           */
          if (counter < this.iCounter) {
            return;
          }
          this._handleScroll(evt);
        });
      }
      this.debouncing = true;
    }
    _handleScroll(evt) {
      var scrollTop = window.scrollY;
      let isDown = false;
      if (scrollTop > this.lastScrollTop) {
        isDown = true;
      }
      this.lastScrollTop = scrollTop;
      if (this.isScrollDown === isDown) {
        return;
      }
      this.isScrollDown = isDown;
      var f = this.isScrollDown ? 'onScrollDown' : 'onScrollUp';
      var ref = this.refs.child;
      if (!ref || !(ref[f] instanceof Function)) {
        return;
      }
      ref[f](evt);
    }
    render() {
      return <Element {...this.props} ref="child" />;
    }
  }
  return ScrollEventCompnent;
}
export default withScrollEvent;
