import createBrowserHistory from 'history/createBrowserHistory';
import createMemoryHistory from 'history/createMemoryHistory';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import { isSendoUrl } from 'sendo-web-sdk/helpers/url';

var history;

export const getInstance = () => {
  if (history) {
    return history;
  }
  if (__SERVER__) {
    history = createMemoryHistory();
  } else {
    history = createBrowserHistory();
  }
  return history;
};

export const isHistoryEmpty = (historyLength = 0) => {
  let windowHistory = _get(window, 'history', {});
  if (_isEmpty(windowHistory.state)) {
    let referrer = window.document.referrer;
    if (referrer && isSendoUrl(referrer)) {
      return false;
    }
  }
  if (historyLength === 0) {
    return true;
  }
  return false;
};
