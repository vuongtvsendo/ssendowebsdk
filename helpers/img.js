import defaultImg from 'sendo-web-sdk/images/default.png';

export const onImgError = (event, imgError) => {
  if (event.target.nodeName === 'IMG') {
    event.target.src = imgError || defaultImg;
  }
};
