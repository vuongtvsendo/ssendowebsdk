export const showSmartBanner = banner => {
  return {
    type: 'SHOW_SMARTBANNER',
    payload: banner,
  };
};

export const closeSmartBanner = () => {
  return {
    type: 'CLOSE_SMARTBANNER',
  };
};
