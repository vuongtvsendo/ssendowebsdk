import * as api from 'sendo-web-sdk/utils/apis/cmscontent';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';

export const getCmsContentStaticPage = contentKey => {
  return makePromiseAction(
    'LOAD_CMS_CONTENT_STATIC',
    api.getCmsContentStaticPage(contentKey),
    data => ({
      data: data.result.data,
    })
  );
};
