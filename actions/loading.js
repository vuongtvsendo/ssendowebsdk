export const openPageLoading = () => {
  return {
    type: 'OPEN_LOADING',
  };
};

export const closePageLoading = () => {
  return {
    type: 'CLOSE_LOADING',
  };
};
