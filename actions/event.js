import * as api from 'sendo-web-sdk/utils/apis/event';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import _get from 'lodash/get';
import _omit from 'lodash/omit';

export const getEvent = params => {
  return makePromiseAction('LOAD_EVENT', api.getEvent(params), data => ({
    metadata: data.result.meta_data,
    event_active: _get(data.result, 'data.active.data', []),
    event_deactive: _get(data.result, 'data.deactive.data', []),
  }));
};

export const loadMoreEvent = params => {
  return makePromiseAction('LOAD_MORE_EVENT', api.getEvent(params), data => ({
    metadata: data.result.meta_data,
    event_deactive: _get(data.result, 'data.deactive.data', []),
  }));
};

export const getEventDetail = params => {
  return makePromiseAction(
    'LOAD_EVENT_DETAIL',
    api.getEventDetail(params),
    data => ({
      metadata: {
        ..._get(data.result, 'meta_data', {}),
        ..._omit(_get(data.result, 'data', {}), 'list_cabinet'),
      },
      cabinets: _get(data.result, 'data.list_cabinet', []),
    })
  );
};

export const getEventDetailFullBasic = params => {
  return makePromiseAction(
    'LOAD_EVENT_DETAIL_FULL_BASIC',
    api.getEventDetailFullBasic(params),
    data => ({
      metadata: {
        ..._get(data.result, 'meta_data', {}),
        ..._get(data.result, 'data', {}),
      },
    })
  );
};

export const getEventDetailFullProducts = params => {
  return makePromiseAction(
    'LOAD_EVENT_DETAIL_FULL_PRODUCTS',
    api.getEventDetailFullProducts(params),
    data => ({
      products: _get(data.result, 'data', []),
    })
  );
};

export const loadMoreEventDetailFullProducts = params => {
  return makePromiseAction(
    'LOAD_MORE_EVENT_DETAIL_FULL_PRODUCTS',
    api.getEventDetailFullProducts(params),
    data => ({
      products: _get(data.result, 'data', []),
    })
  );
};

export const clearEvents = () => {
  return {
    type: 'CLEAR_EVENT',
    payload: {},
  };
};

export const clearEventDetail = () => {
  return {
    type: 'CLEAR_EVENT_DETAIL',
    payload: {},
  };
};

export const clearEventDetailFullProducts = () => {
  return {
    type: 'CLEAR_EVENT_DETAIL_FULL_PRODUCTS',
    payload: {},
  };
};

export const clearEventPageState = () => {
  return {
    type: 'CLEAR_EVENT_PAGE_STATE',
    payload: {},
  };
};
