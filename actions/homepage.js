import * as homepage from 'sendo-web-sdk/utils/homepageApi';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';

export const loadBasicHomepage = () => {
  return makePromiseAction(
    'LOAD_HOMEPAGE',
    homepage.loadBasicHomepage(),
    data => ({
      homepage: data.result,
    })
  );
};
