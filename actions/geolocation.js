import * as api from 'sendo-web-sdk/utils/api';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';

export const getCityByLatlng = latlng => {
  return makePromiseAction(
    'GET_CITY_BY_LATLNG',
    api.getCityByLatlng(latlng),
    data => ({
      location: data.result.data,
    })
  );
};

export const getAllCity = () => {
  return makePromiseAction('GET_All_CITY', api.getAllCity(), data => ({
    allCity: data.result.data,
  }));
};
