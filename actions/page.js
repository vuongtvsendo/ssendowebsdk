export const storeSubCatePage = pageState => {
  return {
    type: 'STORE_SUB_CATE_PAGE',
    payload: pageState,
  };
};

export const storeTrendingPage = pageState => {
  return {
    type: 'STORE_TRENDING_PAGE',
    payload: pageState,
  };
};

export const storeEventPage = pageState => {
  return {
    type: 'STORE_EVENT_PAGE',
    payload: pageState,
  };
};

export const storeEventDetailPage = pageState => {
  return {
    type: 'STORE_EVENT_DETAIL_PAGE',
    payload: pageState,
  };
};
export const storeEventDetailFullPage = pageState => {
  return {
    type: 'STORE_EVENT_DETAIL_FULL_PAGE',
    payload: pageState,
  };
};
export const storeSearchPage = pageState => {
  return {
    type: 'STORE_SEARCH_PAGE',
    payload: pageState,
  };
};
export const storeHomePage = pageState => {
  return {
    type: 'STORE_HOME_PAGE',
    payload: pageState,
  };
};

export const storeShopPage = pageState => {
  return {
    type: 'STORE_SHOP_PAGE',
    payload: pageState,
  };
};

export const storeShopCommentPage = pageState => {
  return {
    type: 'STORE_SHOP_COMMENT_PAGE',
    payload: pageState,
  };
};

export const storeShopRatingPage = pageState => {
  return {
    type: 'STORE_SHOP_RATING_PAGE',
    payload: pageState,
  };
};
