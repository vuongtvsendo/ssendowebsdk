import * as api from '../utils/apis/shop';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import _get from 'lodash/get';

export const getShopDetail = shop_alias => {
  return (dispatch, getState) => {
    const state = getState();
    if (_get(state, 'shop.shopInfo.shop_alias') === shop_alias) {
      return Promise.resolve();
    }
    return makePromiseAction(
      'LOAD_SHOP_DETAIL',
      api.getShopDetail(shop_alias),
      data => ({
        shopDetail: data,
      })
    )(dispatch);
  };
};

export const getShopProducts = (shop_alias, params, isLoadMore) => {
  if (isLoadMore) {
    return makePromiseAction(
      'LOAD_SHOP_PRODUCTS_MORE',
      api.getShopProducts(shop_alias, params),
      data => ({
        products: data,
      })
    );
  }
  return makePromiseAction(
    'LOAD_SHOP_PRODUCTS',
    api.getShopProducts(shop_alias, params),
    data => ({
      products: data,
    })
  );
};

export const postShopComment = param => {
  return makePromiseAction(
    'POST_SHOP_COMMENT',
    api.postShopComment(param),
    data => ({
      comment: data.result.data,
    })
  );
};

export const deleteShopComment = param => {
  return makePromiseAction(
    'DELETE_SHOP_COMMENT',
    api.deleteShopComment(param),
    data => ({
      comment: data.result.data,
    })
  );
};

export const getShopCategories = shop_alias => {
  return makePromiseAction(
    'LOAD_SHOP_CATEGORIES',
    api.getShopCategories(shop_alias),
    data => ({
      categories: data,
    })
  );
};

export const clearShopDetail = () => {
  return {
    type: 'CLEAR_SHOP_DETAIL',
    payload: {},
  };
};

export const clearShopProducts = () => {
  return {
    type: 'CLEAR_SHOP_PRODUCTS',
    payload: {},
  };
};

export const clearShopComments = () => {
  return {
    type: 'CLEAR_SHOP_COMMENTS',
    payload: {},
  };
};

export const clearShopRatings = () => {
  return {
    type: 'CLEAR_SHOP_RATINGS',
    payload: {},
  };
};

export const getShopComments = (shop_alias, params, isLoadMore) => {
  if (isLoadMore) {
    return makePromiseAction(
      'LOAD_SHOP_COMMENTS_MORE',
      api.getShopComments(shop_alias, params),
      data => ({
        shopComments: data,
      })
    );
  }
  return makePromiseAction(
    'LOAD_SHOP_COMMENTS',
    api.getShopComments(shop_alias, params),
    data => ({
      shopComments: data,
    })
  );
};

export const getShopRatings = (shop_alias, params, isLoadMore) => {
  if (isLoadMore) {
    return makePromiseAction(
      'LOAD_SHOP_RATINGS_MORE',
      api.getShopRatings(shop_alias, params),
      data => ({
        shopRating: data,
      })
    );
  }
  return makePromiseAction(
    'LOAD_SHOP_RATINGS',
    api.getShopRatings(shop_alias, params),
    data => ({
      shopRating: data,
    })
  );
};

export const checkLike = shopId => {
  return makePromiseAction(
    'CHECKLIKE_SHOP',
    api.checkLikeShop(shopId),
    data => ({
      isLike: data,
    })
  );
};

export const toogleLikeShop = (shopId, isLike) => {
  if (isLike) {
    return makePromiseAction('LIKE_SHOP', api.likeShop(shopId), data => ({
      isLike: data,
    }));
  }
  return makePromiseAction('UNLIKE_SHOP', api.unlikeShop(shopId), data => ({
    isLike: data,
  }));
};

export const setNewShopComment = comment => {
  return {
    type: 'SET_NEW_SHOP_COMMENT',
    payload: comment,
  };
};

export const setDeleteShopComment = comment => {
  return {
    type: 'SET_DELETE_SHOP_COMMENT',
    payload: comment,
  };
};
