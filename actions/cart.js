import * as cartApi from 'sendo-web-sdk/utils/apis/cart';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';

export const addToCart = cart => {
  return makePromiseAction('ADD_TO_CART', cartApi.addToCart(cart), data => ({
    ...data.result.data,
  }));
};

export const updateCart = cart => {
  return makePromiseAction(
    'LOAD_DATA_CART',
    cartApi.updateCart(cart),
    data => ({
      ...data,
    })
  );
};

export const deleteCart = cart => {
  return makePromiseAction(
    'LOAD_DATA_CART',
    cartApi.deleteCart(cart),
    data => ({
      ...data,
    })
  );
};

export const loadDataCart = () => {
  return makePromiseAction('LOAD_DATA_CART', cartApi.loadDataCart(), data => ({
    ...data,
  }));
};

export const loadTotalCart = () => {
  return makePromiseAction(
    'LOAD_TOTAL_CART',
    cartApi.loadTotalCart(),
    data => ({
      ...data.result.data,
    })
  );
};
