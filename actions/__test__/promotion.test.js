import { getPromotionBasicInfo } from '../promotion';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] promotion', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_PROMOTION_BASIC_INFO has been done', () => {
    const res = {
      result: {
        data: {
          list_categories: [],
        },
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_PROMOTION_BASIC_INFO' },
      {
        type: 'LOAD_PROMOTION_BASIC_INFO_SUCCESS',
        payload: {
          metadata: res.result.meta_data,
          ...res.result,
          sort_path: 'thoi-trang-nu/dam-vay',
        },
      },
    ];
    const store = mockStore({});

    return store
      .dispatch(
        getPromotionBasicInfo({
          category_param: {
            cate1: 'thoi-trang-nu',
            cate2: 'dam-vay',
          },
          sort_path: '',
        })
      )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
