import { setOnlineStatus } from '../firer';
import { SET_ONLINE_STATUS } from 'sendo-web-sdk/actions/const';

describe('[Action] firer', () => {
  it('creates SET_ONLINE_STATUS has been done', () => {
    const expectedActions = {
      type: SET_ONLINE_STATUS,
      payload: {},
    };
    expect(setOnlineStatus({})).toEqual(expectedActions);
  });
});
