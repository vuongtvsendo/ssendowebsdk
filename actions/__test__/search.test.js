import { getSearchSuggestions, getSearchInfo } from '../search';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] search', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates GET_SEARCH_SUGGESTIONS has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'GET_SEARCH_SUGGESTIONS',
      },
      {
        type: 'GET_SEARCH_SUGGESTIONS_SUCCESS',
        payload: {
          suggestions: res.result.data,
          query: 'query',
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getSearchSuggestions({ q: 'query' })).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('creates GET_SEARCH_INFO has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'GET_SEARCH_INFO',
      },
      {
        type: 'GET_SEARCH_INFO_SUCCESS',
        payload: {
          sortList: res.result.data.sort_list,
          metadata: res.result.meta_data,
          relate_keywords: [],
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getSearchInfo({ q: 'query' })).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
