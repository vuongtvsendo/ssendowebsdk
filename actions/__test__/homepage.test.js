import { loadBasicHomepage } from '../homepage';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] homepage', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_HOMEPAGE has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'LOAD_HOMEPAGE',
      },
      {
        type: 'LOAD_HOMEPAGE_SUCCESS',
        payload: {
          homepage: res.result,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(loadBasicHomepage({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
