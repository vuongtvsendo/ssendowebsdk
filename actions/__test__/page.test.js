import { storeSubCatePage } from '../page';

describe('[Action] page', () => {
  it('creates STORE_SUB_CATE_PAGE has been done', () => {
    const expectedActions = {
      type: 'STORE_SUB_CATE_PAGE',
      payload: {},
    };
    expect(storeSubCatePage({})).toEqual(expectedActions);
  });
});
