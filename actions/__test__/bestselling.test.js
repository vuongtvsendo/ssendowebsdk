import { getBestSellingBasicInfo } from '../bestselling';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] bestselling', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_BESTSELLING_BASIC_INFO has been done', () => {
    const res = {
      result: {
        data: {
          list_categories: [],
        },
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_BESTSELLING_BASIC_INFO' },
      {
        type: 'LOAD_BESTSELLING_BASIC_INFO_SUCCESS',
        payload: {
          metadata: res.result.meta_data,
          ...res.result,
          sort_path: 'thoi-trang-nu/dam-vay',
        },
      },
    ];
    const store = mockStore({});

    return store
      .dispatch(
        getBestSellingBasicInfo({
          category_param: {
            cate1: 'thoi-trang-nu',
            cate2: 'dam-vay',
          },
        })
      )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
