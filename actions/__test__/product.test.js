import { load } from '../product';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] product', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_PRODUCTS has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'LOAD_PRODUCTS',
      },
      {
        type: 'LOAD_PRODUCTS_SUCCESS',
        payload: {
          products: res.result.data,
          metadata: res.result.meta_data,
          categoryMetadata: {},
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(load({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
