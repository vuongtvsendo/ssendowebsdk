import { login } from '../user';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] user', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates USER_LOGIN has been done', () => {
    const res = {
      result: {
        data: {},
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'USER_LOGIN',
      },
      {
        type: 'USER_LOGIN_SUCCESS',
        payload: res,
      },
    ];
    const store = mockStore({});

    return store.dispatch(login()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
