import { getTrendingHome } from '../trending';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] trending', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_TRENDING_HOME has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_TRENDING_HOME' },
      {
        type: 'LOAD_TRENDING_HOME_SUCCESS',
        payload: {
          metadata: res.result.meta_data,
          ...res.result,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getTrendingHome({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
