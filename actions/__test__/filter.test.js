import { load } from '../filter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] filter', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates FILTER_LOAD has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'FILTER_LOAD',
        payload: {
          filters: res.result.data,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(load({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
