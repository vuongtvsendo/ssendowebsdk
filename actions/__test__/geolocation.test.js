import { getCityByLatlng } from '../geolocation';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] geolocation', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates GET_CITY_BY_LATLNG has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'GET_CITY_BY_LATLNG',
      },
      {
        type: 'GET_CITY_BY_LATLNG_SUCCESS',
        payload: {
          location: res.result.data,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getCityByLatlng({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
