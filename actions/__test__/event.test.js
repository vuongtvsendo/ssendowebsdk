import {
  getEvent,
  getEventDetail,
  getEventDetailFullBasic,
  getEventDetailFullProducts,
} from '../event';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] trending', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_EVENT has been done', () => {
    const res = {
      result: {
        data: {
          active: {
            data: [
              {
                id: '5a615ef66cf941abd542d72a',
                avatar: '',
                campaign_url: '/su-kien/giam-sap-san-tet-hoanh-trang/',
                banner_url: 'https://media3.scdn.vn/img2/2018/1_19/rFB1ED.png',
                name: 'Giảm sập sàn - Tết hoành tráng',
                description:
                  'Sen Đỏ dành tặng quý khách hàng chương trình Giảm sập sàn – Tết hoành tráng với mã giảm giá để mua điện thoại, tivi, máy lạnh, máy giặt… với giá cực sốc.',
                type: 1,
                url: '',
                start_date: '19/01/2018',
                end_date: '30/01/2018',
                is_active: true,
              },
            ],
          },
          deactive: {
            data: [
              {
                id: '5a583c0e6cf941abd52913bf',
                avatar: '',
                campaign_url: '/event/thoi-trang-nam/',
                banner_url: 'https://media3.scdn.vn/img2/2018/1_12/xo6td8.png',
                name: 'Thời trang nam',
                description: '',
                type: 1,
                url: '',
                start_date: '12/01/2018',
                end_date: '17/01/2018',
                is_active: true,
              },
            ],
          },
        },
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_EVENT' },
      {
        type: 'LOAD_EVENT_SUCCESS',
        payload: {
          metadata: res.result.meta_data,
          event_active: res.result.data.active.data,
          event_deactive: res.result.data.deactive.data,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getEvent({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates LOAD_EVENT_DETAIL has been done', () => {
    const res = {
      result: {
        data: {
          banner_url: '',
          campaign_id: 123,
          list_cabinet: [],
        },
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_EVENT_DETAIL' },
      {
        type: 'LOAD_EVENT_DETAIL_SUCCESS',
        payload: {
          metadata: {
            banner_url: '',
            campaign_id: 123,
          },
          cabinets: [],
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getEventDetail({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates LOAD_EVENT_DETAIL_FULL_BASIC has been done', () => {
    const res = {
      result: {
        data: {},
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_EVENT_DETAIL_FULL_BASIC' },
      {
        type: 'LOAD_EVENT_DETAIL_FULL_BASIC_SUCCESS',
        payload: {
          metadata: {},
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getEventDetailFullBasic({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates LOAD_EVENT_DETAIL_FULL_PRODUCTS has been done', () => {
    const res = {
      result: {
        data: [],
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_EVENT_DETAIL_FULL_PRODUCTS' },
      {
        type: 'LOAD_EVENT_DETAIL_FULL_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(getEventDetailFullProducts({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
