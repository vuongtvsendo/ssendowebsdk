import { getCmsContentStaticPage } from '../cmscontent';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';

const mockStore = configureMockStore([thunk]);

describe('[Action] cmscontent', () => {
  afterEach(() => {
    global.fetch.restore();
  });
  it('LOAD_CMS_CONTENT_STATIC has been done', () => {
    const res = {
      result: {
        data: {},
        meta_data: [],
      },
    };
    stubFetch(res);
    const expectedActions = [
      {
        type: 'LOAD_CMS_CONTENT_STATIC',
      },
      {
        type: 'LOAD_CMS_CONTENT_STATIC_SUCCESS',
        payload: {
          data: res.result.data,
        },
      },
    ];

    const store = mockStore({});
    return store.dispatch(getCmsContentStaticPage({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
