import { loadDataCart } from '../cart';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] cart', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_DATA_CART has been done', () => {
    const res = {
      result: {
        data: {},
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_DATA_CART' },
      {
        type: 'LOAD_DATA_CART_SUCCESS',
        payload: {
          ...res,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(loadDataCart({})).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
