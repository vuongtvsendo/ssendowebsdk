import { load } from '../category';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { stubFetch } from 'sendo-web-sdk/utils/testing';
const mockStore = configureMockStore([thunk]);

describe('[Action] category', () => {
  afterEach(() => {
    global.fetch.restore();
  });

  it('creates LOAD_CATEGORIES has been done', () => {
    const res = {
      result: {
        data: {
          sub_category: [],
          sort_list: {},
          seo_footer: {},
          cms_content: {},
        },
        meta_data: {},
      },
    };
    stubFetch(res);
    const expectedActions = [
      { type: 'LOAD_CATEGORIES' },
      {
        type: 'LOAD_CATEGORIES_SUCCESS',
        payload: {
          categories: res.result.data.sub_category,
          metadata: res.result.meta_data,
          sortList: res.result.data.sort_list,
          seoFooter: res.result.data.seo_footer,
          cmsContent: res.result.data.cms_content,
        },
      },
    ];
    const store = mockStore({});

    return store.dispatch(load({ category_path: '' })).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
