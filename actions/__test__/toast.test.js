import { openToast } from '../toast';

describe('[Action] toast', () => {
  it('creates OPEN_TOAST has been done', () => {
    const expectedActions = {
      type: 'OPEN_TOAST',
      payload: {},
    };
    expect(openToast({})).toEqual(expectedActions);
  });
});
