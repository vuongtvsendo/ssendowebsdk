import * as api from 'sendo-web-sdk/utils/apis/promotion';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import { fetchCategorySlugFromUrl } from 'sendo-web-sdk/helpers/url';

export const getPromotionBasicInfo = params => {
  params.sort_path = fetchCategorySlugFromUrl(params.category_param);
  return makePromiseAction(
    'LOAD_PROMOTION_BASIC_INFO',
    api.getPromotionBasicInfo(params),
    data => ({
      metadata: data.result.meta_data,
      ...data.result,
      sort_path: params.sort_path,
    })
  );
};

export const getPromotionProducts = params => {
  return makePromiseAction(
    'LOAD_PROMOTION_PRODUCTS',
    api.getPromotionProducts(params),
    data => ({
      products: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const loadMore = params => {
  return makePromiseAction(
    'LOAD_MORE_PROMOTION_PRODUCTS',
    api.getPromotionProducts(params),
    data => ({
      products: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const clearProducts = () => {
  return {
    type: 'CLEAR_PROMOTION_PRODUCTS',
    payload: {},
  };
};
