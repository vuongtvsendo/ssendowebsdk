import * as api from 'sendo-web-sdk/utils/apis/bestselling';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import { fetchCategorySlugFromUrl } from 'sendo-web-sdk/helpers/url';

export const getBestSellingBasicInfo = params => {
  params.sort_path = fetchCategorySlugFromUrl(params.category_param);
  return makePromiseAction(
    'LOAD_BESTSELLING_BASIC_INFO',
    api.getBestSellingBasicInfo(params),
    data => ({
      metadata: data.result.meta_data,
      ...data.result,
      sort_path: params.sort_path,
    })
  );
};

export const getBestSellingProducts = params => {
  return makePromiseAction(
    'LOAD_BESTSELLING_PRODUCTS',
    api.getBestSellingProducts(params),
    data => ({
      products: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const loadMore = params => {
  return makePromiseAction(
    'LOAD_MORE_BESTSELLING_PRODUCTS',
    api.getBestSellingProducts(params),
    data => ({
      products: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const clearProducts = () => {
  return {
    type: 'CLEAR_BESTSELLING_PRODUCTS',
    payload: {},
  };
};
