import * as api from 'sendo-web-sdk/utils/api';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import _get from 'lodash/get';

export const load = params => {
  return (dispatch, getState) => {
    const state = getState();
    return makePromiseAction(
      'LOAD_PRODUCTS',
      api.getProducts(params),
      data => ({
        products: data.result.data,
        metadata: data.result.meta_data,
        categoryMetadata: _get(state, 'category.metadata', {}),
      })
    )(dispatch);
  };
};
export const loadMore = params => {
  return (dispatch, getState) => {
    const state = getState();
    return makePromiseAction(
      'LOAD_MORE_PRODUCTS',
      api.getProducts(params),
      data => ({
        products: data.result.data,
        metadata: data.result.meta_data,
        categoryMetadata: _get(state, 'category.metadata', {}),
      })
    )(dispatch);
  };
};

export const setProductDetail = (product, cateMetadata) => {
  return {
    type: 'SET_PRODUCT_DETAIL',
    payload: {
      productDetail: { ...product, cateMetadata: cateMetadata },
    },
  };
};

export const loadBasicProductDetail = id => {
  return (dispatch, getState) => {
    const state = getState();
    if (state.product.productDetail) {
      return Promise.resolve();
    }
    return makePromiseAction(
      'LOAD_DETAIL_PRODUCT',
      api.getBasicProductDetail(id),
      data => ({
        productDetail: data.result.data,
        productMetadata: data.result.meta_data,
      })
    )(dispatch);
  };
};

export const loadMoreProductDetail = id => {
  return makePromiseAction(
    'LOAD_MORE_DETAIL_PRODUCT',
    api.getProductDetail(id),
    data => ({
      productDetail: data.result.data,
      productMetadata: data.result.meta_data,
    })
  );
};

export const getProductComment = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_COMMENT',
    api.getProductComment(product, param),
    data => ({
      comments: data.result,
    })
  );
};

export const getProductCommentMore = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_COMMENT_MORE',
    api.getProductComment(product, param),
    data => ({
      comments: data.result,
    })
  );
};
export const postProductComment = param => {
  return makePromiseAction(
    'POST_PRODUCT_COMMENT',
    api.postProductComment(param),
    data => ({
      commentPost: data.result.data,
    })
  );
};
// export const testPostProductComment = param => {
//   return dispatch => {
//     dispatch({
//       type: 'TEST_UPDATE_COMMENT',
//       payload: param,
//     });
//   };
// };
export const deleteProductComment = param => {
  return makePromiseAction(
    'DELETE_PRODUCT_COMMENT',
    api.deleteProductComment(param),
    data => ({
      comment: data.result.data,
    })
  );
  // return dispatch => {
  //   dispatch({
  //     type: 'DELETE_COMMENT_SUCCESS',
  //     payload: param,
  //   });
  // };
};

export const getProductRating = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_RATING',
    api.getProductRating(product, param),
    data => ({
      ratings: data.result.data,
    })
  );
};

export const getProductRatingMore = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_RATING_MORE',
    api.getProductRating(product, param),
    data => ({
      ratings: data.result.data,
    })
  );
};

export const getProductRelated = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_RELATED',
    api.getProductRelated(product, param),
    data => ({
      relateds: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const getProductRecommend = (product, param) => {
  return makePromiseAction(
    'LOAD_PRODUCT_RECOMMEND',
    api.getProductRecommend(product, param),
    data => ({
      recommends: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};
export const getShippingInfo = (product, param) => {
  return makePromiseAction(
    'LOAD_SHIPPING_INFO',
    api.getShippingInfo(product, param),
    data => ({
      shippingInfo: data.result.data,
    })
  );
};

export const clearProducts = () => {
  return {
    type: 'CLEAR_PRODUCTS',
    payload: {},
  };
};

export const setAttributesUserSelected = value => {
  return {
    type: 'SET_ATTRIBUTES_USER_SELECTED',
    payload: value,
  };
};

export const isProductFavorite = product_id => {
  return makePromiseAction(
    'CHECK_PRODUCT_FAVORITE',
    api.isProductFavorite(product_id),
    data => data.result.data
  );
};

export const addToWishlist = id => {
  return makePromiseAction(
    'ADD_TO_WISHLISH',
    api.addToWishlist(id),
    data => data.result.data
  );
};

export const removeToWishlist = id => {
  return makePromiseAction(
    'REMOVE_TO_WISHLISH',
    api.removeToWishlist(id),
    data => data.result.data
  );
};

export const setShippingCarrier = value => {
  return {
    type: 'SET_SHIPPING_CARRIER',
    payload: value,
  };
};

export const setShippingDestination = value => {
  return {
    type: 'SET_SHIPPING_DESTINATION',
    payload: value,
  };
};

export const getSponsoredProducts = params => {
  return (dispatch, getState) => {
    const state = getState();
    return makePromiseAction(
      'LOAD_SPONSORED_PRODUCTS',
      api.getSponsoredProducts(params),
      data => ({
        products: data.result.data,
        metadata: data.result.meta_data,
        categoryMetadata: _get(state, 'category.metadata', {}),
      })
    )(dispatch);
  };
};
