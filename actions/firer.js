import { SET_ONLINE_STATUS } from './const';

export const setOnlineStatus = value => {
  return {
    type: SET_ONLINE_STATUS,
    payload: value,
  };
};
