import * as userApi from 'sendo-web-sdk/utils/apis/user';
import * as api from 'sendo-web-sdk/utils/api';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import {
  LOGIN_TYPE_COOKIE_NAME,
  LOGIN_TYPE_SENDO_ID,
  LOGIN_TYPE_FACEBOOK,
  LOGIN_TYPE_GOOGLE,
  JWT_COOKIE_NAME,
} from 'sendo-web-sdk/helpers/const';
import { generateUUID } from 'sendo-web-sdk/helpers/common';
import * as Cookies from 'sendo-web-sdk/helpers/cookie';
import _get from 'lodash/get';
import { isLoggedIn } from 'sendo-web-sdk/helpers/auth';

const generationTrackingId = () => {
  let tracking_id = Cookies.get('tracking_id');
  if (!tracking_id) {
    tracking_id = generateUUID();
    //Cookies.set('tracking_id', tracking_id);
  }
  return tracking_id;
};

export const login = params => {
  return makePromiseAction('USER_LOGIN', userApi.login(params), res => {
    Cookies.set(LOGIN_TYPE_COOKIE_NAME, LOGIN_TYPE_SENDO_ID);
    return res;
  });
};

export const fbLogin = params => {
  return makePromiseAction('USER_LOGIN', userApi.fbLogin(params), () => {
    Cookies.set(LOGIN_TYPE_COOKIE_NAME, LOGIN_TYPE_FACEBOOK);
    return {};
  });
};

export const googleLogin = params => {
  return makePromiseAction('USER_LOGIN', userApi.googleLogin(params), res => {
    Cookies.set(LOGIN_TYPE_COOKIE_NAME, LOGIN_TYPE_GOOGLE);
    return res;
  });
};

export const logout = params => {
  return dispatch => {
    return userApi.logout().then(res => {
      // const identity = _get(res, 'result.data') || {};
      // Cookies.remove(JWT_COOKIE_NAME);
      // Cookies.remove(SENDO_SESSIONID);
      Cookies.remove(LOGIN_TYPE_COOKIE_NAME);
      return dispatch({
        type: 'USER_LOGOUT',
        payload: {},
      });
    });
  };
};

export const register = params => {
  return dispatch => {
    return userApi.register(params).then(res => {
      // const identity = _get(res, 'result.data') || {};
      return dispatch({
        type: 'USER_REGISTER',
        payload: res,
      });
    });
  };
};

export const checkValidOtp = params => {
  return () => {
    return userApi.checkValidOtp(params).then(res => {
      return res;
    });
  };
};

export const checkValidAndPostPhoneEmail = params => {
  return () => {
    return userApi.checkValidAndPostPhoneEmail(params).then(res => {
      return res;
    });
  };
};

export const resendOtp = params => {
  return () => {
    return userApi.resendOtp(params).then(res => {
      return res;
    });
  };
};

export const getUserInfo = (force = false) => {
  return makePromiseAction('USER_LOGIN', userApi.getUserInfo(), res => {
    let identity = _get(res, 'data.user', {});
    identity = { ...identity, session_id: _get(res, 'session_id', false) };
    if (!isLoggedIn(identity)) {
      Cookies.remove(JWT_COOKIE_NAME, { domain: '' });
    }
    return { user_info: identity };
  });
};

export const getUserInfoFull = () => {
  return makePromiseAction(
    'GET_USERS_INFO_FULL',
    userApi.getUserInfoFull(),
    res => {
      const data = _get(res, 'result.data', {});
      return data;
    }
  );
};

export const getUserOrderStatus = () => {
  return makePromiseAction(
    'LOAD_USER_ORDER_STATUS',
    userApi.getUserOrderStatus(),
    res => {
      const data = _get(res, 'result.data') || {};
      return data;
    }
  );
};

export const getUserRecommendCategories = () => {
  let tracking_id = generationTrackingId();
  return makePromiseAction(
    'LOAD_USER_RECOMMEND_CATEGORIES',
    userApi.getUserRecommendCategories(tracking_id),
    res => {
      const data = _get(res, 'result.data') || {};
      return data;
    }
  );
};

export const getUserRecommendProducts = () => {
  let tracking_id = generationTrackingId();
  return makePromiseAction(
    'LOAD_USER_RECOMMEND_PRODUCTS',
    userApi.getUserRecommendProducts(tracking_id),
    data => ({
      products: _get(data, 'result.data') || {},
      metadata: _get(data, 'result.meta_data') || {},
    })
  );
};
export const getUserRecommendProductsMore = params => {
  let tracking_id = generationTrackingId();
  return makePromiseAction(
    'LOAD_USER_RECOMMEND_PRODUCTS_MORE',
    userApi.getUserRecommendProducts(tracking_id, params),
    data => ({
      products: _get(data, 'result.data') || {},
      metadata: _get(data, 'result.meta_data') || {},
    })
  );
};

export const setUserLocation = location => {
  Cookies.set('ecom_cus_to_region_id', location.city_id);
  Cookies.set('ecom_cus_to_region_name', location.city_name);
  return dispatch => {
    dispatch({
      type: 'SET_USER_LOCATION',
      location: location,
    });
  };
};

export const setUserProductRecent = product => {
  return {
    type: 'SET_USER_PRODUCT_RECENT',
    payload: { ...product, viewTime: Date.now() },
  };
};

export const updateInfoProductRecent = ids => {
  return makePromiseAction(
    'UPDATE_INFO_PRODUCT_RECENT',
    api.getProductInfoViaIds(ids),
    res => {
      const data = _get(res, 'result.data') || {};
      return data;
    }
  );
};

export const fbLoginOld = () => {
  return dispatch => {
    return userApi.fbLoginOld().then(() => {
      return dispatch({
        type: 'LOGIN_FACEBOOK_1',
      });
    });
  };
};

export const ggLoginOld = params => {
  return dispatch => {
    return userApi.ggLoginOld(params).then(() => {
      return dispatch({
        type: 'LOGIN_GOOGLE_1',
      });
    });
  };
};
