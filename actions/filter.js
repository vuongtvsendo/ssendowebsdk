import * as api from 'sendo-web-sdk/utils/api';

/**
 * @deprecated
 */
export const load = params => {
  return dispatch => {
    return api.getFilterByCategory(params).then(res => {
      dispatch({
        type: 'FILTER_LOAD',
        payload: {
          filters: res.result.data,
        },
      });
    });
  };
};

export const getFilterSearch = params => {
  return dispatch => {
    return api.getFilterSearch(params).then(res => {
      dispatch({
        type: 'FILTER_LOAD',
        payload: {
          filters: res.result.data,
        },
      });
    });
  };
};

export const getFilterPromotion = params => {
  return dispatch => {
    return api.getFilterPromotion(params).then(res => {
      dispatch({
        type: 'FILTER_LOAD',
        payload: {
          filters: res.result.data,
        },
      });
    });
  };
};

export const getFilterBestSelling = params => {
  return dispatch => {
    return api.getFilterBestSelling(params).then(res => {
      dispatch({
        type: 'FILTER_LOAD',
        payload: {
          filters: res.result.data,
        },
      });
    });
  };
};
