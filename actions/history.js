export const historyDecrease = () => {
  return {
    type: 'HISTORY_DECREASE',
  };
};

export const historyIncrease = () => {
  return {
    type: 'HISTORY_INCREASE',
  };
};
