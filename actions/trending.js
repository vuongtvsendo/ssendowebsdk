import * as api from 'sendo-web-sdk/utils/apis/trending';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';

export const getTrendingHome = params => {
  return makePromiseAction(
    'LOAD_TRENDING_HOME',
    api.getTrendingHome(params),
    data => ({
      metadata: data.result.meta_data,
      ...data.result,
    })
  );
};

export const loadMoreTrendingHome = params => {
  return makePromiseAction(
    'LOAD_MORE_TRENDING_HOME',
    api.getTrendingHome(params),
    data => ({
      ...data.result,
    })
  );
};

export const getTrendingProducts = params => {
  return makePromiseAction(
    'LOAD_TRENDING_PRODUCTS',
    api.getTrendingProducts(params),
    data => ({
      metadata: data.result.meta_data,
      products: data.result.data.product_list,
      url_key: data.result.data.url_key,
      trending_info: { ...data.result.data, product_list: [] },
    })
  );
};

export const loadMoreTrendingProducts = params => {
  return makePromiseAction(
    'LOAD_MORE_TRENDING_PRODUCTS',
    api.getTrendingProducts(params),
    data => ({
      metadata: data.result.meta_data,
      products: data.result.data.product_list,
    })
  );
};

export const clearTrendings = () => {
  return {
    type: 'CLEAR_TRENDING_HOME',
    payload: {},
  };
};

export const clearTrendingProducts = () => {
  return {
    type: 'CLEAR_TRENDING_PRODUCTS',
    payload: {},
  };
};
