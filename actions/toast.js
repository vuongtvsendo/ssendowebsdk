export const openToast = toast => {
  return {
    type: 'OPEN_TOAST',
    payload: toast,
  };
};

export const closeToast = () => {
  return {
    type: 'CLOSE_TOAST',
  };
};
