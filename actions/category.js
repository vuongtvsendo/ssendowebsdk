import * as api from 'sendo-web-sdk/utils/api';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import _get from 'lodash/get';

export const load = params => {
  if (params.is_shop) {
  }
  return makePromiseAction(
    'LOAD_CATEGORIES',
    api.getCatLevel3rd(params),
    data => {
      if (params.is_shop) {
        return {
          categories: _get(data, 'sub_category', {}),
          sortList: _get(data, 'sort_list', []),
          metadata: _get(data, 'meta_data', {}),
          seoFooter: _get(data, 'seo_footer', {}),
          cmsContent: _get(data, 'cms_content', {}),
        };
      }
      return {
        categories: _get(data, 'result.data.sub_category', {}),
        sortList: _get(data, 'result.data.sort_list', []),
        metadata: _get(data, 'result.meta_data', {}),
        seoFooter: _get(data, 'result.data.seo_footer', {}),
        cmsContent: _get(data, 'result.data.cms_content', {}),
      };
    }
  );
};

export const getAllCategories = () => {
  return makePromiseAction(
    'LOAD_ALL_CATEGORIES',
    api.getAllCategories(),
    data => ({
      ...data.result,
    })
  );
};

export const clearCategoryPageState = () => {
  return {
    type: 'CLEAR_CATEGORY_PAGE_STATE',
    payload: {},
  };
};
