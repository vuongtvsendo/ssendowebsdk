import * as api from 'sendo-web-sdk/utils/apis/search';
import { makePromiseAction } from 'sendo-web-sdk/utils/action';
import _get from 'lodash/get';

export const getSearchSuggestions = params => {
  return makePromiseAction(
    'GET_SEARCH_SUGGESTIONS',
    api.getSearchSuggestions(params),
    data => ({
      suggestions: data.result.data,
      query: params.q,
    })
  );
};

export const getSearchTrending = () => {
  return makePromiseAction(
    'GET_SEARCH_TRENDING',
    api.getSearchTrending(),
    data => ({
      trending: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};

export const clearSearchSuggestion = params => {
  return {
    type: 'CLEAR_SEARCH_SUGGESTIONS',
    payload: {},
  };
};

export const clearSearchProducts = () => {
  return {
    type: 'CLEAR_SEARCH_PRODUCTS',
    payload: {},
  };
};

export const clearSearchPageState = () => {
  return {
    type: 'CLEAR_SEARCH_PAGE_STATE',
    payload: {},
  };
};
export const addSearchRecentKeyword = keyword => {
  return {
    type: 'ADD_SEARCH_RECENT_KEYWORD',
    payload: {
      keyword: keyword,
    },
  };
};

export const load = params => {
  return makePromiseAction(
    'LOAD_SEARCH_PRODUCTS',
    api.getProducts(params),
    data => ({
      products: data.result.data,
      metadata: data.result.meta_data,
    })
  );
};
export const loadMore = params => {
  return makePromiseAction(
    'LOAD_SEARCH_MORE_PRODUCTS',
    api.getProducts(params),
    data => ({
      products: data.result.data,
    })
  );
};

export const getSearchInfo = params => {
  return makePromiseAction(
    'GET_SEARCH_INFO',
    api.getSearchInfo(params),
    data => ({
      sortList: data.result.data.sort_list,
      metadata: data.result.meta_data,
      relate_keywords: _get(data, 'result.data.sub_category', []),
    })
  );
};
