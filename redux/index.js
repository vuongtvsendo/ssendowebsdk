import { createStore, applyMiddleware, compose } from 'redux';
import Immutable from 'seamless-immutable';
import thunk from 'redux-thunk';
import array from './middlewares/array';
import promise from './middlewares/promise';
import createReducer from './reducers';

const canShowReduxLogger = () => {
  if (!process.env.ENABLE_DEBUG_JS) {
    return false;
  }
  if (process.env.NODE_ENV === 'test') {
    return false;
  }
  if (__SERVER__ === true) {
    return false;
  }
  return true;
};
export const configureStore = (initialState, reducer = createReducer()) => {
  const middlewares = [thunk, promise, array];
  /**
   |-----------------------------------------------------------------------
   | Bo di redux-logger su dung redux-devtools voi chrome extensions
   |-----------------------------------------------------------------------
   */
  if (canShowReduxLogger()) {
    const reduxLogger = require('redux-logger');
    const logger = reduxLogger.createLogger({
      collapsed: true,
    });
    middlewares.push(logger);
  }
  var enhancers = [applyMiddleware(...middlewares)];
  if (process.env.ENABLE_REDUX_DEVTOOLS) {
    const composeWithDevTools = require('redux-devtools-extension')
      .composeWithDevTools;
    enhancers = [composeWithDevTools(applyMiddleware(...middlewares))];
  }

  // convert plain object to immutable
  const immutaleState = initialState
    ? Object.keys(initialState).reduce((finalState, key) => {
        finalState[key] = Immutable.from(initialState[key]);
        return finalState;
      }, {})
    : undefined;
  let store = createStore(reducer, immutaleState, compose(...enhancers));
  store.asyncReducers = {};
  return store;
};

/**
 * Inject reducer to exist store
 * @param {object} store - redux store
 * @param {string} key - reducer key
 * @param {function} asyncReducer - redux reducer
 */
export function injectAsyncReducer(store, key, asyncReducer) {
  if (store.asyncReducers[key]) return;
  store.asyncReducers[key] = asyncReducer;
  store.replaceReducer(createReducer(store.asyncReducers));
}

/**
 * Inject multiple reducer to exist store
 * @param {object} store - redux store
 * @param {array} asyncReducers - list reducers [{key: <string>, reducer: <function>}] - reducer is redux reducer
 */
export function injectAsyncReducers (store, asyncReducers) {
  if(!store || !asyncReducers) return;
  for (const r of asyncReducers) {
    const key = r.key,
      reducer = r.reducer;
    injectAsyncReducer(store, key, reducer);
  }
};
