import Immutable from 'seamless-immutable';

const initialState = Immutable({
  historyLength: 0,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'HISTORY_DECREASE': {
      let length = state.historyLength
        ? state.historyLength - 1
        : state.historyLength;
      return state.set('historyLength', length);
    }
    case 'HISTORY_INCREASE': {
      let length = state.historyLength + 1;
      return state.set('historyLength', length);
    }
    default: {
      return state;
    }
  }
};
