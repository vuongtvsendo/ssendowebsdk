import Immutable from 'seamless-immutable';
import _get from 'lodash/get';

const initialState = Immutable({
  html: '',
  css: '',
  js: '',
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_CMS_CONTENT_STATIC_SUCCESS': {
      return state
        .set('html', Immutable.from(_get(action.payload.data, 'html', '')))
        .set('css', Immutable.from(_get(action.payload.data, 'css', '')))
        .set('js', Immutable.from(_get(action.payload.data, 'js', '')));
    }
    default: {
      return state;
    }
  }
};
