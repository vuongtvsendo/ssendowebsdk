import Immutable from 'seamless-immutable';
const initialState = Immutable({
  loadingState: {
    isOpen: false,
  },
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_LOADING': {
      return state.set('loadingState', Immutable.from({ isOpen: true }));
    }
    case 'CLOSE_LOADING': {
      return state.set('loadingState', Immutable.from({ isOpen: false }));
    }
    default: {
      return state;
    }
  }
};
