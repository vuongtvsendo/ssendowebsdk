import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import user from './user';
import homepage from './homepage';
import category from './category';
import product from './product';
import filter from './filter';
import location from './location';
import toast from './toast';
import loading from './loading';
import smartbanner from './smartbanner';
import cart from './cart';
import page from './page';
import search from './search';
import firer from './firer';
import promotion from './promotion';
import bestselling from './bestselling';
import trending from './trending';
import cmscontent from './cmscontent';
import event from './event';
import shop from './shop';

import historydata from './history';

export default function createReducer(injectedReducers) {
  return combineReducers({
    form: formReducer,
    user,
    homepage,
    category,
    product,
    filter,
    location,
    toast,
    loading,
    smartbanner,
    cart,
    page,
    search,
    firer,
    promotion,
    bestselling,
    trending,
    cmscontent,
    event,
    shop,
    historydata,
      ...injectedReducers,
  });
}

