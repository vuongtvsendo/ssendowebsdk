import Immutable from 'seamless-immutable';
const initialState = Immutable({
  allCity: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_All_CITY_SUCCESS': {
      return state.set('allCity', action.payload.allCity);
    }
    default: {
      return state;
    }
  }
};
