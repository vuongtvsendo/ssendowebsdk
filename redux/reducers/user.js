import Immutable from 'seamless-immutable';
import LRUMap from 'sendo-web-sdk/libs/lru';
import _get from 'lodash/get';
import _filter from 'lodash/filter';
import _defaultsDeep from 'lodash/defaultsDeep';
import * as Cookies from 'sendo-web-sdk/helpers/cookie';
import { MAX_RENCENT_PRODUCTS, LOGIN_TYPE_COOKIE_NAME } from 'sendo-web-sdk/helpers/const';
var listRencentProducts = new LRUMap(MAX_RENCENT_PRODUCTS);

const initRecentProduct = () => {
  let storageData = JSON.parse(
    window.localStorage.getItem('listRencentProducts')
  );
  if (storageData) {
    storageData.forEach(item => {
      listRencentProducts.set(item.key, item.value);
    });
  }
  return listRencentProducts.toJSON();
};

const getInitialState = () => {
  let clientState = {};
  let generalState = {
    identity: {},
    location: {},
    orderStatus: [],
    recommendCategories: {},
    recommendProducts: { loading: false, products: [] },
    recentProducts: [],
    initRecentProduct: false,
  };
  if (__CLIENT__) {
    clientState = {
      identity: {
        login_type: Cookies.get(LOGIN_TYPE_COOKIE_NAME),
      },
      location: {
        city_id: parseInt(Cookies.get('ecom_cus_to_region_id') || '1', 10),
        city_name: Cookies.get('ecom_cus_to_region_name') || 'Hồ Chí Minh',
      },
      recentProducts: initRecentProduct(),
    };
  }
  const initialState = _defaultsDeep(clientState, generalState);
  return initialState;
};
const preValidatePayload = userInfo => {
  if (!userInfo) return;
  if (!userInfo.city_id) {
    delete userInfo.city_id;
  }
  if (!userInfo.city_name) {
    delete userInfo.city_name;
  }
};

const defaultState = Immutable({});
export default (state = defaultState, action) => {
  switch (action.type) {
    case '@@INIT':
    case '@@redux/INIT': {
      const initialState = getInitialState();
      return Immutable(_defaultsDeep(initialState, state));
    }
    case 'GET_USERS_INFO_FULL_SUCCESS': {
      preValidatePayload(action.payload);
      const identity = {
        ...state.identity,
        ...action.payload,
      };
      const city = {
        city_id: identity.city_id,
        city_name: identity.city_name,
      };
      return state
        .set('identity', Immutable.from(identity))
        .set('location', Immutable.from(city));
    }

    case 'USER_LOGIN_SUCCESS': {
      let user_info = _get(action.payload, 'user_info', {});
      preValidatePayload(user_info);
      const identity = {
        ...state.identity,
        ...user_info,
      };
      return state.set('identity', Immutable.from(identity));
    }
    case 'USER_LOGOUT': {
      /**
       * Revert cac gia tri default sau khi user logout
       */
      return state
        .set(
          'identity',
          Immutable.from({
            login_type: Cookies.get(LOGIN_TYPE_COOKIE_NAME),
          })
        )
        .set('orderStatus', Immutable.from([]));
    }
    case 'LOAD_USER_ORDER_STATUS_SUCCESS': {
      return state.set('orderStatus', Immutable.from(action.payload.data.list));
    }
    case 'LOAD_USER_RECOMMEND_CATEGORIES_SUCCESS': {
      return state.set(
        'recommendCategories',
        Immutable.from(action.payload.data.list)
      );
    }
    case 'LOAD_USER_RECOMMEND_PRODUCTS_SUCCESS': {
      let products = _get(action.payload, 'products.data.list', []);
      products.length &&
        products.map(product => {
          return (product.productMetadata = action.payload.metadata);
        });
      return state.setIn(
        ['recommendProducts', 'products'],
        Immutable.from(products)
      );
    }
    case 'LOAD_USER_RECOMMEND_PRODUCTS_MORE': {
      return state.setIn(['recommendProducts', 'loading'], true);
    }
    case 'LOAD_USER_RECOMMEND_PRODUCTS_MORE_FAILED': {
      return state.setIn(['recommendProducts', 'loading'], false);
    }
    case 'LOAD_USER_RECOMMEND_PRODUCTS_MORE_SUCCESS': {
      let products = _get(action.payload, 'products.data.list', []);
      products.length &&
        products.map(product => {
          return (product.productMetadata = action.payload.metadata);
        });
      const recommendProducts = [
        ...state.recommendProducts.products,
        ...products,
      ];
      return state
        .setIn(
          ['recommendProducts', 'products'],
          Immutable.from(recommendProducts)
        )
        .setIn(['recommendProducts', 'loading'], false);
    }
    case 'SET_USER_LOCATION': {
      const location = {
        ...state.location,
        ...action.location,
      };
      return state.set('location', Immutable.from(location));
    }
    case 'GET_CITY_BY_LATLNG_SUCCESS': {
      /*
      * Gia tri merge vao la region_code va region_name
      * Khi nao user bam "xac nhan" moi ghi de len city_id va city_name
      */
      const location = {
        ...state.location,
        ...action.payload.location,
      };
      return state.set('location', Immutable.from(location));
    }
    case 'SET_USER_PRODUCT_RECENT': {
      listRencentProducts.set(
        action.payload.product_id || action.payload.id,
        action.payload
      );
      window.localStorage.setItem(
        'listRencentProducts',
        JSON.stringify(listRencentProducts)
      );
      let products = listRencentProducts.toJSON();
      return state.set('recentProducts', Immutable.from(products));
    }
    case 'UPDATE_INFO_PRODUCT_RECENT_SUCCESS': {
      let recentList = state.recentProducts;
      let updateList = action.payload;
      if (recentList.length <= 0) return state;
      let newList = recentList.map(recentItem => {
        var matchPrd = _filter(updateList, item => recentItem.key === item.id);
        return {
          ...recentItem,
          value: { ...recentItem.value, ...matchPrd[0], updated: true },
        };
      });
      return state
        .set('recentProducts', Immutable.from(newList))
        .set('initRecentProduct', true);
    }
    default: {
      return state;
    }
  }
};
