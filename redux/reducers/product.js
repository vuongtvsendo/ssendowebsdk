import Immutable from 'seamless-immutable';
import _remove from 'lodash/remove';
import _get from 'lodash/get';
import _findIndex from 'lodash/findIndex';
const initialState = Immutable({
  products: [],
  sponsoredProducts: [],
  init: false,
  initSponsoredProducts: false,
  loading: false,
  loadingMore: false,
  metadata: {},
  loadingDetailProduct: true,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_PRODUCT_DETAIL': {
      return state.set('productDetail', action.payload.productDetail);
    }
    case 'LOAD_DETAIL_PRODUCT_SUCCESS': {
      const productDetail = {
        ...action.payload.productDetail,
        metadata: action.payload.productMetadata,
      };
      return state.set('productDetail', productDetail);
    }
    case 'LOAD_MORE_DETAIL_PRODUCT': {
      return state.set('loadingDetailProduct', true);
    }
    case 'LOAD_MORE_DETAIL_PRODUCT_FAILED': {
      return state.set('loadingDetailProduct', false);
    }
    case 'LOAD_MORE_DETAIL_PRODUCT_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        ...action.payload.productDetail,
        metadata: {
          ...state.productDetail.metadata,
          ...action.payload.productMetadata,
        },
      };
      return state
        .set('productDetail', Immutable.from(productDetail))
        .set('loadingDetailProduct', false);
    }
    case 'CHECK_PRODUCT_FAVORITE_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        isFavorite: action.payload === 1,
      };
      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'ADD_TO_WISHLISH_SUCCESS': {
      if (action.payload) {
        const productDetail = {
          ...state.productDetail,
          isFavorite: true,
        };
        return state.set('productDetail', Immutable.from(productDetail));
      }
      return state;
    }
    case 'REMOVE_TO_WISHLISH_SUCCESS': {
      if (action.payload) {
        const productDetail = {
          ...state.productDetail,
          isFavorite: false,
        };
        return state.set('productDetail', Immutable.from(productDetail));
      }
      return state;
    }
    case 'LOAD_PRODUCT_COMMENT_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        comments: action.payload.comments.data,
        total_comment: _get(
          action.payload,
          'comments.meta_data.total_count',
          0
        ),
      };
      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'LOAD_PRODUCT_COMMENT_MORE': {
      return state.setIn(['productDetail', 'comment_loading'], true);
    }
    case 'LOAD_PRODUCT_COMMENT_MORE_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        comments: [
          ...state.productDetail.comments,
          ...action.payload.comments.data,
        ],
        comment_loading: false,
        total_comment: _get(
          action.payload,
          'comments.meta_data.total_count',
          0
        ),
      };
      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'LOAD_PRODUCT_COMMENT_MORE_FAILED': {
      return state.setIn(['productDetail', 'comment_loading'], false);
    }
    case 'POST_PRODUCT_COMMENT': {
      return state.setIn(['productDetail', 'comment_post_loading'], true);
    }
    case 'POST_PRODUCT_COMMENT_SUCCESS': {
      let commentPost = _get(action.payload, 'commentPost.comment_data', {});
      commentPost.id = _get(action.payload, 'commentPost.comment_id', 0);
      let productDetail = { ...state.productDetail };
      if (commentPost.parent_id && parseInt(commentPost.parent_id, 10) === 0) {
        productDetail = {
          ...state.productDetail,
          comments: [commentPost, ...state.productDetail.comments],
          comment_post_loading: false,
        };
      } else {
        let commentUpdateIndex = _findIndex(state.productDetail.comments, i => {
          return i.id === commentPost.parent_id;
        });
        let commentUpdate = state.productDetail.comments.map(
          (comment, index) => {
            if (index === commentUpdateIndex) {
              return { ...comment, sub: [...comment.sub, commentPost] };
            }
            return comment;
          }
        );
        productDetail = {
          ...state.productDetail,
          comments: commentUpdate,
          comment_post_loading: false,
        };
      }

      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'POST_PRODUCT_COMMENT_FAILED': {
      return state.setIn(['productDetail', 'comment_post_loading'], false);
    }
    case 'DELETE_PRODUCT_COMMENT_SUCCESS': {
      var comments = [...state.productDetail.comments];
      if (action.payload.comment.err_code === 0) {
        var commentId = _get(action.payload, 'comment.comment_data.id');
        var parentId = _get(action.payload, 'comment.comment_data.parent_id');
        if (parseInt(parentId, 10) === 0 || commentId === parentId) {
          _remove(comments, i => {
            return i.id === commentId;
          });
          return state.setIn(['productDetail', 'comments'], comments);
        } else {
          let commentRemoveIndex = _findIndex(comments, i => {
            return i.id === parentId;
          });
          var commentUpdate = comments.map((comment, index) => {
            if (index === commentRemoveIndex) {
              var subArr = [...comment.sub];
              _remove(subArr, i => {
                return i.id === commentId;
              });
              return {
                ...comment,
                sub: subArr,
              };
            }
            return comment;
          });
          return state.setIn(['productDetail', 'comments'], commentUpdate);
        }
      }
      return state;
    }

    case 'LOAD_PRODUCT_RATING_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        ratings: action.payload.ratings,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'LOAD_PRODUCT_RATING_MORE': {
      return state.setIn(['productDetail', 'rating_loading'], true);
    }
    case 'LOAD_PRODUCT_RATING_FAILED': {
      return state.setIn(['productDetail', 'rating_loading'], false);
    }
    case 'LOAD_PRODUCT_RATING_MORE_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        ratings: [...state.productDetail.ratings, ...action.payload.ratings],
        rating_loading: false,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }

    case 'LOAD_PRODUCT_RELATED_SUCCESS': {
      let products = action.payload.relateds;
      products.length &&
        products.map(products => {
          return (products.productMetadata = action.payload.metadata);
        });
      const productDetail = {
        ...state.productDetail,
        relateds: products,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }

    case 'LOAD_PRODUCT_RECOMMEND_SUCCESS': {
      let products = action.payload.recommends;
      products.length &&
        products.map(products => {
          return (products.productMetadata = action.payload.metadata);
        });
      const productDetail = {
        ...state.productDetail,
        recommends: products,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }

    case 'LOAD_SHIPPING_INFO_SUCCESS': {
      const productDetail = {
        ...state.productDetail,
        shipingInfo: action.payload.shippingInfo,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }

    case 'SET_ATTRIBUTES_USER_SELECTED': {
      const productDetail = {
        ...state.productDetail,
        selectedAttr: action.payload,
      };

      return state.set('productDetail', Immutable.from(productDetail));
    }

    case 'LOAD_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'LOAD_PRODUCTS_FAILED': {
      return state.set('loading', false).set('init', true);
    }
    case 'LOAD_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.length &&
        products.map(products => {
          return (products.productMetadata = {
            ...action.payload.metadata,
            ..._get(action, 'payload.categoryMetadata', {}),
          });
        });
      return state
        .set('loading', false)
        .set('metadata', action.payload.metadata)
        .set('init', true)
        .set('products', Immutable.from(products));
    }
    case 'LOAD_MORE_PRODUCTS': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_MORE_PRODUCTS_FAILED': {
      return state.set('loadingMore', false);
    }
    case 'LOAD_MORE_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.length &&
        products.map(products => {
          return (products.productMetadata = {
            ...action.payload.metadata,
            ..._get(action, 'payload.categoryMetadata', {}),
          });
        });
      return state
        .set('loadingMore', false)
        .set('products', state.products.concat(Immutable.from(products)));
    }
    case 'CLEAR_PRODUCTS': {
      return state
        .set('products', [])
        .set('init', false)
        .set('sponsoredProducts', []);
    }
    case 'SET_SHIPPING_CARRIER': {
      const productDetail = {
        ...state.productDetail,
        shipping_carrier: action.payload,
      };
      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'SET_SHIPPING_DESTINATION': {
      const productDetail = {
        ...state.productDetail,
        shipping_destination: action.payload,
      };
      return state.set('productDetail', Immutable.from(productDetail));
    }
    case 'LOAD_SPONSORED_PRODUCTS': {
      return state;
    }
    case 'LOAD_SPONSORED_PRODUCTS_FAILED': {
      return state.set('initSponsoredProducts', true);
    }
    case 'LOAD_SPONSORED_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.length &&
        products.map(products => {
          return (products.productMetadata = {
            ...action.payload.metadata,
            ..._get(action, 'payload.categoryMetadata', {}),
          });
        });
      return state
        .set('sponsoredProducts', Immutable.from(products))
        .set('initSponsoredProducts', true);
    }
    default: {
      return state;
    }
  }
};
