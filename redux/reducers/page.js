import Immutable from 'seamless-immutable';
const initialState = Immutable({
  subCate: {
    //Xu ly dac biet cho nay cho lan dau, can phai set lastScrollPage can phai = false
    lastScrollY: false,
    curentProductPage: 1,
    isListView: false,
  },
  trendingPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  eventPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  eventDetailPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  eventDetailFullPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  searchPage: {
    //Xu ly dac biet cho nay cho lan dau, can phai set lastScrollPage can phai = false
    lastScrollY: false,
    curentProductPage: 1,
    isListView: false,
  },
  homePage: {
    //Xu ly dac biet cho nay cho lan dau, can phai set lastScrollPage can phai = false
    lastScrollY: false,
    curentProductPage: 1,
  },
  shopPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  shopCommentPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
  shopRatingPage: {
    lastScrollY: false,
    curentProductPage: 1,
  },
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'STORE_SUB_CATE_PAGE': {
      let subCate = { ...state.subCate, ...action.payload };
      return state.set('subCate', Immutable.from(subCate));
    }
    case 'STORE_TRENDING_PAGE': {
      let trendingPage = { ...state.trendingPage, ...action.payload };
      return state.set('trendingPage', Immutable.from(trendingPage));
    }
    case 'STORE_SEARCH_PAGE': {
      let searchPage = { ...state.searchPage, ...action.payload };
      return state.set('searchPage', Immutable.from(searchPage));
    }
    case 'STORE_HOME_PAGE': {
      let homePage = { ...state.homePage, ...action.payload };
      return state.set('homePage', Immutable.from(homePage));
    }
    case 'STORE_EVENT_PAGE': {
      let eventPage = { ...state.eventPage, ...action.payload };
      return state.set('eventPage', Immutable.from(eventPage));
    }
    case 'STORE_EVENT_DETAIL_FULL_PAGE': {
      let eventDetailFullPage = {
        ...state.eventDetailFullPage,
        ...action.payload,
      };
      return state.set(
        'eventDetailFullPage',
        Immutable.from(eventDetailFullPage)
      );
    }
    case 'STORE_SHOP_PAGE': {
      let shopPage = { ...state.shopPage, ...action.payload };
      return state.set('shopPage', Immutable.from(shopPage));
    }
    case 'STORE_SHOP_COMMENT_PAGE': {
      let shopCommentPage = { ...state.shopCommentPage, ...action.payload };
      return state.set('shopCommentPage', Immutable.from(shopCommentPage));
    }
    case 'STORE_SHOP_RATING_PAGE': {
      let shopRatingPage = { ...state.shopRatingPage, ...action.payload };
      return state.set('shopRatingPage', Immutable.from(shopRatingPage));
    }
    default: {
      return state;
    }
  }
};
