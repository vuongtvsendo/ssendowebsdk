import Immutable from 'seamless-immutable';
const initialState = Immutable({
  metadata: {},
  banners: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_HOMEPAGE_SUCCESS': {
      let { homepage } = action.payload;
      return state
        .set('metadata', Immutable.from(homepage.meta_data))
        .set('banners', Immutable.from(homepage.data.data.list));
    }
    default: {
      return state;
    }
  }
};
