import Immutable from 'seamless-immutable';
import _defaultsDeep from 'lodash/defaultsDeep';
import _uniq from 'lodash/uniq';
import { MAX_RECENT_KEYWORDS } from 'sendo-web-sdk/helpers/const';

const initRecentKeywords = () => {
  if (__SERVER__) return [];
  if (!window.localStorage.getItem('listRecentKeywords')) {
    return [];
  }
  let listRecentKeywords = JSON.parse(
    window.localStorage.getItem('listRecentKeywords')
  );
  return listRecentKeywords;
};

// const initialState = Immutable({
//   suggestions: [],
//   data: [],
//   metadata: {},
//   trending: [],
//   keywords: initRecentKeywords(),
//   products: [],
//   init: false,
//   loading: false,
//   loadingMore: false,
// });

const getInitialState = () => {
  let clientState = {};
  let generalState = {
    suggestions: [],
    data: [],
    metadata: {},
    trending: [],
    keywords: [],
    products: [],
    sortList: [],
    init: false,
    redirecting: false,
    loading: false,
    loadingMore: false,
    relate_tags: [],
  };
  if (__CLIENT__) {
    clientState = {
      keywords: initRecentKeywords(),
    };
  }
  const initialState = _defaultsDeep(clientState, generalState);
  return initialState;
};
const defaultState = Immutable({});
export default (state = defaultState, action) => {
  switch (action.type) {
    case '@@INIT':
    case '@@redux/INIT': {
      const initialState = getInitialState();
      return Immutable(_defaultsDeep(initialState, state));
    }
    case 'GET_SEARCH_SUGGESTIONS_SUCCESS': {
      let { suggestions } = action.payload;
      return state.set('suggestions', Immutable.from(suggestions || []));
    }
    case 'GET_SEARCH_TRENDING': {
      return state.set('loading', true);
    }
    case 'GET_SEARCH_TRENDING_FAILED': {
      return state.set('loading', false);
    }
    case 'GET_SEARCH_TRENDING_SUCCESS': {
      let { trending } = action.payload;
      return state
        .set('trending', Immutable.from(trending || []))
        .set('loading', false);
    }
    case 'CLEAR_SEARCH_SUGGESTIONS': {
      return state.set('suggestions', []);
    }
    case 'ADD_SEARCH_RECENT_KEYWORD': {
      var keywords = state.keywords;
      if (keywords.length >= MAX_RECENT_KEYWORDS) {
        keywords = keywords.slice(keywords.length - MAX_RECENT_KEYWORDS + 1);
      }
      /**
       * Use the Set data structure, which automatically takes an iterable and removes duplicates.
       * Then, use the from function of the Array object which will then take an iterable
       * and convert it into an array.
       */
      keywords = _uniq([action.payload.keyword, ...keywords]); //prepend keyword
      window.localStorage.setItem(
        'listRecentKeywords',
        JSON.stringify(keywords)
      );
      return state.set('keywords', keywords);
    }
    case 'LOAD_SEARCH_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'LOAD_SEARCH_PRODUCTS_FAILED': {
      if (action.redirecting) {
        return state
          .set('redirecting', Immutable.from(true))
          .set('loading', true)
          .set('init', true);
      } else {
        return state
          .set('redirecting', Immutable.from(false))
          .set('loading', false)
          .set('init', true);
      }
    }
    case 'LOAD_SEARCH_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      let metadata = action.payload.metadata;
      products.map(products => {
        return (products.productMetadata = metadata);
      });
      return state
        .set('loading', false)
        .set('redirecting', false)
        .set('init', true)
        .set('products', Immutable.from(products));
    }
    case 'LOAD_SEARCH_MORE_PRODUCTS': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_SEARCH_MORE_PRODUCTS_FAILED': {
      return state.set('loadingMore', false);
    }
    case 'LOAD_SEARCH_MORE_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      let metadata = action.payload.metadata;
      products.length &&
        products.map(products => {
          return (products.productMetadata = metadata);
        });
      return state
        .set('loadingMore', false)
        .set('products', state.products.concat(Immutable.from(products)));
    }
    case 'CLEAR_SEARCH_PRODUCTS': {
      return state.set('products', []).set('init', false);
    }
    case 'CLEAR_SEARCH_PAGE_STATE': {
      return state.set('suggestions', Immutable.from([]));
    }
    case 'GET_SEARCH_INFO': {
      return state.set('loading', true);
    }
    case 'GET_SEARCH_INFO_FAILED': {
      return state.set('loading', false);
    }
    case 'GET_SEARCH_INFO_SUCCESS': {
      let sortList = action.payload.sortList;
      let metadata = action.payload.metadata;
      return state
        .set('sortList', Immutable.from(sortList))
        .set('metadata', Immutable.from(metadata))
        .set('loading', false)
        .set('relate_keywords', Immutable.from(action.payload.relate_keywords));
    }
    default: {
      return state;
    }
  }
};
