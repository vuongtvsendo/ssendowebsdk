import reducer from '../trending';
import Immutable from 'seamless-immutable';

describe('trending reducer', () => {
  const initialState = Immutable({
    trending_list: [],
    trending_info: {},
    sortList: [],
    metadata_home: {},
    metadata_detail: {},
    sitemapMetadata: {},
    trending_products: [],
    url_key: '',
    init_home: false,
    init_detail: false,
    loading_home: false,
    loading_detail: false,
    sort_path: '',
    loadingMore_home: false,
    loadingMore_detail: false,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_TRENDING_HOME_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_TRENDING_HOME_SUCCESS',
        payload: {
          data: [],
          metadata: {},
        },
      })
    ).toMatchObject({
      trending_list: [],
      metadata_home: {},
      init_home: true,
      loading_home: false,
    });
  });
  it('should handle LOAD_MORE_TRENDING_HOME_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_TRENDING_HOME_SUCCESS',
        payload: {
          data: [],
        },
      })
    ).toMatchObject({
      trending_list: [],
      loadingMore_home: false,
    });
  });
  it('should handle LOAD_TRENDING_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_TRENDING_PRODUCTS_SUCCESS',
        payload: {
          products: [],
          trending_info: {},
          url_key: '',
          metadata: {},
        },
      })
    ).toMatchObject({
      trending_products: [],
      metadata_detail: {},
      init_detail: true,
      loading_detail: false,
      trending_info: {},
      url_key: '',
    });
  });
  it('should handle LOAD_MORE_TRENDING_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_TRENDING_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({
      trending_products: [],
      loadingMore_detail: false,
    });
  });
});
