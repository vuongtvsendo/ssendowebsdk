import reducer from '../filter';
import Immutable from 'seamless-immutable';

describe('filter reducer', () => {
  const initialState = Immutable({
    filters: [],
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle FILTER_LOAD', () => {
    expect(
      reducer(initialState, {
        type: 'FILTER_LOAD',
        payload: {
          filters: [],
        },
      })
    ).toMatchObject({
      filters: [],
    });
  });
});
