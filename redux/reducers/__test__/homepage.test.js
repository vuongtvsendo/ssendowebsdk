import reducer from '../homepage';
import Immutable from 'seamless-immutable';

describe('homepage reducer', () => {
  const initialState = Immutable({
    metadata: {},
    banners: [],
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_HOMEPAGE_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_HOMEPAGE_SUCCESS',
        payload: {
          homepage: {
            meta_data: {},
            data: {
              data: {
                list: [],
              },
            },
          },
        },
      })
    ).toMatchObject({
      metadata: {},
      banners: [],
    });
  });
});
