import reducer from '../category';
import Immutable from 'seamless-immutable';

describe('category reducer', () => {
  const initialState = Immutable({
    categories: [],
    allCategories: [],
    sortList: [],
    metadata: {},
    sitemapMetadata: {},
    redirecting: false,
    seoFooter: {},
    cmsContent: {},
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_CATEGORIES_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_CATEGORIES_SUCCESS',
        payload: {
          metadata: {},
          sortList: [],
          categories: [],
        },
      })
    ).toMatchObject({
      metadata: {},
      sortList: [],
      categories: [],
    });
  });
  it('should handle LOAD_ALL_CATEGORIES_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_ALL_CATEGORIES_SUCCESS',
        payload: {
          data: [],
          meta_data: {},
        },
      })
    ).toMatchObject({
      allCategories: [],
      sitemapMetadata: {},
    });
  });
  it('should handle CLEAR_CATEGORY_PAGE_STATE', () => {
    expect(
      reducer(initialState, {
        type: 'CLEAR_CATEGORY_PAGE_STATE',
      })
    ).toMatchObject({
      metadata: {},
    });
  });
});
