import reducer from '../search';
import Immutable from 'seamless-immutable';

describe('search reducer', () => {
  const initialState = Immutable({
    suggestions: [],
    data: [],
    metadata: {},
    trending: [],
    keywords: [],
    products: [],
    sortList: [],
    init: false,
    loading: false,
    redirecting: false,
    loadingMore: false,
    relate_tags: [],
  });
  it('should return the default state', () => {
    expect(reducer(undefined, {})).toEqual({});
  });
  it('should return the initial state', () => {
    expect(reducer(initialState, { type: '@@redux/INIT' })).toEqual(
      initialState
    );
  });
  it('should handle ADD_SEARCH_RECENT_KEYWORD', () => {
    expect(
      reducer(initialState, {
        type: 'ADD_SEARCH_RECENT_KEYWORD',
        payload: {
          keyword: 1,
        },
      })
    ).toMatchObject({
      keywords: [1],
    });
  });
  it('should handle LOAD_SEARCH_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SEARCH_PRODUCTS_SUCCESS',
        payload: {
          metadata: {},
          products: [],
        },
      })
    ).toMatchObject({
      metadata: {},
      loading: false,
      init: true,
      products: [],
    });
  });
  it('should handle LOAD_SEARCH_MORE_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SEARCH_MORE_PRODUCTS_SUCCESS',
        payload: {
          products: [],
          metadata: {},
        },
      })
    ).toMatchObject({
      loadingMore: false,
      products: [],
    });
  });
});
