import reducer from '../bestselling';
import Immutable from 'seamless-immutable';

describe('cart reducer', () => {
  const initialState = Immutable({
    bestselling_categories: [],
    sortList: [],
    metadata: {},
    sitemapMetadata: {},
    bestselling_products: [],
    init: false,
    loading: false,
    sort_path: '',
    loadingMore: false,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_BESTSELLING_BASIC_INFO_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_BESTSELLING_BASIC_INFO_SUCCESS',
        payload: {
          data: {
            list_categories: [],
          },
          metadata: {},
          sort_path: '',
        },
      })
    ).toMatchObject({
      bestselling_categories: [{ name: 'Tất cả', url_path: '' }],
      metadata: {},
      sort_path: '',
    });
  });
  it('should handle LOAD_BESTSELLING_PRODUCTS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_BESTSELLING_PRODUCTS',
      })
    ).toMatchObject({
      loading: true,
    });
  });
  it('should handle LOAD_BESTSELLING_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_BESTSELLING_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({ bestselling_products: [], init: true, loading: false });
  });
  it('should handle LOAD_MORE_BESTSELLING_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_BESTSELLING_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({ bestselling_products: [], loadingMore: false });
  });
});
