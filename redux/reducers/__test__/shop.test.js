import reducer from '../shop';
import Immutable from 'seamless-immutable';

describe('shop reducer', () => {
  const initialState = Immutable({
    shopInfo: {},
    products: [],
    sortList: [],
    categories: [],
    metadata: {},
    init: false,
    loading: false,
    loadingMore: false,
    shopComments: [],
    shopRatings: [],
    totalRating: 0,
  });
  it('should handle LOAD_SHOP_DETAIL_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SHOP_DETAIL_SUCCESS',
        payload: {
          shopDetail: {},
        },
      })
    ).toMatchObject({
      shopInfo: {},
    });
  });
  it('should handle LOAD_SHOP_CATEGORIES_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SHOP_CATEGORIES_SUCCESS',
        payload: {
          categories: [],
        },
      })
    ).toMatchObject({
      categories: [],
    });
  });
  it('should handle CHECKLIKE_SHOP_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'CHECKLIKE_SHOP_SUCCESS',
        payload: {
          isLike: 1,
        },
      })
    ).toMatchObject({
      shopInfo: {
        is_liked: 1,
      },
    });
  });
  it('should handle LIKE_SHOP_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LIKE_SHOP_SUCCESS',
        payload: {
          isLike: 1,
        },
      })
    ).toMatchObject({
      shopInfo: {
        is_liked: 1,
      },
    });
  });
  it('should handle UNLIKE_SHOP_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'UNLIKE_SHOP_SUCCESS',
        payload: {
          isLike: 1,
        },
      })
    ).toMatchObject({
      shopInfo: {
        is_liked: 0,
      },
    });
  });
  it('should handle LOAD_SHOP_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SHOP_PRODUCTS_SUCCESS',
        payload: {
          products: {
            data: [],
          },
        },
      })
    ).toMatchObject({
      products: [],
      loading: false,
    });
  });
  it('should handle LOAD_SHOP_COMMENTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SHOP_COMMENTS_SUCCESS',
        payload: {},
      })
    ).toMatchObject({
      loading: false,
    });
  });
  it('should handle LOAD_SHOP_RATINGS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_SHOP_RATINGS_SUCCESS',
        payload: {
          shopRating: {
            total_count: 10,
            data: [],
          },
        },
      })
    ).toMatchObject({
      shopRatings: [],
      totalRating: 10,
      loading: false,
    });
  });
});
