import reducer from '../event';
import Immutable from 'seamless-immutable';

describe('event reducer', () => {
  const initialState = Immutable({
    event_list: {
      active: [],
      deactive: [],
    },
    event_detail_list: [],
    sortList: [],
    metadata: {},
    metadata_detail: {},
    metadata_detail_full: {},
    event_products: [],
    init_home: false,
    init_detail: false,
    init_detail_full: false,
    loading_home: false,
    loading_detail: false,
    loading_detail_full: false,
    sort_path: '',
    loadingMore_home: false,
    loadingMore_detail: false,
    loadingMore_detail_full: false,
  });
  it('should handle LOAD_EVENT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_EVENT_SUCCESS',
        payload: {
          event_active: ['active'],
          event_deactive: ['deactive'],
          metadata: {},
        },
      })
    ).toMatchObject({
      event_list: {
        active: ['active'],
        deactive: ['deactive'],
      },
      metadata: {},
      init_home: true,
      loading_home: false,
    });
  });
  it('should handle LOAD_MORE_EVENT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_EVENT_SUCCESS',
        payload: {
          event_deactive: ['deactive'],
        },
      })
    ).toMatchObject({
      event_list: {
        deactive: ['deactive'],
      },
    });
  });
  it('should handle LOAD_EVENT_DETAIL_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_EVENT_DETAIL_SUCCESS',
        payload: {
          cabinets: ['cabinet'],
          metadata: {},
        },
      })
    ).toMatchObject({
      event_detail_list: ['cabinet'],
      metadata_detail: {},
      init_detail: true,
      loading_detail: false,
    });
  });
});
