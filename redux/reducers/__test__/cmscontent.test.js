import reducer from '../cmscontent';
import Immutable from 'seamless-immutable';

describe('cmscontent reducer', () => {
  const initialState = Immutable({
    html: '',
    css: '',
    js: '',
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_CMS_CONTENT_STATIC_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_CMS_CONTENT_STATIC_SUCCESS',
        payload: {
          data: {
            html: '',
            css: '',
            js: '',
          },
        },
      })
    ).toMatchObject({
      html: '',
      css: '',
      js: '',
    });
  });
});
