import reducer from '../cart';
import Immutable from 'seamless-immutable';

describe('cart reducer', () => {
  const initialState = Immutable({
    metadata: {},
    cartList: [],
    loading: false,
    init: false,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_DATA_CART', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_DATA_CART',
      })
    ).toMatchObject({
      loading: true,
    });
  });
  it('should handle LOAD_DATA_CART_FAILED', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_DATA_CART_FAILED',
      })
    ).toMatchObject({
      loading: false,
    });
  });
  it('should handle ADD_TO_CART_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'ADD_TO_CART_SUCCESS',
      })
    ).toMatchObject({
      loading: false,
    });
  });
  it('should handle LOAD_TOTAL_CART_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_TOTAL_CART_SUCCESS',
        payload: { total_count: 10 },
      })
    ).toMatchObject({
      metadata: {
        total_count: 10,
      },
    });
  });
});
