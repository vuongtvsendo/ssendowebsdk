import reducer from '../user';
import Immutable from 'seamless-immutable';

describe('user reducer', () => {
  const initialState = Immutable({
    identity: {
      login_type: undefined,
    },
    location: {
      city_id: 1,
      city_name: 'Hồ Chí Minh',
    },
    orderStatus: [],
    recommendCategories: {},
    recommendProducts: { loading: false, products: [] },
    recentProducts: [],
    initRecentProduct: false,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, { type: '@@INIT' })).toEqual(initialState);
  });
  it('should handle GET_USERS_INFO_FULL_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'GET_USERS_INFO_FULL_SUCCESS',
        payload: {
          city_id: 1,
          city_name: 'city_name',
        },
      })
    ).toMatchObject({
      identity: {
        city_id: 1,
        city_name: 'city_name',
      },
    });
  });
  it('should handle LOAD_USER_RECOMMEND_PRODUCTS_MORE_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_USER_RECOMMEND_PRODUCTS_MORE_SUCCESS',
        payload: {
          data: {
            list: [],
          },
        },
      })
    ).toMatchObject({
      recommendProducts: {
        products: [],
        loading: false,
      },
    });
  });
  it('should handle SET_USER_LOCATION', () => {
    expect(
      reducer(initialState, {
        type: 'SET_USER_LOCATION',
        location: {
          city_id: 0,
          city_name: '',
        },
      })
    ).toMatchObject({
      location: {
        city_id: 0,
        city_name: '',
      },
    });
  });
  it('should handle GET_CITY_BY_LATLNG_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'GET_CITY_BY_LATLNG_SUCCESS',
        payload: {
          location: {
            region_code: 0,
            region_name: '',
          },
        },
      })
    ).toMatchObject({
      location: {
        city_id: 1,
        city_name: 'Hồ Chí Minh',
        region_code: 0,
        region_name: '',
      },
    });
  });
});
