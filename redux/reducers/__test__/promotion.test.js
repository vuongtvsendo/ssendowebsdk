import reducer from '../promotion';
import Immutable from 'seamless-immutable';

describe('promotion reducer', () => {
  const initialState = Immutable({
    promotion_categories: [],
    sortList: [],
    metadata: {},
    sitemapMetadata: {},
    promotion_products: [],
    init: false,
    loading: false,
    sort_path: '',
    loadingMore: false,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle LOAD_PROMOTION_BASIC_INFO_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PROMOTION_BASIC_INFO_SUCCESS',
        payload: {
          data: {
            list_categories: [],
          },
          metadata: {},
          sort_path: '',
        },
      })
    ).toMatchObject({
      promotion_categories: [{ name: 'Tất cả', url_path: '' }],
      metadata: {},
      sort_path: '',
    });
  });
  it('should handle LOAD_PROMOTION_PRODUCTS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PROMOTION_PRODUCTS',
      })
    ).toMatchObject({
      loading: true,
    });
  });
  it('should handle LOAD_PROMOTION_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PROMOTION_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({ promotion_products: [], init: true, loading: false });
  });
  it('should handle LOAD_MORE_PROMOTION_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_PROMOTION_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({ promotion_products: [], loadingMore: false });
  });
});
