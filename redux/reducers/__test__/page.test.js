import reducer from '../page';
import Immutable from 'seamless-immutable';

describe('page reducer', () => {
  const initialState = Immutable({
    subCate: {
      lastScrollY: false,
      curentProductPage: 1,
      isListView: false,
    },
    homePage: {
      lastScrollY: false,
      curentProductPage: 1,
    },
    searchPage: {
      curentProductPage: 1,
      isListView: false,
      lastScrollY: false,
    },
    shopCommentPage: {
      curentProductPage: 1,
      lastScrollY: false,
    },
    shopPage: {
      curentProductPage: 1,
      lastScrollY: false,
    },
    shopRatingPage: {
      curentProductPage: 1,
      lastScrollY: false,
    },
    trendingPage: {
      lastScrollY: false,
      curentProductPage: 1,
    },
    eventPage: {
      lastScrollY: false,
      curentProductPage: 1,
    },
    eventDetailPage: {
      lastScrollY: false,
      curentProductPage: 1,
    },
    eventDetailFullPage: {
      lastScrollY: false,
      curentProductPage: 1,
    },
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle STORE_SUB_CATE_PAGE', () => {
    expect(
      reducer(initialState, {
        type: 'STORE_SUB_CATE_PAGE',
        payload: {
          lastScrollY: true,
          curentProductPage: 2,
          isListView: true,
        },
      })
    ).toMatchObject({
      subCate: {
        lastScrollY: true,
        curentProductPage: 2,
        isListView: true,
      },
    });
  });
  it('should handle STORE_HOME_PAGE', () => {
    expect(
      reducer(initialState, {
        type: 'STORE_HOME_PAGE',
        payload: {
          lastScrollY: true,
          curentProductPage: 2,
        },
      })
    ).toMatchObject({
      homePage: {
        lastScrollY: true,
        curentProductPage: 2,
      },
    });
  });
});
