import reducer from '../firer';
import Immutable from 'seamless-immutable';
import { SET_ONLINE_STATUS } from 'sendo-web-sdk/actions/const';

describe('firer reducer', () => {
  const initialState = Immutable({
    isOnline: true,
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle SET_ONLINE_STATUS', () => {
    expect(
      reducer(initialState, {
        type: SET_ONLINE_STATUS,
        payload: false,
      })
    ).toMatchObject({
      isOnline: false,
    });
  });
});
