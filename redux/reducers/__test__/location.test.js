import reducer from '../location';
import Immutable from 'seamless-immutable';

describe('location reducer', () => {
  const initialState = Immutable({
    allCity: [],
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle GET_All_CITY_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'GET_All_CITY_SUCCESS',
        payload: {
          allCity: [],
        },
      })
    ).toMatchObject({
      allCity: [],
    });
  });
});
