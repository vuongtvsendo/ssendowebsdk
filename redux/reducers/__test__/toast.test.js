import reducer from '../toast';
import Immutable from 'seamless-immutable';

describe('toast reducer', () => {
  const initialState = Immutable({
    toastState: {
      isOpen: false,
      content: '',
      autoClose: true,
    },
  });
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle OPEN_TOAST', () => {
    expect(
      reducer(initialState, {
        type: 'OPEN_TOAST',
        payload: {
          content: 'content',
        },
      })
    ).toMatchObject({
      toastState: {
        isOpen: true,
        content: 'content',
      },
    });
  });
  it('should handle CLOSE_TOAST', () => {
    expect(
      reducer(initialState, {
        type: 'CLOSE_TOAST',
      })
    ).toMatchObject({
      toastState: {
        isOpen: false,
        content: '',
      },
    });
  });
});
