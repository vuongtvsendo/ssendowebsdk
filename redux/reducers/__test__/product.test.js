import reducer from '../product';
import Immutable from 'seamless-immutable';

describe('product reducer', () => {
  const initialState = Immutable({
    products: [],
    init: false,
    loading: false,
    loadingMore: false,
    productDetail: {
      comments: [],
      ratings: [],
      metadata: {},
    },
  });
  // it('should return the initial state', () => {
  //   expect(reducer(undefined, {})).toEqual(initialState);
  // });
  it('should handle SET_PRODUCT_DETAIL', () => {
    expect(
      reducer(initialState, {
        type: 'SET_PRODUCT_DETAIL',
        payload: {
          productDetail: {},
        },
      })
    ).toMatchObject({
      productDetail: {},
    });
  });
  it('should handle LOAD_MORE_DETAIL_PRODUCT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_MORE_DETAIL_PRODUCT_SUCCESS',
        payload: {
          productDetail: {},
          productMetadata: {},
        },
      })
    ).toMatchObject({
      productDetail: {
        metadata: {},
      },
    });
  });
  it('should handle CHECK_PRODUCT_FAVORITE_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'CHECK_PRODUCT_FAVORITE_SUCCESS',
        payload: 1,
      })
    ).toMatchObject({
      productDetail: {
        isFavorite: true,
      },
    });
  });
  it('should handle ADD_TO_WISHLISH_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'ADD_TO_WISHLISH_SUCCESS',
        payload: 1,
      })
    ).toMatchObject({
      productDetail: {
        isFavorite: true,
      },
    });
  });
  it('should handle REMOVE_TO_WISHLISH_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'REMOVE_TO_WISHLISH_SUCCESS',
        payload: 1,
      })
    ).toMatchObject({
      productDetail: {
        isFavorite: false,
      },
    });
  });
  it('should handle LOAD_PRODUCT_COMMENT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_COMMENT_SUCCESS',
        payload: {
          comments: {
            data: [],
            meta_data: {
              total_count: 1,
            },
          },
        },
      })
    ).toMatchObject({
      productDetail: {
        comments: [],
        total_comment: 1,
      },
    });
  });
  it('should handle LOAD_PRODUCT_COMMENT_MORE', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_COMMENT_MORE',
      })
    ).toMatchObject({
      productDetail: {
        comment_loading: true,
      },
    });
  });
  it('should handle LOAD_PRODUCT_COMMENT_MORE_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_COMMENT_MORE_SUCCESS',
        payload: {
          comments: {
            data: [],
            meta_data: {
              total_count: 1,
            },
          },
        },
      })
    ).toMatchObject({
      productDetail: {
        comments: [],
        total_comment: 1,
        comment_loading: false,
      },
    });
  });
  it('should handle LOAD_PRODUCT_COMMENT_MORE_FAILED', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_COMMENT_MORE_FAILED',
      })
    ).toMatchObject({
      productDetail: {
        comment_loading: false,
      },
    });
  });
  it('should handle POST_PRODUCT_COMMENT', () => {
    expect(
      reducer(initialState, {
        type: 'POST_PRODUCT_COMMENT',
      })
    ).toMatchObject({
      productDetail: {
        comment_post_loading: true,
      },
    });
  });
  it('should handle POST_PRODUCT_COMMENT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'POST_PRODUCT_COMMENT_SUCCESS',
        commentPost: {
          comment_data: [],
          comment_id: 1,
          parent_id: 0,
        },
      })
    ).toMatchObject({
      productDetail: {
        comment_post_loading: false,
        comments: [],
      },
    });
  });
  it('should handle POST_PRODUCT_COMMENT_FAILED', () => {
    expect(
      reducer(initialState, {
        type: 'POST_PRODUCT_COMMENT_FAILED',
      })
    ).toMatchObject({
      productDetail: {
        comment_post_loading: false,
      },
    });
  });
  it('should handle DELETE_PRODUCT_COMMENT_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'DELETE_PRODUCT_COMMENT_SUCCESS',
        payload: {
          comment: {
            err_code: 0,
            comment_data: {
              id: 0,
              parent_id: 0,
            },
          },
        },
      })
    ).toMatchObject({
      productDetail: {
        comments: [],
      },
    });
  });
  it('should handle LOAD_PRODUCT_RATING_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_RATING_SUCCESS',
        payload: {
          ratings: [],
        },
      })
    ).toMatchObject({
      productDetail: {
        comments: [],
        ratings: [],
      },
    });
  });
  it('should handle LOAD_PRODUCT_RATING_MORE_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_RATING_MORE_SUCCESS',
        payload: {
          ratings: [],
        },
      })
    ).toMatchObject({
      productDetail: {
        comments: [],
        ratings: [],
        rating_loading: false,
      },
    });
  });
  it('should handle LOAD_PRODUCT_RELATED_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_RELATED_SUCCESS',
        payload: {
          relateds: [],
          metadata: {},
        },
      })
    ).toMatchObject({
      productDetail: {
        relateds: [],
      },
    });
  });
  it('should handle LOAD_PRODUCT_RECOMMEND_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCT_RECOMMEND_SUCCESS',
        payload: {
          recommends: [],
        },
      })
    ).toMatchObject({
      productDetail: {
        recommends: [],
      },
    });
  });
  it('should handle LOAD_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: 'LOAD_PRODUCTS_SUCCESS',
        payload: {
          products: [],
        },
      })
    ).toMatchObject({
      loading: false,
      init: true,
      products: [],
    });
  });
});
