import Immutable from 'seamless-immutable';
const initialState = Immutable({
  isShow: false,
  height: 0,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SHOW_SMARTBANNER': {
      let banner = {
        isShow: true,
        ...action.payload,
      };
      return Immutable(banner);
    }
    case 'CLOSE_SMARTBANNER': {
      let banner = { isShow: false, height: 0 };
      return Immutable(banner);
    }
    default: {
      return state;
    }
  }
};
