import Immutable from 'seamless-immutable';
const initialState = Immutable({
  filters: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'FILTER_LOAD': {
      return state.set('filters', Immutable.from(action.payload.filters));
    }
    default: {
      return state;
    }
  }
};
