import Immutable from 'seamless-immutable';
import { SET_ONLINE_STATUS } from 'sendo-web-sdk/actions/const';
import _get from 'lodash/get';

const initialState = Immutable({
  isOnline: __SERVER__ ? true : _get(window, 'navigator.onLine', true),
});

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ONLINE_STATUS: {
      return state.set('isOnline', action.payload);
    }
    default: {
      return state;
    }
  }
};
