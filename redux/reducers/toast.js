import Immutable from 'seamless-immutable';
const initialState = Immutable({
  toastState: {
    isOpen: false,
    content: '',
    autoClose: true,
  },
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_TOAST': {
      let toast = { ...state.toastState, isOpen: true, ...action.payload };
      return state.set('toastState', Immutable.from(toast));
    }
    case 'CLOSE_TOAST': {
      let toast = { isOpen: false, content: '' };
      return state.set('toastState', Immutable.from(toast));
    }
    default: {
      return state;
    }
  }
};
