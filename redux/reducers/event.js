import Immutable from 'seamless-immutable';
import _get from 'lodash/get';
const initialState = Immutable({
  event_list: {
    active: [],
    deactive: [],
  },
  event_detail_list: [],
  sortList: [],
  metadata: {},
  metadata_detail: {},
  metadata_detail_full: {},
  event_products: [],
  init_home: false,
  init_detail: false,
  init_detail_full: false,
  loading_home: false,
  loading_detail: false,
  loading_detail_full: false,
  sort_path: '',
  loadingMore_home: false,
  loadingMore_detail: false,
  loadingMore_detail_full: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_EVENT_SUCCESS': {
      return state
        .setIn(
          ['event_list', 'active'],
          Immutable.from(_get(action.payload, 'event_active', []))
        )
        .setIn(
          ['event_list', 'deactive'],
          Immutable.from(_get(action.payload, 'event_deactive', []))
        )
        .set('metadata', Immutable.from(action.payload.metadata))
        .set('init_home', true)
        .set('loading_home', false);
    }
    case 'LOAD_EVENT': {
      return state.set('loading_home', true);
    }
    case 'LOAD_EVENT_FAILED': {
      return state.set('loading_home', false).set('init_home', true);
    }
    case 'LOAD_MORE_EVENT': {
      return state.set('loadingMore_home', true);
    }
    case 'LOAD_MORE_EVENT_FAILED': {
      return state.set('loadingMore_home', false).set('init_home', true);
    }
    case 'CLEAR_EVENT': {
      return state
        .setIn(['event_list', 'active'], [])
        .setIn(['event_list', 'deactive'], [])
        .set('init_home', false);
    }
    case 'LOAD_MORE_EVENT_SUCCESS': {
      return state
        .set('loadingMore_home', false)
        .setIn(
          ['event_list', 'deactive'],
          state.event_list['deactive'].concat(
            Immutable.from(_get(action.payload, 'event_deactive', []))
          )
        );
    }
    case 'LOAD_EVENT_DETAIL_SUCCESS': {
      return state
        .set(
          'event_detail_list',
          Immutable.from(_get(action.payload, 'cabinets', []))
        )
        .set('metadata_detail', Immutable.from(action.payload.metadata))
        .set('init_detail', true)
        .set('loading_detail', false);
    }
    case 'LOAD_EVENT_DETAIL': {
      return state.set('loading_detail', true);
    }
    case 'LOAD_EVENT_DETAIL_FULL_BASIC_SUCCESS': {
      return state
        .set('metadata_detail_full', Immutable.from(action.payload.metadata))
        .set(
          'sortList',
          Immutable.from(_get(action.payload, 'metadata.sort_list', []))
        );
    }
    case 'LOAD_EVENT_DETAIL_FULL_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'LOAD_EVENT_DETAIL_FULL_PRODUCTS_FAILED': {
      return state.set('loading', false).set('init', true);
    }
    case 'LOAD_EVENT_DETAIL_FULL_PRODUCTS_SUCCESS': {
      return state
        .set('loading_detail_full', false)
        .set('init_detail_full', true)
        .set('event_products', Immutable.from(action.payload.products));
    }
    case 'LOAD_MORE_EVENT_DETAIL_FULL_PRODUCTS': {
      return state.set('loadingMore_detail_full', true);
    }
    case 'LOAD_MORE_EVENT_DETAIL_FULL_PRODUCTS_FAILED': {
      return state.set('loadingMore_detail_full', false);
    }
    case 'LOAD_MORE_EVENT_DETAIL_FULL_PRODUCTS_SUCCESS': {
      return state
        .set('loadingMore_detail_full', false)
        .set(
          'event_products',
          state.event_products.concat(Immutable.from(action.payload.products))
        );
    }
    case 'CLEAR_EVENT_DETAIL': {
      return state
        .set('event_detail_list', [])
        .set('init_detail', false)
        .set('metadata_detail', {});
    }
    case 'CLEAR_EVENT_PAGE_STATE': {
      return state.set('metadata_detail_full', {});
    }
    case 'CLEAR_EVENT_DETAIL_FULL_PRODUCTS': {
      return state.set('event_products', []).set('init_detail_full', false);
    }
    default: {
      return state;
    }
  }
};
