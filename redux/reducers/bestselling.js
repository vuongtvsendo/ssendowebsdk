import Immutable from 'seamless-immutable';
import { defaultAllCategoryNav } from '../../res/mainmenu';
const initialState = Immutable({
  bestselling_categories: [],
  sortList: [],
  metadata: {},
  sitemapMetadata: {},
  bestselling_products: [],
  init: false,
  loading: false,
  sort_path: '',
  loadingMore: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_BESTSELLING_BASIC_INFO_SUCCESS': {
      let list = action.payload.data.list_categories;
      list.unshift(defaultAllCategoryNav);

      return state
        .set('bestselling_categories', Immutable.from(list))
        .set('sort_path', Immutable.from(action.payload.sort_path))
        .set('metadata', Immutable.from(action.payload.metadata));
    }
    case 'LOAD_BESTSELLING_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'LOAD_BESTSELLING_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.map(products => {
        return (products.productMetadata = action.payload.metadata);
      });
      return state
        .set('bestselling_products', Immutable.from(products))
        .set('init', true)
        .set('loading', false);
    }
    case 'LOAD_MORE_BESTSELLING_PRODUCTS': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_MORE_BESTSELLING_PRODUCTS_FAILED': {
      return state.set('loadingMore', false);
    }
    case 'LOAD_MORE_BESTSELLING_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.map(products => {
        return (products.productMetadata = action.payload.metadata);
      });
      return state
        .set('loadingMore', false)
        .set(
          'bestselling_products',
          state.bestselling_products.concat(Immutable.from(products))
        );
    }
    case 'CLEAR_BESTSELLING_PRODUCTS': {
      return state.set('bestselling_products', []).set('init', false);
    }
    default: {
      return state;
    }
  }
};
