import Immutable from 'seamless-immutable';

const initialState = Immutable({
  categories: [],
  allCategories: [],
  sortList: [],
  metadata: {},
  sitemapMetadata: {},
  redirecting: false,
  seoFooter: {},
  cmsContent: {},
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_CATEGORIES_FAILED': {
      if (action.redirecting) {
        return state.set('redirecting', Immutable.from(true));
      } else {
        return state.set('redirecting', Immutable.from(false));
      }
    }
    case 'LOAD_CATEGORIES_SUCCESS': {
      return state
        .set('metadata', Immutable.from(action.payload.metadata))
        .set('sortList', Immutable.from(action.payload.sortList))
        .set('redirecting', Immutable.from(false))
        .set('categories', Immutable.from(action.payload.categories))
        .set('seoFooter', Immutable.from(action.payload.seoFooter))
        .set('cmsContent', Immutable.from(action.payload.cmsContent));
    }
    case 'LOAD_ALL_CATEGORIES_SUCCESS': {
      return state
        .set('allCategories', Immutable.from(action.payload.data))
        .set('sitemapMetadata', Immutable.from(action.payload.meta_data));
    }
    case 'CLEAR_CATEGORY_PAGE_STATE': {
      return state.set('metadata', Immutable.from({}));
    }
    default: {
      return state;
    }
  }
};
