import Immutable from 'seamless-immutable';
const initialState = Immutable({
  trending_list: [],
  trending_info: {},
  sortList: [],
  metadata_home: {},
  metadata_detail: {},
  sitemapMetadata: {},
  trending_products: [],
  url_key: '',
  init_home: false,
  init_detail: false,
  loading_home: false,
  loading_detail: false,
  sort_path: '',
  loadingMore_home: false,
  loadingMore_detail: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_TRENDING_HOME_SUCCESS': {
      return state
        .set('trending_list', Immutable.from(action.payload.data))
        .set('metadata_home', Immutable.from(action.payload.metadata))
        .set('init_home', true)
        .set('loading_home', false);
    }
    case 'LOAD_TRENDING_HOME': {
      return state.set('loading_home', true);
    }
    case 'LOAD_MORE_TRENDING_HOME': {
      return state.set('loadingMore_home', true);
    }
    case 'LOAD_MORE_TRENDING_HOME_FAILED': {
      return state.set('loadingMore_home', false);
    }
    case 'CLEAR_TRENDING_HOME': {
      return state.set('trending_list', []).set('init_home', false);
    }
    case 'LOAD_MORE_TRENDING_HOME_SUCCESS': {
      return state
        .set('loadingMore_home', false)
        .set(
          'trending_list',
          state.trending_list.concat(Immutable.from(action.payload.data))
        );
    }
    case 'LOAD_TRENDING_PRODUCTS': {
      return state.set('loading_detail', true);
    }
    case 'LOAD_TRENDING_PRODUCTS_SUCCESS': {
      return state
        .set('trending_products', Immutable.from(action.payload.products))
        .set('trending_info', Immutable.from(action.payload.trending_info))
        .set('init_detail', true)
        .set('url_key', action.payload.url_key)
        .set('metadata_detail', action.payload.metadata)
        .set('loading_detail', false);
    }
    case 'LOAD_MORE_TRENDING_PRODUCTS': {
      return state.set('loadingMore_detail', true);
    }
    case 'LOAD_MORE_TRENDING_PRODUCTS_FAILED': {
      return state.set('loadingMore_detail', false);
    }
    case 'LOAD_MORE_TRENDING_PRODUCTS_SUCCESS': {
      return state
        .set('loadingMore_detail', false)
        .set(
          'trending_products',
          state.trending_products.concat(
            Immutable.from(action.payload.products)
          )
        );
    }
    case 'CLEAR_TRENDING_PRODUCTS': {
      return state
        .set('trending_products', [])
        .set('trending_info', {})
        .set('init_detail', false);
    }
    default: {
      return state;
    }
  }
};
