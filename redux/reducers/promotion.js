import Immutable from 'seamless-immutable';
import { defaultAllCategoryNav } from '../../res/mainmenu';
const initialState = Immutable({
  promotion_categories: [],
  sortList: [],
  metadata: {},
  sitemapMetadata: {},
  promotion_products: [],
  init: false,
  loading: false,
  sort_path: '',
  loadingMore: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_PROMOTION_BASIC_INFO_SUCCESS': {
      let list = action.payload.data.list_categories;
      list.unshift(defaultAllCategoryNav);

      return state
        .set('promotion_categories', Immutable.from(list))
        .set('sort_path', Immutable.from(action.payload.sort_path))
        .set('metadata', Immutable.from(action.payload.metadata));
    }
    case 'LOAD_PROMOTION_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'LOAD_PROMOTION_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.map(products => {
        return (products.productMetadata = action.payload.metadata);
      });
      return state
        .set('promotion_products', Immutable.from(products))
        .set('init', true)
        .set('loading', false);
    }
    case 'LOAD_MORE_PROMOTION_PRODUCTS': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_MORE_PROMOTION_PRODUCTS_FAILED': {
      return state.set('loadingMore', false);
    }
    case 'LOAD_MORE_PROMOTION_PRODUCTS_SUCCESS': {
      let products = action.payload.products;
      products.map(products => {
        return (products.productMetadata = action.payload.metadata);
      });
      return state
        .set('loadingMore', false)
        .set(
          'promotion_products',
          state.promotion_products.concat(Immutable.from(products))
        );
    }
    case 'CLEAR_PROMOTION_PRODUCTS': {
      return state.set('promotion_products', []).set('init', false);
    }
    default: {
      return state;
    }
  }
};
