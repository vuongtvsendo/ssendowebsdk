import Immutable from 'seamless-immutable';
import _get from 'lodash/get';
const initialState = Immutable({
  metadata: {},
  cartList: [],
  loading: false,
  init: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_DATA_CART': {
      return state.set('loading', Immutable.from(true));
    }
    case 'LOAD_DATA_CART_FAILED': {
      return state.set('loading', Immutable.from(false));
    }
    case 'LOAD_DATA_CART_SUCCESS': {
      if (action.payload.error) {
        return state;
      }
      let result = _get(action.payload, 'result', {});
      return state
        .set('metadata', Immutable.from(result.meta_data))
        .set('cartList', Immutable.from(_get(result, 'data', [])))
        .set('init', Immutable.from(true))
        .set('loading', Immutable.from(false));
    }
    case 'LOAD_TOTAL_CART_SUCCESS': {
      if (action.payload.error) {
        return state;
      }
      const total = _get(action.payload, 'total_count', 0);
      let metadata = {
        ...state.metadata,
        total_count: parseInt(total, 10),
      };
      return state.set('metadata', Immutable.from(metadata));
    }
    case 'ADD_TO_CART': {
      return state.set('loading', Immutable.from(true));
    }
    case 'ADD_TO_CART_FAILED': {
      return state.set('loading', Immutable.from(false));
    }
    case 'ADD_TO_CART_SUCCESS': {
      return state.set('loading', Immutable.from(false));
    }
    default: {
      return state;
    }
  }
};
