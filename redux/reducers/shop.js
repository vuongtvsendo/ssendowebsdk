import Immutable from 'seamless-immutable';
import _get from 'lodash/get';
import _remove from 'lodash/remove';
import _findIndex from 'lodash/findIndex';
import { getShopAlias } from 'sendo-web-sdk/helpers/url';

const initialState = Immutable({
  shopInfo: {},
  products: [],
  sortList: [],
  categories: [],
  metadata: {},
  init: false,
  loading: false,
  loadingMore: false,
  shopComments: [],
  shopRatings: [],
  totalRating: 0,
  commentLoading: false,
  commentMessage: '',
  newComment: {},
  deleteComment: {},
});

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_SHOP_DETAIL': {
      return state.set('loading', true);
    }
    case 'LOAD_SHOP_DETAIL_SUCCESS': {
      let shopInfo = _get(action, 'payload.shopDetail');
      const shopAlias = getShopAlias(_get(shopInfo, 'shop_url', null));
      if (shopAlias) {
        shopInfo = { ...shopInfo, ...{ shop_alias: shopAlias } };
      }
      return state
        .set('shopInfo', Immutable.from(shopInfo))
        .set('loading', false);
    }
    case 'LOAD_SHOP_CATEGORIES_SUCCESS': {
      let categories = _get(action, 'payload.categories');
      return state.set('categories', Immutable.from(categories));
    }
    case 'LOAD_SHOP_PRODUCTS': {
      return state.set('loading', true);
    }
    case 'CHECKLIKE_SHOP_SUCCESS': {
      let is_liked = action.payload.isLike === 1 ? 1 : 0;
      let shopInfo = { ...state.shopInfo, is_liked };
      return state.set('shopInfo', Immutable.from(shopInfo));
    }
    case 'LIKE_SHOP_SUCCESS': {
      if (!action.payload.isLike) return state;
      let shopInfo = { ...state.shopInfo, is_liked: 1 };
      return state.set('shopInfo', Immutable.from(shopInfo));
    }
    case 'UNLIKE_SHOP_SUCCESS': {
      if (!action.payload.isLike) return state;
      let shopInfo = { ...state.shopInfo, is_liked: 0 };
      return state.set('shopInfo', Immutable.from(shopInfo));
    }
    case 'LOAD_SHOP_PRODUCTS_SUCCESS': {
      let products = _get(action, 'payload.products.data');
      return state
        .set('products', Immutable.from(products))
        .set('init', true)
        .set('loading', false);
    }
    case 'LOAD_SHOP_PRODUCTS_MORE': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_SHOP_PRODUCTS_MORE_SUCCESS': {
      let products = _get(action, 'payload.products.data');
      products = [...state.products, ...products];
      return state
        .set('products', Immutable.from(products))
        .set('loadingMore', false);
    }
    case 'LOAD_SHOP_COMMENTS': {
      return state.set('loading', true);
    }
    case 'LOAD_SHOP_COMMENTS_SUCCESS': {
      let comments = _get(action, 'payload.shopComments.result.data', []);
      return state
        .set('shopComments', Immutable.from(comments))
        .set('loading', false)
        .set('init', true);
    }
    case 'LOAD_SHOP_COMMENTS_MORE': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_SHOP_COMMENTS_MORE_SUCCESS': {
      let comments = _get(action, 'payload.shopComments.result.data', []);
      comments = [...state.shopComments, ...comments];
      return state
        .set('shopComments', Immutable.from(comments))
        .set('loadingMore', false);
    }
    case 'LOAD_SHOP_RATINGS': {
      return state.set('loading', true);
    }
    case 'LOAD_SHOP_RATINGS_SUCCESS': {
      const totalCount = _get(action, 'payload.shopRating.total_count', 0);
      let ratings = _get(action, 'payload.shopRating.data', []);
      return state
        .set('shopRatings', Immutable.from(ratings))
        .set('totalRating', Immutable.from(totalCount))
        .set('loading', false)
        .set('init', true);
    }
    case 'LOAD_SHOP_RATINGS_MORE': {
      return state.set('loadingMore', true);
    }
    case 'LOAD_SHOP_RATINGS_MORE_SUCCESS': {
      const totalCount = _get(
        action,
        'payload.shopRating.total_count',
        state.totalRating
      );
      let ratings = _get(action, 'payload.shopRating.data', []);
      ratings = [...state.shopRatings, ...ratings];
      return state
        .set('shopRatings', Immutable.from(ratings))
        .set('totalRating', Immutable.from(totalCount))
        .set('loadingMore', false);
    }
    case 'CLEAR_SHOP_DETAIL': {
      return state.set('shopInfo', action.payload);
    }
    case 'CLEAR_SHOP_PRODUCTS': {
      return state.set('products', []);
    }
    case 'CLEAR_SHOP_COMMENTS': {
      return state.set('shopComments', []);
    }
    case 'CLEAR_SHOP_RATINGS': {
      return state.set('shopRatings', []);
    }
    case 'SET_NEW_SHOP_COMMENT': {
      let newComment = _get(action, 'payload', {});
      return state.set('newComment', Immutable.from(newComment));
    }
    case 'SET_DELETE_SHOP_COMMENT': {
      let deleteComment = _get(action, 'payload');
      return state.set('deleteComment', Immutable.from(deleteComment));
    }
    case 'DELETE_SHOP_COMMENT': {
      return state.set('commentLoading', true);
    }
    case 'DELETE_SHOP_COMMENT_SUCCESS': {
      let deleteComment = _get(state, 'deleteComment');
      let errorCode = _get(action, 'payload.comment.err_code');
      let message = _get(action, 'payload.comment.message');
      let shopComments = [...state.shopComments];
      if (errorCode === 0 && deleteComment) {
        if (!deleteComment.isReply) {
          // Remove comment
          _remove(shopComments, i => {
            return i.comment.comment_id === deleteComment.comment_id;
          });
          return state
            .setIn(['shopComments'], Immutable.from(shopComments))
            .set('commentLoading', false)
            .set('commentMessage', message);
        } else {
          // Remove reply
          let removeIndex = _findIndex(shopComments, i => {
            return i.comment.comment_id === deleteComment.parentId;
          });
          let shopCommentsUpdate = shopComments.map((shopComment, index) => {
            if (index === removeIndex) {
              let replies = [...shopComment.comment.reply];
              _remove(replies, i => {
                return i.comment_id === deleteComment.comment_id;
              });
              return {
                ...shopComment,
                comment: {
                  ...shopComment.comment,
                  reply: [...replies],
                },
              };
            }
            return shopComment;
          });
          return state
            .setIn(['shopComments'], Immutable.from(shopCommentsUpdate))
            .set('commentLoading', false)
            .set('commentMessage', message);
        }
      }
      return state.set('commentLoading', false).set('commentMessage', message);
    }
    case 'POST_SHOP_COMMENT': {
      return state.set('commentLoading', true);
    }
    case 'POST_SHOP_COMMENT_SUCCESS': {
      let postComment = _get(state, 'newComment');
      let newComment = _get(action, 'payload.comment.comment_data');
      let message = _get(action, 'payload.comment.message', '');
      if (newComment && postComment) {
        let shopComments = [...state.shopComments];
        let index = _findIndex(shopComments, item => {
          return item.product.id === postComment.product_id;
        });
        let shopCommentsUpdate = shopComments.map((shopComment, i) => {
          if (i === index) {
            // Reply comment
            return {
              ...shopComment,
              comment: {
                ...shopComment.comment,
                reply: [...shopComment.comment.reply, newComment],
              },
            };
            // if (
            //   newComment.hasOwnProperty('parent_id') &&
            //   newComment.parent_id === 0
            // ) {
            //   // Comment
            //   return {
            //     ...shopComment,
            //     comment: { ...shopComment.comment, ...newComment },
            //   };
            // } else {
            //   // Reply comment
            //   return {
            //     ...shopComment,
            //     comment: {
            //       ...shopComment.comment,
            //       reply: [...shopComment.comment.reply, newComment],
            //     },
            //   };
            // }
          }
          return shopComment;
        });
        return state
          .set('shopComments', Immutable.from(shopCommentsUpdate))
          .set('commentLoading', false)
          .set('commentMessage', message);
      } else {
        return state
          .set('commentLoading', false)
          .set('commentMessage', message);
      }
    }
    default: {
      return state;
    }
  }
};
